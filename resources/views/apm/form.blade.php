@extends('layout')
@section('css')
<style type="text/css">
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('title', 'PID Baru')
@section('heading')
<h1><a href="/monitorapm" class="ion-arrow-left-a"></a> Tambah PID monitor</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" id="form-laporan">
    <div class="form-group form-message-dark">
        <label for="pid" class="col-md-2 control-label">PID</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="pid" id="pid" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_project" class="col-md-2 control-label">Nama Project</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="nama_project" id="nama_project" required>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script type="text/javascript">
</script>
@endsection