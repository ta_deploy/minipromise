@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    @media
      only screen 
    and (max-width: 760px), (min-device-width: 768px) 
    and (max-device-width: 1024px)  {

        /* Force table to not be like tables anymore */
        table, thead, tbody, th, td, tr {
            display: block;
        }

        /* Hide table headers (but not display: none;, for accessibility) */
        thead tr {
            position: absolute;
            top: -9999px;
            left: -9999px;
        }

    tr {
      margin: 0 0 1rem 0;
    }
      
    tr:nth-child(odd) {
      background: #ccc;
    }
    
        td {
            /* Behave  like a "row" */
            border: none;
            border-bottom: 1px solid #eee;
            position: relative;
            padding-left: 50%;
        }

        td:before {
            /* Now like a table header */
            position: absolute;
            /* Top/left values mimic padding */
            top: 0;
            left: 6px;
            width: 45%;
            padding-right: 10px;
            white-space: nowrap;
        }

        /*
        Label the data
    You could also use a data-* attribute and content for this. That way "bloats" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.
        */
        td:nth-of-type(1):before { content: "#"; }
        td:nth-of-type(2):before { content: "PID"; }
        td:nth-of-type(3):before { content: "Nama Project"; }
        td:nth-of-type(4):before { content: "Material : Non Stock"; }
        td:nth-of-type(5):before { content: "Material : Stock"; }
        td:nth-of-type(6):before { content: "Non Material : Jasa"; }
        td:nth-of-type(7):before { content: "Non Material : Sewa"; }
        td:nth-of-type(8):before { content: "Non Material : Tenaga Kerja"; }
        td:nth-of-type(9):before { content: "Non Materiall : Operasional"; }
        td:nth-of-type(10):before { content: "Act"; }
    }
</style>
@endsection
@section('title', 'BTS || APM')
@section('heading')
<div class="row">
    <div class="col-md-9">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Monitor APM</span>
        </h1>
    </div>
    <div class="col-md-3">
        <a href="/monitorapm/input" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus-round"></i> Tambah PID</span></a>
    </div>  
</div>
@endsection
@section('title', 'Monitor APM')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
    <table class="table" id="myTable">
        <thead>
            <tr>
                <th>#</th>
                <th>PID</th>
                <th>Nama Project</th>
                <th>Material : Non Stock</th>
                <th>Material : Stock</th>
                <th>Non Material : Jasa</th>
                <th>Non Material : Sewa</th>
                <th>Non Material : Tenaga Kerja</th>
                <th>Non Materiall : Operasional</th>
                <th>Act</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $isi)
            <tr>
                <td class="text-right">{{ $isi->pid ?: '-' }}</td>
                <td class="text-right">{{ $isi->sapid ?: '-' }}</td>
                <td class="text-right">{{ $isi->nama_project ?: '-' }}</td>
                <td class="text-right">{{ number_format($isi->Material_Non_Stock) ?: '-' }}</td>
                <td class="text-right">{{ number_format($isi->Material_Stock) ?: '-' }}</td>
                <td class="text-right">{{ number_format($isi->Non_Material_Jasa) ?: '-' }}</td>
                <td class="text-right">{{ number_format($isi->Non_Material_Sewa)  ?: '-'}}</td>
                <td class="text-right">{{ number_format($isi->Non_Material_Tenaga_Kerja) ?: '-' }}</td>
                <td class="text-right">{{ number_format($isi->Non_Materiall_Operasional) ?: '-' }}</td>
                <td class="text-right">
                    @if($isi->apm_count)
                    <span class="btn btn-sm btn-info detil" data-idpid="{{ $isi->sapid }}" data-bg="modal-info"><i class="ion ion-info"></i>&nbsp;&nbsp;{{ $isi->apm_count }}  Detail</span>
                    @endif
                </td>
            </tr>
            @endforeach
    </table>
</div>
</div>
</div>
<div id="modalHapus" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Hapus PID</h4>
          </div>
          <div class="modal-body">
            <p id="content"></p>
          </div>
          <div class="modal-footer">    
            <form action="/deletemonitorapm" method="post">
                <input type="hidden" name="id" id="id">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Hapus!</button>
            </form>
          </div>
        </div>
    </div>
</div>
<div id="modalDetil" class="modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title detilTitle">Detil</h4>
          </div>
          <div class="modal-body">
            <p id="contentDetil"></p>
          </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(function() {
    $(".stts").click(function(e){
        $("#content").html("Sure ?");
        $("#id").val(this.dataset.idpid);
        $("#modalHapus").removeClass("modal-success");
        $("#modalHapus").removeClass("modal-danger");
        $("#modalHapus").addClass(this.dataset.bg);
        $("#modalHapus").modal('show');
    });
    $(".detil").click(function(e){
        $.get("/getCashListByPID/"+this.dataset.idpid, function( data ) {
          $("#contentDetil").html( data );
        });
        $(".detilTitle").html("Detil:"+this.dataset.idpid);
        $("#modalDetil").modal('show');
    });
});
</script>
@endsection
