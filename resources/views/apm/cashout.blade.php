@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('title', 'BTS || APM')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Cashout / </span>List 
</h1>
@endsection
@section('title', 'cashout')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Doc</th>
                <th>Fund</th>
                <th>Detail</th>
                <th>PID</th>
                <th>Amount</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ explode('/',$d->date)[2]."-".explode('/',$d->date)[1]."-".explode('/',$d->date)[0] }}</td>
                    <td>{{ $d->doc }}</td>
                    <td>{{ $d->desc_fund }}</td>
                    <td>{{ $d->text_detail }}</td>
                    <td>{{ $d->pid }}</td>
                    <td class="text-right">{{ number_format(str_replace(",","",$d->amount_detail)) }}</td>
                    <td>{{ $d->status }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
 $('#datatables').dataTable();
    $('#datatables_wrapper .table-caption').text('Some header text');
    $('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
    });
</script>
@endsection
