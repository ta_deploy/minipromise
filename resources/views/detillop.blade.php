<div class="row">
  <div class="col-md-7">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <span class="panel-title">Progress</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <div class="table-primary table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2" class="valign-middle">#</th>
                <th colspan="2" class="text-xs-center">Aktor</th>
              </tr>
              <tr>
                <th>Admin Project</th>
                <th>Waspang</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $urutan = 0;
              foreach($step as $b){
                if($b->id == $data->step_id){
                  $urutan = $b->urutan;
                  break;
                }
              }
              ?>
              @foreach($step as $no => $b)

              <?php
              $label = 'label-success';
              if($b->urutan == $urutan){
                $label = 'label-info';
              }else if($b->urutan > $urutan){
                $label = 'label-default';
              }
              ?>
              <tr>
                <td>{{ ++$no }}</td>
                @if($b->id == 9)
                  <td class="text-xs-center" colspan="2"><span class="label label-pill {{ $label }}">{{ $b->step }}</span></td>
                @else
                  <td class="text-xs-center"><span class="label label-pill {{ $label }}">{{ $b->actor==2?$b->step:'' }}</span></td>
                  <td class="text-xs-center"><span class="label label-pill {{ $label }}">{{ $b->actor==1?$b->step:'' }}</span></td>
                @endif

              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-danger panel-dark">
      <div class="panel-heading">
        <span class="panel-title">Files</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <a href="{{ '/storage/'.$data->id.'/unwizingfile/'.$data->mcore_unwizing }}" target="_blank">
            <button type="button" class="btn {{ $data->mcore_unwizing?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">MCORE Aanwijzing</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/unwizingfile/'.$data->boq_unwizing }}" target="_blank">
            <button type="button" class="btn {{ $data->boq_unwizing?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">BOQ Aanwijzing</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/unwizingfile/'.$data->kml_unwizing }}" target="_blank">
            <button type="button" class="btn {{ $data->kml_unwizing?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">KML Aanwijzing</button>
        </a>

        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->abd }}" target="_blank">
            <button type="button" class="btn {{ $data->abd?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">ABD</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->otdr }}">
            <button type="button" class="btn {{ $data->otdr?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">OTDR</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->qrcode }}">
            <button type="button" class="btn {{ $data->qrcode?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">QR CODE</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->kml_instalasi }}">
            <button type="button" class="btn {{ $data->kml_instalasi?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">KML INSTALASI</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->mcore_instalasi }}">
            <button type="button" class="btn {{ $data->mcore_instalasi?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">M.CORE INSTALASI</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->boq_instalasi }}">
            <button type="button" class="btn {{ $data->boq_instalasi?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">BOQ INSTALASI</button>
        </a>
      </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="panel panel-success" style="overflow-y:auto; height: 484.417px;">
      <div class="panel-heading">
        <span class="panel-title"><i class="panel-title-icon fa fa-bullhorn"></i>Logs</span>

      </div>

      <div id="support-tickets" class="ps-block">
        @foreach($log as $l)
        <?php
          $durasi = '';
          if($l->updated_at!='0000-00-00 00:00:00' && $l->update_end!='0000-00-00 00:00:00'){
            $dteStart = new \DateTime($l->updated_at);
            $dteEnd   = new \DateTime($l->update_end);
            $dteDiff  = $dteStart->diff($dteEnd);
            $durasi   = $dteDiff->format("%dd %Hh %Im");
          }
        ?>
        <div class="widget-support-tickets-item" style="pointer-events: none;">
          <a href="#" title="" class="widget-support-tickets-title">
            {{ $l->updated_at }}
            <span class="widget-support-tickets-id"> By [{{ $l->update_by }}] {{ $durasi }}</span>
          </a>
          <span class="widget-support-tickets-info">{{ $l->catatan }}</span>
        </div>
        @endforeach
      </div>
    </div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <span class="panel-title">ODP</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <div class="table-primary table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2">#</th>
                <th rowspan="2">ODP</th>
                <th rowspan="2">IP OLT</th>
                <th rowspan="2">PORT OLT</th>
                <th colspan="4">E-SIDE</th>
                <th colspan="4">O-SIDE</th>
                <th colspan="2">FEEDER</th>
                <th colspan="4">ODC</th>
                <th colspan="4">ODP</th>
                <th colspan="2">DIST</th>
                <th rowspan="2">ODP</th>
                <th rowspan="2">VALINS</th>
              </tr>
              <tr>
                <th>FTM</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>KAP</th>
                <th>FTM</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>KAP</th>
                <th>NAMA</th>
                <th>KAP</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>SPL</th>
                <th>KAP</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>SPL</th>
                <th>KORDINAT</th>
                <th>NAMA</th>
                <th>KAP</th>
              </tr>
            </thead>
            <tbody>
              @foreach($core as $no => $c)
              <tr>
                <td rowspan="2" class="valign-middle">{{ ++$no }}</td>
                <td>{{ $c->Plan_Odp_Nama }}</td>
                <td>{{ $c->Plan_IP_OLT }}</td>
                <td>{{ $c->Plan_Olt_Port }}</td>
                <td>{{ $c->Plan_Odf_Otb }}</td>
                <td>{{ $c->Plan_Otb_Panel }}</td>
                <td>{{ $c->Plan_Otb_Port }}</td>
                <td>{{ $c->Plan_Otb_Kap }}</td>
                <td>{{ $c->Plan_Otb_Odf_o }}</td>
                <td>{{ $c->Plan_Otb_Panel_o }}</td>
                <td>{{ $c->Plan_Otb_Port_o }}</td>
                <td>{{ $c->Plan_Otb_Kap_o }}</td>
                <td>{{ $c->Plan_FE_Nama }}</td>
                <td>{{ $c->Plan_FE_Kap }}</td>
                <td>{{ $c->Plan_Odc_Panel }}</td>
                <td>{{ $c->Plan_Odc_Port }}</td>
                <td>{{ $c->Plan_Odc_Spl }}</td>
                <td>{{ $c->Plan_Odc_Kap }}</td>
                <td>{{ $c->Plan_Odp_Panel }}</td>
                <td>{{ $c->Plan_Odp_Port }}</td>
                <td>{{ $c->Plan_Odp_Spl }}</td>
                <td>{{ $c->Plan_Odp_LAT.' '.$c->Plan_Odp_LON }}</td>
                <td>{{ $c->Plan_Dist_Nama }}</td>
                <td>{{ $c->Plan_Dist_Kap }}</td>
                <td>{{ $c->Plan_Tenos }}</td>
                <td></td>
              </tr>
              <tr>
                <td>{{ $c->Real_Odp_Nama }}</td>
                <td>{{ $c->Real_IP_OLT }}</td>
                <td>{{ $c->Real_Olt_Port }}</td>
                <td>{{ $c->Real_Odf_Otb }}</td>
                <td>{{ $c->Real_Otb_Panel }}</td>
                <td>{{ $c->Real_Otb_Port }}</td>
                <td>{{ $c->Real_Otb_Kap }}</td>
                <td>{{ $c->Real_Otb_Odf_o }}</td>
                <td>{{ $c->Real_Otb_Panel_o }}</td>
                <td>{{ $c->Real_Otb_Port_o }}</td>
                <td>{{ $c->Real_Otb_Kap_o }}</td>
                <td>{{ $c->Real_FE_Nama }}</td>
                <td>{{ $c->Real_FE_Kap }}</td>
                <td>{{ $c->Real_Odc_Panel }}</td>
                <td>{{ $c->Real_Odc_Port }}</td>
                <td>{{ $c->Real_Odc_Spl }}</td>
                <td>{{ $c->Real_Odc_Kap }}</td>
                <td>{{ $c->Real_Odp_Panel }}</td>
                <td>{{ $c->Real_Odp_Port }}</td>
                <td>{{ $c->Real_Odp_Spl }}</td>
                <td>{{ $c->Real_Odp_LAT.' '.$c->Real_Odp_LON }}</td>
                <td>{{ $c->Real_Dist_Nama }}</td>
                <td>{{ $c->Real_Dist_Kap }}</td>
                <td>{{ $c->Real_Tenos }}</td>
                <td></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>

  </div>
  <div class="col-md-12">
    <div class="panel panel-danger panel-dark">
      <div class="panel-heading">
        <span class="panel-title">BOQ</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <div class="table-primary">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Designator</th>
                <th>Plan</th>
                <th>Real</th>
              </tr>
            </thead>
            <tbody>
              @foreach($boq as $no => $b)
              <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $b->designator }}</td>
                <td>{{ $b->qty }}</td>
                <td>{{ $b->inst }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>