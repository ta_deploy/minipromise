<div class="table-primary">
  <div class="table-header">
    <div class="table-caption">
      Detil
    </div>
  </div>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>WO</th>
        <th>Ring</th>
        <th>Nama Lop</th>
        <th>Waspang</th>
        <th>Mitra</th>
      </tr>
    </thead>
    <tbody>
      @foreach($data as $no => $d)
      <tr>
        <td>{{ ++$no }}</td>
        <td>{{ $d->nama_project }}</td>
        <td>{{ $d->ring }}</td>
        <td>{{ $d->nama_lop }}</td>
        <td>{{ $d->waspang_nm }}</td>
        <td>{{ $d->nama_mitra }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>