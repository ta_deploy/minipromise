@extends('layout')
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>KHS / </span>Upload
</h1>
@endsection
@section('title', 'KHS')
@section('content')
<form class="form-horizontal" method="post" id="uploadkhs" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="KHS" class="col-md-2 control-label">KHS</label>
        <div class="col-md-10">
            <label id="KHS" class="custom-file px-file" for="KHSi">
                <input type="file" id="KHSi" class="custom-file-input" name="KHS" required>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_khs" class="col-md-2 control-label">Nama KHS</label>
        <div class="col-md-10">
            <input type="text" name="nama_khs" id="nama_khs" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="harga" class="col-md-2 control-label">Harga</label>
        <div class="col-md-10">
            <input type="text" name="harga" id="harga" class="form-control" required/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
     
</form>
@endsection

@section('js')
<script>
  $(function() {
  	$('#uploadkhs').pxValidate();
    $('#KHS').pxFile();
    $('#harga').select2({
        placeholder:"Select Harga",
        data:[{id:"TA", text:"TA"},{id:"TI", text:"TI"}],
        dropdownCssClass : 'no-search',
    })
  });
</script>
@endsection
