<!DOCTYPE html>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
    <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
    <!-- a -->
   <?php
        $auth = session('auth') ?: null;
        $path = Request::path();
        $theme = 'mint-dark';
        if(isset($auth->pixeltheme)){
            if($auth->pixeltheme){
                $theme = $auth->pixeltheme;
            }
        }
        $color = '';
        if (str_contains($theme, 'dark')) {
            $color = '-dark';
        }
        $lvl = null;
        if(($auth)){
            $lvl = $auth->promise_level;
        }
    ?>
    <link href="/css/bootstrap{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/pixeladmin{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/widgets{{ $color }}.min.css" rel="stylesheet" type="text/css">
    <link href="/css/themes/{{ $theme }}.min.css" rel="stylesheet" type="text/css">

    <script src="/pace/pace.min.js"></script>
    @yield('css')
</head>
<body>
    <?php
    $admin = array('97150038', '17920262');

    ?>
    <nav class="px-nav px-nav-left px-nav-fixed">
        <button type="button" class="px-nav-toggle" data-toggle="px-nav">
            <span class="px-nav-toggle-arrow"></span>
            <span class="navbar-toggle-icon"></span>
            <span class="px-nav-toggle-label font-size-11">HIDE MENU</span>
        </button>

        <ul class="px-nav-content">
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-ios-home"></i><span class="px-nav-label">Dashboard</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/home"><span class="px-nav-label">Home</span></a></li>
                    <li class="px-nav-item"><a href="/dashflow/all/all"><span class="px-nav-label">Dashboard Mitratel</span></a></li>
                </ul>
            </li>

            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-android-checkbox-outline"></i><span class="px-nav-label">Proaktif</span></a>
                <ul class="px-nav-dropdown-menu">
                    <!-- <li class="px-nav-item"><a href="/proaktif"><span class="px-nav-label">List</span></a></li> -->
                    <li class="px-nav-item"><a href="/plan_tl_list"><span class="px-nav-label">Plan</span></a></li>
                    <li class="px-nav-item"><a href="/review"><span class="px-nav-label">Review</span></a></li>
                </ul>
            </li>
            @if($lvl==1)
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-filing"></i><span class="px-nav-label">Waspang</span></a>
                <ul class="px-nav-dropdown-menu">
                    <!-- <li class="px-nav-item"><a href="/lop/register"><span class="px-nav-label">Register</span></a></li> -->
                    <!-- <li class="px-nav-item"><a href="/list/6"><span class="px-nav-label">Aanwijzing</span></a></li> -->
                    <li class="px-nav-item"><a href="/"><span class="px-nav-label">Inbox Waspang</span><span class="label label-danger badge">~</span></a></li>
                    <!-- <li class="px-nav-item"><a href="/planwaspang"><span class="px-nav-label">Plan Waspang</span><span class="label label-danger badge"></span></a></li> -->
                </ul>
            </li>
            @endif
            @if($lvl==2)
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-clipboard"></i><span class="px-nav-label">Admin Project</span></a>
                <ul class="px-nav-dropdown-menu">
                    <!-- <li class="px-nav-item"><a href="/proaktif"><span class="px-nav-label">Proaktif</span></a></li> -->
                    <li class="px-nav-item"><a href="/project"><span class="px-nav-label">Project/Tematik</span></a></li>
                    <li class="px-nav-item"><a href="/list/8"><span class="px-nav-label">QC Dokumen</span><span class="label label-danger badge">~</span></a></li>
                    <!-- <li class="px-nav-item"><a href="/sp"><span class="px-nav-label">SP</span></a></li> -->
                    <li class="px-nav-item"><a href="/mitra"><span class="px-nav-label">Mitra</span></a></li>
                    <li class="px-nav-item"><a href="/waspang"><span class="px-nav-label">Waspang</span></a></li>

                </ul>
            </li>
            @endif
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-ios-paper-outline"></i><span class="px-nav-label">Report</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/gantt"><span class="px-nav-label">Timeplan</span></a></li>
                    <li class="px-nav-item"><a href="/matrikprogress/mitra"><span class="px-nav-label">Matrik By Mitra</span></a></li>
                    <li class="px-nav-item"><a href="/matrikprogress/waspang"><span class="px-nav-label">Matrik By Waspang</span></a></li>
                    <li class="px-nav-item"><a href="/matrikprogress/sto"><span class="px-nav-label">Matrik By STO</span></a></li>
                    <li class="px-nav-item"><a href="/matrikprogress/tl"><span class="px-nav-label">Matrik By TL</span></a></li>
                </ul>
            </li>

            @if($lvl==2)
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-cash"></i><span class="px-nav-label">Cost (APM)</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/monitorapm"><span class="px-nav-label">Monitor Budget APM</span></a></li>
                    <li class="px-nav-item"><a href="/apm/cashout"><span class="px-nav-label">Monitor Cashout APM</span></a></li>
                </ul>
            </li>
            @endif
            <!-- <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-ios-paper-outline"></i><span class="px-nav-label">SP</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/sp"><span class="px-nav-label">List</span></a></li>
                </ul>
            </li> -->
            @if($lvl != 3)
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-android-color-palette"></i><span class="px-nav-label">Material (Alista)</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/carirfc"><span class="px-nav-label">Cari RFC</span></a></li>
                    <li class="px-nav-item"><a href="/rekap_pengeluaran/{{ date('Y-m-d').':'.date('Y-m-d') }}/all/all"><span class="px-nav-label">Rekap RFC</span></a></li>
                </ul>
            </li>
            @endif
            
            <!-- <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-ios-paper-outline"></i><span class="px-nav-label">Mitra</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/mitra"><span class="px-nav-label">List Mitra</span></a></li>
                    <li class="px-nav-item"><a href="/timmitra/list"><span class="px-nav-label">Tim Mitra</span></a></li>
                </ul>
            </li>
            <li class="px-nav-item">
                <a href="/waspang_list"><i class="px-nav-icon ion-ios-color-filter"></i><span
                class="px-nav-label">Waspang</span></a>
            </li> -->
            <!-- <li class="px-nav-item">
                <a href="/refer_pt2"><i class="px-nav-icon ion-ios-color-filter"></i><span
                class="px-nav-label">Referensi PT-2</span></a>
            </li> -->
            <!-- <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-person"></i><span class="px-nav-label">Mitratel</span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item"><a href="/mitratel/upload"><span class="px-nav-label">Upload</span></a></li>
                    <li class="px-nav-item"><a href="/mitratelplan"><span class="px-nav-label">Plan</span></a></li>
                    <li class="px-nav-item"><a href="/mitratel"><span class="px-nav-label">Dashboard</span></a></li>
                </ul>
            </li> -->
            <li class="px-nav-item px-nav-dropdown">
                <a href="#"><i class="px-nav-icon ion-person"></i><span class="px-nav-label">{{ ($auth?$auth->nama:'~') }} <code>{{ ($auth?$auth->witel:'~') }}</code></span></a>
                <ul class="px-nav-dropdown-menu">
                    <li class="px-nav-item">
                        <a href="https://perwira.tomman.app/input_potensi_b/{{ ($auth?$auth->id_karyawan:'') }}"><i class="px-nav-icon ion-ios-heart"></i>Lapor Potensi Bahaya</a>
                    </li>
                    <li class="px-nav-item"><a href="/theme"><span class="px-nav-label">Themes</span></a></li>
                    <li class="px-nav-item"><a href="/logout"><span class="px-nav-label">Logout</span></a></li>
                </ul>
            </li>
        </ul>
    </nav>

    <nav class="navbar px-navbar">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">BTS</a>
        </div>

        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#px-navbar-collapse" aria-expanded="false"><i class="navbar-toggle-icon"></i></button>

        <div class="collapse navbar-collapse" id="px-navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <form class="navbar-form" role="search" action="/search">
                        <div class="form-group">
                            <input type="text" name="q" class="form-control" placeholder="Search PID/LOP" style="width: 240px;">
                        </div>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="px-content">
        <div class="page-header">
            @yield('heading')
        </div>
        <div id="block-alert-with-timer" class="m-b-1"></div>
        <div class="konten">
            @yield('content')

        </div>
    </div>

    <footer class="px-footer px-footer-bottom">
        Board of projecT Supervision. Supported by Telkomakses. Powered by Laravel-Pixeladmin.
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/pixeladmin.min.js"></script>
    <script type="text/javascript">
        $(function(){
            var alertExist = <?= json_encode(Session::has('alerts')); ?>;
            if(alertExist){
                toastr.options = {
                    "closeButton": true,
                    "positionClass": "toast-top-center",
                    "onclick": null,
                    "timeOut": "200000"
                }
                var msg = <?= json_encode(session('alerts')[0]); ?>;
                toastr.info(msg.text);
            }

            var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
            if(alertBlock){
                var $container = $('#block-alert-with-timer');
                var alrt = <?= json_encode(session('alertblock')[0]); ?>;
                $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
            }

            var file = window.location.pathname;

            $('body > .px-nav')
            .find('.px-nav-item > a[href="' + file + '"]')
            .parent()
            .addClass('active');

            $('body > .px-nav').pxNav();
            $('body > .px-footer').pxFooter();
        });
    </script>
    @yield('js')
</body>
</html>
