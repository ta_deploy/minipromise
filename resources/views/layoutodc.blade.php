<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
  
  <title>Sign In</title>

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin" rel="stylesheet" type="text/css">
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Pace.js -->

  <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="css/pixeladmin.min.css" rel="stylesheet" type="text/css">
  <link href="css/widgets.min.css" rel="stylesheet" type="text/css">

  <!-- Theme -->
  <link href="css/themes/asphalt.min.css" rel="stylesheet" type="text/css">

  <!-- Custom styling -->
  <style>

    .page-signin-header {
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    .page-signin-header .btn {
      position: absolute;
      top: 12px;
      right: 15px;
    }

    html[dir="rtl"] .page-signin-header .btn {
      right: auto;
      left: 15px;
    }

    .page-signin-container {
      width: auto;
      margin: 30px 10px;
    }

    .page-signin-container form {
      border: 0;
      box-shadow: 0 2px 2px rgba(0,0,0,.05), 0 1px 0 rgba(0,0,0,.05);
    }

    @media (min-width: 544px) {
      .page-signin-container {
        width: 350px;
        margin: 60px auto;
      }
    }

    .page-signin-social-btn {
      width: 40px;
      padding: 0;
      line-height: 40px;
      text-align: center;
      border: none !important;
    }

    #page-signin-forgot-form { display: none; }
  </style>
  <!-- / Custom styling -->
</head>
<body>
  <div class="box">
    @for($j=1;$j<=12;$j++)
      <div class="box-row">
        <div class="box-cell col-xs-1 col-sm-1 col-md-1 col-lg-1 col-xl-1 text-center valign-middle b-a-2 border-rounded border-default">
          {{$j}}
        </div>
        <div class="box-cell col-xs-11 col-sm-11 col-md-11 col-lg-11 col-xl-11">
          <div class="box-container">
            <div class="box-row">
              @for($i=1;$i<=12;$i++)
              <div class="box-cell port col-sm-1 col-md-1 col-lg-1 col-xl-1 col-xs-1 text-center b-a-2 border-rounded border-black">
                {{ $i }}<br>
                99
              </div>
              @endfor
            </div>
          </div>
        </div>
      </div>
    @endfor
  </div>
    <!-- / Reset form -->

  <!-- ==============================================================================
  |
  |  SCRIPTS
  |
  =============================================================================== -->

  <!-- jQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/pixeladmin.min.js"></script>
  <script src="/pace/pace.min.js"></script>

  <script type="text/javascript">
    $(function(){
      var alertExist = <?= json_encode(Session::has('alerts')); ?>;
      if(alertExist){
        toastr.options = {
          "closeButton": true,
          "positionClass": "toast-top-center",
          "onclick": null,
          "timeOut": "200000"
        }
        var msg = <?= json_encode(session('alerts')[0]); ?>;
        toastr.info(msg.text);
      }

      var alertBlock = <?= json_encode(Session::has('alertblock')); ?>;
      if(alertBlock){
        var $container = $('#block-alert-with-timer');
        var alrt = <?= json_encode(session('alertblock')[0]); ?>;
        $container.pxBlockAlert(alrt.text, { type: alrt.type, style: 'light', timer: 3 });
      }

    });

  </script>
</body>
</html>
