@extends('layout')
@section('heading')
<h1>Export Tiang</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-export">
    <div class="form-group form-message-dark">
        <label for="datel" class="col-md-2 control-label">datel</label>
        <div class="col-md-9">
            <input type="text" id="datel" class="form-control" name="datel">
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="sto" class="col-md-2 control-label">STO</label>
        <div class="col-md-9">
            <input type="text" id="sto" class="form-control" name="sto">
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="kml" class="col-md-2 control-label">KML</label>
        <div class="col-md-9">
            <label class="custom-file px-file" for="spi" id="kml">
                <input type="file" id="spi" class="custom-file-input" name="kml" required>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-export').pxValidate();
        $('#kml').pxFile();
    });
</script>
@endsection