@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-9">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RFC</span>
        </h1>
    </div>
</div>
@endsection
@section('title', 'Report')
@section('content')
<div class="panel">
  <div class="panel-body">
    <form method="get" action="/carirfc">
    <div class="input-group">
        
      <input type="text" name="q" class="form-control">
      <div class="input-group-btn">
        <button type="submit" class="btn">Cari</button>
      </div>
    </div>
    </form>
    <br>
    <div class="table-responsive table-primary">
        <table class="table">
            <thead>
                <tr>
                    <th>INFO</th>
                    <th>PENGESAHAN</th>
                    <th>ID BARANG</th>
                    <th>MINTA</th>
                    <th>BERI</th>
                </tr>
            </thead>
            <tbody>
                    <?php
                    $no=0;
                    ?>
                @foreach($dataarray as $key => $d)

                    <?php
                        $span = count($d->list);
                        $path = '/rfc/rfc_ttd/'.$d->RFC.'.pdf';
                        $pathori = '/rfc/rfc/'.$d->FILE_RFC;
                    ?>
                    <tr>
                        <td rowspan="{{ $span }}">
                            <span class="label label-info">{{ ++$no }} . </span>
                            <span class="label label-primary">RFC : {{ $d->RFC }}</span><br>
                            <span class="label label-primary">PID : {{ $d->PID }}</span><br>
                            <span class="label label-primary">{{ $d->NAMA_PROJECT }}</span><br>
                            <span class="label label-primary">GUDANG : {{ $d->NAMA_GUDANG }}</span><br>
                            <span class="label label-primary">{{ $d->MITRA }}</span><br>
                            <span class="label label-primary">LOKASI : {{ $d->LOKASI }}</span>
                        </td>
                        <td rowspan="{{ $span }}">
                            @if (file_exists(public_path().$pathori))
                                <a href="{{ $pathori }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC ASLI</span></a>
                            @endif
                            @if (file_exists(public_path().$path))
                                <a href="{{ $path }}" target="_blank"><span class="btn btn-xs btn-success"><i class="ion ion-ios-cloud-download"></i> Download RFC TTD</span></a><br>
                            @else
                                <span class="label label-danger">RFC Bertandatangan BLM ADA</span><br>
                            @endif
                            <span class="label label-primary">TGL : {{ $d->TGL }}</span><br>
                            <span class="label label-primary">MENYERAHKAN : {{ $d->MENYERAHKAN }}</span><br>
                            <span class="label label-primary">MENERIMA : {{ $d->MENERIMA }}</span><br>
                            <span class="label label-primary">MENGETAHUI : {{ $d->MENGETAHUI }}</span>
                           
                        </td>
                        @foreach($d->list as $nos => $l)
                            @if($nos)
                            <tr>
                            @endif
                                <td class="valign-middle">
                                    <span class="label label-success">{{ $l['ID_BARANG'] }}</span> <span class="label label-default">{{ $l['SATUAN'] }}</span><br>
                                    <span class="label label-primary">{{ $l['NAMA_BARANG'] }}</span>
                                </td>
                                <td class="valign-middle"><span class="pull-right">{{ number_format($l['MINTA']) }}</span></td>
                                <td class="valign-middle"><span class="pull-right">{{ number_format($l['BERI']) }}</span></td>
                            </tr>
                        @endforeach

                @endforeach
            </tbody>
        </table>
    </div>
  </div>
</div>
@endsection

@section('js')
@endsection
 