@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')

<div class="row">
    <div class="col-md-3">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>RFC / </span>Rekap</span>
        </h1>
    </div>
    <div class="col-md-3">
        <button id="daterange-4" class="btn dropdown-toggle pull-right m-r-1"></button>
    </div>
    <div class="col-md-2">
        <input type="text" name="mitra" value="{{ Request::segment(3) }}" class="pull-right m-r-1" id="mitra"/>
    </div>
    <div class="col-md-2">
        <input type="text" name="pid" value="{{ Request::segment(4) }}" class="pull-right m-r-1" id="pid"/>
    </div>
    <div class="col-md-2 dropdown">
      <button type="button" class="btn dropdown-toggle btn-success pull-right" data-toggle="dropdown" aria-expanded="false">
        Download
      </button>
      <div class="dropdown-menu">
        <li><a href="/downloads/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}">Excel</a></li>
        <li><a href="/downloadspdfrar/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}"><span class="label label-pill label-danger pull-right">{{ count($rfc) }}</span> RFC</a></li>
      </div>
    </div>
</div>
@endsection
@section('title', 'Rekap Material')
@section('content')
<input type="hidden" name="start" id="start" value="{{ explode(':', Request::segment(2))[0] }}">
<input type="hidden" name="end" id="end" value="{{ explode(':', Request::segment(2))[1] }}">
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>ID BARANG</th>
                <th>WH BJM</th>
                <th>SO BJM1</th>
                <th>SO BJM2</th>
                <th>SO BJB</th>
                <th>SO BLC</th>
                <th>SO TJL</th>
                <th>SO BRI</th>
                <th>TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $total=0;
            ?>
            @foreach($data as $no => $d)

            <?php
                $total = $d->WH_BJM+$d->WHSO_BJM1+$d->WHSO_BJM2+$d->WHSO_BJB+$d->WHSO_BLC+$d->WHSO_TJL+$d->WHSO_BRI;
            ?>
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->ID_BARANG }}</td>
                    <td><span class="pull-right">{{ number_format($d->WH_BJM)?:'-' }}</span></td>
                    <td><span class="pull-right">{{ number_format($d->WHSO_BJM1)?:'-' }}</span></td>
                    <td><span class="pull-right">{{ number_format($d->WHSO_BJM2)?:'-' }}</span></td>
                    <td><span class="pull-right">{{ number_format($d->WHSO_BJB)?:'-' }}</span></td>
                    <td><span class="pull-right">{{ number_format($d->WHSO_BLC)?:'-' }}</span></td>
                    <td><span class="pull-right">{{ number_format($d->WHSO_TJL)?:'-' }}</span></td>
                    <td><span class="pull-right">{{ number_format($d->WHSO_BRI)?:'-' }}</span></td>
                    <td><b class="pull-right">{{ number_format($total) }}</b></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        var start = moment($('#start').val());
        var end = moment($('#end').val());

        function cb(start, end) {
          $('#daterange-4').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#daterange-4').daterangepicker({
          startDate: start,
          endDate: end,
          ranges: {
           'Hari Ini': [moment(), moment()],
           'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
           '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
           'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
           'Bulan Sebelumnya': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
          }
        }, cb);

        cb(start, end);
        $('#daterange-4').on('apply.daterangepicker', function(ev, picker) {
            var s = picker.startDate.format('YYYY-MM-DD');
            var e = picker.endDate.format('YYYY-MM-DD');
            var mitrapid = $(location).attr('href').split("/")[5]+'/'+$(location).attr('href').split("/")[6];
            // alert()
            window.location.href = document.location.origin+"/rekap_pengeluaran/"+s+":"+e+"/"+mitrapid;
        });
        $('#mitra').select2({
            data:$.merge([{'id':'all','text':'Semua Mitra'}],<?= json_encode($mitra); ?>),
            placeholder:'Select Mitra',

        }).change(function(){
            var segments = $(location).attr('href').split( '/' );
            var pid = segments[6];
            var tgl = segments[4];
            window.location.href = document.location.origin+"/rekap_pengeluaran/"+tgl+"/"+this.value+"/"+pid;
        });
        $('#pid').select2({
            data:$.merge([{'id':'all','text':'Semua PID'}],<?= json_encode($pid); ?>),
            placeholder:'Select Mitra',

        }).change(function(){
            var segments = $(location).attr('href').split( '/' );
            var mitra = segments[5];
            var tgl = segments[4];
            // alert(tgl)
            window.location.href = document.location.origin+"/rekap_pengeluaran/"+tgl+"/"+mitra+"/"+this.value;
        });;
    });
</script>
@endsection
