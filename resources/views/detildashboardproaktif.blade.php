@extends('layout')
@section('heading')
<h1> <h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard / </span>List</h1></h1>
@endsection
@section('content')
<div class="table-primary">
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="datatables">
			<thead>
				<tr>
					<th>#</th>
					<th>Project ID</th>
					<th>Nama Project</th>
					<th>Portofolio</th>
					<th>Nomor Kontrak</th>
					<th>PM</th>
					<th>Tanggal Mulai</th>
					<th>Tanggal Selesai</th>
					<th>Tipe Project</th>
					<th>Fase</th>
					<th>Customer</th>
					<th>Status Project</th>
				</tr>
				<tbody>
					@foreach($assetjump as $no => $dataasset)
					<tr>
						<td>{{ ++$no }}</td>
						<td><a href="#">{{ $dataasset->Project_ID}}</a></td>
						<td>{{ $dataasset->Nama_Project}}</td>
						<td>{{ $dataasset->Portofolio}}</td>
						<td>{{ $dataasset->Nomor_Kontrak}}</td>
						<td>{{ $dataasset->PM}}</td>
						<td>{{ $dataasset->Tanggal_Mulai}}</td>
						<td>{{ $dataasset->Tanggal_Selesai}}</td>
						<td>{{ $dataasset->Tipe_Project}}</td>
						<td>{{ $dataasset->Fase}}</td>
						<td>{{ $dataasset->Customer}}</td>
						<td>{{ $dataasset->Status_Project}}</td>
					</tr>
					@endforeach
				</tbody>
			</thead> 
		</table>
	</div>
</div>
@endsection
@section('js')
<script>
	$(function() {
		$('#datatables').dataTable();
		$('#datatables_wrapper .table-caption').text('Proaktif List');
		$('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
	});
</script>
@endsection