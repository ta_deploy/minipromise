@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Import Proaktif</span>
</h1>
@endsection
@section('title', 'BTS || Import Proaktif')
@section('content')
<?php
    $nama = explode('-', $data->nama_project);
?>
<form class="form-horizontal" method="post" id="form-register" enctype="multipart/form-data">
    <input type="hidden" name="proaktif_id" value="{{ Request::segment(2) }}">
    <input type="hidden" name="boq" value="{{ json_encode($boq) }}">
    <div class="form-group">
        <label for="waspannama_lopg" class="col-md-1 control-label">Nama LOP</label>
        <div class="col-md-3 form-message-dark">
            <input type="text" name="nama_lop" id="nama_lop" class="form-control" required value="{{ @$data->nama_project }}" readonly />
        </div>
        <label for="sto" class="col-md-1 control-label">STO</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="sto" id="sto" class="form-control" required value="{{ @$nama[1] }}" />
        </div>
        <label for="odc" class="col-md-1 control-label">ODC</label>
        <div class="col-md-1 form-message-dark">
            <input type="text" name="odc" id="odc" class="form-control" required value="{{ @$nama[2] }}" />
        </div>
        <label for="pid" class="col-md-1 control-label">PID</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="pid" id="pid" class="form-control" required value="{{ @$data->proaktif_id }}" readonly />
        </div>
    </div>
    <div class="form-group">
        <label for="mitra" class="col-md-1 control-label">Mitra</label>
        <div class="col-md-3 form-message-dark">
            <input type="text" name="mitra" id="mitra" class="form-control" required value="{{ @$data->mitra     }}" />
        </div>
        <label for="waspang" class="col-md-1 control-label">Waspang</label>
        <div class="col-md-3 form-message-dark">
            <input type="text" name="waspang" id="waspang" class="form-control" required value="{{ @$data->waspang }}"/>
        </div>
        <label for="jenis_pekerjaan" class="col-md-2 control-label">Jenis Pekerjaan</label>
        <div class="col-md-2 form-message-dark">
            <select name="jenis_pekerjaan" id="jenis_pekerjaan" class="form-control" required>
                <option value="PT3" {{ str_contains($data->nama_project, 'OSP')?'selected':'' }}>PT3</option>
                <option value="NODE-B" {{ str_contains($data->nama_project, 'NODE')?'selected':'' }}>NODE-B</option>
                <option value="FEEDERISASI" {{ str_contains($data->nama_project, 'FEE')?'selected':'' }}>FEEDERISASI</option>
            </select>
        </div>
    </div>

    <?php
        $todayrange = date('Y-m-d').' - '.date('Y-m-d');
    ?>
    <div class="form-group">

        <label for="tematik" class="col-md-1 control-label">Tematik</label>
        <div class="col-md-3 form-message-dark">
            <input type="text" name="tematik" id="tematik" class="form-control" required />
        </div>
        
        <label for="waspannama_lopg" class="col-md-2 control-label">Timeplan Pekerjaan Fisik</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" id="daterange-1" name="timeplan_fisik" value="{{ @$data->timeplan_fisik ?? $todayrange }}" class="form-control">
        </div>
        <label for="waspang" class="col-md-2 control-label">Timeplan Pemberkasan</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" id="daterange-2" name="timeplan_berkas" value="{{ @$data->timeplan_berkas ?? $todayrange }}" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label for="sapid" class="col-md-1 control-label">SAP ID</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="sapid" id="sapid" class="form-control" required value="{{ @$data->sapid }}" readonly />
        </div>
        <div class="col-md-9 form-message-dark">
            <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
<div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>#</th>
                <th>Designator</th>
                <th>Uraian</th>
                <th>Satuan</th>
                <th>Material Telkom</th>
                <th>Jasa Telkom</th>
                <th>Material Mitra</th>
                <th>Mitra Jasa</th>
                <th>Volume</th>
                <th>Total Telkom</th>
                <th>Total Mitra</th>
            </tr>
        </thead>
        <tbody>
            @foreach($boq as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d['designator'] }}</td>
                    <td>{{ $d['uraian'] }}</td>
                    <td>{{ $d['satuan'] }}</td>
                    <td class="text-right">{{ $d['telkom_material'] }}</td>
                    <td class="text-right">{{ $d['telkom_jasa'] }}</td>
                    <td class="text-right">{{ $d['mitra_material'] }}</td>
                    <td class="text-right">{{ $d['mitra_jasa'] }}</td>
                    <td class="text-right">{{ $d['volume'] }}</td>
                    <td class="text-right">{{ $d['telkom_total'] }}</td>
                    <td class="text-right">{{ $d['mitra_total'] }}</td>

                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('js')
<script>
    $(function() {
        $("#project").select2({
            placeholder: 'Masukan Nama Project Atau PID',
            minimumInputLength: 3,
            ajax: {
                url: "/projectsearching",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        find: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
        var mitra = <?= json_encode($mitra); ?>;
        $('#mitra').select2({
            data:mitra,
            placeholder:"Pilih Mitra"
        });
        var waspang = <?= json_encode($waspang); ?>;
        $('#waspang').select2({
            data:waspang,
            placeholder:"Pilih Waspang"
        });
        var sto = <?= json_encode($sto); ?>;
        $('#sto').select2({
            data:sto,
            placeholder:"Pilih STO"
        });
        var tematik = <?= json_encode($tematik); ?>;
        $('#tematik').select2({
            data:tematik,
            placeholder:"Pilih tematik"
        });
        $('#daterange-1').daterangepicker({
          locale: {
            format: 'YYYY-MM-DD'
          }
        });
        $('#daterange-2').daterangepicker({
          locale: {
            format: 'YYYY-MM-DD'
          }
        });
    });
</script>
@endsection
