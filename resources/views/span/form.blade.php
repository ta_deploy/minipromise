@extends('layout')
@section('heading')
<h1><a href="/span" class="ion-arrow-left-a"></a> Form Span {{ $data->nama_span or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-span">
    <div class="form-group form-message-dark">
        <label for="nama_span" class="col-md-2 control-label">nama_span</label>
        <div class="col-md-9">
            <input type="text" class="form-control nama_span" name="nama_span" id="nama_span" value="{{ $data->nama_span or '' }}" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-span').pxValidate();
    });
</script>
@endsection