@extends('layout')
@section('css')
  <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />

@endsection
@section('heading')
  <h1><a href="/span" class="ion-arrow-left-a"></a> View Span {{ $data->nama_span or '' }}</h1>
@endsection
@section('content')
<div class="row">
  <div class="col-md-6">
    <form>
      <div class="row">
        <div class="col-sm-3 form-group">
          <label for="nama_span">Nama Span</label>
          <input type="text" id="nama_span" class="form-control" value="{{ $data->nama_span or '' }}" readonly>
        </div>
        <div class="col-sm-5 form-group">
          <label for="ring">Ring</label>
          <input type="text" id="ring" class="form-control" readonly>
        </div>
        <div class="col-sm-4 form-group">
          <label for="ownership">Ownership</label>
          <input type="text" id="ownership" class="form-control" readonly>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 form-group">
          <label for="alamat_site">Alamat Site</label>
          <textarea id="alamat_site" class="form-control" readonly></textarea>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12 form-group">
          <label for="pic_permit">Pic Permit</label>
          <input type="text" id="pic_permit" class="form-control" readonly>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-6 form-group">
          <label for="wakar">Wakar</label>
          <input type="text" id="wakar" class="form-control" readonly>
        </div>
        <div class="col-sm-6 form-group">
          <label for="telp">No.Telp</label>
          <input type="text" id="telp" class="form-control" readonly>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 form-group">
          <label for="alamat_wakar">Alamat Wakar</label>
          <textarea id="alamat_wakar" class="form-control" readonly></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12 form-group">
          <button type="submit" class="btn btn-block">Simpan</button>
        </div>
      </div>
    </form>
  </div>
  <div class="col-md-6">
    <div id="map" style="height: 400px;"></div>
  </div>
</div>


@endsection
@section('js')
<script src="https://unpkg.com/leaflet/dist/leaflet.js"></script>
<script src="/js/KML.js"></script>
<script>
  $(function() {
    $('#form-span').pxValidate();
  });
</script>
<script>
  const map = L.map('map').setView([-3.3309412,114.5882068], 13);
  const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
  }).addTo(map);
  var kmlLayer = new L.KML("/upload/unwizingfile/kml_unwizing.kml", {async: true});
  console.log(kmlLayer)
   kmlLayer.on("loaded", function(e) {
      map.fitBounds(e.target.getBounds());
   });
   L.control.layers({}, {'KML': kmlLayer}, {collapsed: false}).addTo(map);
</script>
@endsection