@extends('layout')
@section('css')
<style type="text/css">
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('title', 'BTS || Buat Mitra')
@section('heading')
<h1><a href="/" class="ion-arrow-left-a"></a> Create Mitra</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" id="form-laporan">
    <div class="form-group form-message-dark">
        <label for="mitra" class="col-md-2 control-label">Mitra</label>
        <div class="col-md-9">
            <select name="mitra" class="form-control" id="mitra">
                @foreach ($mitra as $m)
                    <option value="{{ $m->mitra }}"> {{ $m->mitra }} </option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="uraian" class="col-md-2 control-label">Nama Tim</label>
        <div class="col-md-9">
            <input type="text" class="form-control uraian" name="uraian" id="uraian" value=" {{$data->uraian or '' }} " required>
        </div>
    </div>
    <div class="form-group">
        <label for="nik1" class="col-md-2 control-label">Nik 1</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="nik1" id="nik1" class="form-control" required value="{{$data->nik1 or '' }}"/>
        </div>
        <label for="nik2" class="col-md-1 control-label">NIk 2</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="nik2" id="nik2" class="form-control" required value="{{$data->nik2 or '' }}"/>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan Laporan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script type="text/javascript">
    $(function() {
        $('#mitra').select2()
    });
</script>
@endsection