@extends('layout')
@section('title', 'BTS || Buat Mitra')
@section('heading')
<h1><a href="/mitra" class="ion-arrow-left-a"></a> Form Mitra {{ $data->mitra or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-mitra">
    <div class="form-group form-message-dark">
        <label for="mitra" class="col-md-2 control-label">Nama Mitra</label>
        <div class="col-md-9">
            <input type="text" class="form-control mitra" name="mitra" id="mitra" value="{{ $data->mitra or '' }}" placeholder="Panjang huruf 50" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="label" class="col-md-2 control-label">Label</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="label" id="label" value="{{ $data->label or '' }}" placeholder="Tiga huruf untuk show chart" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="witel_mitra" class="col-md-2 control-label">Witel</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="witel_mitra" id="witel_mitra" value="{{ $data->witel_mitra or '' }}" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="status" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" class="form-control status" name="status" id="status" value="{{ $data->status or '' }}" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-mitra').pxValidate();
        $('#status').select2({
            data:[{"id":"Aktif","text":"Aktif"},{"id":"Tidak Aktif","text":"Tidak Aktif"}],
            placeholder:"pilih status"
        });
        $('#witel_mitra').select2({
            data:<?=  json_encode($witel) ?>,
            placeholder:"pilih witel"
        });
    });
</script>
@endsection