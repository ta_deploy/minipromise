@extends('layout')
@section('title', 'BTS || List Team')
@section('content')
<div>
    <a type="button" class="btn btn-info" href="/timmitra/create">Tambah Tim</a>
</div>
<span><b>List Tim Mitra</b></span>
<div class="row">
    @foreach ($list as $m)
    <div class="col-sm-3">
        <div class="panel panel-warning">
            <div class="panel-heading">{{ $m->mitra }}</div>
            <div class="panel-body">
                {{ $m->uraian }}
            </div>
            <div class="panel-footer">
                <a type="button" href="" class="btn btn-warning">Edit</a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection