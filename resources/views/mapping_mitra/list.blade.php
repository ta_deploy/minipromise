@extends('layout')
@section('css')
<style type="text/css">
  .no-search .select2-search {
      display:none
  }
</style>
@endsection
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Mapping LOP </span> </h1>
@endsection
@section('title', 'BTS || List Unmapping')
@section('content')
@if(count($count))
<div class="row text-center">
  @foreach($count as $c)
  <?php
    $str = '';
    foreach($mapped as $m){
// dd($m);
      if($m->mitra == $c->mitra){
        $str .= "<b>".$m->nama_lop."</b> - <a href='/remove_mapping_mitra/".$m->id."'>Remove</a><br/>";
      }
    }
  ?>
  <div class="col-md-2" tabindex="0" data-placement="bottom"
   class="btn btn-lg btn-primary" 
   role="button" 
   data-html="true" 
   data-toggle="popover" 
   data-trigger="focus" 
   title="{{ $c->nama_mitra }}" 
   data-content="<div>{{ $str }}</div>">
    <div class="box bg-info">
      <div class="box-cell p-x-3 p-y-1">
        <div class="font-weight-semibold font-size-12">{{ $c->label }}</div>
        <div class="font-weight-bold font-size-20">{{ $c->Baris }}</div>
      </div>
    </div>
  </div>
    @endforeach
    <div class="col-md-2">
      <div class="box bg-primary">
        <div class="box-cell p-x-3 p-y-1">
          <a href="/done_mapping_mitra/{{ Request::segment(2) }}">
            <div class="font-weight-semibold font-size-12">Kirim Aanwijzing</div>
            <div class="font-weight-bold font-size-20">Selesai</div>
            <i class="box-bg-icon middle right font-size-52 ion-arrow-graph-up-right"></i>
          </a>
        </div>
      </div>
    </div>
</div>
@endif
<div class="row">
  <div class="table-responsive table-primary">
    <table class="table" id="datatables">
      <thead>
        <tr>
          <th>#</th>
          <th>Ring</th>
          <th>Nama Lop</th>
          <th>Witel</th>
          <th>STO</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($unmapping as $no => $d)
          <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $d->ring }}</td>
            <td>{{ $d->nama_lop }}</td>
            <td>{{ $d->witel_lop }}</td>
            <td>{{ $d->sto }}</td>
            <td><button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modal-mapping" data-lopid="{{ $d->id }}" data-mitosid="{{ $d->promitos_id }}" data-nama_lop="{{ $d->nama_lop }}" ><i class="ion ion-checkmark"></i> Mapping</button></td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
<div class="modal fade" id="modal-mapping" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">Mapping LOP <span class="nama_lop"></span></h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal" method="post" id="formMapping" autocomplete="off" action="/mapping_mitra/{{ Request::segment(2) }}" enctype="multipart/form-data">
          <input type="hidden" name="tematik" id="tematik" class="form-control" value="{{ Request::segment(2) }}"/>
          <input type="hidden" name="lop_id" id="lop_id" class="form-control"/>
          <input type="hidden" name="mitos_id" id="mitos_id" class="form-control"/>
          <div class="form-group form-message-dark">
            <label for="mitra" class="col-md-3 control-label">Mitra</label>
            <div class="col-md-9">
              <input type="text" name="mitra" id="mitra" class="form-control" required/>
            </div>
          </div>
          <div class="form-group form-message-dark">
            <label for="waspang" class="col-md-3 control-label">Waspang</label>
            <div class="col-md-9">
              <input type="text" name="waspang" id="waspang" class="form-control" required/>
            </div>
          </div>
          <div class="form-group form-message-dark">
            <label for="jenis_pekerjaan" class="col-md-3 control-label">Jenis Pekerjaan</label>
            <div class="col-md-9">
              <select name="jenis_pekerjaan" id="jenis_pekerjaan" class="form-control" required>
                <option value="PT3" >PT3 OSP</option>
                <option value="NODE-B" >NODE-B</option>
                <option value="FEEDERISASI" >FEEDERISASI</option>
                <option value="MITRATEL" >MITRATEL</option>
              </select>
            </div>
          </div>
        </form>
        <div class="promitos table-responsive table-primary"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit" form="formMapping" value="Submit">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $('[data-toggle="popover"]').popover();
    $('#modal-mapping').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget)
      var modal = $(this)
      var recipient = button.data('lopid') 
      var mitos = button.data('mitosid') 
      $.getJSON("https://promitos.tomman.app/API-GetBoq/"+mitos, function(result){
        var table = "<table class='table table-bordered'><thead><tr><th>Designator</th><th>QTY</th><th>H.Material</th><th>H.Jasa</th></tr></thead><tbody>"
        $.each(result, function(i, field){
          if(field.qty){
            table += "<tr><td>"+field.designator+"</td><td><span class='pull-right'>"+numberWithCommas(field.qty)+"</span></td><td><span class='pull-right'>"+numberWithCommas(field.hargamaterial)+"</span></td><td><span class='pull-right'>"+numberWithCommas(field.hargajasa)+"</span></td></tr>"
          }
        });
        table += "</tbody></table>"
        $(".promitos").html(table);
      });
      modal.find('#lop_id').val(recipient)
      modal.find('.nama_lop').html(button.data('nama_lop'))
      modal.find('#mitos_id').val(mitos)
    });
    $('#mitra').select2({
        data:<?= $mitra; ?>,
        placeholder:'Pilih Mitra',
        dropdownParent: $("#modal-mapping")
    });
    $('#waspang').select2({
        data:<?= $waspang; ?>,
        placeholder:'Pilih Waspang',
        dropdownParent: $("#modal-mapping")
    });
    function numberWithCommas(x) {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
</script>
@endsection
