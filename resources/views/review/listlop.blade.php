@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Review / </span>List LOP </h1>
@endsection
@section('title', 'BTS || Review')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>LOP</th>
                <th>Waspang</th>
                <th>Review Progress</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama_lop }}</td>
                    <td>{{ $d->waspang_nm }}</td>
                    <td>
                        <?php
                        // dd($d);
                            $rp = $d->review_progress;
                            $clr = "danger";
                            if($rp>69){
                                $clr="success";
                            }else if($rp>30){
                                $clr="warning";
                            }
                        ?>
                        <div class="progress m-a-0 p-a-0" style="height: 20px;">
                          <div class="progress-bar progress-bar-{{$clr}} progress-bar-striped active" style="width: {{$rp}}%">{{ $rp }}%</div>
                        </div>
                    </td>
                    <td class="text-right">
                        @if($d->ada_plan)
                        <a href="/review/{{ $d->mitra }}/{{ $d->lop_id }}" class="btn btn-xs btn-info detail"><i class="ion-update"></i>&nbsp;&nbsp;Review</a>
                        @else
                          <span class="label label-warning">Blm Update Plan</span>  
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
@endsection
