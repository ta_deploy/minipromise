@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <a href="/review/{{ Request::segment(2) }}" class="ion-arrow-left-a"></a> Form Review {{ $lop->nama_lop }}
</h1>
@endsection
@section('title', 'BTS || Review')
@section('content')
<div class="row">
    <div id="overview-chart" style="height: 200px"></div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info panel-dark">
      <div class="panel-body">
        <form method="post" id="form-review">
            <div class="row">
                <div class="col-sm-3 form-group">
                  <label for="tgl">Tgl</label>
                  <input type="text" name="tgl" id="tgl" class="form-control" autocomplete="off" required>
                </div>
                <div class="col-sm-4 form-group">
                  <label for="status">Status</label>
                  <input type="text" name="status" id="status" class="form-control" autocomplete="off" required>
                </div>
                <div class="col-sm-5 text-right">

                <button type="submit" class="btn btn-md btn-info btn-outline btn-3d" style="margin-top: 25px;">Tambah</button>
                <button type="button" class="btn btn-md btn-success btn-outline btn-3d" style="margin-top: 25px;" data-toggle="modal" data-target="#modal-base">Selesai</button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="catatan">Catatan</label>
                  <textarea id="catatan" name="catatan" class="form-control" autocomplete="off" required></textarea>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    
    <div class="table-responsive table-primary">
          <table class="table" id="datatables">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tgl</th>
                    <th>Status</th>
                    <th>Catatan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $d)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $d->tgl }}</td>
                        <td>{{ $d->status }}</td>
                        <td>{{ $d->catatan }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
  </div>
</div>
<div class="modal" id="modal-base">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
    <form method="post" action="/savereviewprogress/{{ Request::segment(2) }}/{{ Request::segment(3) }}">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Isi Progress Sekarang</h4>
      </div>
      <div class="modal-body">
        <?php
            $rp = $lop->review_progress;
            $clr = "danger";
            if($rp>69){
                $clr="success";
            }else if($rp>30){
                $clr="warning";
            }
        ?>
        <div class="progress m-a-0 p-a-0" style="height: 20px;">
          <div class="progress-bar progress-bar-{{$clr}} progress-bar-striped active" style="width: {{$rp}}%">Last Review:{{ $rp }}%</div>
        </div>
        
            <div class="row">
                <div class="col-sm-12 form-group">
                  <label for="review_progress">Progress</label>
                  <input type="text" name="review_progress" id="review_progress" class="form-control" autocomplete="off" required>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save Progress</button>
      </div>
    </form>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        $('#status').select2({
            data:[{"id":"Hibat","text":"Hibat"},{"id":"On the Track","text":"On the Track"},{"id":"Kendala","text":"Kendala"},{"id":"Anomali","text":"Anomali"}],
            placeholder:"Pilih Status",
            dropdownCssClass : 'no-search'
        });
        $('#tgl').datepicker({
            autoclose:true,
            format: 'yyyy-mm-dd',
            orientation: "bottom left"
        });
        new Morris.Line({
          element:        'overview-chart',
          data:           <?= json_encode($bobot); ?>,
          xkey:           'tgl',
          ykeys:          ['plan','real'],
          labels:          ['plan','real'],
          grid:false,
          xLabelFormat: function (d) {
            return ("0" + d.getDate()).slice(-2) + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + d.getFullYear();
          },
        });
        $('#knob-responsive').knob();
    });
</script>
@endsection
