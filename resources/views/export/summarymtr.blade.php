<table>
  <thead>
    <tr>
      <th>WO</th>
      <th>SPAN</th>
      <th>RING</th>
      <th>mpromise_project_id</th>
      <th>waspang_nm</th>
      <th>tl</th>
      <th>mitra</th>
      <th>step</th>
      <th>ODC_PLAN</th>
      <th>ODP_PLAN</th>
      <th>KABEL_PLAN</th>
      <th>TIANG_PLAN</th>
      <th>ODC_REAL</th>
      <th>ODP_REAL</th>
      <th>KABEL_REAL</th>
      <th>TIANG_REAL</th>
      <th>ODC_MOS</th>
      <th>ODP_MOS</th>
      <th>KABEL_MOS</th>
      <th>TIANG_MOS</th>
      <th>ODC_REAL_HM1</th>
      <th>ODP_REAL_HM1</th>
      <th>KABEL_REAL_HM1</th>
      <th>TIANG_REAL_HM1</th>
      <th>ODC_ACH</th>
      <th>ODP_ACH</th>
      <th>KABEL_ACH</th>
      <th>TIANG_ACH</th>
      <th>ACH_HI</th>
      <th>ACH_HM1</th>
    </tr>
  </thead>
  <tbody>
    @foreach($mtr as $d)
      <tr>
        <td>{{ $d->WO }}</td>
        <td>{{ $d->SPAN }}</td>
        <td>{{ $d->RING }}</td>
        <td>{{ $d->mpromise_project_id }}</td>
        <td>{{ $d->waspang_nm }}</td>
        <td>{{ $d->tl }}</td>
        <td>{{ $d->mitra }}</td>
        <td>{{ $d->step }}</td>
        <td>{{ $d->ODC_PLAN }}</td>
        <td>{{ $d->ODP_PLAN }}</td>
        <td>{{ $d->KABEL_PLAN }}</td>
        <td>{{ $d->TIANG_PLAN }}</td>
        <td>{{ $d->ODC_REAL }}</td>
        <td>{{ $d->ODP_REAL }}</td>
        <td>{{ $d->KABEL_REAL }}</td>
        <td>{{ $d->TIANG_REAL }}</td>
        <td>{{ $d->ODC_MOS }}</td>
        <td>{{ $d->ODP_MOS }}</td>
        <td>{{ $d->KABEL_MOS }}</td>
        <td>{{ $d->TIANG_MOS }}</td>
        <td>{{ $d->ODC_REAL_HM1 }}</td>
        <td>{{ $d->ODP_REAL_HM1 }}</td>
        <td>{{ $d->KABEL_REAL_HM1 }}</td>
        <td>{{ $d->TIANG_REAL_HM1 }}</td>
        <td>{{ $d->ODC_ACH }}</td>
        <td>{{ $d->ODP_ACH }}</td>
        <td>{{ $d->KABEL_ACH }}</td>
        <td>{{ $d->TIANG_ACH }}</td>
        <td>{{ $d->ACH_HI }}</td>
        <td>{{ $d->ACH_HM1 }}</td>
      </tr>
    @endforeach
  </tbody>
</table>