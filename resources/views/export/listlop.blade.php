<table>
    <thead>
    <tr>
        <th>step</th>
        <th>id</th>
        <th>step_id</th>
        <th>nama_lop</th>
        <th>pid</th>
        <th>sto</th>
        <th>waspang</th>
        <th>waspang_nm</th>
        <th>nama_project</th>
        <th>jenis_pekerjaan</th>
        <th>mitra</th>
        <th>plan_odp</th>
        <th>real_odp</th>
        <th>odp_golive</th>
        <th>timeplan_fisik</th>
        <th>timeplan_berkas</th>
        <th>created_at</th>
        <th>updated_at</th>
        <th>created_by</th>
        <th>status</th>
        <th>tgl_material_tiba</th>
        <th>progres_instalasi</th>
        <th>catatan_instalasi</th>
        <th>Done_Aanwijzing</th>
        <th>Done_Pengiriman_Material</th>
        <th>Done_Pragelaran</th>
        <th>Done_Tanam_Tiang</th>
        <th>Done_Gelar_Kabel</th>
        <th>Done_Pasang_Terminal</th>
        <th>Done_Terminasi</th>
        <th>Done_Perijinan</th>
        <th>status_qc</th>
        <th>DROPLOP</th>
        <th>PENGGANTI_LOP</th>
        <th>isHapus</th>
        <th>NILAI_LOP</th>
        <th>TGL_QC_OK</th>
        <th>TGL_GO_LIVE</th>
    </tr>
    </thead>
    <tbody>
    @foreach($data as $d)
        <tr>
            <td>{{ $d->step }}</td>
            <td>{{ $d->id }}</td>
            <td>{{ $d->step_id }}</td>
            <td>{{ $d->nama_lop }}</td>
            <td>{{ $d->pid }}</td>
            <td>{{ $d->sto }}</td>
            <td>{{ $d->waspang }}</td>
            <td>{{ $d->waspang_nm }}</td>
            <td>{{ $d->nama_project }}</td>
            <td>{{ $d->jenis_pekerjaan }}</td>
            <td>{{ $d->mitra }}</td>
            <td>{{ $d->plan_odp }}</td>
            <td>{{ $d->real_odp }}</td>
            <td>{{ $d->odp_golive }}</td>
            <td>{{ $d->timeplan_fisik }}</td>
            <td>{{ $d->timeplan_berkas }}</td>
            <td>{{ $d->created_at }}</td>
            <td>{{ $d->updated_at }}</td>
            <td>{{ $d->created_by }}</td>
            <td>{{ $d->status }}</td>
            <td>{{ $d->tgl_material_tiba }}</td>
            <td>{{ $d->progres_instalasi }}</td>
            <td>{{ $d->catatan_instalasi }}</td>
            <td>{{ $d->Done_Aanwijzing }}</td>
            <td>{{ $d->Done_Pengiriman_Material }}</td>
            <td>{{ $d->Done_Pragelaran }}</td>
            <td>{{ $d->Done_Tanam_Tiang }}</td>
            <td>{{ $d->Done_Gelar_Kabel }}</td>
            <td>{{ $d->Done_Pasang_Terminal }}</td>
            <td>{{ $d->Done_Terminasi }}</td>
            <td>{{ $d->Done_Perijinan }}</td>
            <td>{{ $d->status_qc }}</td>
            <td>{{ $d->DROPLOP }}</td>
            <td>{{ $d->PENGGANTI_LOP }}</td>
            <td>{{ $d->isHapus }}</td>
            <td>{{ $d->NILAI_LOP }}</td>
            <td>{{ $d->TGL_QC_OK }}</td>
            <td>{{ $d->TGL_GO_LIVE }}</td>
        </tr>
    @endforeach
    </tbody>
</table>