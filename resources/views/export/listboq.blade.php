<table>
    <thead>
    <tr>
        <th rowspan="2">Designator</th>
        @foreach($data as $dt)
            <th>{{ $dt->ring }}</th>
        @endforeach
    </tr>
    <tr>
        @foreach($data as $dt)
            <th>{{ $dt->nama_lop }}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
    @foreach($designator as $d)
        <tr>
            <td>{{ $d->id_barang }}</td>
            @foreach($data as $dt)
                @if(array_key_exists($dt->id,$boq) && array_key_exists($d->id_barang,$boq[$dt->id]))
                    <td>{{ $boq[$dt->id][$d->id_barang] }}</td>
                @else
                    <td>-</td>
                @endif
            @endforeach
        </tr>
    @endforeach
    </tbody>
</table>