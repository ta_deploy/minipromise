@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Project / </span>Register
</h1>
@endsection
@section('title', 'BTS || Buat Project')
@section('content')
@if (Session::has('aletrs'))
<div class="alert alert-{{ Session::get('aletrs')['type'] }}">Data Berhasil Diinput</div>
@endif
<form class="form-horizontal" method="post" id="form-register" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="nama" class="col-md-2 control-label">Nama</label>
        <div class="col-md-10 form-message-dark">
            <input type="text" name="nama" id="nama" class="form-control" required />
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="switcher-sm" class="col-md-2 control-label">PT3?</label>
        <div class="col-md-9">
          <label for="switcher-sm" class="switcher switcher-blank switcher-success">
            <input type="checkbox" id="switcher-sm" name="pt3">
            <div class="switcher-indicator">
              <div class="switcher-yes">YES</div>
              <div class="switcher-no">NO</div>
            </div>
          </label>
        </div>
    </div>
    <!-- <div class="form-group">
        <label for="pid" class="col-md-2 control-label">PID</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="pid" id="pid" class="form-control" required/>
        </div>
        <label for="sto" class="col-md-1 control-label">STO</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="sto" id="sto" class="form-control" required/>
        </div>
    </div>
    <div class="form-group">
        <label for="waspang" class="col-md-2 control-label">NIK Waspang</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="waspang" id="waspang" class="form-control" required />
        </div>
        <label for="waspang" class="col-md-2 control-label">Nama Waspang</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="waspang_nm" id="waspang_nm" class="form-control" required />
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="BOQ_Ondesk" class="col-md-2 control-label">BOQ Ondesk</label>
        <div class="col-md-10">
            <label id="BOQ_Ondesk" class="custom-file px-file" for="grid-input-6">
                <input type="file" id="grid-input-6" class="custom-file-input" name="boq_ondesk" required>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="KML_Ondesk" class="col-md-2 control-label">KML Ondesk</label>
        <div class="col-md-10">
            <label id="KML_Ondesk" class="custom-file px-file" for="KML_Ondesk">
                <input type="file" id="KML_Ondesk" class="custom-file-input" name="kml_ondesk" required>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="MCORE_Ondesk" class="col-md-2 control-label">MCORE Ondesk</label>
        <div class="col-md-10">
            <label id="MCORE_Ondesk" class="custom-file px-file" for="MCORE_Ondesk">
                <input type="file" id="MCORE_Ondesk" class="custom-file-input" name="mcore_ondesk" required>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div> -->
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        $('#form-register').pxValidate();
        $('#BOQ_Ondesk').pxFile();
        $('#MCORE_Ondesk').pxFile();
        $('#KML_Ondesk').pxFile();

        // $("#waspang").select2({
        //     placeholder: 'Masukan NIK Waspang',
        //     minimumInputLength: 3,
        //     ajax: {
        //         url: "/waspang_search",
        //         type: "get",
        //         dataType: 'json',
        //         delay: 250,
        //         data: function (params) {
        //             return {
        //              find: params.term
        //             };
        //         },
        //         processResults: function (response) {
        //             return {
        //                 results: response
        //             };
        //         },
        //         cache: true
        //     }
        // });

        var sto = <?= json_encode($sto); ?>;
        
        $('#sto').select2({
            placeholder:"Select STO",
            data:sto
        }).change(function() {
            $(this).valid();
        });

        $("#formContent").submit(function(e){
            e.preventDefault();
            var formdata = new FormData(this);
            $.ajax({
                url: "/saveproject",
                type: "POST",
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                error: function(){
                    alert("okey");
                }
            });
        });
    });
</script>
@endsection
