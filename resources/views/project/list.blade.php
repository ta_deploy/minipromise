@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Tematik / </span>List
</h1>
<a href="/project/register" class="pull-right"><span class="btn btn-info"><i class="ion ion-plus"></i> Tambah Tematik</span></a>
@endsection
@section('title', 'BTS || List LOP')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Project</th>
                <th>Tahun Create</th>
                <th>Total LOP</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama }}</td>
                    <td>{{ substr($d->created_at,0,4) }}</td>
                    <td>{{ $d->totallop }}</td>
                    <td><span class="label label-{{ $d->stts?'danger':'success' }}">{{ $d->stts?'NonActive':'Active' }}</span></td>
                    <td>
                        <!-- <a><i class="ion-update"></i>&nbsp;&nbsp;Update</a> -->
                        @if(!$d->stts)
                            <a href="/listLopByProject/{{ $d->id }}"><span class="btn btn-sm btn-primary"><i class="ion ion-drag"></i>&nbsp;&nbsp;List LOP</span></a>
                            <!-- <a href="/lop/{{ $d->id }}/register/null"><span class="btn btn-sm btn-info"><i class="ion ion-plus-round"></i>&nbsp;&nbsp;Tambah LOP</span></a>
                            <a href="/lop/{{ $d->id }}/upload"><span class="btn btn-sm btn-info"><i class="ion ion-upload"></i>&nbsp;&nbsp;Upload LOP</span></a> -->
                            <a href="/downloadtematik/{{ $d->id }}"><span class="btn btn-sm btn-info"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Download</span></a>
                            <span class="btn btn-sm btn-danger stts" data-idtematik="{{ $d->id }}" data-tematik="{{ $d->nama }}" data-stts="1" data-bg="modal-danger"><i class="ion ion-close-circled"></i>&nbsp;&nbsp;Non Active</span>

                            <a href="/mon_material/{{ $d->id }}"><span class="btn btn-sm btn-primary"><i class="ion ion-info"></i>&nbsp;&nbsp;Material</span></a>
                            <a href="/mapping_mitra/{{ $d->id }}"><span class="btn btn-sm btn-primary"><i class="ion ion-info"></i>&nbsp;&nbsp;Mapping</span></a>

                        @else
                            <a href="/downloadtematik/{{ $d->id }}"><span class="btn btn-sm btn-info"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Download</span></a>
                            <span class="btn btn-sm btn-success stts" data-idtematik="{{ $d->id }}" data-tematik="{{ $d->nama }}" data-stts="0" data-bg="modal-success"><i class="ion ion-checkmark-circled"></i>&nbsp;&nbsp;Active</span>
                        @endif

                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

<div id="modal-info" class="modal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title">Merubah Status Tematik</h4>
          </div>
          <div class="modal-body">
            <p id="content"></p>
          </div>
          <div class="modal-footer">
            <form action="/projectstts" method="post">
                <input type="hidden" name="id" id="id">
                <input type="hidden" name="stts" id="stts">
                <button type="button" class="btn" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $(".stts").click(function(e){
            var status = this.dataset.stts==1?"Non Active":"Active";
            $("#content").html(this.dataset.tematik+" menjadi "+status);
            $("#id").val(this.dataset.idtematik);
            $("#stts").val(this.dataset.stts);
            $("#modal-info").removeClass("modal-success");
            $("#modal-info").removeClass("modal-danger");
            $("#modal-info").addClass(this.dataset.bg);
            $("#modal-info").modal('show');
        });
    });
</script>
@endsection
