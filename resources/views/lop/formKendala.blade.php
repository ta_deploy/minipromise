@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || KENDALA')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Kendala
</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" id="formKendala" autocomplete="off" action="/kendala/{{ Request::segment(2) }}" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="status_material" class="col-md-3 control-label">Kendala</label>
        <div class="col-md-9">
            <input type="text" name="status" id="status_material" class="form-control" required/>
        </div>
    </div>

    <?php
        $todayrange = date('Y-m-d').' - '.date('Y-m-d');
    ?>
    <div class="form-group">
        <label for="waspannama_lopg" class="col-md-3 control-label">Timeplan Pekerjaan Fisik</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" id="daterange-1" name="timeplan_fisik" value="{{ @$data->timeplan_fisik ?? $todayrange }}" class="form-control">
        </div>
        <label for="waspang" class="col-md-2 control-label">Timeplan Pemberkasan</label>
        <div class="col-md-3 form-message-dark">
            <input type="text" id="daterange-2" name="timeplan_berkas" value="{{ @$data->timeplan_berkas ?? $todayrange }}" class="form-control">
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-3 control-label">&nbsp;</label>
        <?php
            $foto = ["Evidence_Kendala"];
        ?>
        <div class="col-md-9">
         @foreach($foto as $f)
         <div class="col-xs-3 col-sm-3 input-photos text-center {{$f}}">
            <?php
            $input=$f;
            $path = "/storage/".Request::segment(2)."/instalasifile/evident/".$input;
            $th   = $path."-th.jpg";
            $img  = $path.".jpg";
            $flag = "";
            $name = "flag_".$input;
            ?>
            @if (file_exists(public_path().$th))
            <a href="{{ $img }}" target="_blank">
                <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
            <?php
            $flag = 1;
            ?>
            @else
            <img src="/image/placeholder.gif" alt="" />
            @endif
            <br />
            <input type="text" style="opacity: 0;pointer-events: none;" size="6" class="text pull-left" name="flag_{{ $input }}" value="{{ $flag }}" required />
            <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
            <button type="button" class="btn btn-sm btn-info m-t-1">
                <i class="ion-camera"></i>
            </button>
            <p>Foto {{ str_replace("_"," ",$input)  }}</p>
        </div>
        @endforeach
    </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $('#formDelivery').pxValidate();

    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
            $(inputEl).parent().find('input[type=text]').val(1);
            var reader = new FileReader();
            reader.onload = function(e) {
                $(inputEl).parent().find('img').attr('src', e.target.result);

            }
            reader.readAsDataURL(inputEl.files[0]);
        }
    });
    $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
    });
    
    $('#daterange-1').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD'
      }
    });
    $('#daterange-2').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD'
      }
    });
</script>
@endsection
