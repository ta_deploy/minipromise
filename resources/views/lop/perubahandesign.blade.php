@extends('layout')
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Perubahan Desgin
</h1>
@endsection
@section('title', 'Perubahan Desgin')
@section('content')
<input type="hidden" id="lop_id" value="{{ Request::segment(2) }}"/>
<form class="form-horizontal" method="post" id="form-unwizing" enctype="multipart/form-data" action="/perubahandesign/{{ Request::segment(2) }}">
    <div class="form-group form-message-dark">
        <label for="BOQ_Unwizing" class="col-md-2 control-label">BOQ aanwijzing</label>
        <div class="col-md-8">
            <label id="BOQ_Unwizing" class="custom-file px-file" for="BOQ_Unwizingi">
                <input type="file" id="BOQ_Unwizingi" class="custom-file-input" name="boq_unwizing" {{ $data->mcore_unwizing?'':'required' }}>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            <a href="/download/storage+BOQ_FORMAT.xlsx" target="_blank"><span class="label label-info pull-lg-right">Unduh Format</span></a>
        </div>
        <div class="col-md-1">
            {!! ($data->mcore_unwizing ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="MCORE_Unwizing" class="col-md-2 control-label">MCORE aanwijzing</label>
        <div class="col-md-8">
            <label id="MCORE_Unwizing" class="custom-file px-file" for="MCORE_Unwizingi">
                <input type="file" id="MCORE_Unwizingi" class="custom-file-input" name="mcore_unwizing"  {{ $data->mcore_unwizing ?: 'required' }}>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            <a href="/download/storage+mcore_format.xlsx" target="_blank"><span class="label label-info pull-lg-right">Unduh Format</span></a>
        </div>
        <div class="col-md-1">
            {!! ($data->mcore_unwizing ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-10">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
            @if(!count($core))
                <span class="label label-danger label-pill">MCORE Plan Tidak Terbaca Sistem</span>
            @endif
            @if(!count($boq))
                <span class="label label-danger label-pill">BOQ Plan Tidak Terbaca Sistem</span>
            @endif
        </div>
    </div>
     
</form>

<div class="col-md-12">
    <div class="panel panel-danger">
      <div class="panel-heading">
        <span class="panel-title">ODP</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <div id="editable-table" class="table-primary table-responsive">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th rowspan="2">#</th>
                <th rowspan="2">ODP</th>
                <th rowspan="2">IP OLT</th>
                <th rowspan="2">PORT OLT</th>
                <th colspan="4">E-SIDE</th>
                <th colspan="4">O-SIDE</th>
                <th colspan="2">FEEDER</th>
                <th colspan="4">ODC</th>
                <th colspan="5">ODP</th>
                <th colspan="2">DIST</th>
                <th rowspan="2">ODP</th>
                <th rowspan="2">VALINS</th>
              </tr>
              <tr>
                <th>FTM</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>KAP</th>
                <th>FTM</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>KAP</th>
                <th>NAMA</th>
                <th>KAP</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>SPL</th>
                <th>KAP</th>
                <th>PANEL</th>
                <th>PORT</th>
                <th>SPL</th>
                <th>LAT</th>
                <th>LONG</th>
                <th>NAMA</th>
                <th>KAP</th>
              </tr>
            </thead>
            <tbody>
              @foreach($core as $no => $c)
              <tr>
                <td rowspan="2" class="valign-middle">{{ ++$no }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odp_Nama">{{ $c->Plan_Odp_Nama }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_IP_OLT">{{ $c->Plan_IP_OLT }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Olt_Port">{{ $c->Plan_Olt_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odf_Otb">{{ $c->Plan_Odf_Otb }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Panel">{{ $c->Plan_Otb_Panel }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Port">{{ $c->Plan_Otb_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Kap">{{ $c->Plan_Otb_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Odf_o">{{ $c->Plan_Otb_Odf_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Panel_o">{{ $c->Plan_Otb_Panel_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Port_o">{{ $c->Plan_Otb_Port_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Otb_Kap_o">{{ $c->Plan_Otb_Kap_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_FE_Nama">{{ $c->Plan_FE_Nama }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_FE_Kap">{{ $c->Plan_FE_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odc_Panel">{{ $c->Plan_Odc_Panel }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odc_Port">{{ $c->Plan_Odc_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odc_Spl">{{ $c->Plan_Odc_Spl }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odc_Kap">{{ $c->Plan_Odc_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odp_Panel">{{ $c->Plan_Odp_Panel }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odp_Port">{{ $c->Plan_Odp_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odp_Spl">{{ $c->Plan_Odp_Spl }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odp_LAT">{{ $c->Plan_Odp_LAT }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Odp_LON">{{ $c->Plan_Odp_LON }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Dist_Nama">{{ $c->Plan_Dist_Nama }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Dist_Kap">{{ $c->Plan_Dist_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Plan_Tenos">{{ $c->Plan_Tenos }}</td>
                <td></td>
              </tr>
              <tr>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odp_Nama">{{ $c->Real_Odp_Nama }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_IP_OLT">{{ $c->Real_IP_OLT }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Olt_Port">{{ $c->Real_Olt_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odf_Otb">{{ $c->Real_Odf_Otb }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Panel">{{ $c->Real_Otb_Panel }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Port">{{ $c->Real_Otb_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Kap">{{ $c->Real_Otb_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Odf_o">{{ $c->Real_Otb_Odf_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Panel_o">{{ $c->Real_Otb_Panel_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Port_o">{{ $c->Real_Otb_Port_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Otb_Kap_o">{{ $c->Real_Otb_Kap_o }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_FE_Nama">{{ $c->Real_FE_Nama }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_FE_Kap">{{ $c->Real_FE_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odc_Panel">{{ $c->Real_Odc_Panel }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odc_Port">{{ $c->Real_Odc_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odc_Spl">{{ $c->Real_Odc_Spl }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odc_Kap">{{ $c->Real_Odc_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odp_Panel">{{ $c->Real_Odp_Panel }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odp_Port">{{ $c->Real_Odp_Port }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odp_Spl">{{ $c->Real_Odp_Spl }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odp_LAT">{{ $c->Real_Odp_LAT }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Odp_LON">{{ $c->Real_Odp_LON }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Dist_Nama">{{ $c->Real_Dist_Nama }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Dist_Kap">{{ $c->Real_Dist_Kap }}</td>
                <td data-idcore="{{ $c->ID_Sys }}" data-kolom="Real_Tenos">{{ $c->Real_Tenos }}</td>
                <td></td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
    
  </div>
  <div class="col-md-12">
    <div class="panel panel-danger panel-dark">
      <div class="panel-heading">
        <span class="panel-title">BOQ</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <div class="table-primary">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Designator</th>
                <th>Plan</th>
                <th>Real</th>
              </tr>
            </thead>
            <tbody>
              @foreach($boq as $no => $b)
              <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $b->designator }}</td>
                <td>{{ $b->qty }}</td>
                <td>{{ $b->inst }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script>
  $(function() {
  	$('#form-unwizing').pxValidate();
    $('#BOQ_Unwizing').pxFile();
    $('#MCORE_Unwizing').pxFile();
    $('#btnNext').click(function(){
        $('#form-next').submit();
    });
    $('#editable-table').editableTableWidget({
      editor: $('<textarea class="form-control">')
    });
    $('#editable-table td').on('change', function(evt, newValue) {
      console.log(this.dataset)
      console.log(newValue)
      if(this.dataset.idcore && this.dataset.kolom){
        $.post("/manualsetmcore", { ID_Sys: this.dataset.idcore, kolom: this.dataset.kolom, value: newValue })
          .done(function( data ) {
            
          });
      }else{
        return false
      }
    });
  });
</script>
@endsection
