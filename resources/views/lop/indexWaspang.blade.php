@extends('layout')
@section('css')

@endsection
@section('heading')
<h1>Inbox Waspang</h1>
<a href="/listAanwijzing/{{ session('auth')->id_karyawan }}" class="pull-right"><span class="btn btn-info"> Aanwijzing List</span></a>
@endsection
@section('title', 'BTS || Inbox Waspang')
@section('content')
<ul class="nav nav-pills" id="tab-resize-pills">
    @foreach($tematik as $no => $t)
      <li><a href="#tab-{{ $no }}" data-toggle="tab">{{$t}}</a></li>
    @endforeach
</ul>
<div class="tab-content">
    @foreach($tematik as $no => $t)
      <div class="tab-pane" id="tab-{{ $no }}">
        <div class="widget-pricing widget-pricing-expanded">
            <div class="widget-pricing-inner row">
                @foreach($data as $no => $d)
                @if($d->nama == $t)
                <div class="col-md-4">
                    <!-- Counters -->
                    <div class="panel panel-info panel-dark widget-profile">
                      <div class="panel-heading">

                        <span class="label label-default pull-right">{{ $d->pid?:'BLM ADA PID' }}</span>
                        <h3 class="widget-profile-header">
                          {{ $d->nama }}
                          <a href="#" class="widget-profile-secondary-text">{{ $d->nama_lop }}</a>
                        </h3>
                        <i class="widget-profile-bg-icon font-size-52">{{ ++$no }}</i>
                      </div>

                      <div class="panel-footer">{{ $d->nama_mitra }}<span class="badge badge-warning pull-right">{{ $d->step }}</span></div>
                      <div class="panel-footer">
                        <span class="btn btn-sm btn-info detail" data-lopid="{{ $d->id_lop }}" data-lopnama="{{ $d->nama_lop }}"><i class="ion-information"></i>&nbsp;&nbsp;Detail</span>
                        @if(!in_array($d->step_id,[1,3,5,6,9]))
                            <a href="/perubahandesign/{{ $d->id_lop }}" class="btn btn-sm btn-success"><i class="ion ion-checkmark"></i>&nbsp;&nbsp;Perubahan Design</a>
                        @endif
                        @if($d->step_id==4)
                        <span class="btn btn-sm btn-primary plan" data-lopid="{{ $d->id_lop }}" data-lopnama="{{ $d->nama_lop }}"><i class="ion ion-plus pull"></i>&nbsp;&nbsp;Plan</span>
                        @endif
                        <span class="dropdown open pull-right">
                          <button type="button" class="btn btn-primary dropdown-toggle btn-sm" data-toggle="dropdown" aria-expanded="false">
                            Update
                          </button>
                          <div class="dropdown-menu">
                            <li><a href="/matdev/{{ $d->id_lop }}">Matdev</a></li>
                            <li><a href="/permit/{{ $d->id_lop }}">Permit</a></li>
                            @if($d->step_id==2)
                                <li><a href="/matdev/{{ $d->id_lop }}">Progress</a></li>
                            @else
                                <li><a href="/progress/{{ $d->id_lop }}">Progress</a></li>
                            @endif
                          </div>
                        </span>
                    </div>
                    </div>
                </div>
                @endif
                @endforeach
            </div>
        </div>
      </div>
    @endforeach
</div>

<div class="modal fade" id="modal-mtr" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title" id="myModalLabel">Laporan Material</h4>
            </div>
            <div class="modal-body" style="height:450px;">
                <input id="searchinput" class="form-control input-sm" type="search" placeholder="Search...">
                <ul class="form-group col-sm-12 col-xs-12" id="items" style="height:400px;">
                </ul>
            </div>
            <div class="modal-footer">
                <form action="/planwaspang" method="post" id="planwaspangform">
                    <input type="hidden" name="id" id="idlop">
                    <input type="hidden" name="item" id="planitem">
                    <button type="button" class="btn" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Plan</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modal-info">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title tittle">Modal title</h4>
      </div>
      <div class="modal-body" id="detilContent">
        <p>One fine body…</p>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script src="/bower_components/vue-2.5.17/dist/vue.min.js"></script>
<script type="text/javascript">
    $(function() {
        $("#planwaspangform").submit(function(e){

            var result=[];
            $('#items :input').serializeArray().forEach(function(item) {
                if(item.value)result.push({designator: item.name, plan: item.value});
            });
            $('#planitem').val(JSON.stringify(result));
            if(!result.length){
                alert('item harus diisi');
                e.preventDefault();
            }
        });
        $(".detail").click(function(e){
            console.log(e.target.dataset);
            var lopid = e.target.dataset.lopid;
            var lopnama = e.target.dataset.lopnama;
            $('.tittle').html('Detail LOP '+lopnama);
            var url = "/detillop/"+lopid;
            $.get(url, function(r){
                console.log(r);
                $("#detilContent").html(r);
                $("#modal-info").modal('show');
            });
        });
        // $(".plan").click(function(e){
        //     var lopid = e.target.dataset.lopid;
        //     var lopnama = e.target.dataset.lopnama;
        //     $('#myModalLabel').html('Plan Waspang '+lopnama);
        //     $('#idlop').val(lopid);
        //     var url = "/ajaxGetMtr/"+lopid;
        //     $.getJSON(url, function(r){
        //         console.log(r);
        //         $('#items').html('');
        //         $.each(r, function(index, value) {

        //           $('#items').append(
        //             '<li class="list-group-item">' +
        //             '<strong class="ini_item_bujang">' + value.id_barang + '</strong><div class="widget-products-footer input-group has-success" style="width:150px;float:right;"><button type="button" class="btn btn-success btn-outline pull-left" id="qButtonMinus" onclick="minus(this)">-</button><input class="form-control value" type="text" name="'+value.id_barang+'" style="width:80px;"><button type="button" class="btn btn-success btn-outline pull-right" onclick="add(this)" id="qButtonAdd">+</button></div>' +
        //             '<p>' + value.nama_barang + ' </p>' +
        //             '<p>BoQ Plan:' + value.avail + ' / Terpasang : ' + value.terpasang + ' / Sisa : ' + (value.avail-value.terpasang) + '</p>' +' </li>'
        //           );
        //         });
        //         $('#items').perfectScrollbar({
        //             suppressScrollX: true
        //         });

        //         $("#modal-mtr").modal('show');
        //     });

        // });
        add = function(obj) {
                  var count = $(obj).siblings('input').val();

                  $(obj).siblings('input').val(parseInt(++count))
                }

                minus = function(obj) {
                  var count = $(obj).siblings('input').val();

                  if (count > 0) {
                    $(obj).siblings('input').val(parseInt(count - 1))

                  }
                }

        $("#searchinput").keyup(function() {
            setTimeout(function(){
                var input, filter, ul, li, a, i;
                input = document.getElementById("searchinput");
                filter = input.value.toUpperCase();
                ul = document.getElementById("items");
                li = ul.getElementsByTagName("li");
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByClassName("ini_item_bujang")[0];
                    console.log(a.innerHTML);
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";

                    }
                }
            }, 1000);
        });
        $('#tab-resize-pills').pxTabResize();
    });
</script>
@endsection