@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || QC')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>QC <i>{{ $data->nama_lop }}</i>
</h1>
@endsection
@section('content')
  <div class="row">
  <div class="col-md-10">
    <div class="panel">
      <div class="panel-heading">
        <span class="panel-title">Form QC</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <form class="form-horizontal" method="post" id="form-qc" autocomplete="off" action="/qc/{{ Request::segment(2) }}">
            <div class="form-group form-message-dark">
                <label for="status_qc" class="col-md-2 control-label">Status QC</label>
                <div class="col-md-10">
                    <input type="text" name="status_qc" id="status_qc" class="form-control" required/>
                </div>
            </div>
            <div class="form-group form-message-dark">
                <label for="catatan" class="col-md-2 control-label">Catatan</label>
                <div class="col-md-10">
                    <textarea name="catatan" id="catatan" class="form-control" required></textarea>
                </div>
            </div>
            <div class="form-group form-message-dark field_ba">
                <label for="ba_cti" class="col-md-2 control-label">BA CT</label>
                <div class="col-md-8">
                    <label id="BA_CT" class="custom-file px-file" for="ba_cti">
                        <input type="file" id="ba_cti" class="custom-file-input" name="ba_ct" {{ $data->ba_ct?'':'required' }}>
                        <span class="custom-file-control form-control">Choose file...</span>
                        <div class="px-file-buttons">
                            <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                            <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                        </div>
                    </label>
                </div>
                <div class="col-md-2">
                    {!! ($data->ba_ct ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
                </div>
            </div>
            <div class="form-group form-message-dark field_ba">
                <label for="ba_uti" class="col-md-2 control-label">BA UT</label>
                <div class="col-md-8">
                    <label id="BA_UT" class="custom-file px-file" for="ba_uti">
                        <input type="file" id="ba_uti" class="custom-file-input" name="ba_ut" {{ $data->ba_ut?'':'required' }}>
                        <span class="custom-file-control form-control">Choose file...</span>
                        <div class="px-file-buttons">
                            <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                            <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                        </div>
                    </label>
                </div>
                <div class="col-md-2">
                    {!! ($data->ba_ut ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-9">
                    <button type="submit" class="btn btn-info"><i class="ion-soup-can"></i> Simpan</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="panel {{ isset($data->abd,$data->otdr,$data->otdr,$data->kml_instalasi,$data->mcore_instalasi,$data->boq_instalasi)?'panel-primary':'panel-warning' }} panel-dark">
      <div class="panel-heading">
        <span class="panel-title">Files</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->abd }}" target="_blank">
            <button type="button" class="btn {{ $data->abd?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">ABD</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->otdr }}">
            <button type="button" class="btn {{ $data->otdr?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">OTDR</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->qrcode }}">
            <button type="button" class="btn {{ $data->qrcode?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">QR CODE</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->kml_instalasi }}">
            <button type="button" class="btn {{ $data->kml_instalasi?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">KML INSTALASI</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->mcore_instalasi }}">
            <button type="button" class="btn {{ $data->mcore_instalasi?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">M.CORE INSTALASI</button>
        </a>
        <a href="{{ '/storage/'.$data->id.'/instalasifile/'.$data->boq_instalasi }}">
            <button type="button" class="btn {{ $data->boq_instalasi?'btn-primary':'btn-danger' }} btn-outline btn-rounded m-b-1">BOQ INSTALASI</button>
        </a>
      </div>
    </div>
  </div>
</div>
<div class="panel">
  <div class="panel-heading">
    <span class="panel-title">Photo ZIP, format harus <code>.ZIP</code></span>
  </div>
  <div class="panel-body">
    
      @foreach($fotorar as $f)
        <div class="panel panel-info panel-body-colorful">
          <div class="panel-heading">
            <span class="panel-title">{{ str_replace(public_path()."/storage/".Request::segment(2)."/fotorar/",'',$f['url']) }}</span>
            <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
          </div>
          <div class="panel-body">
            <div class="row text-center input-photos">
            @foreach($f['name'] as $no => $input)
            <div class="col-xs-2 col-sm-2">
                <?php
                $path = str_replace(public_path(),'',$f['url']).'/'.$input;
                ?>
                @if (file_exists(public_path().$path))
                <a href="{{ $path }}">
                    <img src="{{ $path }}" alt="{{ $input }}" />
                </a>
                @else
                <img src="/image/placeholder.gif" alt="" />
                @endif
                <br />
                <p>{{ $input }}</p>
            </div>
            @endforeach
        </div>
          </div>
        </div>
      @endforeach
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-info panel-dark">
      <div class="panel-heading">
        <span class="panel-title">Foto Evident</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
        @if(count($core))
            <div class="table-responsive table-primary">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Data</th>
                            <th>Evident</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($core as $no => $d)
                        <?php
                        $namaodc = str_replace('ODP', 'ODC', explode('/', $d->Real_Odp_Nama)[0]);
                        $foto = [
                            $d->Real_Olt_Port?str_replace('/','_',str_replace(' ','_',$d->Real_Olt_Port)):'NoData_OLT',
                            isset($d->Real_Odf_Otb,$d->Real_Otb_Panel,$d->Real_Otb_Port)?'ODF_E_'.$d->Real_Odf_Otb.'-'.$d->Real_Otb_Panel.'-'.$d->Real_Otb_Port:'NoData_OTB_E',
                            isset($d->Real_Otb_Odf_o,$d->Real_Otb_Panel_o,$d->Real_Otb_Port_o)?'ODF_O_'.$d->Real_Otb_Odf_o.'-'.$d->Real_Otb_Panel_o.'-'.$d->Real_Otb_Port_o:'NoData_OTB_O',
                            $namaodc?$namaodc:'NoData_ODC_NAMA',
                            isset($d->Real_Odc_Panel,$d->Real_Odc_Port)?'ODC_'.$d->Real_Odc_Panel.'-'.$d->Real_Odc_Port:'NoData_ODC_PANEL-PORT',
                            isset($d->Real_Odp_Panel,$d->Real_Odp_Port)?'ODC_'.$d->Real_Odp_Panel.'-'.$d->Real_Odp_Port:'NoData_ODC_PANEL-PORT'
                        ];

                        for ($i = 1; $i <= 8; $i++){
                            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_OPM_'.$i:'NoData_ODP_NAMA';
                        }
                        $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).' OPM_IN_1':'NoData_ODP_NAMA';
                        $qrcode = ['ODP', 'TIANG', 'SPL'];
                        foreach ($qrcode as $q) {
                            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_QRCODE_'.$q:'NoData_ODP_NAMA';
                        }
                        $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_KANAN':'NoData_ODP_NAMA';
                        $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_KIRI':'NoData_ODP_NAMA';
                        $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_TENGAH':'NoData_ODP_NAMA';
                        $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_LABEL_ODP':'NoData_ODP_NAMA';
                        $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_FOTO_AKSESORIS':'NoData_ODP_NAMA';
                        ?>
                        <tr>
                            <td>{{ $d->Real_Odp_Nama?:$d->Plan_Odp_Nama }}</td>
                            @if($d->Real_Odp_Nama)
                            <td>
                                <div class="row text-center input-photos">
                                    @foreach($foto as $no => $input)
                                    <div class="col-xs-2 col-sm-2">
                                        <?php
                                            $path = "/storage/".Request::segment(2)."/instalasifile/evident/photo-".$input;
                                            $th   = $path."-th.jpg";
                                            $img  = $path.".jpg";
                                        ?>
                                        @if (file_exists(public_path().$th))
                                            <a href="{{ $img }}">
                                                <img src="{{ $th }}" alt="{{ $input }}" />
                                            </a>
                                        @else
                                            <img src="/image/placeholder.gif" alt="" />
                                        @endif
                                        <br />
                                        <p>{{ $input }}</p>
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                            @else
                            <td></td>
                            @endif
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @else
            <span class="label label-danger">Perlu Upload file MCORE Instalasi terlebih dahulu</span>
            @endif
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script>
    $('#form-qc').pxValidate();
    $('#BA_CT').pxFile();
    $('#BA_UT').pxFile();
    $('#status_qc').select2({
        placeholder:"Select Status QC",
        data:[{id:"QC_OK", text:"QC OK"},{id:"QC_NOK", text:"QC NOK"}],
        dropdownCssClass : 'no-search',
    }).change(function() {
        if($(this).val() == 'QC_OK'){
            $("#ba_ut").rules("add", {
                required: true
            });
            $("#ba_ct").rules("add", {
                required: true
            });
            $(".field_ba").show();
        }else{
            $("#ba_ut").rules("remove");
            $("#ba_ct").rules("remove");
            $(".field_ba").hide();
        }
    });
</script>
@endsection
