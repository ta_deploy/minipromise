@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || Perizinan')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Perizinan <i>{{ $data->nama_lop }}</i>
</h1>
@endsection
@section('content')
<div class="wizard wizard-bordered" id="wizard-bordered">
  <div class="wizard-wrapper">
    <ul class="wizard-steps">
      <li data-target="#wizard-bordered-step1">
        <span class="wizard-step-number">1</span>
        <span class="wizard-step-complete"><i class="ion ion-checkmark-circled text-success"></i></span>
        <span class="wizard-step-caption">Create Pengajuan</span>
      </li>
      <li data-target="#wizard-bordered-step2" class="active">
        <span class="wizard-step-number">2</span>
        <span class="wizard-step-complete"><i class="ion ion-checkmark-circled text-success"></i></span>
        <span class="wizard-step-caption">Proses Perizinan</span>
      </li>
      <li data-target="#wizard-bordered-step3">
        <span class="wizard-step-number">3</span>
        <span class="wizard-step-complete"><i class="ion ion-checkmark-circled text-success"></i></span>
        <span class="wizard-step-caption">Waiting Approval</span>
      </li>
      <li data-target="#wizard-bordered-step4">
        <span class="wizard-step-number">4</span>
        <span class="wizard-step-complete"><i class="ion ion-checkmark-circled text-success"></i></span>
        <span class="wizard-step-caption">Approve</span>
      </li>
    </ul>
  </div>
  <div class="wizard-content">
    <div class="wizard-pane" id="wizard-bordered-step1">
        <form class="form-horizontal" method="post" id="form-qc" autocomplete="off" action="/perizinanupdate/{{ Request::segment(2) }}/1">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group form-message-dark">
                    <label for="tgl" class="col-sm-3 control-label">Tanggal</label>
                    <div class="col-sm-9">
                        <input type="text" name="tgl" id="tgl" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group form-message-dark">
                    <label for="type_perizinan" class="col-sm-4 control-label">Type Perizinan</label>
                    <div class="col-sm-8">
                        <input type="text" name="type_perizinan" id="type_perizinan" class="form-control" required/>
                    </div>
                </div>
                <!-- <div class="col-sm-6 form-group form-message-dark field_ba">
                    <label for="ba_cti" class="col-sm-2 control-label">BA</label>
                    <div class="col-sm-10">
                        <label id="BA_CT" class="custom-file px-file" for="ba_cti">
                            <input type="file" id="ba_cti" class="custom-file-input" name="ba_ct" {{ $data->ba_ct?'':'required' }}>
                            <span class="custom-file-control form-control">Choose file...</span>
                            <div class="px-file-buttons">
                                <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                                <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                            </div>
                        </label>
                    </div>
                </div> -->
              </div>
              <div class="col-sm-6">
                <div class="form-group form-message-dark">
                <label for="catatan" class="col-sm-2 control-label" >Catatan</label>
                <div class="col-sm-10">
                    <textarea name="catatan" id="catatan" class="form-control" rows="3" required></textarea>
                </div>
            </div>
            
              </div>
            </div>
        
            @if($form->status>1)
                <button type="button" class="btn btn-primary" data-wizard-action="next">Next</button>
            @endif
            <button type="submit" class="btn btn-info pull-right"><i class="ion-soup-can"></i> {{ Request::segment(3)=='input'?'Simpan':'Update' }}</button>
      </form>
    </div>
    <div class="wizard-pane" id="wizard-bordered-step2">
        <form class="form-horizontal" method="post" id="form-qc" autocomplete="off" action="/perizinanupdate/{{ Request::segment(2) }}/2">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group form-message-dark">
                    <label for="tgl_proses" class="col-sm-3 control-label">Tanggal Proses</label>
                    <div class="col-sm-9">
                        <input type="text" name="tgl_proses" id="tgl_proses" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group form-message-dark">
                    <label for="nilai" class="col-sm-3 control-label">Nilai</label>
                    <div class="col-sm-9">
                        <input type="text" name="nilai" id="nilai" class="form-control" required/>
                    </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group form-message-dark">
                <label for="catatan" class="col-sm-2 control-label" >Catatan</label>
                <div class="col-sm-10">
                    <textarea name="catatan" id="catatan" class="form-control" rows="3" required></textarea>
                </div>
            </div>
            
              </div>
            </div>
        
          <button type="button" class="btn" data-wizard-action="prev">Prev</button>
          @if($form->status>2)
            <button type="button" class="btn btn-primary" data-wizard-action="next">Next</button>
          @endif
          <button type="submit" class="btn btn-info pull-right"><i class="ion-soup-can"></i> {{ Request::segment(3)=='input'?'Simpan':'Update' }}</button>
      </form>
    </div>
    <div class="wizard-pane" id="wizard-bordered-step3">
        <form class="form-horizontal" method="post" id="form-qc" autocomplete="off" action="/perizinanupdate/{{ Request::segment(2) }}/3">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group form-message-dark">
                    <label for="tgl_approve" class="col-sm-3 control-label">Tanggal Approve</label>
                    <div class="col-sm-9">
                        <input type="text" name="tgl_approve" id="tgl_approve" class="form-control" required/>
                    </div>
                </div>
               
              </div>
              <div class="col-sm-6">
                <div class="form-group form-message-dark">
                <label for="catatan" class="col-sm-2 control-label" >Catatan</label>
                <div class="col-sm-10">
                    <textarea name="catatan" id="catatan" class="form-control" rows="3" required></textarea>
                </div>
            </div>
            
              </div>
            </div>
        
          <button type="button" class="btn" data-wizard-action="prev">Prev</button>
          @if($form->status>3)
            <button type="button" class="btn btn-primary" data-wizard-action="next">Next</button>
          @endif
          <button type="submit" class="btn btn-info pull-right"><i class="ion-soup-can"></i> {{ Request::segment(3)=='input'?'Simpan':'Update' }}</button>
        </form>
    </div>
    <div class="wizard-pane" id="wizard-bordered-step4">
        <form class="form-horizontal" method="post" id="form-qc" autocomplete="off" action="/perizinanupdate/{{ Request::segment(2) }}/4">
        
          <button type="button" class="btn" data-wizard-action="prev">Prev</button>
          <button type="submit" class="btn btn-xl btn-primary btn-rounded pull-right"><i class="ion ion-checkmark-circled"></i> GO ON TRACK</button>
        </form>
    </div>
  </div>
</div>


<div class="panel">
  <div class="panel-body">
    <form action="/upload" id="dropzonejs" class="dropzone-box">
  <div class="dz-default dz-message">
    <div class="dz-upload-icon"></div>
    Upload Eviden in here<br>
    <span class="dz-text-small">or click to pick manually</span>
  </div>
  <div class="fallback">
    <input name="file" type="file" multiple>
  </div>
</form>
  </div>
</div>
<div class="table-light">
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>#</th>
        <th>Type</th>
        <th>Status</th>
        <th>Nilai</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
        @foreach($list as $no => $l)
          <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $l->tipe_izin }}</td>
            <td>{{ $l->status }}</td>
            <td>{{ $l->nilai }}</td>
            <td>Detil</td>
          </tr>
        @endforeach
    </tbody>
  </table>
  @if(!count($list))
  <div class="table-footer">
    Data Masih Kosong.
  </div>
  @endif
</div>
@endsection

@section('js')
<script>
    $('#form-qc').pxValidate();
    $('#BA_CT').pxFile();
    $('#BA_UT').pxFile();
    $('#type_perizinan').select2({
        placeholder:"Select Type Perizinan",
        data:[{id:"PU", text:"PU"},{id:"COMMCASE", text:"COMMCASE"}],
        dropdownCssClass : 'no-search',
    }).change(function() {
        if($(this).val() == 'SUDAH_APPROVE'){
            $("#ba_ut").rules("add", {
                required: true
            });
            $(".field_ba").show();
        }else{
            $("#ba_ct").rules("remove");
            $(".field_ba").hide();
        }
    });
    $('#tgl, #tgl_proses, #tgl_approve').datepicker({
        autoclose:true,
        format: 'yyyy-mm-dd',
        orientation: "bottom"
    });
</script>
<script>
  $(function() {
    $('#wizard-bordered').pxWizard();
  });
</script><script>
  $(function() {
    $('#dropzonejs').dropzone({
      parallelUploads: 2,
      maxFilesize:     50000,
      filesizeBase:    1000,

      resize: function(file) {
        return {
          srcX:      0,
          srcY:      0,
          srcWidth:  file.width,
          srcHeight: file.height,
          trgWidth:  file.width,
          trgHeight: file.height,
        };
      },
    });


    // Mock the file upload progress (only for the demo)
    //
    Dropzone.prototype.uploadFiles = function(files) {
      var minSteps         = 6;
      var maxSteps         = 60;
      var timeBetweenSteps = 100;
      var bytesPerStep     = 100000;
      var isUploadSuccess  = Math.round(Math.random());

      var self = this;

      for (var i = 0; i < files.length; i++) {

        var file = files[i];
        var totalSteps = Math.round(Math.min(maxSteps, Math.max(minSteps, file.size / bytesPerStep)));

        for (var step = 0; step < totalSteps; step++) {
          var duration = timeBetweenSteps * (step + 1);

          setTimeout(function(file, totalSteps, step) {
            return function() {
              file.upload = {
                progress: 100 * (step + 1) / totalSteps,
                total: file.size,
                bytesSent: (step + 1) * file.size / totalSteps
              };

              self.emit('uploadprogress', file, file.upload.progress, file.upload.bytesSent);
              if (file.upload.progress == 100) {

                if (isUploadSuccess) {
                  file.status =  Dropzone.SUCCESS;
                  self.emit('success', file, 'success', null);
                } else {
                  file.status =  Dropzone.ERROR;
                  self.emit('error', file, 'Some upload error', null);
                }

                self.emit('complete', file);
                self.processQueue();
              }
            };
          }(file, totalSteps, step), duration);
        }
      }
    };
  });
</script>
@endsection
