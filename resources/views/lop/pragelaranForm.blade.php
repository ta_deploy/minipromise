@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Pragelaran
</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" id="formPragelaran" action="/savePragelaran/{{ Request::segment(2) }}" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="status" class="col-md-3 control-label">Status Pragelaran</label>
        <div class="col-md-9">
            <input type="text" name="status" id="status" class="form-control" required/>
        </div>
    </div>
    <div class="row text-center col-md-offset-3 input-photos">
        <?php
        $foto = ["Cat_Tiang", "Slack_Kabel"];
        ?>

        @foreach($foto as $input)
        <div class="col-xs-6 col-sm-3">
            <?php
            $path = "/storage/".Request::segment(2)."/$input";
            $th   = "$path-th.jpg";
            $img  = "$path.jpg";
            $flag = "";
            $name = "flag_".$input;
            ?>
            @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
                <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
            <?php
            $flag = 1;
            ?>
            @else
            <img src="/image/placeholder.gif" alt="" />
            @endif
            <br />
            <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
            <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
            <button type="button" class="btn btn-sm btn-info">
                <i class="ion-camera"></i>
            </button>
            <p>{{ str_replace('_',' ',$input) }}</p>
            {!! $errors->first($name, '<span class="label label-danger">:message</span>') !!}
        </div>
        @endforeach
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $('#formPragelaran').pxValidate();
    $('#status').select2({
        placeholder:"Select status",
        data:[{id:"Pragelaran_Selesai", text:"Pragelaran Selesai"},{id:"Pragelaran_Lanjut_Besok", text:"Pragelaran Lanjut Besok"}],
        dropdownCssClass : 'no-search',
    }).change(function() {
        $(this).valid();
    });
</script>
@endsection
