@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || CT&UT')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>CT&UT <i>{{ $data->nama_lop }}</i>
</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" id="formDelivery" autocomplete="off" action="/ctut/{{ Request::segment(2) }}">
<div class="row">
  <div class="col-md-10">
    <div class="panel panel-info">
      <div class="panel-heading">
        <span class="panel-title">Form</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
            <div class="form-group form-message-dark">
                <label for="catatan" class="col-md-3 control-label">Catatan</label>
                <div class="col-md-9">
                    <textarea name="catatan" id="catatan" class="form-control" required></textarea>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-3 col-md-9">
                    <button type="submit" class="btn btn-info pull-right"><i class="ion-soup-can"></i> Simpan</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <div class="panel panel-info panel-dark">
      <div class="panel-heading">
        <span class="panel-title">Photo</span>
        <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
      </div>
      <div class="panel-body">
         <div class="col-xs-2 col-sm-2 input-photos">
            <?php
            $input="BUKTI_SETOR_BERKAS";
            $path = "/storage/".Request::segment(2)."/instalasifile/photo-".$input;
            $th   = $path."-th.jpg";
            $img  = $path.".jpg";
            $flag = "";
            $name = "flag_".$input;
            ?>
            @if (file_exists(public_path().$th))
            <a href="{{ $img }}">
                <img src="{{ $th }}" alt="{{ $input }}" />
            </a>
            <?php
            $flag = 1;
            ?>
            @else
            <img src="/image/placeholder.gif" alt="" />
            @endif
            <br />
            <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
            <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
            <button type="button" class="btn btn-sm btn-info">
                <i class="ion-camera"></i>
            </button>
            <p>{{ $input }}</p>
        </div>
      </div>
    </div>
  </div>
</div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        $('input[type=file]').change(function() {
            console.log(this.name);
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                $(inputEl).parent().find('input[type=text]').val(1);
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);

                }
                reader.readAsDataURL(inputEl.files[0]);
            }
        });
        $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();
        });
    });
</script>
@endsection
