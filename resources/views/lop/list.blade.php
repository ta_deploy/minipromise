@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    body .modal-xl {
        width: 1250px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>List
</h1>
@endsection
@section('title', 'List LOP')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
      <table class="table" id="datatables">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama LOP</th>
                <th>PID</th>
                <th>Waspang</th>
                <th>Tanggal Registrasi</th>
                <th>Loker</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
                <tr class="{{$d->DROPLOP?'text-danger':''}}">
                    <td>{{ ++$no }}</td>
                    <td>{{ $d->nama_lop }}</td>
                    <td>{{ $d->pid }}</td>
                    <td>{{ $d->waspang_nm }}</td>
                    <td>{{ $d->created_at }}</td>
                    <td>
                        <span class="label label-success m-b-1">{{ $d->step }}</span>
                    </td>
                    <td class="text-right">
                        <a href="#" class="btn btn-xs btn-info detail" data-lopid="{{ $d->id_lop }}" data-lopnama="{{ $d->nama_lop }}"><i class="ion-information"></i>&nbsp;&nbsp;Detail</a>
                            <a href="/progress/{{ $d->id_lop }}" class="btn btn-xs btn-info detail"><i class="ion-update"></i>&nbsp;&nbsp;Update</a>
                        
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
</div>
</div>

<div class="modal" id="modal-info">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title tittle">Modal title</h4>
      </div>
      <div class="modal-body" id="detilContent">
        <p>One fine body…</p>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $(".detail").click(function(e){
            console.log(e.target.dataset);
            var lopid = e.target.dataset.lopid;
            var lopnama = e.target.dataset.lopnama;
            $('.tittle').html('Detail LOP '+lopnama);
            var url = "/detillop/"+lopid;
            $.get(url, function(r){
                console.log(r);
                $("#detilContent").html(r);
                $("#modal-info").modal('show');
            });
        });
    });
</script>
@endsection