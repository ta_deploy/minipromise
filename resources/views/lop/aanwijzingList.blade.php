@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('title', 'BTS || Aanwijzing')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Aanwijzing </span> </h1>
@endsection
@section('title', 'List Aanwijzing')
@section('content')
@if(count($data))
<div class="row text-center">
    @foreach($data as $c)
        <?php
        $str = '';
        foreach($c['anak'] as $m){
            $str .= "<b>".$m->nama_lop."</b> - <a href='/aanwijzing/".$m->mpromise_project_id."/".$m->waspang."/".$c['parent']->mitra."'>Update</a><br/>";
        }
        ?>
      <div class="col-md-2" tabindex="0" data-placement="bottom"
       class="btn btn-lg btn-primary" 
       role="button" 
       data-html="true" 
       data-toggle="popover" 
       data-trigger="focus" 
       title="{{ $c['parent']->label }}" 
       data-content="<div>{{ $str }}</div>">
        <div class="box bg-info">
          <div class="box-cell p-x-3 p-y-1">
            <div class="font-weight-semibold font-size-12">{{ $c['parent']->label }}</div>
            <div class="font-weight-bold font-size-20">{{ $c['parent']->Baris }}</div>
          </div>
        </div>
      </div>
    @endforeach
</div>
@endif
@endsection

@section('js')
<script type="text/javascript">
    $('[data-toggle="popover"]').popover();
</script>
@endsection
