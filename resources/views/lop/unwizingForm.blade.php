@extends('layout')
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>aanwijzing

</h1>
<button class="btn btn-primary" type="submit" form="formSelesaiAanwijzing" value="Submit">Selesai Aanwijzing</button>
<a href="/listAanwijzing/{{ session('auth')->id_karyawan }}" class="pull-right"><span class="btn btn-info"> Aanwijzing List</span></a>
@endsection
@section('title', 'BTS || Aanwijzing')
@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-success">
      <div class="panel-body">
        <form class="form-horizontal" method="post" id="form-unwizing" enctype="multipart/form-data" action="/selesaiaanwijzing/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}">
            <div class="form-group form-message-dark">
                <label for="KML_Unwizing" class="col-md-3 control-label">KML Aanwijzing</label>
                <div class="col-md-9">
                    <label id="KML_Unwizing" class="custom-file px-file" for="KML_Unwizingi">
                        <input type="file" id="KML_Unwizingi" class="custom-file-input" name="kml_unwizing">
                        <span class="custom-file-control form-control">Choose file...</span>
                        <div class="px-file-buttons">
                            <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                            <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                        </div>
                    </label>
                </div>
                <div class="col-md-1">
                    
                </div>
            </div>
            <div class="form-group form-message-dark">
                <label for="MCORE_Unwizing" class="col-md-3 control-label">MCORE Aanwijzing</label>
                <div class="col-md-7">
                    <label id="MCORE_Unwizing" class="custom-file px-file" for="MCORE_Unwizingi">
                        <input type="file" id="MCORE_Unwizingi" class="custom-file-input" name="mcore_unwizing">
                        <span class="custom-file-control form-control">Choose file...</span>
                        <div class="px-file-buttons">
                            <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                            <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                        </div>
                    </label>
                </div>
                <div class="col-md-2">
                    <a href="/download/storage+mcore_format.xlsx" target="_blank"><span class="label label-info pull-xl-right">Unduh Format</span></a>
                </div>
                
            </div>
            <div class="form-group form-message-dark">
                <label for="APD_Unwizing" class="col-md-3 control-label">APD Aanwijzing</label>
                <div class="col-md-7">
                    <label id="APD_Unwizing" class="custom-file px-file" for="MCORE_Ondeski">
                        <input type="file" id="APD_Unwizingi" class="custom-file-input" name="apd_unwizing">
                        <span class="custom-file-control form-control">Choose file...</span>
                        <div class="px-file-buttons">
                            <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                            <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                        </div>
                    </label>
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn btn-sm btn-primary pull-right">Simpan</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel panel-success">
      <div class="panel-body">
        <div class="table-light">
          <div class="table-header">
            <div class="table-caption">
              List LOP
            </div>
          </div>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>#</th>
                <th>Nama LOP</th>
                <th>Tematik</th>
                <th>Mitra</th>
                <th>Waspang</th>
              </tr>
            </thead>
            <tbody>
            @foreach($data as $no => $d)
              <tr>
                <td>1</td>
                <td>{{ $d->nama_lop }}</td>
                <td>{{ $d->nama_project }}</td>
                <td>{{ $d->nama_mitra }}</td>
                <td>{{ $d->waspang_nm }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<form method="post" action="/selesaiaanwijzing/{{ Request::segment(2) }}/{{ Request::segment(3) }}/{{ Request::segment(4) }}" id="formSelesaiAanwijzing">
    
</form>
@endsection

@section('js')
<script>
  $(function() {
  	$('#form-unwizing').pxValidate();
    $('#BOQ_Unwizing').pxFile();
    $('#KML_Unwizing').pxFile();
    $('#MCORE_Unwizing').pxFile();
    $('#APD_Unwizing').pxFile();
    $('#LIST_ODP_Unwizing').pxFile();
    var mitra = <?= json_encode($mitra); ?>;
    $('#mitra').select2({
        data:mitra,
        placeholder:"Pilih Mitra"
    });

    $('#btnNext').click(function(){
        $('#form-next').submit();
    });
    $('#boq_unwizing_exist').select2({
        placeholder:"Ketik Nama Project",
        allowClear: true,
        minimumInputLength: 5,
        ajax: {
            url: "/find_boq_unw",
            dataType: 'json',
            data: function (params) {
                return {
                    q: $.trim(params.term)
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            },
            cache: true
        }
    });
});
</script>
@endsection
