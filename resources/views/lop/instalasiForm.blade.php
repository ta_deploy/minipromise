@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
<link href="/css/progress-wizard.min.css" rel="stylesheet" type="text/css">
@endsection
@section('heading')
<h1>
    <a href="/" class="ion-arrow-left-a"></a> {{ $data->nama_lop }}/Instalasi (Harian/ {{ date('Y-m-d') }})
</h1>
@endsection
@section('title', 'BTS || Instalasi')
@section('content')
<ul class="progress-indicator" style="margin-bottom:20px">
    <li class="{{ str_contains($data->progres_instalasi, ['Tanam_Tiang_Selesai']) ? 'completed':'' }}">
        <span class="bubble"></span>
        <span>Tanam Tiang</span>
    </li>
    <li class="{{ str_contains($data->progres_instalasi, ['Gelar_Kabel_Selesai']) ? 'completed':'' }}">
        <span class="bubble"></span>
        <span>Gelar Kabel</span>
    </li>
    <li class="{{ str_contains($data->progres_instalasi, ['Pasang_Terminal_Selesai']) ? 'completed':'' }}">
        <span class="bubble"></span>
        <span>Pasang Terminal</span>
    </li>
    <li class="{{ str_contains($data->progres_instalasi, ['Terminasi_Selesai']) ? 'completed':'' }}">
        <span class="bubble"></span>
        <span>Terminasi</span>
    </li>
</ul>
<div class="row">
    <div class="col-sm-6">
        <div class="panel panel-info panel-dark">
          <div class="panel-body">
            <form class="form-horizontal" method="post" id="formInstalasi" action="/saveInstalasi/{{ Request::segment(2) }}" enctype="multipart/form-data">
                <input type="hidden" name="item" value="[]" />
                <input type="hidden" name="itemplan" value="{{ json_encode($mtr) }}">
                <div class="form-group form-message-dark">
                    <label for="status" class="col-md-2 control-label">Status</label>
                    <div class="col-md-9">
                        <input type="text" name="status" id="status" class="form-control" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Progress Instalasi</label>
                    <div class="col-md-2">
                        <label class="custom-control custom-checkbox" for="grid-input-10">
                            <input type="checkbox" name="status2[]" id="grid-input-10" class="custom-control-input Tanam_Tiang" value="Tanam_Tiang_Selesai" {{ str_contains($data->progres_instalasi, ['Tanam_Tiang_Selesai']) ? 'checked':''  }}>
                            <span class="custom-control-indicator"></span>
                            Tanam Tiang Selesai
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label class="custom-control custom-checkbox" for="grid-input-11">
                            <input type="checkbox" name="status2[]" id="grid-input-11" class="custom-control-input Gelar_Kabel" value="Gelar_Kabel_Selesai" {{ str_contains($data->progres_instalasi, ['Gelar_Kabel_Selesai']) ? 'checked':''  }}>
                            <span class="custom-control-indicator"></span>
                            Gelar Kabel Selesai
                        </label>
                    </div>
                    <div class="col-md-3">
                        <label class="custom-control custom-checkbox" for="grid-input-12">
                            <input type="checkbox" name="status2[]" id="grid-input-12" class="custom-control-input Pasang_Terminal" value="Pasang_Terminal_Selesai" {{ str_contains($data->progres_instalasi, ['Pasang_Terminal_Selesai']) ? 'checked':''  }}>
                            <span class="custom-control-indicator"></span>
                            Pasang Terminal (ODP/ODC) Selesai
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label class="custom-control custom-checkbox" for="grid-input-13">
                            <input type="checkbox" name="status2[]" id="grid-input-13" class="custom-control-input Terminasi" value="Terminasi_Selesai" {{ str_contains($data->progres_instalasi, ['Terminasi_Selesai']) ? 'checked':''  }}>
                            <span class="custom-control-indicator"></span>
                            Terminasi Selesai
                        </label>
                    </div>
                </div>

                <div class="form-group form-message-dark">
                    <label for="catatan" class="col-md-2 control-label">Catatan</label>
                    <div class="col-md-9">
                        <textarea name="catatan" id="catatan" class="form-control" required></textarea>
                    </div>
                </div>

                <div class="form-group form-message-dark nilai_lop">
                    <label for="catatan" class="col-md-2 control-label">Nilai LOP</label>
                    <div class="col-md-9">
                        <input type="text" name="NILAI_LOP" id="nilai_lop" value="{{ $data->NILAI_LOP }}" class="form-control"/>
                    </div>
                </div>
                <div class="form-group form-message-dark">
                    <label for="catatan" class="col-md-2 control-label">&nbsp;</label>
                    <?php
                        $foto = ["Tanam_Tiang","Gelar_Kabel","Pasang_Terminal","Terminasi"];
                    ?>
                    <div class="col-md-9">
                     @foreach($foto as $f)
                     <div class="col-xs-3 col-sm-3 input-photos text-center {{$f}}">
                        <?php
                        $input=$f;
                        $path = "/storage/".Request::segment(2)."/instalasifile/evident/".$input;
                        $th   = $path."-th.jpg";
                        $img  = $path.".jpg";
                        $flag = "";
                        $name = "flag_".$input;
                        ?>
                        @if (file_exists(public_path().$th))
                        <a href="{{ $img }}" target="_blank">
                            <img src="{{ $th }}" alt="{{ $input }}" />
                        </a>
                        <?php
                        $flag = 1;
                        ?>
                        @else
                        <img src="/image/placeholder.gif" alt="" />
                        @endif
                        <br />
                        <input type="hidden" style="opacity: 0;pointer-events: none;" size="6" class="text pull-left" name="flag_{{ $input }}" value="{{ $flag }}"/>
                        <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
                        <button type="button" class="btn btn-sm btn-info m-t-1">
                            <i class="ion-camera"></i>
                        </button>
                        <p>Foto {{ str_replace("_"," ",$input)  }}</p>
                    </div>
                    @endforeach
                </div>
                </div>
                <!-- <div class="row">
                    <div class="col-md-offset-2 col-md-9">
                        <ul id="Itemlist" class="list-group">
                            <li class="list-group-item" v-for="item in items" v-if="item.qty">
                                <span class="badge" v-text="item.qty"></span>
                                <strong v-text="item.id_barang"></strong>
                                <p v-text="item.nama_barang"></p>
                            </li>
                        </ul>
                    </div>
                </div> -->
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-9">
                        <!-- <button type="button" class="btn btn-info material_chk" data-toggle="modal" data-target="#modal-mtr"><i class="ion-clipboard"></i> Pilih Material</button> -->
                        <button type="submit" class="btn btn-success pull-right"><i class="ion-soup-can"></i> Simpan Laporan</button>
                        @if(!count($mtrcount))
                        <span class="label label-danger">Belum input Material instalasi</span>
                        @endif
                    </div>
                </div>
            </form>
          </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-info panel-dark">
          <div class="panel-body">
            <div class="table-primary">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Designator</th>
                    <th>MoS</th>
                    <th>Real</th>
                    <th>Update</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($mtr as $no => $b)
                  <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $b->designator }}</td>
                    <td class="text-right">{{ $b->onsite }}</td>
                    <td class="text-right">{{ $b->installed }}</td>
                    <td class="text-right"><input type="text" name="{{ $b->designator }}" class="form-control touchspin input-sm" placeholder="max:{{ $b->onsite-$b->installed }}" value="0" data-bts-max="{{ $b->onsite-$b->installed }}" data-bts-min="-999999"></td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-info panel-dark">
            <div class="panel-body">
                <form class="form-horizontal" method="post" id="form_jointer" action="/pickup_jointer/{{ Request::segment(2) }}">
                    @if(str_contains($data->progres_instalasi, 'Gelar_Kabel_Selesai') )
                        <div class="form-group form-message-dark">
                            <label for="pilih_jointer" class="col-md-2 control-label">Jointer</label>
                            <div class="col-md-9">
                                <select name="pilih_jointer" class="form-control" id="pilih_jointer"></select>
                            </div>
                        </div>
                        <div class="form-group form-message-dark field_jointer_tim">
                            <label for="tim_jointer" class="col-md-2 control-label">Tim Jointer</label>
                            <div class="col-md-9">
                                <select name="tim_jointer" class="form-control" id="tim_jointer"></select>
                            </div>
                        </div>
                        <div class="form-group form-message-dark">
                            <label for="odp" class="col-md-2 control-label">ODP</label>
                            <div class="col-md-9">
                                <select name="odp[]" class="form-control" id="odp" multiple>
                                    @foreach($get_all_tenor as $v)
                                        <option value="{{ $v->id }}">{{ $v->nama_odp }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-9">
                            <button type="submit" class="btn btn-success submit_join"><i class="ion-soup-can"></i>Simpan Tim Jointer</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
</div>

@endsection

@section('js')
<!-- <script src="/bower_components/vue-2.5.17/dist/vue.min.js"></script> -->
<script>

$(function() {
    
    $('.touchspin').TouchSpin();
    $(".submit_join").on('click', function(e) {
        e.preventDefault();
        $("#form_jointer").submit();
    })
    $('#formInstalasi').pxValidate({
      submitHandler: function(form,event) {
        $('input[name=item]').val(JSON.stringify($('.touchspin').serializeArray()));
        var datalop = <?= json_encode($data); ?>;
        if($('#status').val()=='Instalasi_Selesai' && datalop.jenis_pekerjaan=='MITRATEL'){
            if(!datalop.tgl_permit_a && !datalop.tgl_permit_b){
                alert("Update Permit dulu dong.");
                event.preventDefault();
            }else{
                form.addClass("form-loading form-loading-inverted");
                form.submit();
            }
        }else{
            form.addClass("form-loading form-loading-inverted");
            form.submit();
        }
      }
     });

    $('#odp').select2();

    var data = [
        {id: 'pick_up', text: 'Pilih Team'},
        {id: 'freelance', text: 'Freelance'},
    ],
    jointer_tim_jenis = $('#pilih_jointer').select2({
        data: data,
        placeholder: "Pilih Jenis Tim"
    });

    $('#pilih_jointer').on('change', function(){
        if($(this).val() == 'pick_up'){
            $(".field_jointer_tim").show();
            $('#tim_jointer').attr('required', 'required');
        }else{
            $(".field_jointer_tim").hide();
            $('#tim_jointer').removeAttr('required');
        }
    });

    $('#pilih_jointer').val('pick_up').change();

    var data = {!! json_encode($tim_jointer) !!},
    load_data = {!! json_encode($load_data) !!}
    data_tim = [];
    $.each(data, function(k, v){
        data_tim.push({
            id: v.id_user,
            text: v.nama
        });
    })
    var tim = $('#tim_jointer').select2({
        data: data_tim,
        placeholder: "Pilih Tim"
    });


    if(load_data.length != 0)
    {
        var collect_data = []
        $.each(load_data, function(k, v){
            collect_data.push(v.id)
        })

        if(load_data[0].nik1 == null){
            $("#pilih_jointer").val('freelance').change();
        }else{
            $("#pilih_jointer").val('pick_up').change();
            $("#tim_jointer").val(load_data[0].nik1).change();
        }

        $("#odp").val(collect_data).trigger("change");
    }

    var mtrcount = <?= json_encode($mtrcount); ?>.length;
    var status = $('#status').select2({
        placeholder:"Select status",
        data:
        [
            {"id":"Progress_Instalasi", "text":"Progress Instalasi"},
            {"id":"Kendala", "text":"Kendala"}
        ],
        dropdownCssClass : 'no-search',
    }).change(function() {
        $(this).valid();
    });
    if($("[type='checkbox']:checked").length==3 && mtrcount){
            status.select2({placeholder:"Select status",data:[
            {"id":"Progress_Instalasi", "text":"Progress Instalasi"},
            {"id":"Kendala", "text":"Kendala"},
            {"id":"Instalasi_Selesai", "text":"Instalasi Selesai"}
        ]});
    }
    
    $('.bttab').click(function(){
        $('.bttab').removeClass('btn-primary');
        $(this).addClass('btn-primary');
    });

    $('.perfect-scrollbar-scroll-y').perfectScrollbar({
        suppressScrollX: true
    });
    $('input[type=checkbox]').change(function(){
        // alert(mtrcount);
        if($("[type='checkbox']:checked").length==4 && mtrcount){
            status.select2({placeholder:"Select status",data:[
            {"id":"Progress_Instalasi", "text":"Progress Instalasi"},
            {"id":"Kendala", "text":"Kendala"},
            {"id":"Instalasi_Selesai", "text":"Instalasi Selesai"}
        ]});

        }
    });
    if($('#grid-input-13').is(':checked')){
        $('.nilai_lop').show();
        $('#nilai_lop').attr('required','true');
        $('.Terminasi').show();
        $('input[name="flag_Terminasi"]').attr('required','true');
    }else{
        $('.nilai_lop').hide();
        $('#nilai_lop').attr('required','false');
        $('.Terminasi').hide();
        $('input[name="flag_Terminasi"]').attr('required','false');
    }
    $('#grid-input-13').change(function(){
        if($('#grid-input-13').is(':checked')){
            $('.nilai_lop').show();
            $('#nilai_lop').attr('required','true');
            $('.Terminasi').show();
            $('input[name="flag_Terminasi"]').attr('required','true');
        }else{
            $('.nilai_lop').hide();
            $('#nilai_lop').attr('required','false');
            $('.Terminasi').hide();
            $('input[name="flag_Terminasi"]').attr('required','false');
        }
    });

    if($('#grid-input-10').is(':checked')){
        $('.Tanam_Tiang').show();
        $('input[name="flag_Tanam_Tiang"]').attr('required','true');
    }else{
        $('.Tanam_Tiang').hide();
        $('input[name="flag_Tanam_Tiang"]').attr('required','false');
    }
    $('#grid-input-10').change(function(){
        if($('#grid-input-10').is(':checked')){
            $('.Tanam_Tiang').show();
            $('input[name="flag_Tanam_Tiang"]').attr('required','true');
        }else{
            $('.Tanam_Tiang').hide();
            $('input[name="flag_Tanam_Tiang"]').attr('required','false');
        }
    });
    //gelar kabel
    if($('#grid-input-11').is(':checked')){
        $('.Gelar_Kabel').show();
        $('input[name="flag_Gelar_Kabel"]').attr('required','true');
    }else{
        $('.Gelar_Kabel').hide();
        $('input[name="flag_Gelar_Kabel"]').attr('required','false');
    }
    $('#grid-input-11').change(function(){
        if($('#grid-input-11').is(':checked')){
            $('.Gelar_Kabel').show();
            $('input[name="flag_Gelar_Kabel"]').attr('required','true');
        }else{
            $('.Gelar_Kabel').hide();
            $('input[name="flag_Gelar_Kabel"]').attr('required','false');
        }
    });
    //Pasang_Terminal
    if($('#grid-input-12').is(':checked')){
        $('.Pasang_Terminal').show();
        $('input[name="flag_Pasang_Terminal"]').attr('required','true');
    }else{
        $('.Pasang_Terminal').hide();
        $('input[name="flag_Pasang_Terminal"]').attr('required','false');
    }
    $('#grid-input-12').change(function(){
        if($('#grid-input-12').is(':checked')){
            $('.Pasang_Terminal').show();
            $('input[name="flag_Pasang_Terminal"]').attr('required','true');
        }else{
            $('.Pasang_Terminal').hide();
            $('input[name="flag_Pasang_Terminal"]').attr('required','false');
        }
    });
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
            $(inputEl).parent().find('input[type=text]').val(1);
            var reader = new FileReader();
            reader.onload = function(e) {
                $(inputEl).parent().find('img').attr('src', e.target.result);

            }
            reader.readAsDataURL(inputEl.files[0]);
        }
    });
    $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
    });
});
</script>
@endsection
