@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>{{ empty($data) ? 'Input LOP u/ Project' : 'Edit LOP u/ Project' }}</span>
</h1>
@endsection
@section('title', empty($data) ? 'Buat Lop' : 'Edit LOP')
@section('content')
<form class="form-horizontal form-bordered" method="post" id="form-register" enctype="multipart/form-data" action="{{ empty($data) ? '' : '/editLop/'.$id }}">
    <input type="hidden" name="PENGGANTI_LOP" value="{{ Request::segment(4) }}">
    <div class="form-group">
        <label for="waspannama_lopg" class="col-md-2 control-label">Nama LOP</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="nama_lop" id="nama_lop" class="form-control" required value="{{ @$data->nama_lop }}" readonly/>
        </div>
        <label for="waspang" class="col-md-1 control-label">Waspang</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="waspang" id="waspang" class="form-control" required value="{{ @$data->waspang }}"/>
        </div>

    </div>
    <div class="form-group">
        <label for="pid" class="col-md-2 control-label">PID</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="pid" id="pid" class="form-control" required value="{{ @$data->pid }}" readonly/>
        </div>
        <label for="sto" class="col-md-1 control-label">STO</label>
        <div class="col-md-2 form-message-dark">
            <input type="text" name="sto" id="sto" class="form-control" required value="{{ @$data->sto }}" />
        </div>
        <label for="mitra" class="col-md-1 control-label">Mitra</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" name="mitra" id="mitra" class="form-control" required value="{{ @$data->mitra }}" />
        </div>
    </div>

    <?php
        $todayrange = date('Y-m-d').' - '.date('Y-m-d');
    ?>
    <div class="form-group">
        <label for="waspannama_lopg" class="col-md-2 control-label">Timeplan Pekerjaan Fisik</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" id="daterange-1" name="timeplan_fisik" value="{{ @$data->timeplan_fisik ?? $todayrange }}" class="form-control">
        </div>
        <label for="waspang" class="col-md-2 control-label">Timeplan Pemberkasan</label>
        <div class="col-md-4 form-message-dark">
            <input type="text" id="daterange-2" name="timeplan_berkas" value="{{ @$data->timeplan_berkas ?? $todayrange }}" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>

@endsection

@section('js')
<script>
    $(function() {
        $('#BOQ_Unwizing').pxFile();

        $("#project").select2({
            placeholder: 'Masukan Nama Project Atau PID',
            minimumInputLength: 3,
            ajax: {
                url: "/projectsearching",
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        find: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });
        var mitra = <?= json_encode($mitra); ?>;
        $('#mitra').select2({
            data:mitra,
            placeholder:"Pilih Mitra"
        });
        var waspang = <?= json_encode($waspang); ?>;
        $('#waspang').select2({
            data:waspang,
            placeholder:"Pilih Waspang"
        });
        var sto = <?= json_encode($sto); ?>;
        $('#sto').select2({
            data:sto,
            placeholder:"Pilih STO"
        });
        $('#daterange-1').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD'
      }
    });
        $('#daterange-2').daterangepicker({
      locale: {
        format: 'YYYY-MM-DD'
      }
    });
    });
</script>
@endsection
