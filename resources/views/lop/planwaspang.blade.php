@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('title', 'BTS || Plan Waspang')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i></span>PLAN WASPANG</i>
</h1>
@endsection
@section('content')
        
        @foreach($data as $no => $d)
        <div class="col-md-4">
            <!-- Counters -->
            <div class="panel panel-info panel-dark widget-profile">
              <div class="panel-heading">
                <h3 class="widget-profile-header">
                  {{ $d->nama }}
                  <a href="#" class="widget-profile-secondary-text">{{ $d->nama_lop }}</a>
                  <a href="#" class="widget-profile-secondary-text">{{ $d->pid?:'BLM ADA PID' }}</a>
                </h3>
                <i class="widget-profile-bg-icon font-size-52">#</i>
              </div>
              
              <div class="panel-footer"><i class="fa fa-tasks list-group-icon"></i>Uncompleted tasks<span class="badge badge-warning pull-right">7</span></div>
              <div class="panel-footer">New Item <a href="#" class="btn btn-sm btn-success pull-right"><i class="ion ion-plus pull"></i>&nbsp;&nbsp;Tambah Plan</a></div>
            </div>
        </div>
        @endforeach


@endsection

@section('js')
@endsection
