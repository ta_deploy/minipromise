@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || Permberkasan')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Evident
</h1>
@endsection
@section('content')
<input type="hidden" id="id" value="{{Request::segment(2)}}">
@if(count($data))
<div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Data</th>
                <th>Evident</th>
            </tr>
        </thead>
        <tbody>
            @foreach($data as $no => $d)
            <?php
            $foto = [
                $d->Real_Olt_Port?str_replace('/','_',str_replace(' ','_',$d->Real_Olt_Port)):'NoData_OLT',
                isset($d->Real_Odf_Otb,$d->Real_Otb_Panel,$d->Real_Otb_Port)?'ODF_E_'.$d->Real_Odf_Otb.'-'.$d->Real_Otb_Panel.'-'.$d->Real_Otb_Port:'NoData_OTB_E',
                isset($d->Real_Otb_Odf_o,$d->Real_Otb_Panel_o,$d->Real_Otb_Port_o)?'ODF_O_'.$d->Real_Otb_Odf_o.'-'.$d->Real_Otb_Panel_o.'-'.$d->Real_Otb_Port_o:'NoData_OTB_O',
                $d->Real_Odc_Nama?$d->Real_Odc_Nama:'NoData_ODC_NAMA',
                isset($d->Real_Odc_Panel,$d->Real_Odc_Port)?'ODC_'.$d->Real_Odc_Panel.'-'.$d->Real_Odc_Port:'NoData_ODC_PANEL-PORT',
                isset($d->Real_Odp_Panel,$d->Real_Odp_Port)?'ODC_'.$d->Real_Odp_Panel.'-'.$d->Real_Odp_Port:'NoData_ODC_PANEL-PORT'
            ];

            for ($i = 1; $i <= 8; $i++){
                $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_OPM_'.$i:'NoData_ODP_NAMA';
            }
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).' OPM_IN_1':'NoData_ODP_NAMA';
            $qrcode = ['ODP', 'TIANG', 'SPL'];
            foreach ($qrcode as $q) {
                $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_QRCODE_'.$q:'NoData_ODP_NAMA';
            }
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_KANAN':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_KIRI':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_TENGAH':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_LABEL_ODP':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_FOTO_AKSESORIS':'NoData_ODP_NAMA';
            ?>
            <tr>
                <td>{{ $d->Real_Odp_Nama?:$d->Plan_Odp_Nama }}</td>
                @if($d->Real_Odp_Nama)
                <td>
                    <div class="row text-center input-photos">
                        @foreach($foto as $no => $input)
                        <div class="col-xs-2 col-sm-2">
                            <?php
                            $path = "/storage/".Request::segment(2)."/instalasifile/evident/photo-".$input;
                            $th   = $path."-th.jpg";
                            $img  = $path.".jpg";
                            $flag = "";
                            $name = "flag_".$input;
                            ?>
                            @if (file_exists(public_path().$th))
                            <a href="{{ $img }}">
                                <img src="{{ $th }}" alt="{{ $input }}" />
                            </a>
                            <?php
                            $flag = 1;
                            ?>
                            @else
                            <img src="/image/placeholder.gif" alt="" />
                            @endif
                            <br />
                            <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
                            <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
                            <button type="button" class="btn btn-sm btn-info {{ explode('_',$input)[0]=='NoData'?'hide':'' }}">
                                <i class="ion-camera"></i>
                            </button>
                            <p>{{ $input }}</p>
                        </div>
                        @endforeach
                    </div>
                </td>
                @else
                <td></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
<span class="label label-danger">Perlu Upload file MCORE Instalasi terlebih dahulu</span>
@endif
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $('input[type=file]').change(function() {
            console.log(this.name);
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                $(inputEl).parent().find('input[type=text]').val(1);
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);

                }
                reader.readAsDataURL(inputEl.files[0]);
            }
            if($(this).val()!=''){
                upload(this,this.name, $('#id').val());
            }
        });
        $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();
        });
        function upload(img,nama, id) {
            var form_data = new FormData();
            form_data.append('file', img.files[0]);
            form_data.append('nama', nama);
            form_data.append('id', id);
            $('#loading').css('display', 'block');
            $.ajax({
                url: "/uploadevident",
                data: form_data,
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        alert('error');
                    }
                    else {
                        alert('success');
                    }
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }
    });
</script>
@endsection
