@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || Pemberkasan')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>PEMBERKASAN <i>{{ $data->nama_lop }}</i>
</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" id="form-next" action="/nextstep">
    <input type="hidden" name="id" value="{{ $data->id }}">
</form>
<form class="form-horizontal" method="post" action="/uploadInstalasi/{{ Request::segment(2) }}" id="form-upload" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="abd" class="col-md-2 control-label">ABD</label>
        <div class="col-md-9">
            <label id="abd" class="custom-file px-file" for="ABD">
                <input type="file" id="ABD" class="custom-file-input" name="abd">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            {!! ($data->abd ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="OTDR" class="col-md-2 control-label">OTDR</label>
        <div class="col-md-9">
            <label id="OTDR" class="custom-file px-file" for="OTDRi">
                <input type="file" id="OTDRi" class="custom-file-input" name="otdr">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="QRCODE" class="col-md-2 control-label">QR CODE</label>
        <div class="col-md-9">
            <label id="QRCODE" class="custom-file px-file" for="QRCODEi">
                <input type="file" id="QRCODEi" class="custom-file-input" name="qrcode">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            {!! ($data->qrcode ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="kml_instalasi" class="col-md-2 control-label">KML INSTALASI</label>
        <div class="col-md-9">
            <label id="kml_instalasi" class="custom-file px-file" for="kml_instalasii">
                <input type="file" id="kml_instalasii" class="custom-file-input" name="kml_instalasi">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            {!! ($data->kml_instalasi ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="mcore_instalasi" class="col-md-2 control-label">M.CORE INSTALASI</label>
        <div class="col-md-8">
            <label id="mcore_instalasi" class="custom-file px-file" for="mcore_instalasii">
                <input type="file" id="mcore_instalasii" class="custom-file-input" name="mcore_instalasi">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>

        <div class="col-md-1">
            <a href="/download/storage+mcore_format.xlsx" target="_blank"><span class="label label-info pull-lg-right">Unduh Format</span></a>
        </div>
        <div class="col-md-1">
            {!! ($data->mcore_instalasi ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="boq_instalasi" class="col-md-2 control-label">BOQ INSTALASI</label>
        <div class="col-md-9">
            <label id="boq_instalasi" class="custom-file px-file" for="boq_instalasii">
                <input type="file" id="boq_instalasii" class="custom-file-input" name="boq_instalasi">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            {!! ($data->boq_instalasi ? '<span class="label label-success pull-lg-right">Done</span>' : '<span class="label label-danger pull-lg-right">Perlu Upload</span>') !!}
        </div>
    </div>

    <div class="form-group form-message-dark">
        <label for="Perijinan" class="col-md-2 control-label">DOK. PERIJINAN</label>
        <div class="col-md-9">
            <label id="perijinan" class="custom-file px-file" for="perijinani">
                <input type="file" id="perijinani" class="custom-file-input" name="perijinan">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        <div class="col-md-1">
            {!! ($data->perijinan ? '<span class="label label-success pull-lg-right">Done</span>' : '') !!}
        </div>
    </div>

    <div class="form-group form-message-dark">
        <label for="RFS" class="col-md-2 control-label">RFS</label>
        <div class="col-md-9">
            <label id="RFS" class="custom-file px-file" for="perijinani">
                <input type="file" id="RFSi" class="custom-file-input" name="RFS">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>

        <div class="col-md-1">
            {!! ($data->RFS ? '<span class="label label-success pull-lg-right">Done</span>' : '') !!}
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn btn-info"><i class="ion-soup-can"></i> Simpan</button>
            <button type="button" class="btn btn-primary" id="btnNext" lopid="{{ $data->id }}">Next <i class="ion-ios-arrow-forward"></i></button>
        </div>
    </div>
</form>

<input type="hidden" id="id" value="{{Request::segment(2)}}">
<div class="panel">
  <div class="panel-heading">
    <span class="panel-title">Photo ZIP, format harus <code>.ZIP</code></span>
    <span id="hapusfotozip" class="btn btn-danger">Hapus Foto</span>
    <div class="panel-heading-controls col-sm-4">
      <form method="post" action="/uploadfotorar/{{Request::segment(2)}}" enctype="multipart/form-data">
        <div class="input-group input-group-sm">
          <input type="file" class="form-control" name="fotorar">
          <span class="input-group-btn">
            <button class="btn" type="submit">
              <span class="ion ion-upload">Upload ZIP</span>
            </button>
          </span>
        </div>
      </form>
    </div>
  </div>
  <div class="panel-body">

      @foreach($fotorar as $f)
        <div class="panel panel-info panel-body-colorful">
          <div class="panel-heading">
            <span class="panel-title">{{ str_replace(public_path()."/storage/".Request::segment(2)."/fotorar/",'',$f['url']) }}</span>
            <div class="panel-heading-icon"><i class="fa fa-inbox"></i></div>
          </div>
          <div class="panel-body">
            <div class="row text-center input-photos">
            @foreach($f['name'] as $no => $input)
            <div class="col-xs-2 col-sm-2">
                <?php
                $path = str_replace(public_path(),'',$f['url']).'/'.$input;
                ?>
                @if (file_exists(public_path().$path))
                <a href="{{ $path }}">
                    <img src="{{ $path }}" alt="{{ $input }}" />
                </a>
                @else
                <img src="/image/placeholder.gif" alt="" />
                @endif
                <br />
                <p>{{ $input }}</p>
            </div>
            @endforeach
        </div>
          </div>
        </div>
      @endforeach
  </div>
</div>
<form id="formdeletefotozip" method="post" action="/deletefotorar/{{ Request::segment(2) }}"></form>
@if(count($core))
<div class="table-responsive table-primary">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Data</th>
                <th>Evident</th>
            </tr>
        </thead>
        <tbody>
            @foreach($core as $no => $d)
            <?php

            $namaodc = str_replace('ODP', 'ODC', explode('/', $d->Real_Odp_Nama)[0]);

            $foto = [
                $d->Real_Olt_Port?str_replace('/','_',str_replace(' ','_',$d->Real_Olt_Port)):'NoData_OLT',
                isset($d->Real_Odf_Otb,$d->Real_Otb_Panel,$d->Real_Otb_Port)?'ODF_E_'.$d->Real_Odf_Otb.'-'.$d->Real_Otb_Panel.'-'.$d->Real_Otb_Port:'NoData_OTB_E',
                isset($d->Real_Otb_Odf_o,$d->Real_Otb_Panel_o,$d->Real_Otb_Port_o)?'ODF_O_'.$d->Real_Otb_Odf_o.'-'.$d->Real_Otb_Panel_o.'-'.$d->Real_Otb_Port_o:'NoData_OTB_O',
                $namaodc?$namaodc:'NoData_ODC_NAMA',
                isset($d->Real_Odc_Panel,$d->Real_Odc_Port)?'ODC_'.$d->Real_Odc_Panel.'-'.$d->Real_Odc_Port:'NoData_ODC_PANEL-PORT',
                isset($d->Real_Odp_Panel,$d->Real_Odp_Port)?'ODC_'.$d->Real_Odp_Panel.'-'.$d->Real_Odp_Port:'NoData_ODP_PANEL-PORT'
            ];

            for ($i = 1; $i <= 8; $i++){
                $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_OPM_'.$i:'NoData_ODP_NAMA';
            }
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).' OPM_IN_1':'NoData_ODP_NAMA';
            $qrcode = ['ODP', 'TIANG', 'SPL'];
            foreach ($qrcode as $q) {
                $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_QRCODE_'.$q:'NoData_ODP_NAMA';
            }
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_KANAN':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_KIRI':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_JARAK_JAUH_TENGAH':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_LABEL_ODP':'NoData_ODP_NAMA';
            $foto[] = $d->Real_Odp_Nama?str_replace('/','_',$d->Real_Odp_Nama).'_FOTO_AKSESORIS':'NoData_ODP_NAMA';
            ?>
            <tr>
                <td>{{ $d->Real_Odp_Nama?:$d->Plan_Odp_Nama }}</td>
                @if($d->Real_Odp_Nama)
                <td>
                    <div class="row text-center input-photos">
                        @foreach($foto as $no => $input)
                        <div class="col-xs-2 col-sm-2">
                            <?php
                            $path = "/storage/".Request::segment(2)."/instalasifile/evident/photo-".$input;
                            $th   = $path."-th.jpg";
                            $img  = $path.".jpg";
                            $flag = "";
                            $name = "flag_".$input;
                            ?>
                            @if (file_exists(public_path().$th))
                            <a href="{{ $img }}">
                                <img src="{{ $th }}" alt="{{ $input }}" />
                            </a>
                            <?php
                            $flag = 1;
                            ?>
                            @else
                            <img src="/image/placeholder.gif" alt="" />
                            @endif
                            <br />
                            <input type="text" class="hidden" name="flag_{{ $input }}" value="{{ $flag }}"/>
                            <input type="file" class="hidden filephoto" name="photo-{{ $input }}" accept="image/jpeg" />
                            <button type="button" class="btn btn-sm btn-info {{ explode('_',$input)[0]=='NoData'?'hide':'' }}">
                                <i class="ion-camera"></i>
                            </button>
                            <p>{{ $input }}</p>
                        </div>
                        @endforeach
                    </div>
                </td>
                @else
                <td></td>
                @endif
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@else
<span class="label label-danger">Perlu Upload file MCORE Instalasi terlebih dahulu</span>
@endif
@endsection

@section('js')
<script>
    $(function() {
        $('#abd').pxFile();
        $('#OTDR').pxFile();
        $('#QRCODE').pxFile();
        $('#kml_instalasi').pxFile();
        $('#mcore_instalasi').pxFile();
        $('#boq_instalasi').pxFile();
        $('#perijinan').pxFile();
        $('#RFS').pxFile();


        $('.filephoto').change(function() {
            console.log(this.name);
            var inputEl = this;
            if (inputEl.files && inputEl.files[0]) {
                $(inputEl).parent().find('input[type=text]').val(1);
                var reader = new FileReader();
                reader.onload = function(e) {
                    $(inputEl).parent().find('img').attr('src', e.target.result);

                }
                reader.readAsDataURL(inputEl.files[0]);
            }
            if($(this).val()!=''){
                upload(this,this.name, $('#id').val());
            }
        });
        $('#btnNext').click(function(){
            $('#form-next').submit();
        });
        $('.input-photos').on('click', 'button', function() {
            $(this).parent().find('input[type=file]').click();
        });
        function upload(img,nama, id) {
            var form_data = new FormData();
            form_data.append('file', img.files[0]);
            form_data.append('nama', nama);
            form_data.append('id', id);
            $('#loading').css('display', 'block');
            $.ajax({
                url: "/uploadevident",
                data: form_data,
                type: 'POST',
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                    if (data.fail) {
                        alert('error');
                    }
                    else {
                        alert('success');
                    }
                },
                error: function (xhr, status, error) {
                    alert(xhr.responseText);
                }
            });
        }[]
        $('#hapusfotozip').click(function(){
            var r = confirm("Yakin nih?");
            if (r == true) {
                $("#formdeletefotozip").submit();
            }
        });
    });

</script>
@endsection
