@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Silahkan ke Form yg baru
</h1>
@endsection
@section('content')
<!-- <form class="form-horizontal" method="post" id="formDelivery" autocomplete="off" action="/saveDelivery/{{ Request::segment(2) }}" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="status_material" class="col-md-3 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" name="status" id="status_material" class="form-control" required/>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="tgl_material_tiba" class="col-md-3 control-label">Tanggal Status</label>
        <div class="col-md-9">
            <input type="text" name="tgl_material_tiba" id="tgl_material_tiba" class="form-control" required/>
        </div>
    </div>
     <div class="form-group form-message-dark">
        <label for="catatan" class="col-md-3 control-label">&nbsp;</label>
        <?php
            $foto = ["Material_Dimuat"];
        ?>
        <div class="col-md-9">
            @foreach($foto as $f)
                <div class="col-xs-3 col-sm-3 input-photos text-center {{$f}}">
                    <?php
                    $input=$f;
                    $path = "/storage/".Request::segment(2)."/instalasifile/evident/".$input;
                    $th   = $path."-th.jpg";
                    $img  = $path.".jpg";
                    $flag = "";
                    $name = "flag_".$input;
                    ?>
                    @if (file_exists(public_path().$th))
                        <a href="{{ $img }}" target="_blank">
                            <img src="{{ $th }}" alt="{{ $input }}" />
                        </a>
                        <?php
                            $flag = 1;
                        ?>
                    @else
                        <img src="/image/placeholder.gif" alt="" />
                    @endif
                    <br />
                    <input type="hidden" style="opacity: 0;pointer-events: none;" size="6" class="text pull-left" name="flag_{{ $input }}" value="{{ $flag }}" required />
                    <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
                    <button type="button" class="btn btn-sm btn-info m-t-1">
                        <i class="ion-camera"></i>
                    </button>
                    <p>Foto {{ str_replace("_"," ",$input)  }}</p>
                </div>
            @endforeach
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form> -->
@endsection

@section('js')
<script>
    $('#formDelivery').pxValidate();
    $('#status_material').select2({
        placeholder:"Select Status Material",
        data:[{id:"Menunggu_PO", text:"Menunggu PO"},{id:"Material_On_Delivery", text:"Material On Delivery"},{id:"Material_On_Site", text:"Material On Site"},{id:"Material_Ready", text:"Material Ready"}],
        dropdownCssClass : 'no-search',
    }).change(function() {
        if($(this).val() == '2'){
            $("#tgl_material_tiba").rules("add", {
                required: true,
                minlength: 2
            });
        }else{
            $("#tgl_material_tiba").rules("remove");
            $("#tgl_material_tiba").valid();
        }
        $(this).valid();
    });
    $('#tgl_material_tiba').datepicker({
        autoclose:true,
        format: 'yyyy-mm-dd'
    });
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
            $(inputEl).parent().find('input[type=text]').val(1);
            var reader = new FileReader();
            reader.onload = function(e) {
                $(inputEl).parent().find('img').attr('src', e.target.result);

            }
            reader.readAsDataURL(inputEl.files[0]);
        }
    });
    $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
    });
</script>
@endsection
