@extends('layout')
@section('heading')
<style>
    .sk-cube-grid {
    position: fixed;
    left: 50%;
    z-index: 9999999;
    width: 40px;
    height: 40px;
    margin: 100px auto;
  }
  .sk-cube-grid:before {
  content: '';
  display: block;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
    background: radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0, .8));

  background: -webkit-radial-gradient(rgba(20, 20, 20,.8), rgba(0, 0, 0,.8));
  }

.sk-cube-grid .sk-cube {
  width: 33%;
  height: 33%;
  background-color: #ffffff;
  float: left;
  -webkit-animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out;
          animation: sk-cubeGridScaleDelay 1.3s infinite ease-in-out; 
}
.sk-cube-grid .sk-cube1 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube2 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube3 {
  -webkit-animation-delay: 0.4s;
          animation-delay: 0.4s; }
.sk-cube-grid .sk-cube4 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube5 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }
.sk-cube-grid .sk-cube6 {
  -webkit-animation-delay: 0.3s;
          animation-delay: 0.3s; }
.sk-cube-grid .sk-cube7 {
  -webkit-animation-delay: 0s;
          animation-delay: 0s; }
.sk-cube-grid .sk-cube8 {
  -webkit-animation-delay: 0.1s;
          animation-delay: 0.1s; }
.sk-cube-grid .sk-cube9 {
  -webkit-animation-delay: 0.2s;
          animation-delay: 0.2s; }

@-webkit-keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1); 
  }
}

@keyframes sk-cubeGridScaleDelay {
  0%, 70%, 100% {
    -webkit-transform: scale3D(1, 1, 1);
            transform: scale3D(1, 1, 1);
  } 35% {
    -webkit-transform: scale3D(0, 0, 1);
            transform: scale3D(0, 0, 1);
  } 
}

</style>
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Register
</h1>
@endsection
@section('title', 'BTS || Pemberkasan')
@section('content')
<div class="sk-cube-grid" style="display: none;">
    <div class="sk-cube sk-cube1"></div>
    <div class="sk-cube sk-cube2"></div>
    <div class="sk-cube sk-cube3"></div>
    <div class="sk-cube sk-cube4"></div>
    <div class="sk-cube sk-cube5"></div>
    <div class="sk-cube sk-cube6"></div>
    <div class="sk-cube sk-cube7"></div>
    <div class="sk-cube sk-cube8"></div>
    <div class="sk-cube sk-cube9"></div>
  </div>
<div class="parent"></div>
<div class="modal fade" id="modal_prev" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Upload ODP</h4>
      </div>

      <div class="col-md-12">
        <table id="preview_dt" class="table table-bordered">
            
        </table>
      </div>
    </div>
  </div>
</div>
<form class="form-horizontal form-bordered" method="post" id="pemberkasan" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="nama_lop" class="col-md-2 control-label">Nama LOP</label>
        <div class="col-md-10">
            {{ $data->nama_lop }}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_lop" class="col-md-2 control-label">Nama Project</label>
        <div class="col-md-10">
            {{ $data->nama }}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_lop" class="col-md-2 control-label">PID</label>
        <div class="col-md-10">
            {{ $data->pid }}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_lop" class="col-md-2 control-label">STO</label>
        <div class="col-md-10">
            {{ $data->sto }}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_lop" class="col-md-2 control-label">NIK Waspang</label>
        <div class="col-md-10">
            {{ $data->waspang }}
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_lop" class="col-md-2 control-label">Nama Waspang</label>
        <div class="col-md-10">
            {{ $data->waspang_nm }}
        </div>
    </div>
    <div class="box">
        <div class="box-row">
            <div class="box-cell bg-info">
                <div class="form-group form-message-dark">
                    <label for="boq_unwizing" class="col-md-2 control-label">File Designator</label>
                    <div class="col-md-10">
                        <div class="col-md-12">
                            <a href="/storage/{{ Request::segment(2)}}/unwizingfile/{{ $data->boq_unwizing }}">{{ $data->boq_unwizing }}</a>
                        </div>
                        <label class="col-md-12">Atau</label>
                        <div class="col-md-12">
                            <label id="boq_unwizing" class="custom-file px-file" for="grid-input-6">
                                <input type="file" id="grid-input-6" class="custom-file-input ini_filenya_tuan" name="boq_unwizing" id="boq_unwizing">
                                <span class="custom-file-control form-control">Choose file...</span>
                                <div class="px-file-buttons">
                                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group form-message-dark">
                    <label for="kolom" class="col-md-2 control-label">Kolom</label>
                    <div class="col-md-10">
                        <input type="text" style="text-transform: uppercase;" data-inputmask="'mask': 'a,a'" name="kolom_sheet_boq" id="kolom" class="form-control " />
                        <span style="color: black;">*Masukkan Nama Kolom Dengan Format: <u>kolom Designator, kolom jumlah</u>. <br /> Contoh: B,G</span>
                        <input type="hidden" class="spy_klm">
                    </div>
                </div>
                <input type="hidden" id="result_berkas" name="result_berkas"/>
                <div class="form-group">
                    <div class="col-md-offset-2 col-md-9">
                        <button type="button" class="btn" id="upload"><i class="ion-pinpoint"></i> Proses</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.min.js" integrity="sha512-DVfS/GbZzLMmxBL/CW92N84eHP2Fq9d+r9RKbvctcvzISVfu+WvD+MCvbK9j8I6nVLrntGo3UUVrNFUDX0ukBw==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/bindings/inputmask.binding.min.js" integrity="sha512-ID9bgUOj+ZWEBbXo8VDvXcWmO63YrbYQStfhQfkzYFIMBQkZYTHXNDpLDqZXuZimOns21yHBd7VqXKA7+SyvAw==" crossorigin="anonymous"></script>
<script>
    $(function() {        
        $('#boq_unwizing').pxFile();

        $('#upload').on('click', function(e) {
            $('.spy_klm').val('yesssssss');
            var file_data = $('.ini_filenya_tuan').prop('files')[0];
            var form_data = new FormData();
            var link = window.location.pathname.split('/');
            form_data.append('file', file_data);
            form_data.append('data', $('#kolom').val());
            form_data.append('id', link[2]);
            $.ajax({
                type: 'POST',
                url: '/excel/preview_excel', // point to server-side PHP script
                contentType: false,
                cache: false,
                processData: false,
                data: form_data,
                dataType: 'json', // what to expect back from the PHP script, if anything
                beforeSend: function() {
                    $('.sk-cube-grid').css({
                        'display': 'block'
                    });
                },
                success: function(data){
                    $('.sk-cube-grid').css({
                        'display': 'none'
                    });
                    $('#result_berkas').val(JSON.stringify(data));

                    var $container = $('#block-alert-with-timer');
                    $container.pxBlockAlert('Berhasil Mendapatkan Data!', { type: 'success', style: 'light', timer: 3 });
                    
                    var content = "<tbody>"
                    $.each( data, function( key_p, value_p ) {
                        content += '<tr>';
                        content += '<td>';
                        content += value_p[0];
                        content += '</td>';
                        content += '<td>';
                        content += value_p[1];
                        content += '</td>';
                        content += '</tr>';
                    });
                    content +="</tbody>";
                    $('#preview_dt').html(content);

                    $('#modal_prev').modal('show');
                }
            });
        });

        $("#project").select2({
            placeholder: 'Masukan Nama Project',
            minimumInputLength: 3,
            ajax: {
                url: "/projectsearching",
                type: "GET",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        find: params.term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });

        $('#pemberkasan').on('submit', function(e){
            if ($('.spy_klm').val() != 'yesssssss') {
                e.preventDefault();
            }
        })

    });
</script>
@endsection
