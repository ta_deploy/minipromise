@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || Permit')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>LOP / </span>Permit Site {{ $data->nama_lop }}
</h1>
@endsection
@section('content')

    <!-- <div class="form-group form-message-dark">
        <label for="status_material" class="col-md-3 control-label">Kendala</label>
        <div class="col-md-9">
            <input type="text" name="status" id="status_material" class="form-control" required/>
        </div>
    </div> -->

    <?php
        $todayrange = date('Y-m-d').' - '.date('Y-m-d');
    ?>
    <form class="form-horizontal" method="post" id="formPermit" autocomplete="off" action="/permit/{{ Request::segment(2) }}" enctype="multipart/form-data">
    <div class="row">
      <div class="col-md-6">
            <div class="form-group">
                <label for="waspannama_lopg" class="col-md-3 control-label">Tgl Submit Permit A</label>
                <div class="col-md-9 form-message-dark">
                    <input type="text" id="daterange-1" name="tgl_submit_permit_a" value="{{ @$data->tgl_submit_permit_a }}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="waspang" class="col-md-3 control-label">Tgl Approve Permit A</label>
                <div class="col-md-9 form-message-dark">
                    <input type="text" id="daterange-2" name="tgl_permit_a" value="{{ @$data->tgl_permit_a }}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="waspannama_lopg" class="col-md-3 control-label">Tgl Submit Permit B</label>
                <div class="col-md-9 form-message-dark">
                    <input type="text" id="daterange-3" name="tgl_submit_permit_b" value="{{ @$data->tgl_submit_permit_b }}" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="waspang" class="col-md-3 control-label">Tgl Approve Permit B</label>
                <div class="col-md-9 form-message-dark">
                    <input type="text" id="daterange-4" name="tgl_permit_b" value="{{ @$data->tgl_permit_b }}" class="form-control">
                </div>
            </div>
          <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
              <button type="submit" class="btn">Submit</button>
            </div>
          </div>
      </div>
      <div class="col-md-6">
            <!-- <label for="catatan" class="col-md-4 control-label">&nbsp;</label> -->
            <?php
                $foto = ["Permit_A","Permit_B"];
            ?>
            <div class="col-md-9">
             @foreach($foto as $f)
             <div class="col-xs-4 col-sm-4 input-photos text-center {{$f}}">
                <?php
                $input=$f;
                $path = "/storage/".Request::segment(2)."/instalasifile/evident/".$input;
                $th   = $path."-th.jpg";
                $img  = $path.".jpg";
                $flag = "";
                $name = "flag_".$input;
                ?>
                @if (file_exists(public_path().$th))
                <a href="{{ $img }}" target="_blank">
                    <img src="{{ $th }}" alt="{{ $input }}" />
                </a>
                <?php
                $flag = 1;
                ?>
                @else
                <img src="/image/placeholder.gif" alt="" />
                @endif
                <br />
                <input type="text" style="opacity: 0;pointer-events: none;" size="6" class="text pull-left" name="flag_{{ $input }}" value="{{ $flag }}" required />
                <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
                <button type="button" class="btn btn-sm btn-info m-t-1">
                    <i class="ion-camera"></i>
                </button>
                <p>Foto {{ str_replace("_"," ",$input)  }}</p>
            </div>
            @endforeach
      </div>
    </div>
    </div>
    </form>
    
</form>
@endsection

@section('js')
<script>
    $('#formPermit').pxValidate();

    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
            $(inputEl).parent().find('input[type=text]').val(1);
            var reader = new FileReader();
            reader.onload = function(e) {
                $(inputEl).parent().find('img').attr('src', e.target.result);

            }
            reader.readAsDataURL(inputEl.files[0]);
        }
    });
    $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
    });
    
    $('#daterange-1,#daterange-2,#daterange-3,#daterange-4').datepicker({
        autoclose:true,
        format: 'yyyy-mm-dd'
    });
</script>
@endsection
