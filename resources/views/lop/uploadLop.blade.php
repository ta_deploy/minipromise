@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Upload LOP u/ Project {{ $data->nama }}</span>
</h1>
@endsection
@section('title', 'BTS || Buat LOP')
@section('content')
<form class="form-horizontal form-bordered" method="post" id="form-register" enctype="multipart/form-data">
    <div class="form-group form-message-dark">
        <label for="upload_lop" class="col-md-2 control-label">Upload LOP</label>
        <div class="col-md-10">
            <label id="upload_lop" class="custom-file px-file" for="grid-input-6">
                <input type="file" class="custom-file-input" name="upload_lop" id="upload_lop">
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn"><i class="ion-soup-can"></i> Simpan</button>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        $('#upload_lop').pxFile();

    });
</script>
@endsection
