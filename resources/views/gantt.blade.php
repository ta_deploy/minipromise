@extends('layout')
@section('heading')
<h1> <h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard</span></h1></h1>
@endsection
@section('css')
<style type="text/css">
  tr > th {
    text-align: center;
  }
</style>
<!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"> -->
<link href="//cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.css" rel="stylesheet" type="text/css">
<link href="https://taitems.github.io/jQuery.Gantt/css/style.css" rel="stylesheet" type="text/css">
@endsection
@section('title', 'Dashboard')
@section('content')
<div class="gantt"></div>
<!-- <div id='calendar-container'>
        <div id='calendar'></div>
    </div> -->
@endsection
@section('js')
<!-- <script src="https://taitems.github.io/jQuery.Gantt/js/jquery.min.js"></script> -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script src="https://taitems.github.io/jQuery.Gantt/js/jquery.fn.gantt.js"></script>
    <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/prettify/r298/prettify.min.js"></script>
    <!-- <link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.3.1/main.min.js'></script> -->
<!-- <link href='https://cdn.jsdelivr.net/npm/fullcalendar-scheduler/main.css' rel='stylesheet' />
<script src='https://cdn.jsdelivr.net/npm/fullcalendar-scheduler/main.js'></script> -->
    <script>
        $(function() {
            "use strict";

            var demoSource = <?=  json_encode($dataarray) ?>;

            
            $(".gantt").gantt({
                source: demoSource,
                navigate: "scroll",
                scale: "days",
                maxScale: "months",
                minScale: "days",
                itemsPerPage: 18,
                scrollToToday: false,
                useCookie: true,
                onItemClick: function(data) {
                    alert("Item clicked - show some details");
                },
                onAddClick: function(dt, rowId) {
                    alert("Empty space clicked - add an item!");
                },
                onRender: function() {
                    if (window.console && typeof console.log === "function") {
                        console.log("chart rendered");
                    }
                }
            });

            $(".gantt").popover({
                selector: ".bar",
                title: function _getItemText() {
                    return this.textContent;
                },
                container: '.gantt',
                content: "Here's some useful information.",
                trigger: "hover",
                placement: "auto right"
            });

            prettyPrint();
            
        });
        // document.addEventListener('DOMContentLoaded', function() {
        //     var calendarEl = document.getElementById('calendar');
        //     var calendar = new FullCalendar.Calendar(calendarEl, {
        //         initialView: 'resourceTimelineMonth',
        //         duration:4,
        //         resources: [
        //         // your resource list
        //         ]
        //     });
        //     calendar.render();
        //     });
    </script>
@endsection