@extends('layout')
@section('heading')
<h1><a href="/sp" class="ion-arrow-left-a"></a> Form SP {{ $data->no_sp or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-sp">
    <div class="form-group form-message-dark">
        <label for="no_sp" class="col-md-2 control-label">No SP</label>
        <div class="col-md-9">
            <input type="text" class="form-control no_sp" name="no_sp" id="no_sp" value="" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="sp" class="col-md-2 control-label">SP pdf</label>
        <div class="col-md-9">
            <label class="custom-file px-file" for="spi" id="sp">
                <input type="file" id="spi" class="custom-file-input" name="sp" {{ file_exists(public_path().'/storage/sp/'.Request::segment(2).'.pdf')?'':'required' }}>
                <span class="custom-file-control form-control">Choose file...</span>
                <div class="px-file-buttons">
                    <button type="button" class="btn btn-xs px-file-clear">Clear</button>
                    <button type="button" class="btn btn-primary btn-xs px-file-browse">Browse</button>
                </div>
            </label>
        </div>
        @if(file_exists(public_path()."/storage/sp/".Request::segment(2).".pdf"))
        <div class="col-md-1">
            <a href="/download/storage+sp+{{Request::segment(2)}}.pdf" target="_blank"><span class="label label-info pull-lg-right">Download File</span></a>
        </div>
        @endif
    </div>


    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-sp').pxValidate();
        $('#sp').pxFile();
    });
</script>
@endsection