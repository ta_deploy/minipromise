<thead>
    <tr>
        <th rowspan="2" class="valign-middle">NAMA PROJECT</th>
        <th rowspan="2" class="valign-middle">AKTOR</th>
        <th colspan="2" class="text-xs-center">PLAN</th>
        <th colspan="6" class="text-xs-center">REDLINE</th>
        <th colspan="8" class="text-xs-center">REAL</th>
        <th colspan="4" class="text-xs-center">PERSENTASE</th>
        <th colspan="2" class="text-xs-center">SCORE TOC</th>
    </tr>
    <tr>
        <th class="text-xs-center valign-middle">LOP</th>
        <th class="text-xs-center valign-middle">ODP</th>
        <th class="text-xs-center valign-middle">PREP.</th>
        <th class="text-xs-center valign-middle">MAT DEV</th>
        <th class="text-xs-center valign-middle">OGP INST</th>
        <th class="text-xs-center valign-middle">OGP TERM</th>
        <th class="text-xs-center valign-middle">CT&UT</th>
        <th class="text-xs-center valign-middle">FINISH</th>
        <th class="text-xs-center valign-middle">DROP</th>
        <th class="text-xs-center valign-middle">LOP GANTI</th>
        <th class="text-xs-center valign-middle">INST</th>
        <th class="text-xs-center valign-middle">TERM</th>
        <th class="text-xs-center valign-middle">DOC</th>
        <th class="text-xs-center valign-middle">ODP</th>
        <th class="text-xs-center valign-middle">ODP DROP</th>
        <th class="text-xs-center valign-middle">ODP GOLIVE</th>
        <th class="text-xs-center valign-middle">INST (A)</th>
        <th class="text-xs-center valign-middle">ODP (B)</th>
        <th class="text-xs-center valign-middle">DOC (C)</th>
        <th class="text-xs-center valign-middle">PROD (A+B+C)</th>
        <th class="text-xs-center valign-middle">FISIK (D)</th>
        <th class="text-xs-center valign-middle">BERKAS (E)</th>
        <!-- <th class="text-xs-center valign-middle">TOTAL (%)</th> -->
    </tr>
</thead>
<?php
    $total_lop=$odpplan=$unwizing=$delivery=$ogp_instalasi=$ogp_terminasi=$ctut=$finish=$lopdrop=$loppengganti=$finish_instalasi=$finish_terminasi=$odpreal=$odpdrop=$doc=$golive=$p_ins=$p_odp=$p_doc=$p_all=$rows=$totaldoc=$totaldoc=0;
?>
@foreach($data as $no => $isi)
    <?php
        $instalasi = $isi->total_lop?($isi->finish_instalasi+$isi->lopdrop)*100/$isi->total_lop: 0;
        $odp       = $isi->odpplan?($isi->odpreal+$isi->odpdrop)*100/$isi->odpplan             : 0;
        $doc       = $isi->total_lop-$isi->lopdrop;
        $docx      = $doc?$isi->doc*100/$doc                                                   : 0;
        $prod      = $instalasi*0.3+$odp*0.5+$docx*0.2;
        $bg = '';
        if(round($prod)>=0 && round($prod)<60){
            $bg = ' bg-danger';
        }else if(round($prod)>=60 && round($prod)<80){
            $bg = ' bg-warning';
        }else if(round($prod)>=80){
            $bg = ' bg-success';
        }
        $rows++;
        $total_lop        += $isi->total_lop;
        $odpplan          += $isi->odpplan;
        $unwizing         += $isi->unwizing;
        $delivery         += $isi->delivery;
        $ogp_instalasi    += $isi->ogp_instalasi;
        $ogp_terminasi    += $isi->ogp_terminasi;
        $ctut             += $isi->ctut;
        $finish           += $isi->finish;
        $lopdrop          += $isi->lopdrop;
        $loppengganti     += $isi->loppengganti;
        $finish_instalasi += $isi->finish_instalasi;
        $finish_terminasi += $isi->finish_terminasi;
        $odpreal          += $isi->odpreal;
        $odpdrop          += $isi->odpdrop;
        $totaldoc         += $isi->doc;
        $golive           += $isi->golive;
        $p_ins            += $instalasi;
        $p_odp            += $odp;
        $p_doc            += $docx;
        $p_all            += $prod;
        if(Request::segment(2)=='mitra'){
            $aktor = $isi->mitra;
            $label = 1;
        }else if(Request::segment(2)=='sto'){
            $aktor = $isi->sto;
            $label = 2;
        }else if(Request::segment(2)=='tl'){
            $aktor = $isi->tl;
            $label = 3;
        }else{
            $aktor = $isi->waspang_nm;
            $label = 0;
        }
    ?>
    <tr>
        <td class="valign-middle">{{ $isi->nama_project }}</td>
        <td class="valign-middle">{{ $isi->label }}</td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="PLAN_LOP" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->total_lop ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="2" data-col="PLAN_ODP" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->odpplan ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REDLINE_PREP" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->unwizing ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REDLINE_MATDEV" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->delivery?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REDLINE_INSTALASI" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->ogp_instalasi?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REDLINE_TERMINASI" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->ogp_terminasi?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REDLINE_TESTCOMM" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->ctut?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REDLINE_FINISH" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->finish?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REAL_DROP" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->lopdrop ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REAL_PENGGANTI" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->loppengganti ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REAL_INSTALASI" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->finish_instalasi?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REAL_FINISH" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->finish_terminasi?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="1" data-col="REAL_DOC" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->doc ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="2" data-col="REAL_ODP" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->odpreal ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="2" data-col="REAL_ODP_DROP" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->odpdrop ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">
            <span data-toggle="sidebar" data-jenis="2" data-col="REAL_ODP_GOLIVE" data-row="{{ $isi->id }}" data-mitra="{{ $aktor }}" data-label="{{ $label }}" data-id_project="{{ $isi->mpromise_project_id }}" style="cursor: pointer;" data-target="#sidebar-right">{{ $isi->golive ?:'-' }}</span>
        </td>
        <td class="text-xs-center valign-middle">{{ $instalasi?round($instalasi).'%':'-' }}</td>
        <td class="text-xs-center valign-middle">{{ $odp?round($odp).'%':'-' }}</td>
        <td class="text-xs-center valign-middle">{{ $docx?round($docx).'%':'-' }}</td>
        <td class="text-xs-center valign-middle {{ $bg }}">{{ $prod?round($prod).'%':'0%' }}</td>
        <td class="text-xs-center valign-middle">{{ $isi->score_fisik?number_format($isi->score_fisik):'-' }}</td>
        <td class="text-xs-center valign-middle">{{ $isi->score_berkas?number_format($isi->score_berkas):'-' }}</td>
        <!-- <td class="text-xs-center valign-middle">{{ ((($isi->score_fisik+$isi->score_berkas)/2)/$isi->total_lop*100)?round((($isi->score_fisik+$isi->score_berkas)/2)/$isi->total_lop*100,2).'%':'-' }}</td> -->
    </tr>
@endforeach
<?php
// dd($rows);
    if($rows){
        $p_ins=$p_ins/$rows;
        $p_odp=$p_odp/$rows;
        $p_doc=$p_doc/$rows;
        $p_all=$p_all/$rows;

    }
    if(round($p_all)>=0 && round($p_all)<60){
        $bg = ' bg-danger';
    }else if(round($p_all)>=60 && round($p_all)<80){
        $bg = ' bg-warning';
    }else if(round($p_all)>=80){
        $bg = ' bg-success';
    }
?>
<tr>
    <td class="valign-middle bg-info" colspan="2"><b>TOTAL</b></td>
    <td class="text-xs-center valign-middle bg-info">{{ $total_lop ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $odpplan ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $unwizing ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $delivery ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $ogp_instalasi ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $ogp_terminasi ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $ctut ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $finish ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $lopdrop ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $loppengganti ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $finish_instalasi ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $finish_terminasi ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $totaldoc ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $odpreal ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $odpdrop ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $golive ?:'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $p_ins?round($p_ins).'%':'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $p_odp?round($p_odp).'%':'-' }}</td>
    <td class="text-xs-center valign-middle bg-info">{{ $p_doc?round($p_doc).'%':'-' }}</td>
    <td class="text-xs-center valign-middle {{ $bg }}">{{ $p_all?round($p_all).'%':'0%' }}</td>
    <td colspan="2" class="text-xs-center valign-middle bg-info">-</td>
</tr>