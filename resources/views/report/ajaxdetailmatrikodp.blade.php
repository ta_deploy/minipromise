<?php
  $noproyek = $noodp = 0;
?>
@foreach($dataarray as $no => $d)
    <div class="col-md-12">
        <!-- Counters -->
        <div class="panel panel-info panel-dark widget-profile">
          <div class="panel-heading">

            <span class="label label-default pull-right">{{ $d->pid?:'BLM ADA PID' }}</span>
            <h3 class="widget-profile-header">
              {{ $d->nama_tematik }}-{{ $d->nama_mitra }}-{{ $d->step }}
              <a href="#" class="widget-profile-secondary-text">{{ $d->nama_lop }}</a>
            </h3>
            <i class="widget-profile-bg-icon font-size-52">{{ ++$noproyek }}</i>
          </div>
          @foreach($d->list as $l)
          <?php
            if($l['Real_Odp_Nama']){
              if($l['GOLIVE']){
                $status = "<span class='label label-success'>SUDAH GOLIVE</span>";
              }else{
                $status = "<span class='label label-warning'>BELUM GOLIVE</span>";
              }
            }else{
              $status = "<span class='label label-danger'>BELUM UPDATE TOMMAN</span>";
            }
          ?>
          <div class="widget-profile-counters">
            <span class="col-xs-4">
              {{ ++$noodp }}. {{ $l['Plan_Odp_Nama'] }}
            </span>
            <span class="col-xs-4">
              {{ $l['Real_Odp_Nama'] }}
            </span>
            <span class="col-xs-4">
              {!! $status !!}
            </span>
          </div>
          @endforeach
        </div>
    </div>
@endforeach