@extends('layout')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Kurva</span>
</h1>
@endsection
@section('title', 'Kurva')
@section('content')
    <div id="overview-chart" style="height: 250px"></div>
@endsection

@section('js')
<script>
    $(function() {
        new Morris.Line({
          element:        'overview-chart',
          data:           [{"baris":0,"ItemDate":"2022-05-27"},
{"baris":1,"ItemDate":"2022-05-28"},
{"baris":14,"ItemDate":"2022-06-01"},
{"baris":15,"ItemDate":"2022-06-21"},
{"baris":19,"ItemDate":"2022-06-23"},
{"baris":20,"ItemDate":"2022-06-26"},
{"baris":21,"ItemDate":"2022-08-01"},
{"baris":212,"ItemDate":"2022-08-10"},
{"baris":213,"ItemDate":"2022-08-11"},
{"baris":214,"ItemDate":"2022-08-19"},
{"baris":215,"ItemDate":"2022-08-28"},
{"baris":null,"ItemDate":"2022-10-18"}
],
          xkey:           'ItemDate',
          ykeys:          ['baris'],
          labels:          ['baris'],
          grid:false,
          xLabelFormat: function (d) {
            return ("0" + d.getDate()).slice(-2) + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + d.getFullYear();
          },
        });
    });
</script>
@endsection
