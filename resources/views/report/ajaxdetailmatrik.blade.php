@foreach($data as $no => $d)
    <div class="col-md-6">
        <!-- Counters -->
        <div class="panel panel-info panel-dark widget-profile">
          <div class="panel-heading">

            <span class="label label-default pull-right">{{ $d->pid?:'BLM ADA PID' }}</span>
            <h3 class="widget-profile-header">
              {{ $d->nama_tematik }}
              <a href="#" class="widget-profile-secondary-text">{{ $d->nama_lop }}</a>
            </h3>
            <i class="widget-profile-bg-icon font-size-52">{{ ++$no }}</i>
          </div>
          <div class="widget-profile-counters">
            <span class="col-xs-6">
              <span class="widget-profile-counter">{{ $d->nama_mitra }}</span>
            </span>
            <span class="col-xs-6">
              <span class="widget-profile-counter">-</span>
              <span class="badge badge-warning">{{ $d->step }}</span>
            </span>
          </div>
          <div class="widget-profile-counters">
            <span class="col-xs-6">
              <span class="widget-profile-counter">{{ number_format($d->score_fisik) }}</span>
              Score Fisik
            </span>
            <span class="col-xs-6">
              <span class="widget-profile-counter">{{ number_format($d->score_berkas) }}</span>
              Score Berkas
            </span>
          </div>
          
          <div class="panel-footer">
            <span class="btn btn-sm btn-info detail" data-lopid="{{ $d->id_lop }}" data-lopnama="{{ $d->nama_lop }}"><i class="ion-information"></i>&nbsp;&nbsp;Detail</span> 
            @if($d->step_id==4)
            <span class="btn btn-sm btn-primary plan" data-lopid="{{ $d->id_lop }}" data-lopnama="{{ $d->nama_lop }}"><i class="ion ion-plus pull"></i>&nbsp;&nbsp;Plan</span>
            @endif
            @if(session('auth')->promise_level!=3)
            <a href="/progress/{{ $d->id_lop }}" class="btn btn-sm btn-success pull-right"><i class="ion ion-edit"></i>&nbsp;&nbsp;Update</a>
            @endif
        </div>
        </div>
    </div>
@endforeach