@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-6">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Matrik Progress {{ Request::segment(2) }}</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="hidden" name="jns" value="{{ Request::segment(2) }}" id="jns"/>
        <select type="text" name="tematik" class=" pull-right" id="filter" multiple></select>
    </div>
    <div class="col-md-3">
        <input type="text" name="tl" value="all" class=" pull-right" id="filter2"/>
    </div>
</div>
@endsection
@section('title', 'BTS || Report')
@section('content')

<div class="px-sidebar-right" id="sidebar-right" data-width="600">
  <div class="px-sidebar-content">
    <div class="p-a-4">
      <h3 class="m-t-0 "><i class="tittle"></i><button type="button" class="btn pull-right" data-toggle="sidebar" data-target="#sidebar-right">Close</button></h3>
      <div id="cnt"></div>

    </div>
  </div>
</div>
<div class="panel" id="perfect-scrollbar-scroll-x">
  <div class="panel-body">
    <div class="table-responsive table-primary">
    <table class="table" id="myTable">

    </table>
</div>
</div>
</div>
@endsection

@section('js')
<script type="text/javascript">
$(function() {
    $('span[data-toggle=sidebar]').click(function(){
        $('.tittle').html(this.dataset.col+'/'+this.dataset.mitra)
        var id = $('#filter').val()?$('#filter').val():'0';
        $.get('/ajaxDetailMatrik/'+this.dataset.jenis+'/'+id+'/'+this.dataset.col+'/'+this.dataset.row+'/1', function(r){
            $('#cnt').html(r);
        });

    });
    var filter = $('#filter').select2({
        data: [{'id':0,'text':'Semua Tematik'}]
    });
    filter.select2({
        data : <?= json_encode($tematik); ?>,
        placeholder:'Select Tematik'
    });
    $('#filter').change(function(){
      var isi = $('#filter').val();
      isi = $.map(isi, function(x){ return isNaN(parseInt(x) ) ? 0 : parseInt(x) });

      if(isi.length > 0){
        if(isi.length > 1 && isi.indexOf(0) != -1){
          isi = $.grep(isi, function(x){ return x != 0});
          $('#filter').val(isi).change();
        }

        isi = JSON.stringify(isi);
        $.get("/getMatrikMitraAjax/"+$('#jns').val()+"/"+isi+"/"+$('#filter2').val(), function(result){
          $("table").html(result);
          $('span[data-toggle=sidebar]').click(function(){
            console.log(this.dataset)
            $('.tittle').html(this.dataset.col+'/'+this.dataset.mitra);
            $.get('/ajaxDetailMatrik/'+this.dataset.jenis+'/'+this.dataset.id_project+'/'+this.dataset.col+'/'+ (this.dataset.row || 'all') +'/'+this.dataset.label+'/'+$('#filter2').val(), function(r){
              $('#cnt').html(r);
            });
          });
        });
      }
    });

    var filter2 = $('#filter2').select2({
        data: [{'id':0,'text':'Semua TL'}]
    });
    filter2.select2({
        data : $.merge([{'id':'all','text':'Semua TL'}],<?= json_encode($tl); ?>),
        placeholder:'Select TL'
    });

    $('#filter2').change(function(){
      let isi = $('#filter').val();
      isi = $.map(isi, function(x){ return isNaN(parseInt(x) ) ? 0 : parseInt(x) });
      isi = JSON.stringify(isi.length != 0 ? isi : [0]);

      $.get("/getMatrikMitraAjax/"+$('#jns').val()+"/"+isi+"/"+this.value, function(result){
        $("table").html(result);
        $('span[data-toggle=sidebar]').click(function(){
              $('.tittle').html(this.dataset.col+'/'+this.dataset.mitra);
              $.get('/ajaxDetailMatrik/'+this.dataset.jenis+'/'+this.dataset.id_project+'/'+this.dataset.col+'/'+ (this.dataset.row || 'all') +'/'+this.dataset.label+'/'+$('#filter2').val(), function(r){
                  $('#cnt').html(r);
              });
          });
      });

    });
    $('#perfect-scrollbar-scroll-x').perfectScrollbar({
      suppressScrollY: true
    });

    $(window).on('resize', function() {
      $('#perfect-scrollbar-scroll-x').perfectScrollbar('update');
    });
    function sort(){
        var table, rows, switching, i, x, y, shouldSwitch;
        table = document.getElementById("myTable");
        switching = true;
        while (switching) {
            switching = false;
            rows = table.rows;
            for (i = 2; i < (rows.length -2); i++) {
              shouldSwitch = false;
              if(rows[i].getElementsByTagName("td").length==13){
                td = 12;
              }else{
                td = 20;
              }
              x = rows[i].getElementsByTagName("td")[td];
              y = rows[i + 1].getElementsByTagName("td")[td];
              if (parseInt(x.innerHTML) < parseInt(y.innerHTML)) {
                shouldSwitch = true;
                break;
              }
            }
            if (shouldSwitch) {
              rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
              switching = true;
            }
        }
    }
});
</script>
@endsection
