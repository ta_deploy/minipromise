@extends('layout')
@section('heading')
<h1>Monitoring Laporan Asset</h1>
@endsection
@section('content')  

<?php
$row = 1;
$count = 1;
?>
@foreach($teknisi as $no => $t)

@if($row == 1)
<!-- buka -->
<div class="row box-examples">
    @endif
    <div class="col-md-6">
        <div class="panel box">
            <div class="box-row">
                <div class="box-container">
                    <div class="box-cell p-a-1 valign-middle bg-primary">
                        <div class="font-size-17">#{{ ++$no }}. {{ $t->nama_pemakai }}</div>
                    </div>
                </div> <!-- / .box-container -->
            </div>

            <div class="box-row">
                <div class="box-container text-xs-center">
                    @foreach($asset as $a)
                    @if($t->nik_pemakai == $a->nik_pemakai)
                    <div class="box-row valign-middle">
                        <div class="box-cell p-y-1 b-r-1">
                            <div><strong>{{ $a->jenis_barang }}</strong></div>
                        </div>
                        <?php
                        $flag = 1;
                        ?>
                        @foreach($laporan as $l)
                        @if($a->serial_number == $l->sn)
                        <div class="box-cell p-y-1 b-r-1">
                            <div><strong>{{ $l->keluhan }}</strong><span class="label label-info label-sm">{{ $l->status }}</span></div>
                        </div>

                        <div class="box-cell p-y-1">
                            <div><strong>Ev</strong></div>
                        </div>
                        <?php
                        $flag = 0;
                        ?>
                        @endif
                        @endforeach
                        @if($flag)
                        <div class="box-cell p-y-1 b-r-1">
                            <div><span class="label label-danger label-sm">Belum Lapor</span></div>
                        </div>

                        <div class="box-cell p-y-1">
                            <div><strong>Ev</strong></div>
                        </div>
                        @endif
                    </div>
                    @endif
                    @endforeach
                </div> <!-- / .box-container -->
            </div>
        </div>
    </div>
    @if($row == 2 || $count == count($teknisi) )
    <!-- buka -->
</div>
<?php
$row=0;
?>
@endif
<?php
$row++;
$count++;
?>
@endforeach
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(function() {
        $('.box-examples').each(function() {
            var maxHeight = 0;

            $(this)
            .find('.box')
            .each(function() {
                var height = $(this).height();

                if (height > maxHeight) {
                    maxHeight = height;
                }
            })
            .height(maxHeight);
        });
    });
</script>
@endsection