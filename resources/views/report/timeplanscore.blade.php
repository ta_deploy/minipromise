@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-9">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Timeplan Score</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="tematik" class=" pull-right" id="filter"/>
    </div>  
</div>
@endsection
@section('title', 'Report')
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary">
    <table class="table" id="myTable">
        <thead>
            <tr>
                <th rowspan="2" class="valign-middle text-xs-center">WASPANG</th>
                <th rowspan="2" class="valign-middle text-xs-center">TOTAL LOP</th>
                <th rowspan="2" class="valign-middle text-xs-center">OGP</th>
                <th colspan="2" class="text-xs-center">PASS</th>
                <th colspan="2" class="text-xs-center">FAIL</th>
                <th colspan="3" class="text-xs-center">SCORE x NILAI LOP</th>
            </tr>
            <tr>
                <th class="text-xs-center">FISIK</th>
                <th class="text-xs-center">BERKAS</th>
                <th class="text-xs-center">FISIK</th>
                <th class="text-xs-center">BERKAS</th>
                <th class="text-xs-center">FISIK (F)</th>
                <th class="text-xs-center">BERKAS (B)</th>
                <th class="text-xs-center">F+B</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>
</div>
</div>
@endsection

@section('js')
@endsection
