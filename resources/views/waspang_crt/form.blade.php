@extends('layout')
@section('title', 'BTS || Waspang')
@section('heading')
<h1><a href="/waspang" class="ion-arrow-left-a"></a> Form Waspang {{ $data->nama or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-waspang">
    <div class="form-group form-message-dark">
        <label for="nik" class="col-md-2 control-label">Nik Waspang</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="nik" id="nik" value="{{ $data->nik or '' }}" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama" class="col-md-2 control-label">Nama Waspang</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="nama" id="nama" value="{{ $data->nama or '' }}" placeholder="Panjang huruf 75" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="label" class="col-md-2 control-label">Label</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="label" id="label" value="{{ $data->label or '' }}" placeholder="5 huruf untuk show chart" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nik_tl" class="col-md-2 control-label">Nik TL</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="nik_tl" id="nik_tl" value="{{ $data->nik_tl or '' }}" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="nama_tl" class="col-md-2 control-label">Label TL</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="nama_tl" id="nama_tl" value="{{ $data->nama_tl or '' }}" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="waspang_witel" class="col-md-2 control-label">Witel</label>
        <div class="col-md-9">
            <input type="text" class="form-control" name="waspang_witel" id="waspang_witel" value="{{ $data->waspang_witel or '' }}" autocomplete="off" required>
        </div>
    </div>
    <div class="form-group form-message-dark">
        <label for="status" class="col-md-2 control-label">Status</label>
        <div class="col-md-9">
            <input type="text" class="form-control status" name="status" id="status" value="{{ $data->status or '' }}" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-waspang').pxValidate();
        $('#status').select2({
            data:[{"id":"1","text":"Aktif"},{"id":"0","text":"Tidak Aktif"}],
            placeholder:"pilih status"
        });
        
        $('#waspang_witel').select2({
            data:<?=  json_encode($witel) ?>,
            placeholder:"pilih witel"
        });
    });
</script>
@endsection