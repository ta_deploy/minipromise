@extends('layout')
@section('heading')
<h1> <h1><span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard</span></h1></h1>
@endsection
@section('css')
<style type="text/css">
	tr > th {
		text-align: center;
	}
</style>
@endsection
@section('title', 'Dashboard')
@section('content')
<div class="table-primary">
	<div class="table-responsive">
		<table class="table table-striped table-bordered" id="datatables" style="width: 100%;">
			<thead>
				<tr>
					<th rowspan="2">No</th>
					<th rowspan="2">Portofolio</th>
					<th colspan="4">Fase</th>
					<th rowspan="2">Total</th>
				</tr>
				<tr>
					
					<th>Initiation</th> 
					<th>Planning</th>
					<th>Executing</th>
					<th>Closing</th>
				</tr>
				<tbody>
					@foreach($data as $no => $data)
					<tr>
						<td>{{ ++$no }}</td>
						<td>{{ $data->Portofolio }}</td>
						<td><a href="/dashboard/{{$data->Portofolio}}/Initiation">{{ $data->fase_initiation }}</a></td>
						<td><a href="/dashboard/{{$data->Portofolio}}/Planning">{{ $data->fase_planning }}</a></td>
						<td><a href="/dashboard/{{$data->Portofolio}}/Executing">{{ $data->fase_executing }}</a></td>
						<td><a href="/dashboard/{{$data->Portofolio}}/Closing">{{ $data->fase_closing }}</a></td>
						<td><a href="/dashboard/{{$data->Portofolio}}/0">{{ $data->Baris }}</a></td>
					</tr>
					@endforeach
				</tbody>
			</thead> 
		</table>
	</div>
</div>
@endsection
@section('js')
<script>
	$(function() {
		$('#datatables').dataTable();
		$('#datatables_wrapper .table-caption').text('Proaktif');
		$('#datatables_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
	});
</script>
@endsection