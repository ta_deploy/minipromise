@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-9">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="witel" value="all" class=" pull-right" id="witel"/>
    </div>
</div>
    
@endsection
@section('title', 'Matrik')
@section('content')

<div class="row" id="chartsHolder"></div>

@endsection

@section('js')
<script>
    $(function() {
        var data = <?= json_encode($data) ?>;
        var lp = <?= json_encode($lastprogress) ?>;
        $.each( data, function( key, value ) {
          $("#chartsHolder").append("<div class='col-md-6'><div class='panel panel-primary'><div class='panel-heading'><span class='panel-title'>"+key+"</span><div class='panel-heading-icon'><i class='fa fa-inbox'>"+lp[key]+"%</i></div></div><div class='panel-body'><div id='chart"+key.replace("#", "-")+"'></div></div></div></div>");
          var chart = c3.generate({
            bindto: "#chart" + key.replace("#", "-"),
            color: { pattern: [ '#2596be', '#00ff00' ] },
            data: {
              type: 'spline',
              x:'label',
              columns: value,
            },
            axis: {
                x: {
                    type: 'category'
                }
            },
            grid: {
                lines: {
                  front: false
              }
            },
          });
        });
        $('#witel').select2({
            data : $.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($witel) ?>),
            placeholder:'Select Witel'
        }).change(function(){
            $("#chartsHolder").html('');
            $.getJSON("/ajaxdashboardmitratel/"+$('#witel').val(), function(result){
                $.each(result.data, function(key, value){
                    $("#chartsHolder").append("<div class='col-md-6'><div class='panel panel-primary'><div class='panel-heading'><span class='panel-title'>"+key+"</span><div class='panel-heading-icon'><i class='fa fa-inbox'>"+result.lastprogress[key]+"%</i></div></div><div class='panel-body'><div id='chart"+key.replace("#", "-")+"'></div></div></div></div>");
                    var chart = c3.generate({
                    bindto: "#chart" + key.replace("#", "-"),
                    color: { pattern: [ '#2596be', '#00ff00' ] },
                    data: {
                      type: 'spline',
                      x:'label',
                      columns: value,
                    },
                    axis: {
                        x: {
                            type: 'category'
                        }
                    },
                    grid: {
                        lines: {
                          front: false
                      }
                    },
                  });
                });
            });
        });

    });
</script>
@endsection
