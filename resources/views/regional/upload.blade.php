@extends('layout')
@section('heading')
<h1>Form COPAS</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-waspang">
    <div class="form-group form-message-dark">
        <label for="paste" class="col-md-2 control-label">PASTE</label>
        <div class="col-md-9">
            <textarea class="form-control" name="paste" id="paste" placeholder="Paste sesuai format" autocomplete="off" rows="20"></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        
    });
</script>
@endsection