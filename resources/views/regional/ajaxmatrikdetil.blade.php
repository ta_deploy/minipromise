<table class="table" id="datatables">
    <thead>
        <tr>
            <th>#</th>
            <th>WO</th>
            <th>Ring</th>
            <th>Span</th>
            <th>Waspang</th>
            <th>Aanwijzing</th>
            <th>Matdev</th>
            <th>Tanam Tiang</th>
            <th>Gelar Kabel</th>
            <th>Pasang Terminal</th>
            <th>Terminasi</th>
            <th>Test Comm</th>
            <th>RFS</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $no => $d)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $d->nama_project }}</td>
                <td>{{ $d->ring }}</td>
                <td>{{ $d->nama_lop }}</td>
                <td>{{ $d->waspang_nm }}</td>
                <td>{{ $d->Done_Aanwijzing ?:'-' }}</td>
                <td>{{ $d->Done_Pengiriman_Material ?:'-' }}</td>
                <td>{{ $d->Done_Tanam_Tiang ?:'-' }}</td>
                <td>{{ $d->Done_Gelar_Kabel ?:'-' }}</td>
                <td>{{ $d->Done_Pasang_Terminal ?:'-' }}</td>
                <td>{{ $d->Done_Terminasi ?:'-' }}</td>
                <td>{{ $d->ba_ct ?:'-' }}</td>
                <td>{{ $d->TGL_GO_LIVE ?:'-' }}</td>
            </tr>
        @endforeach
    </tbody>
</table>