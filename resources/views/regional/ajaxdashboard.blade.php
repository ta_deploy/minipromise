<table class="table" id="datatables">
    <thead>
        <tr>
            <th>#</th>
            <th>WO</th>
            <th>Witel</th>
            <th>Ring</th>
            <th>Span</th>
            <th>Aanwijzing</th>
            <th>Perizinan</th>
            <th>Pragelaran</th>
            <th>Tanam Tiang</th>
            <th>Gelar Kabel</th>
            <th>Pasang Terminal</th>
            <th>Terminasi</th>
            <th>Test Comm</th>
            <th>Acc Test</th>
            <th>RFS</th>
            <th>UT</th>
            <th>Rekon</th>
            <th>BAST</th>
            <th>Finish</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $T1=$T2=$T3=$T4=$T5=$T6=$T7=$T8=$T9=$T10=$T11=$T12=$T13=$T14=$T15=0;
        ?>
        @foreach($data as $no => $d)
            <?php
                $T1+=$d->BARIS;
                $T2+=$d->AANWIJZING;
                $T3+=$d->PERIZINAN;
                $T4+=$d->PRAGELARAN;
                $T5+=$d->TANAM_TIANG;
                $T6+=$d->GELAR_KABEL;
                $T7+=$d->PASANG_TERMINAL;
                $T8+=$d->TERMINASI;
                $T9+=$d->TEST_COMM;
                $T10+=$d->ACC_TEST;
                $T11+=$d->RFS;
                $T12+=$d->UT;
                $T13+=$d->REKON;
                $T14+=$d->BAST;
                $T15+=$d->FINISH;
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $d->WO }}</td>
                <td>{{ $d->WITEL }}</td>
                <td>{{ $d->RING }}</td>
                <td data-toggle="modal" data-col="ALL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->BARIS }}</td>
                <td data-toggle="modal" data-col="AANWIJZING" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->AANWIJZING ?:'-' }}</td>
                <td data-toggle="modal" data-col="PERIZINAN" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->PERIZINAN ?:'-' }}</td>
                <td data-toggle="modal" data-col="PRAGELARAN" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->PRAGELARAN ?:'-' }}</td>
                <td data-toggle="modal" data-col="TANAM_TIANG" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->TANAM_TIANG ?:'-' }}</td>
                <td data-toggle="modal" data-col="GELAR_KABEL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->GELAR_KABEL ?:'-' }}</td>
                <td data-toggle="modal" data-col="PASANG_TERMINAL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->PASANG_TERMINAL ?:'-' }}</td>
                <td data-toggle="modal" data-col="TERMINASI" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->TERMINASI ?:'-' }}</td>
                <td data-toggle="modal" data-col="TEST_COMM" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->TEST_COMM ?:'-' }}</td>
                <td data-toggle="modal" data-col="ACC_TEST" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->ACC_TEST ?:'-' }}</td>
                <td data-toggle="modal" data-col="RFS" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->RFS ?:'-' }}</td>
                <td data-toggle="modal" data-col="UT" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->UT ?:'-' }}</td>
                <td data-toggle="modal" data-col="REKON" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->REKON ?:'-' }}</td>
                <td data-toggle="modal" data-col="BAST" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->BAST ?:'-' }}</td>
                <td data-toggle="modal" data-col="FINISH" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->FINISH ?:'-' }}</td>
            </tr>
        @endforeach

            <tr>
                <td colspan="4">TOTAL</td>
                <td data-toggle="modal" data-col="ALL" data-row="ALL" data-target="#modaldetail">{{ $T1 }}</td>
                <td data-toggle="modal" data-col="AANWIJZING" data-row="ALL" data-target="#modaldetail">{{ $T2 ?:'-' }}</td>
                <td data-toggle="modal" data-col="PERIZINAN" data-row="ALL" data-target="#modaldetail">{{ $T3 ?:'-' }}</td>
                <td data-toggle="modal" data-col="PRAGELARAN" data-row="ALL" data-target="#modaldetail">{{ $T4 ?:'-' }}</td>
                <td data-toggle="modal" data-col="TANAM_TIANG" data-row="ALL" data-target="#modaldetail">{{ $T5 ?:'-' }}</td>
                <td data-toggle="modal" data-col="GELAR_KABEL" data-row="ALL" data-target="#modaldetail">{{ $T6 ?:'-' }}</td>
                <td data-toggle="modal" data-col="PASANG_TERMINAL" data-row="ALL" data-target="#modaldetail">{{ $T7 ?:'-' }}</td>
                <td data-toggle="modal" data-col="TERMINASI" data-row="ALL" data-target="#modaldetail">{{ $T8 ?:'-' }}</td>
                <td data-toggle="modal" data-col="TEST_COMM" data-row="ALL" data-target="#modaldetail">{{ $T9 ?:'-' }}</td>
                <td data-toggle="modal" data-col="ACC_TEST" data-row="ALL" data-target="#modaldetail">{{ $T10 ?:'-' }}</td>
                <td data-toggle="modal" data-col="RFS" data-row="ALL" data-target="#modaldetail">{{ $T11 ?:'-' }}</td>
                <td data-toggle="modal" data-col="UT" data-row="ALL" data-target="#modaldetail">{{ $T12 ?:'-' }}</td>
                <td data-toggle="modal" data-col="REKON" data-row="ALL" data-target="#modaldetail">{{ $T13 ?:'-' }}</td>
                <td data-toggle="modal" data-col="BAST" data-row="ALL" data-target="#modaldetail">{{ $T14 ?:'-' }}</td>
                <td data-toggle="modal" data-col="FINISH" data-row="ALL" data-target="#modaldetail">{{ $T15 ?:'-' }}</td>
            </tr>
    </tbody>
</table>