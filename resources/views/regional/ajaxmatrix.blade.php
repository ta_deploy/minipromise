
    <div class="table-responsive table-primary">
        <div class="table-header">
        <div class="table-caption">
          Progress Matrix
        </div>
      </div>
      <table class="table table-bordered" id="datatables">
    <thead>
        <tr>
            <th>#</th>
            <th>Witel</th>
            <th>Ring</th>
            <th>Span</th>
            <th>Aanwijzing</th>
            <th>Perizinan</th>
            <th>Delivery Material</th>
            <th>Tanam Tiang</th>
            <th>Gelar Kabel</th>
            <th>Pasang Terminal</th>
            <th>Terminasi</th>
            <th>Test Comm</th>
            <th>RFS</th>
            <th>UT</th>
            <th>Rekon</th>
            <th>BAST</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $T1=$T2=$T3=$T4=$T5=$T6=$T7=$T8=$T9=$T10=$T11=$T12=$T13=$T14=$T15=0;
        ?>
        @foreach($data as $no => $d)
            <?php
                $T1+=$d->BARIS;
                $T2+=$d->AANWIJZING;
                $T3+=$d->PERIZINAN;
                $T4+=$d->DELIVERY_MATERIAL;
                $T5+=$d->TANAM_TIANG;
                $T6+=$d->GELAR_KABEL;
                $T7+=$d->PASANG_TERMINAL;
                $T8+=$d->TERMINASI;
                $T9+=$d->TEST_COMM;
                $T11+=$d->RFS;
                $T12+=$d->UT;
                $T13+=$d->REKON;
                $T14+=$d->BAST;
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $d->WITEL }}</td>
                <td>{{ $d->RING }}</td>
                <td data-toggle="modal" data-col="ALL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->BARIS }}</td>
                <td data-toggle="modal" data-col="AANWIJZING" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->AANWIJZING ?:'-' }}</td>
                <td data-toggle="modal" data-col="PERIZINAN" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->PERIZINAN ?:'-' }}</td>
                <td data-toggle="modal" data-col="DELIVERY_MATERIAL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->DELIVERY_MATERIAL ?:'-' }}</td>
                <td data-toggle="modal" data-col="TANAM_TIANG" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->TANAM_TIANG ?:'-' }}</td>
                <td data-toggle="modal" data-col="GELAR_KABEL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->GELAR_KABEL ?:'-' }}</td>
                <td data-toggle="modal" data-col="PASANG_TERMINAL" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->PASANG_TERMINAL ?:'-' }}</td>
                <td data-toggle="modal" data-col="TERMINASI" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->TERMINASI ?:'-' }}</td>
                <td data-toggle="modal" data-col="TEST_COMM" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->TEST_COMM ?:'-' }}</td>
                <td data-toggle="modal" data-col="RFS" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->RFS ?:'-' }}</td>
                <td data-toggle="modal" data-col="UT" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->UT ?:'-' }}</td>
                <td data-toggle="modal" data-col="REKON" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->REKON ?:'-' }}</td>
                <td data-toggle="modal" data-col="BAST" data-row="{{ $d->RING }}" data-target="#modaldetail">{{ $d->BAST ?:'-' }}</td>
            </tr>
        @endforeach

            <tr>
                <td colspan="3">TOTAL</td>
                <td data-toggle="modal" data-col="ALL" data-row="ALL" data-target="#modaldetail">{{ $T1 }}</td>
                <td data-toggle="modal" data-col="AANWIJZING" data-row="ALL" data-target="#modaldetail">{{ $T2 ?:'-' }}</td>
                <td data-toggle="modal" data-col="PERIZINAN" data-row="ALL" data-target="#modaldetail">{{ $T3 ?:'-' }}</td>
                <td data-toggle="modal" data-col="DELIVERY_MATERIAL" data-row="ALL" data-target="#modaldetail">{{ $T4 ?:'-' }}</td>
                <td data-toggle="modal" data-col="TANAM_TIANG" data-row="ALL" data-target="#modaldetail">{{ $T5 ?:'-' }}</td>
                <td data-toggle="modal" data-col="GELAR_KABEL" data-row="ALL" data-target="#modaldetail">{{ $T6 ?:'-' }}</td>
                <td data-toggle="modal" data-col="PASANG_TERMINAL" data-row="ALL" data-target="#modaldetail">{{ $T7 ?:'-' }}</td>
                <td data-toggle="modal" data-col="TERMINASI" data-row="ALL" data-target="#modaldetail">{{ $T8 ?:'-' }}</td>
                <td data-toggle="modal" data-col="TEST_COMM" data-row="ALL" data-target="#modaldetail">{{ $T9 ?:'-' }}</td>
                <td data-toggle="modal" data-col="RFS" data-row="ALL" data-target="#modaldetail">{{ $T11 ?:'-' }}</td>
                <td data-toggle="modal" data-col="UT" data-row="ALL" data-target="#modaldetail">{{ $T12 ?:'-' }}</td>
                <td data-toggle="modal" data-col="REKON" data-row="ALL" data-target="#modaldetail">{{ $T13 ?:'-' }}</td>
                <td data-toggle="modal" data-col="BAST" data-row="ALL" data-target="#modaldetail">{{ $T14 ?:'-' }}</td>
            </tr>
    </tbody>
</table>
</div>
</div>
@if($wo!='all')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-primary">
      <div class="panel-heading">
        <span class="panel-title">Kurva-S</span>
        <div class="panel-heading-icon">{{ $lastprogress }}%</div>
      </div>
      <div class="panel-body">
        <div id="kurva" id="kurva" style="height: 200px"></div>
      </div>
    </div>
  </div>
</div>
@endif
<div class="row">
  <!-- <div class="col-md-6">
    <div class="table-primary">
      <div class="table-header">
        <div class="table-caption">
          Material On-Site
        </div>
      </div>
    <table class="table table-bordered" id="datatables">
    <thead>
        <tr>
            <th>#</th>
            <th>Span</th>
            <th>KU</th>
            <th>TN</th>
            <th>TE</th>
            <th>ODC</th>
            <th>ODP</th>
            <th>OTB</th>
            <th>%</th>
        </tr>
    </thead>
    <tbody>


            <tr>
                <td></td>
                <td></td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
                <td>-</td>
            </tr>
    </tbody>
</table>
</div>
  </div> -->
  <div class="col-md-12">
    <div class="table-primary">
      <div class="table-header">
        <div class="table-caption">
          Material
        </div>
      </div>
    <table class="table table-bordered" id="datatables">
    <thead>
        <tr>
            <th rowspan="2" class="text-xs-center valign-middle">#</th>
            <th rowspan="2" class="text-xs-center valign-middle">Span</th>
            <th colspan="3" class="text-xs-center valign-middle">KABEL</th>
            <th colspan="3" class="text-xs-center valign-middle">TIANG</th>
            <th colspan="3" class="text-xs-center valign-middle">ODC</th>
            <th colspan="3" class="text-xs-center valign-middle">ODP/OTB</th>
            <th rowspan="2" class="text-xs-center valign-middle">%H-1</th>
            <th rowspan="2" class="text-xs-center valign-middle">%HI</th>
            <th rowspan="2" class="text-xs-center valign-middle">%DEV</th>
        </tr>
        <tr>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
        </tr>
    </thead>
    <tbody>

        @foreach($mtr as $no => $m)
            <?php
                $dev = $m->ACH_HI-$m->ACH_HM1;
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $m->RING }}-{{ $m->SPAN }}</td>
                <td class="text-xs-right">{{ number_format($m->KABEL_PLAN)?:'-' }}</td>
                <td class="text-xs-right">{{ number_format($m->KABEL_REAL)?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->KABEL_ACH*0.01}});color:{{ $m->KABEL_ACH>40?'white':'' }};">{{ number_format($m->KABEL_ACH) }}%</td>
                <td class="text-xs-right">{{ $m->TIANG_PLAN?:'-' }}</td>
                <td class="text-xs-right">{{ $m->TIANG_REAL?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->TIANG_ACH*0.01}});color:{{ $m->TIANG_ACH>40?'white':'' }};">{{ number_format($m->TIANG_ACH) }}%</td>
                <td class="text-xs-right">{{ $m->ODC_PLAN?:'-' }}</td>
                <td class="text-xs-right">{{ $m->ODC_REAL?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ODC_ACH*0.01}});color:{{ $m->ODC_ACH>40?'white':'' }};">{{ number_format($m->ODC_ACH) }}%</td>
                <td class="text-xs-right">{{ $m->ODP_PLAN?:'-' }}</td>
                <td class="text-xs-right">{{ $m->ODP_REAL?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ODP_ACH*0.01}});color:{{ $m->ODP_ACH>40?'white':'' }};">{{ number_format($m->ODP_ACH) }}%</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ACH_HI*0.01}});color:{{ $m->ACH_HI>40?'white':'' }};">{{ number_format($m->ACH_HM1) }}%</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ACH_HI*0.01}});color:{{ $m->ACH_HI>40?'white':'' }};">{{ number_format($m->ACH_HI) }}%</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$dev*0.01}});color:{{ $dev>40?'white':'' }};">{{ number_format($dev) }}%</td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
  </div>
</div>
<script type="text/javascript">
    $(function() {
        var chart = c3.generate({
            bindto: "#kurva",
            color: { pattern: [ '#2596be', '#00ff00' ] },
            data: {
              type: 'spline',
              x:'label',
              columns: <?= json_encode($kurva); ?>,
            },
            axis: {
                x: {
                    type: 'category'
                }
            },
            grid: {
                lines: {
                  front: false
              }
            },
          });
    });
</script>