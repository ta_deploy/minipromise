@extends('layout')
@section('heading')
<h1>Form Plan {{ $wo }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-waspang">
    <div class="form-group form-message-dark">
        <label for="week" class="col-md-1 control-label">Minggu</label>
        <div class="col-md-2">
            <input type="text" class="form-control" name="week" id="week" placeholder="WeekYear" autocomplete="off" required value="{{ old('week') }}">
        </div>
        <label for="persen" class="col-md-1 control-label">Persentase</label>
        <div class="col-md-1">
            <input type="text" class="form-control" name="persen" id="persen" placeholder="Persen" autocomplete="off" required value="{{ old('persen') }}">
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-info ion-plus"> Add</button>
        </div>
    </div>
</form>

<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary content">
      <table class="table" id="datatables">
    <thead>
        <tr>
            <th>#</th>
            <th>Minggu</th>
            <th>Persen</th>
            <th>Act</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $no => $d)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $d->week }}</td>
                <td>{{ $d->persen }}%</td>
                <td><a href="/mitratelplandelete/{{ $d->id }}" class="pull-xs-right">Delete</a></td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
</div>
@endsection
@section('js')
<script>
    $(function() {
        
    });
</script>
@endsection