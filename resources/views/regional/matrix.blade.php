@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<div class="row">
    <div class="col-md-4">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Matrix</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="witel" value="all" class=" pull-right" id="witel"/>
    </div>
    <div class="col-md-3">
        <input type="text" name="wo" value="all" class=" pull-right" id="wo"/>
    </div>
    <div class="col-md-2">
        <button type="button" class="btn btn-primary btn-rounded pull-xs-left" id="gofilter">Filter</button>
        <a href="/mitratel/upload" class="pull-xs-right"><button type="button" class="btn btn-success btn-rounded">Upload</button></a>
    </div>
</div>
    
@endsection
@section('title', 'Matrik')
@section('content')
<div class="content">
</div>

<div class="modal" id="modaldetail">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div class="table-responsive table-primary contentdetil">
      
</div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        $.get('/ajaxmatrixmitratel/all/all', function(r){
            $(".content").html(r);
        });
        $('#witel').select2({
            data : $.merge([{'id':'all','text':'Semua Witel'}],<?= json_encode($witel) ?>),
            placeholder:'Select Witel'
        });
        var wo = $('#wo').select2({
            data : $.merge([{'id':'all','text':'Semua WO'}],<?= json_encode($wo) ?>),
            placeholder:'Select WO'
        });
        $('#witel').change(function(){
            $.get('/ajaxgetwo/'+$('#witel').val(), function(r){
                wo.select2({
                    data : $.merge([{'id':'all','text':'Semua WO'}],r),
                    placeholder:'Select WO'
                });
            });
        });
        $('#gofilter').click(function(){
            $.get('/ajaxmatrixmitratel/'+$('#witel').val()+'/'+encodeURIComponent($('#wo').val()), function(r){
                $(".content").html(r);
            });
        });
        $('#modaldetail').on('shown.bs.modal', function (e) {

            $.get('/ajaxmatrixmitrateldetil/'+$(e.relatedTarget).data('col')+'/'+$(e.relatedTarget).data('row'), function(r){
                $(".contentdetil").html(r);
            });
        });
    });

</script>
@endsection
