@extends('layout')
@section('heading')
<h1>List Plan</h1>
@endsection
@section('content')
<div class="panel">
  <div class="panel-body">
    <div class="table-responsive table-primary content">
      <table class="table" id="datatables">
    <thead>
        <tr>
            <th>#</th>
            <th>WO</th>
            <th>Act</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $no => $d)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $d->id }}</td>
                <td><a href="/mitratelplan/{{ urlencode($d->id) }}" class="pull-xs-right">Create</a></td>
            </tr>
        @endforeach
    </tbody>
</table>
</div>
</div>
</div>
@endsection
@section('js')
<script>
    $(function() {
        
    });
</script>
@endsection