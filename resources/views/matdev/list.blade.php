@extends('layout')
@section('css')
<style type="text/css">
</style>
@endsection
@section('title', 'BTS || MatDev')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Material Delivery</span>
</h1>
<button class="btn btn-primary btn-xs pull-right" data-toggle="modal" data-target="#modal-deliv"><i class="ion ion-checkmark"></i> Selesai Delivery</button>
@endsection
@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="table-light">
      <div class="table-header">
        <div class="table-caption">
          List Delivery
          <a href="/matdev/{{ Request::segment(2) }}/input"><button class="btn btn-primary btn-xs pull-right"><i class="ion ion-plus"></i> Input</button></a>
        </div>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Status</th>
            <th>Tgl. Delivery</th>
            <th>Tgl. On Site</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          $tgl_tiba=0;
          ?>
            @foreach($data as $no=> $d)
          <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $d->status }}</td>
            <td>{{ $d->tgl_dimuat }}</td>
            <td>{{ $d->tgl_tiba }}</td>
            <td>
                @if($d->status == "Material_on_Delivery")
                <a href="/matonsite/{{ Request::segment(2) }}/{{ $d->id }}"><button class="btn btn-primary btn-xs pull-right"><i class="ion ion-pencil"></i> Update Tiba</button></a>
                @else
                
                @endif
            </td>
          </tr>
          <?php
          if($d->tgl_tiba){
            $tgl_tiba = $d->tgl_tiba;
          }
          // dd($tgl_tiba);
          ?>
          @endforeach
        </tbody>
      </table>
      @if(!count($data))
      <div class="table-footer">
        Tidak ada data delivery.
      </div>
      @endif
    </div>
  </div>
  <div class="col-md-6">
    <div class="table-light">
      <div class="table-header">
        <div class="table-caption">
          Info Material
        </div>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th rowspan="2" class="text-center valign-middle">#</th>
            <th rowspan="2" class="text-center valign-middle">Designator</th>
            <th rowspan="2" class="text-center valign-middle">Plan</th>
            <th colspan="2" class="text-center valign-middle">Delivery</th>
          </tr>
          <tr>
            <th class="text-center valign-middle">Dimuat</th>
            <th class="text-center valign-middle">OnSite</th>
          </tr>
        </thead>
        <tbody>
            @foreach($info as $no=> $i)
          <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $i->designator }}</td>
            <td class="text-right">{{ $i->qtyplan?:'-' }}</td>
            <td class="text-right">{{ $i->delv?:'-' }}</td>
            <td class="text-right">{{ $i->onsite?:'-' }}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      @if(!count($info))
      <div class="table-footer">
        Tidak ada data plan.hubungi KOKOH.
      </div>
      @endif
    </div>
  </div>
</div>
<div class="modal fade" id="modal-deliv" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">Form Selesai Pengiriman</h4>
      </div>
      <div class="modal-body">
        LOP akan dilanjutkan ke loker selanjutnya?
        <form class="form-horizontal" method="post" id="formDeliv" autocomplete="off" action="/saveDelivery/{{ Request::segment(2) }}" enctype="multipart/form-data">
            <input type="hidden" name="status" id="status" value="Material_Ready" class="form-control" required/>
            <input type="hidden" name="tgl_material_tiba" id="tgl_material_tiba" value="{{ $tgl_tiba }}" class="form-control" required/>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Eh Hadang</button>
        <button class="btn btn-primary" type="submit" form="formDeliv" value="Submit">Lanjukan!</button>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script>
  $(function() {
  });
</script>
@endsection
