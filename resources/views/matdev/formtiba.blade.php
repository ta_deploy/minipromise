@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
</style>
@endsection
@section('title', 'BTS || MatDev')
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Material Delivery / </span> Update Tiba
</h1>
@endsection
@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="table-light">
        <table class="table table-bordered">
          <div class="table-header">
            <div class="table-caption text-center">
              Material
            </div>
          </div>
          <thead>
            <tr>
              <th class="text-center">Designator</th>
              <th class="text-center">Qty Deliv</th>
            </tr>
          </thead>
          <tbody>
            @foreach($item as $no => $i)
            <tr>
              <td>{{ $i->designator }}</td>
              <td class="text-right">{{ $i->qty }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-6">
      <form class="form-horizontal" method="post" id="formDelivery" autocomplete="off" action="/saveOnsiteNew/{{ Request::segment(2) }}/{{ Request::segment(3) }}" enctype="multipart/form-data">
        <input type="hidden" name="item">

        <input type="hidden" name="status" value="Material_on_Site">
        <div class="form-group form-message-dark">
            <label for="tgl" class="col-md-3 control-label">Tanggal Tiba</label>
            <div class="col-md-9">
                <input type="text" name="tgl" id="tgl" class="form-control " required/>
            </div>
        </div>
         <div class="form-group form-message-dark">
            <label for="catatan" class="col-md-3 control-label">&nbsp;</label>
            <?php
                $foto = ["Material_Dimuat","Material_Tiba"];
            ?>
            <div class="col-md-9">
                @foreach($foto as $f)
                    <div class="col-xs-3 col-sm-3 input-photos text-center {{$f}}">
                        <?php
                        $input=$f;
                        $path = "/storage/".Request::segment(2)."/instalasifile/evident/".Request::segment(3)."/".$input;
                        $th   = $path."-th.jpg";
                        $img  = $path.".jpg";
                        $flag = "";
                        $name = "flag_".$input;
                        ?>
                        @if (file_exists(public_path().$th))
                            <a href="{{ $img }}" target="_blank">
                                <img src="{{ $th }}" alt="{{ $input }}" />
                            </a>
                            <?php
                                $flag = 1;
                            ?>
                        @else
                            <img src="/image/placeholder.gif" alt="" />
                        @endif
                        <br />
                        <input type="hidden" style="opacity: 0;pointer-events: none;" size="6" class="text pull-left" name="flag_{{ $input }}" value="{{ $flag }}" required />
                        <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
                        <button type="button" class="btn btn-sm btn-info m-t-1">
                            <i class="ion-camera"></i>
                        </button>
                        <p>Foto {{ str_replace("_"," ",$input)  }}</p>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 col-md-9">
                <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
            </div>
        </div>
    </form>
  </div>
</div>

@endsection

@section('js')
<script>
    $('#formDelivery').pxValidate();
    
    $('#tgl').datepicker({
        autoclose:true,
        format: 'yyyy-mm-dd'
    });
    $('input[type=file]').change(function() {
        console.log(this.name);
        var inputEl = this;
        if (inputEl.files && inputEl.files[0]) {
            $(inputEl).parent().find('input[type=text]').val(1);
            var reader = new FileReader();
            reader.onload = function(e) {
                $(inputEl).parent().find('img').attr('src', e.target.result);

            }
            reader.readAsDataURL(inputEl.files[0]);
        }
    });
    $('.input-photos').on('click', 'button', function() {
        $(this).parent().find('input[type=file]').click();
    });
</script>
@endsection
