@extends('layout')
@section('css')
<style type="text/css">
  .no-search .select2-search {
    display:none
  }

  .input-photos img {
    width: 100px;
    height: 150px;
    margin-bottom: 5px;
  }
</style>
@endsection
@section('title', 'BTS || MatDev')
@section('heading')
<h1>
  <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Material Delivery / </span> Form
</h1>
@endsection
@section('content')
<div class="modal fade" id="modal-base" tabindex="-1">
  <form class="form-horizontal" method="post" id="formAddition" autocomplete="off" action="/savemataddition/{{ Request::segment(2) }}/{{ Request::segment(3) }}">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title">Tambah Designator</h4>
      </div>
      <div class="modal-body">
          <input type="text" name="designator" class="form-control input-sm" placeholder="Ketik Designator..." required />
          <input type="text" name="hargamaterial" class="form-control m-t-1 input-sm" id="harga_jasa" placeholder="Harga Material..." required />
          <input type="text" name="hargajasa" class="form-control m-t-1 input-sm" id="harga_material" placeholder="Harga Jasa..." required />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
  </form>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="table-light">
        <table class="table table-bordered">
          <div class="table-header">
            <div class="table-caption text-center">
              <div class="col-sm-4">
                Material 
              </div>
              <div class="input-group col-sm-8">
                <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-base">
                  Launch demo modal
                </button>
                
              </div>
            </div>
          </div>
          <thead>
            <tr>
              <th class="text-center">Designator</th>
              <th class="text-center">Qty Plan</th>
              <th class="text-center">Qty Deliv</th>
              <th class="text-center">Qty Dimuat</th>
            </tr>
          </thead>
          <tbody>
            @foreach($item as $no => $i)
            <tr>
              <td>{{ $i->designator }}</td>
              <td class="text-right">{{ number_format($i->qtyplan, 0, ',', '.')?:'-' }}</td>
              <td class="text-right">{{ number_format($i->delv, 0, ',', '.')?:'-' }}</td>
              <td><input type="text" name="{{ $i->designator }}" class="form-control touchspin input-sm" placeholder="max: tidak ada batasan" value="{{ ($i->qtyplan)-($i->delv) }}" data-bts-min="-9999999" data-bts-max="9999999"></td>
              <!--  -->
            </tr>
            @endforeach
          </tbody>

        </table>
          @if(!count($item))
          <div class="table-footer">
            Designator plan tidak ada hubungi KOKOH.
          </div>
          @endif
      </div>
    </div>
  </div>
  <div class="col-md-6">
    @if(count($item))
      <form class="form-horizontal" method="post" id="formDelivery" autocomplete="off" action="/saveDeliveryNew/{{ Request::segment(2) }}/{{ Request::segment(3) }}" enctype="multipart/form-data">
        <input type="hidden" name="item">
        <input type="hidden" name="itemplan" value="{{ json_encode($item) }}">
        <input type="hidden" name="status" value="Material_on_Delivery">
        <!-- <div class="form-group form-message-dark">
            <label for="status_material" class="col-md-3 control-label">Status</label>
            <div class="col-md-9">
                <input type="text" name="status" id="status_material" class="form-control" required/>
            </div>
        </div> -->
        <div class="form-group form-message-dark">
            <label for="tgl" class="col-md-3 control-label">Tanggal</label>
            <div class="col-md-9">
                <input type="text" name="tgl" id="tgl" class="form-control " required/>
            </div>
        </div>
         <div class="form-group form-message-dark">
            <label for="catatan" class="col-md-3 control-label">&nbsp;</label>
            <?php
              $foto = ["Material_Dimuat"];
            ?>
            <div class="col-md-9">
              @foreach($foto as $f)
                <div class="col-xs-3 col-sm-3 input-photos text-center {{$f}}">
                  <?php
                  $input=$f;
                  $path = "/storage/".Request::segment(2)."/instalasifile/evident/".Request::segment(3)."/".$input;
                  $th   = $path."-th.jpg";
                  $img  = $path.".jpg";
                  $flag = "";
                  $name = "flag_".$input;
                  ?>
                  @if (file_exists(public_path().$th))
                      <a href="{{ $img }}" target="_blank">
                          <img src="{{ $th }}" alt="{{ $input }}" />
                      </a>
                      <?php
                          $flag = 1;
                      ?>
                  @else
                      <img src="/image/placeholder.gif" alt="" />
                  @endif
                  <br />
                  <input type="hidden" style="opacity: 0;pointer-events: none;" size="6" class="text pull-left" name="flag_{{ $input }}" value="{{ $flag }}" required />
                  <input type="file" class="hidden" name="photo-{{ $input }}" accept="image/jpeg"/>
                  <button type="button" class="btn btn-sm btn-info m-t-1">
                      <i class="ion-camera"></i>
                  </button>
                  <p>Foto {{ str_replace("_"," ",$input)  }}</p>
                </div>
              @endforeach
            </div>
        </div>
        <div class="form-group">
          <div class="col-md-offset-3 col-md-9">
            <button type="submit" class="btn btn-primary pull-right"><i class="ion-soup-can"></i> Simpan</button>
          </div>
        </div>
      </form>
    @endif
  </div>
</div>

@endsection

@section('js')
<script>
  $(function(){
    $('#formDelivery').pxValidate({
      submitHandler: function(form) {
        $('input[name=item]').val(JSON.stringify($('.touchspin').serializeArray()));
        form.addClass("form-loading form-loading-inverted");
        form.submit();
      }
    });
    $('#formAddition').pxValidate();
    $('input[type=file]').change(function() {
      console.log(this.name);
      var inputEl = this;
      if (inputEl.files && inputEl.files[0]) {
        $(inputEl).parent().find('input[type=text]').val(1);
        var reader = new FileReader();
        reader.onload = function(e) {
            $(inputEl).parent().find('img').attr('src', e.target.result);

        }
        reader.readAsDataURL(inputEl.files[0]);
      }
    });
    $('.input-photos').on('click', 'button', function() {
      $(this).parent().find('input[type=file]').click();
    });
    $('.touchspin').TouchSpin();
    $('#tgl').datepicker({
      autoclose:true,
      format: 'yyyy-mm-dd'
    });
    $('#harga_jasa').mask('9999999999');
    $('#harga_material').mask('9999999999');
  });
</script>
@endsection
