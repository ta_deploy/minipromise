@extends('layout')
@section('css')
<style type="text/css">
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Plan Project / </span>List Plan Mitra
</h1>
@endsection
@section('title', 'BTS || Project Plan')
@section('content')
    <div class="panel">
      <div class="panel-body">
        <div class="table-responsive table-primary">
          <table class="table table-bordered" id="datatables">
            <thead>
                <tr>
                    <th>Tematik</th>
                    <th>Mitra</th>
                    <th>Jumlah Lop</th>
                    <th>Sum Bobot</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data as $no => $d)
                    @foreach($d['data'] as $key => $e)
                        <tr>
                            <td>{{ $d['label'] }}</td>
                            <td>{{ $e['nama'] }}</td>
                            <td>{{ $e['count'] }}</td>
                            <td>{{ $e['bobot']?:'-' }}</td>
                            <td><a href="/plan_per_tl/{{ $no }}/{{ $key }}"><span class="btn btn-sm btn-info"><i class="ion ion-ios-cloud-download"></i>&nbsp;&nbsp;Make Plan</span></a></td>
                        </tr>
                    @endforeach
                @endforeach
            </tbody>
        </table>
        </div>
      </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
    });
</script>
@endsection
