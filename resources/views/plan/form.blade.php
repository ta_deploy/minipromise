@extends('layout')
@section('css')
<style type="text/css">
    .no-search .select2-search {
        display:none
    }
    th.freeze, td.freeze
    {
      position:sticky;
      left:0px;
      background-color:grey;
    }
</style>
@endsection
@section('heading')
<h1>
    <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Plan Project / </span>Make Plan
</h1>
@endsection
@section('title', 'BTS || Project Plan')
@section('content')
<?php
    $color = [];
    foreach($tim as $t){
        $color[$t->tim] = $t->warna;
    }
    // dd($color);
    $start = $startdate?:date('Y')."-".date('m')."-01";
    // dd($start);
    $month = [];
    $date = strtotime($start);
    $year=date('Y',$date);
    
    for($i=0;$i<3;$i++){
        $dn = strtotime($start." +$i Months");
        $mn=date('m',$dn);
        $lastdate = strtotime(date("Y-m-t", $dn ));
        $day = date("d", $lastdate);
        $month[$mn] = range(1,$day);
        // dd($month);
    }
    // dd($data);
?>
<ul class="nav nav-pills" id="tab-resize-pills">
  <li class="active"><a href="#tab-resize-pills-1" data-toggle="tab">Mapping</a></li>
  <li><a href="#tab-resize-pills-2" data-toggle="tab">Planning</a></li>
  <li><a href="#tab-resize-pills-3" data-toggle="tab">TIM</a></li>
</ul>

<div class="tab-content">
  <div class="tab-pane active" id="tab-resize-pills-1">
    <div class="table-responsive">
        <table id="editable-map" class="table table-bordered " style="cursor: pointer;">
          <thead>
            <tr>
              <th rowspan="2" class="valign-middle freeze" style="min-width:300px;">LOP</th>
              @foreach($month as $no=> $m)
                <th colspan="{{ count($m) }}" class="text-center">M{{ $no }}</th>
              @endforeach
            </tr>
            <tr>
              @foreach($month as $no=> $m)
                @foreach($m as $mon)
                    <th>{{ strlen($mon)==1?'0'.$mon:$mon }}</th>
                @endforeach
              @endforeach
            </tr>
          </thead>
          <tbody>
            @foreach($data as $k => $d)
            <tr>
                <td class="freeze">{{ $d['data']->nama_lop }}:{{ $d['data']->sumbobot }}</td>
                @foreach($d['map'] as $e => $f)
                    <?php
                        $clstd = "$e-$k";
                        $id=0;$value=$f;
                        $map='';
                        if($f!='-'){
                            $id = explode(':', $f)[1];
                            $value = explode(':', $f)[0];
                        }
                        if (array_key_exists($value,$color)){
                            $map=$color[$value];
                        }
                    ?>
                    <td data-id="{{ $id }}" data-idlop="{{ $k }}" data-tgl="{{ $e }}" data-tipe="0" data-changewarna="{{$clstd}}" class="{{$clstd}}" style="background-color: {{ $map }};">{{ $value }}</td>
                @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
  <div class="tab-pane" id="tab-resize-pills-2">
    <div class="table-responsive">
        <table id="editable-table" class="table table-bordered " style="cursor: pointer;">
          <thead>
            <tr>
              <th rowspan="2" class="valign-middle freeze" style="min-width:300px;">LOP</th>
              @foreach($month as $no=> $m)
                <th colspan="{{ count($m) }}" class="text-center">M{{ $no }}</th>
              @endforeach
            </tr>
            <tr>
              @foreach($month as $no=> $m)
                @foreach($m as $mon)
                    <th>{{ strlen($mon)==1?'0'.$mon:$mon }}</th>
                @endforeach
              @endforeach
            </tr>
          </thead>
          <tbody>
            @foreach($data as $k => $d)
            <tr>
                <td class="freeze">{{ $d['data']->nama_lop }}:{{ $d['data']->sumbobot }}</td>
                @foreach($d['kolom'] as $e => $f)
                    <?php
                        $map2=$map='';$clstd = "$e-$k";
                        $id=0;$value=$f;
                        if($f!='-'){
                            $id = explode(':', $f)[1];
                            $value = explode(':', $f)[0];
                        }
                        if($d['map'][$e]!='-'){
                            $map2 = explode(':',$d['map'][$e])[0];
                        }
                        if (array_key_exists($map2,$color)){
                            $map=$color[$map2];
                        }
                    ?>
                    <td data-id="{{ $id }}" data-idlop="{{ $k }}" data-tgl="{{ $e }}" data-tipe="1" data-changewarna="{{$clstd}}" class="{{$clstd}}" style="background-color: {{ $map }};">{{ $value }}</td>
                @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
    </div>
  </div>
  <div class="tab-pane" id="tab-resize-pills-3">
    <div class="panel">
      <div class="panel-heading">
        <span class="panel-title">List Tim</span>
        <div class="panel-heading-controls">
          <button class="btn btn-xs btn-success btn-outline-colorless" data-toggle="modal" data-target="#modal-base" data-id="0" data-mitra="{{ Request::segment(3) }}" data-tim="" data-warna="#ff99ee"><i class="fa fa-check"></i> Tambah</button>
        </div>
      </div>
      <div class="panel-body">
        <div class="table-responsive table-primary">
          <table class="table table-bordered" id="datatables">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tim</th>
                    <th>Warna</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($tim as $no => $t)
                    <tr>
                        <td>{{ ++$no }}</td>
                        <td>{{ $t->tim }}</td>
                        <td style="background-color: {{ $t->warna }};">{{ $t->warna }}</td>
                        <th><button class="btn btn-xs btn-success btn-outline-colorless" data-toggle="modal" data-target="#modal-base" data-id="{{ $t->id }}" data-mitra="{{ $t->mitra }}" data-tim="{{ $t->tim }}" data-warna="{{ $t->warna }}"><i class="ion-pencil"></i> Edit</button></th>
                    </tr>
                @endforeach
            </tbody>
        </table>
        </div>
      </div>
    </div>
  </div>
</div>
<div id="overview-chart" style="height: 250px"></div>
<div class="modal" id="modal-base">
  <div class="modal-dialog modal-xl" >
    <div class="modal-content">
        <form method="post" action="/updatetim">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="modal-title tittle">Form Tim</h4>
          </div>
          <div class="modal-body">
            <input type="hidden" id="mitra" name="mitra">
            <input type="hidden" id="id" name="id">
            <input type="text" id="tim" name="tim" class="form-control m-b-2" placeholder="Tim">
            <input type="text" name="warna" id="minicolors-wheel" class="form-control">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">
    $(function() {
        var color = <?= json_encode($color); ?>;
        $('#editable-table').editableTableWidget({
            editor: $('<input class="form-control">')
        });
        $('#editable-table td').on('change', function(evt, newValue) {
            var td = $(this)
            if(this.dataset.idlop && this.dataset.tgl){
                $.post("/plan_persen", { id: this.dataset.id, lopid: this.dataset.idlop, tgl: this.dataset.tgl, value: newValue, tipe: this.dataset.tipe })
                .done(function(data){
                    td.attr("data-id", data.data);
                });
            }else{
                return false;
            }
        });
        $('#editable-map td').click(function(){
            $('#editmap').val('m1');
            console.log($('#editmap'));
        })
        $('#editable-table td').on('change', function(evt, newValue) {
            var td = $(this)
            if(this.dataset.idlop && this.dataset.tgl){
                $.post("/plan_persen", { id: this.dataset.id, lopid: this.dataset.idlop, tgl: this.dataset.tgl, value: newValue, tipe: this.dataset.tipe })
                .done(function(data){
                    td.attr("data-id", data.data);
                });
            }else{
                return false;
            }
        });
        $('#editable-map').editableTableWidget({
            editor: $('<input class="form-control edittable" id="editmap">')
        });
        $('#editable-map td').on('change', function(evt, newValue) {
            var td = $(this)
            warna = this.dataset.changewarna;
            if(this.dataset.idlop && this.dataset.tgl){
                $.post("/plan_persen", { id: this.dataset.id, lopid: this.dataset.idlop, tgl: this.dataset.tgl, value: newValue, tipe: this.dataset.tipe })
                .done(function(data){
                    if(newValue in color){
                        $('.'+warna).css("background-color", color[newValue]);
                    }else{
                        $('.'+warna).css("background-color", "transparent");
                    }
                    
                    td.attr("data-id", data.data);
                });
            }else{
                return false;
            }
        });
        $('#minicolors-wheel').minicolors({
          control:  'wheel',
          position: 'bottom right',
        });
        $('#modal-base').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget)
          var id = button.data('id')
          var mitra = button.data('mitra')
          var tim = button.data('tim')
          var warna = button.data('warna')
          var modal = $(this)
          modal.find('#id').val(id)
          modal.find('#mitra').val(mitra)
          modal.find('#tim').val(tim)
          modal.find('#minicolors-wheel').val(warna)
          $('#minicolors-wheel').minicolors('settings', {
            value: warna
            });
        });
        new Morris.Line({
          element:        'overview-chart',
          data:           <?= json_encode($bobot); ?>,
          xkey:           'ItemDate',
          ykeys:          ['baris'],
          labels:          ['baris'],
          grid:false,
          xLabelFormat: function (d) {
            return ("0" + d.getDate()).slice(-2) + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + d.getFullYear();
          },
        });
    });
</script>
@endsection
