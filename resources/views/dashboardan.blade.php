@extends('layout')
@section('title', 'BTS || Dashboard Mtel')
@section('css')
<style type="text/css">

</style>
@endsection
@section('heading')
<h1></h1>

<div class="row">
    <div class="col-md-6">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Monitoring Project Mitratel</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="mitra" value="{{ Request::segment(2) }}" class=" pull-right" id="filter"/>
    </div>
    <div class="col-md-3">
        <input type="text" name="tl" value="{{ Request::segment(3) }}" class=" pull-right" id="filter2"/>
    </div>
</div>
@endsection
@section('content')
<?php
$totalspan=$totalrfs=$totalblmrfs=$totalogp=$totaltarikandone=0;
foreach($data as $d){
  $totalspan += $d->span;
  $totalrfs += $d->rfs;
  $totalblmrfs += $d->blmrfs;
  $totalogp+=$d->ogp_blmpermit+$d->ogp_sdhpermit;
  $totaltarikandone+=$d->installed_blmpermit+$d->installed_sdhpermit;
}
?>
<div class="row">
  <div class="col-md-4">
    <div class="panel panel-info">
      <div class="table-light">
        <div class="table-header">
          <div class="table-caption text-center">
            JUMLAH SPAN
          </div>
        </div>
        <table class="table table-bordered">
          <tbody>
            @foreach($data as $no => $d)
            <tr>
              <!-- COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR -->
              <td>{{ $d->nama_project?:'Blm Mapping' }}</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="JLH_SPAN" data-row="{{ $d->mpromise_project_id }}">{{ $d->span }}</span></td>
              @if(!$no)
              <td rowspan="3" class="text-center valign-middle font-weight-bold font-size-34">
              <span data-toggle="modal" data-target="#modal-base" data-col="JLH_SPAN" data-row="all">
              {{ $totalspan }}</span></td>
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-success">
      <div class="table-light">
        <div class="table-header">
          <div class="table-caption  text-center">
            RFS
          </div>
        </div>
        <table class="table table-bordered">
          <tbody>
            <!-- COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR -->
            @foreach($data as $no => $d)
            <tr>
              <td>{{ $d->nama_project?:'Blm Mapping' }}</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="RFS" data-row="{{ $d->mpromise_project_id }}">
                {{ $d->rfs }}</span></td>
              @if(!$no)
              <td rowspan="3" class="text-center valign-middle font-weight-bold font-size-34">
              <span data-toggle="modal" data-target="#modal-base" data-col="RFS" data-row="all">
              {{ $totalrfs }}</span></td>
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="table-light">
        <div class="table-header">
          <div class="table-caption  text-center text-center">
            BELUM RFS
          </div>
        </div>
        <table class="table table-bordered">
          <tbody>
            <!-- COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR -->
            @foreach($data as $no => $d)
            <tr>
              <td>{{ $d->nama_project?:'Blm Mapping' }}</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="BLM_RFS" data-row="{{ $d->mpromise_project_id }}">{{ $d->blmrfs }}</span></td>
              @if(!$no)
              <td rowspan="3" class="text-center valign-middle font-weight-bold font-size-34">
              <span data-toggle="modal" data-target="#modal-base" data-col="BLM_RFS" data-row="all">
              {{ $totalblmrfs }}</span></td>
              @endif
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="table-light">
        <table class="table table-bordered">
          <div class="table-header">
            <div class="table-caption text-center">
              Tarikan Done
              <span class="label label-pill label-warning pull-right">{{ $totaltarikandone }}</span>
            </div>
          </div>
          <thead>
            <tr>
              <th class="text-center">WO</th>
              <th class="text-center">Permit Done</th>
              <th class="text-center">Permit Not Yet</th>
              <th class="text-center">Tot</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $tot = $totblm = $totsdh = 0;
            ?>
            <!-- COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR -->
            @foreach($data as $d)
            <?php
              $totblm += $d->installed_blmpermit;
              $totsdh += $d->installed_sdhpermit;
              $tot += $d->installed_blmpermit+$d->installed_sdhpermit;
            ?>
            <tr>
              <td>{{ $d->nama_project?:'Blm Mapping' }}</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="TD_PD" data-row="{{ $d->mpromise_project_id }}">{{ $d->installed_sdhpermit }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="TD_PNY" data-row="{{ $d->mpromise_project_id }}">{{ $d->installed_blmpermit }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="TD_TOT" data-row="{{ $d->mpromise_project_id }}">{{ $d->installed_sdhpermit+$d->installed_blmpermit }}</span></td>
            </tr>
            @endforeach
            <tr>
              <td>Tot</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="TD_PD" data-row="all">{{ $totsdh }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="TD_PNY" data-row="all">{{ $totblm }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="TD_TOT" data-row="all">{{ $tot }}</span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="table-light">
        <table class="table table-bordered">
          <div class="table-header">
            <div class="table-caption text-center">
              OGP Penarikan
              <span class="label label-pill label-warning pull-right">{{ $totalogp }}</span>
            </div>
          </div>
          <thead>
            <tr>
              <th class="text-center">WO</th>
              <th class="text-center">Permit Done</th>
              <th class="text-center">Permit Not Yet</th>
              <th class="text-center">Tot</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $tot = $totblm = $totsdh = 0;
            ?>
            <!-- COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR -->
            @foreach($data as $d)
            <?php
              $totblm += $d->ogp_blmpermit;
              $totsdh += $d->ogp_sdhpermit;
              $tot += $d->ogp_blmpermit+$d->ogp_sdhpermit;
            ?>
            <tr>
              <td>{{ $d->nama_project?:'Blm Mapping' }}</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="OP_PD" data-row="{{ $d->mpromise_project_id }}">{{ $d->ogp_sdhpermit }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="OP_PNY" data-row="{{ $d->mpromise_project_id }}">{{ $d->ogp_blmpermit }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="OP_TOT" data-row="{{ $d->mpromise_project_id }}">{{ $d->ogp_sdhpermit+$d->ogp_blmpermit }}</span></td>
            </tr>
            @endforeach
            <tr>
              <td>Tot</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="OP_PD" data-row="all">{{ $totsdh }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="OP_PNY" data-row="all">{{ $totblm }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="OP_TOT" data-row="all">{{ $tot }}</span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="panel panel-warning">
      <div class="table-light">
      
        <table class="table table-bordered">
          <div class="table-header">
            <div class="table-caption text-center">
              Not Yet

            </div>
          </div>
          <thead>
            <tr>
              <th class="text-center">WO</th>
              <th class="text-center">Commcase</th>
              <th class="text-center">Perizinan PU</th>
              <th class="text-center">Material</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $totpu = $totcom = $totmos = 0;
            ?>
            <!-- COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR -->
            @foreach($data as $d)
            <?php
              $totpu += $d->ny_pu;
              $totcom += $d->ny_comcase;
              $totmos += $d->ny_mos;
            ?>
            <tr>
              <td>{{ $d->nama_project?:'Blm Mapping' }}</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="NY_COMCASE" data-row="{{ $d->mpromise_project_id }}">{{ $d->ny_comcase }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="NY_PU" data-row="{{ $d->mpromise_project_id }}">{{ $d->ny_pu }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="NY_MTR" data-row="{{ $d->mpromise_project_id }}">{{ $d->ny_mos }}</span></td>
            </tr>
            @endforeach
            <tr>
              <td>Tot</td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="NY_COMCASE" data-row="all">{{ $totpu }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="NY_PU" data-row="all">{{ $totcom }}</span></td>
              <td class="text-right"><span data-toggle="modal" data-target="#modal-base" data-col="NY_MTR" data-row="all">{{ $totmos }}</span></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<div class="row" id="chartsHolder"></div>
<div class="row">
@foreach($bobot as $key=>$b)
<div class="col-md-6">
<div class="panel panel-info">
  <div class="table-light">
  
    <table class="table table-bordered font-size-11">
      <div class="table-header">
        <div class="table-caption text-center">BOBOT 
          {{$b['nama']}}

        </div>
      </div>
      <thead>
        <tr>
          <th class="text-center">~</th>
          @foreach($b['data'] as $d)
            <th class="text-center">W{{ $d->tgl }}</th>
          @endforeach
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><a href="/mitratelplan/{{ $key }}" class="pull-xs-center">PLAN</a></td>
          @foreach($b['data'] as $d)
            <td class="text-right">{{ $d->plan }}</td>
          @endforeach
        </tr>
        <tr>
          <td>REAL</td>
          @foreach($b['data'] as $d)
            <td class="text-right">{{ round($d->real,2) }}</td>
          @endforeach
        </tr>
      </tbody>
    </table>
  </div>
</div>
</div>
@endforeach
</div>
<!-- <div class="table-primary">
<div class="table-header">
  <div class="table-caption">
    Monitoring Progress
  </div>
</div>
<table class="table font-size-11" id="datatables">
  <thead>
      <tr>
          <th>#</th>
          <th>WO</th>
          <th>Ring</th>
          <th>Span</th>
          <th>Aanwijzing</th>
          <th>Matdev</th>
          <th>Tanam Tiang</th>
          <th>Gelar Kabel</th>
          <th>Pasang Terminal</th>
          <th>Terminasi</th>
          <th>Test Comm</th>
          <th>RFS</th>
      </tr>
  </thead>
  <tbody>
      <?php
          $T1=$T2=$T3=$T4=$T5=$T6=$T7=$T8=$T9=0;
      ?>
      @foreach($matrik as $no => $d)
          <?php
              $T1+=$d->BARIS;
              $T2+=$d->AANWIJZING;
              $T3+=$d->MATDEV;
              $T4+=$d->TANAM_TIANG;
              $T5+=$d->GELAR_KABEL;
              $T6+=$d->PASANG_TERMINAL;
              $T7+=$d->TERMINASI;
              $T8+=$d->TEST_COMM;
              $T9+=$d->RFS;
          ?>
          <tr>
              <td>{{ ++$no }}</td>
              <td>{{ $d->WO }}</td>
              <td>{{ $d->ring }}</td>
              <td data-toggle="modal" data-col="ALL" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->BARIS }}</td>
              <td data-toggle="modal" data-col="AANWIJZING" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->AANWIJZING ?:'-' }}</td>
              <td data-toggle="modal" data-col="MATDEV" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->MATDEV ?:'-' }}</td>
              <td data-toggle="modal" data-col="TANAM_TIANG" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->TANAM_TIANG ?:'-' }}</td>
              <td data-toggle="modal" data-col="GELAR_KABEL" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->GELAR_KABEL ?:'-' }}</td>
              <td data-toggle="modal" data-col="PASANG_TERMINAL" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->PASANG_TERMINAL ?:'-' }}</td>
              <td data-toggle="modal" data-col="TERMINASI" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->TERMINASI ?:'-' }}</td>
              <td data-toggle="modal" data-col="TEST_COMM" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->TEST_COMM ?:'-' }}</td>
              <td data-toggle="modal" data-col="RFS" data-row="{{ $d->ring }}" data-target="#modaldetail">{{ $d->RFS ?:'-' }}</td>
          </tr>
      @endforeach

          <tr>
              <td colspan="3">TOTAL</td>
              <td data-toggle="modal" data-col="ALL" data-row="ALL" data-target="#modaldetail">{{ $T1 }}</td>
              <td data-toggle="modal" data-col="AANWIJZING" data-row="ALL" data-target="#modaldetail">{{ $T2 ?:'-' }}</td>
              <td data-toggle="modal" data-col="MATDEV" data-row="ALL" data-target="#modaldetail">{{ $T3 ?:'-' }}</td>
              <td data-toggle="modal" data-col="TANAM_TIANG" data-row="ALL" data-target="#modaldetail">{{ $T4 ?:'-' }}</td>
              <td data-toggle="modal" data-col="GELAR_KABEL" data-row="ALL" data-target="#modaldetail">{{ $T5 ?:'-' }}</td>
              <td data-toggle="modal" data-col="PASANG_TERMINAL" data-row="ALL" data-target="#modaldetail">{{ $T6 ?:'-' }}</td>
              <td data-toggle="modal" data-col="TERMINASI" data-row="ALL" data-target="#modaldetail">{{ $T7 ?:'-' }}</td>
              <td data-toggle="modal" data-col="TEST_COMM" data-row="ALL" data-target="#modaldetail">{{ $T8 ?:'-' }}</td>
              <td data-toggle="modal" data-col="RFS" data-row="ALL" data-target="#modaldetail">{{ $T9 ?:'-' }}</td>
          </tr>
  </tbody>
</table>
</div> -->
<ul class="nav nav-pills" id="tab-resize-pills">
  @foreach($wo as $no => $w)
    <li class="{{ $no==0?'active':'' }}"><a href="#tab-resize-pills-{{ $w->mpromise_project_id }}" data-toggle="tab">{{ $w->nama_project }}</a></li>
  @endforeach
</ul>
<div class="tab-content">
  @foreach($wo as $no => $w)
    <div class="tab-pane {{ $no==0?'active':'' }}" id="tab-resize-pills-{{ $w->mpromise_project_id }}">
    <div class="table-primary table-responsive">
      
      <table class="table table-bordered font-size-11" id="datatables">
      <thead>
        <tr>
            <th colspan="29" class="text-xs-center valign-middle">Monitoring Material {{ $w->nama_project }}</th>
        </tr>
        <tr>
            <th rowspan="2" class="text-xs-center valign-middle">#</th>
            <th rowspan="2" class="text-xs-center valign-middle">Ring-Span</th>
            <th rowspan="2" class="text-xs-center valign-middle">Waspang</th>
            <th rowspan="2" class="text-xs-center valign-middle">TL</th>
            <th rowspan="2" class="text-xs-center valign-middle">Mitra</th>
            <th colspan="4" class="text-xs-center valign-middle">KABEL</th>
            <th colspan="4" class="text-xs-center valign-middle">TIANG</th>
            <th colspan="4" class="text-xs-center valign-middle">ODC</th>
            <th colspan="4" class="text-xs-center valign-middle">ODP/OTB</th>
            <th rowspan="2" class="text-xs-center valign-middle">%H-1</th>
            <th rowspan="2" class="text-xs-center valign-middle">%HI</th>
            <th rowspan="2" class="text-xs-center valign-middle">%DEV</th>
            <th rowspan="2" class="text-xs-center valign-middle">Step</th>
            <th rowspan="2" class="text-xs-center valign-middle">TGL SUBMIT PERMIT A</th>
            <th rowspan="2" class="text-xs-center valign-middle">TGL RELEASE PERMIT A</th>
            <th rowspan="2" class="text-xs-center valign-middle">TGL SUBMIT PERMIT B</th>
            <th rowspan="2" class="text-xs-center valign-middle">TGL RELEASE PERMIT B</th>
        </tr>
        <tr>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">MOS</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">MOS</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">MOS</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
            <th class="text-xs-center valign-middle">Plan</th>
            <th class="text-xs-center valign-middle">MOS</th>
            <th class="text-xs-center valign-middle">Real</th>
            <th class="text-xs-center valign-middle">Ach</th>
        </tr>
      </thead>
      <tbody>
        <?php
          $t_k_mos=$t_k_plan=$t_k_real=$t_t_mos=$t_t_plan=$t_t_real=$t_odc_mos=$t_odc_plan=$t_odc_real=$t_odp_mos=$t_odp_plan=$t_odp_real=$no=0;
        ?>
        @foreach($mtr as $m)
          @if($m->mpromise_project_id==$w->mpromise_project_id)
            <?php
                $dev = $m->ACH_HI-$m->ACH_HM1;
                
                $t_k_mos+=$m->KABEL_MOS;
                $t_k_plan+=$m->KABEL_PLAN;
                $t_k_real+=$m->KABEL_REAL;
                $t_t_mos+=$m->TIANG_MOS;
                $t_t_plan+=$m->TIANG_PLAN;
                $t_t_real+=$m->TIANG_REAL;
                $t_odc_mos+=$m->ODC_MOS;
                $t_odc_plan+=$m->ODC_PLAN;
                $t_odc_real+=$m->ODC_REAL;
                $t_odp_mos+=$m->ODP_MOS;
                $t_odp_plan+=$m->ODP_PLAN;
                $t_odp_real+=$m->ODP_REAL;
            ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $m->RING }}<span class="label label-info pull-right">{{ $m->SPAN }}</span></td>
                <td>{{ $m->waspang_nm }}</td>
                <td>{{ $m->tl }}</td>
                <td>{{ $m->mitra }}</td>
                <td class="text-xs-right">{{ number_format($m->KABEL_PLAN)?:'-' }}</td>
                <td class="text-xs-right">{{ number_format($m->KABEL_MOS)?:'-' }}</td>
                <td class="text-xs-right">{{ number_format($m->KABEL_REAL)?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->KABEL_ACH*0.01}});color:{{ $m->KABEL_ACH>40?'white':'' }};">{{ $m->KABEL_ACH?number_format($m->KABEL_ACH).'%':'-' }}</td>
                <td class="text-xs-right">{{ $m->TIANG_PLAN?:'-' }}</td>
                <td class="text-xs-right">{{ $m->TIANG_MOS?:'-' }}</td>
                <td class="text-xs-right">{{ $m->TIANG_REAL?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->TIANG_ACH*0.01}});color:{{ $m->TIANG_ACH>40?'white':'' }};">{{ $m->TIANG_ACH?number_format($m->TIANG_ACH).'%':'-' }}</td>
                <td class="text-xs-right">{{ $m->ODC_PLAN?:'-' }}</td>
                <td class="text-xs-right">{{ $m->ODC_MOS?:'-' }}</td>
                <td class="text-xs-right">{{ $m->ODC_REAL?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ODC_ACH*0.01}});color:{{ $m->ODC_ACH>40?'white':'' }};">{{ $m->ODC_ACH?number_format($m->ODC_ACH).'%':'-' }}</td>
                <td class="text-xs-right">{{ $m->ODP_PLAN?:'-' }}</td>
                <td class="text-xs-right">{{ $m->ODP_MOS?:'-' }}</td>
                <td class="text-xs-right">{{ $m->ODP_REAL?:'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ODP_ACH*0.01}});color:{{ $m->ODP_ACH>40?'white':'' }};">{{ $m->ODP_ACH?number_format($m->ODP_ACH).'%':'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ACH_HI*0.01}});color:{{ $m->ACH_HI>40?'white':'' }};">{{ $m->ACH_HM1?number_format($m->ACH_HM1).'%':'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$m->ACH_HI*0.01}});color:{{ $m->ACH_HI>40?'white':'' }};">{{ $m->ACH_HI?number_format($m->ACH_HI).'%':'-' }}</td>
                <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$dev*0.01}});color:{{ $dev>40?'white':'' }};">{{ $dev?number_format($dev).'%':'-' }}</td>
                <td><span class="label label-primary pull-right">{{ $m->step }}</span></td>
                <td>{{ $m->tgl_submit_permit_a }}</td>
                <td>{{ $m->tgl_permit_a }}</td>
                <td>{{ $m->tgl_submit_permit_b }}</td>
                <td>{{ $m->tgl_permit_b }}</td>
            </tr>
          @endif
        @endforeach
        <?php
          $t_k_ach=$t_k_mos?$t_k_real/$t_k_mos*100:0;
          $t_t_ach=$t_t_mos?$t_t_real/$t_t_mos*100:0;
          $t_odc_ach=$t_odc_mos?$t_odc_real/$t_odc_mos*100:0;
          $t_odp_ach=$t_odp_mos?$t_odp_real/$t_odp_mos*100:0;
        ?>
        <tr>
          <td colspan="5">TOTAL</td>
          <td class="text-xs-right">{{ number_format($t_k_plan)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_k_mos)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_k_real)?:'-' }}</td>
          <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$t_k_ach*0.01}});color:{{ $t_k_ach>40?'white':'' }};">{{ $t_k_ach?number_format($t_k_ach).'%':'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_t_plan)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_t_mos)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_t_real)?:'-' }}</td>
          <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$t_t_ach*0.01}});color:{{ $t_t_ach>40?'white':'' }};">{{ $t_t_ach?number_format($t_t_ach).'%':'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_odc_plan)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_odc_mos)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_odc_real)?:'-' }}</td>
          <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$t_odc_ach*0.01}});color:{{ $t_odc_ach>40?'white':'' }};">{{ $t_odc_ach?number_format($t_odc_ach).'%':'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_odp_plan)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_odp_mos)?:'-' }}</td>
          <td class="text-xs-right">{{ number_format($t_odp_real)?:'-' }}</td>
          <td class="text-xs-right" style="background-color:rgb(0,0,255,{{$t_odp_ach*0.01}});color:{{ $t_odp_ach>40?'white':'' }};">{{ $t_odp_ach?number_format($t_odp_ach).'%':'-' }}</td>
          <td colspan="8"></td>
        </tr>
      </tbody>
    </table>
  </div>
  </div>
  @endforeach
</div>

<div class="modal fade" id="modal-base" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">Detil</h4>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<div class="modal" id="modaldetail">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <div class="table-responsive table-primary contentdetil">
      
</div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
  $(function(){
    $('#tab-resize-pills').pxTabResize();
    $('#modal-base').on('show.bs.modal', function (e) {
      var info = $(e.relatedTarget)
      $.get("/dashflowdetil/"+info.data('col')+"/"+info.data('row'), function(data, status){
        $('.modal-body').html(data)
      });
    });
    var bobot = <?= json_encode($bobot) ?>;
    $.each( bobot, function( key, value ) {
      $("#chartsHolder").append("<div class='col-md-6'><div class='panel panel-primary'><div class='panel-heading'><span class='panel-title'>KURVA-S "+value['nama']+"</span><div class='panel-heading-icon'><i class='fa fa-inbox'></i>"+value['lastbobot']+"%</div></div><div class='panel-body'><div id='chart"+key.replace("#", "-")+"'></div></div></div></div>");
      var chart = c3.generate({
        bindto: "#chart" + key.replace("#", "-"),
        color: { pattern: [ '#2596be', '#00ff00' ] },
        data: {
          type: 'spline',
          x:'label',
          columns: value["kurva"],
        },
        axis: {
            x: {
                type: 'category'
            }
        },
        grid: {
            lines: {
              front: false
          }
        },
      });
    });
    $('#modaldetail').on('shown.bs.modal', function (e) {
      $.get('/getmatrixmitrateldetil/'+$(e.relatedTarget).data('col')+'/'+$(e.relatedTarget).data('row'), function(r){
          $(".contentdetil").html(r);
      });
    });
    var filter = $('#filter').select2({
        data : $.merge([{'id':'all','text':'Semua Mitra'}],<?= json_encode($mitra); ?>),
        placeholder:'Select Mitra'
    }).change(function(){
      window.location.href = document.location.origin+"/dashflow/"+this.value+"/"+$('#filter2').val();
    });
    var filter2 = $('#filter2').select2({
        data : $.merge([{'id':'all','text':'Semua TL'}],<?= json_encode($tl2); ?>),
        placeholder:'Select TL'
    }).change(function(){
      window.location.href = document.location.origin+"/dashflow/"+$('#filter').val()+"/"+this.value;
    });
  });
</script>
@endsection