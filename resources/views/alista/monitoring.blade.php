@extends('layout')
@section('css')
<style type="text/css">

</style>
@endsection
@section('heading')
<h1>Info Material Project {{ $data->nama }}</h1>
@endsection
@section('content')
<div class="row">
  <div class="col-md-6">
    <div class="table-light">
      <div class="table-header">
        <div class="table-caption">
          Summary Material <button type="button" class="btn btn-primary btn-rounded btn-xs pull-right" data-toggle="modal" data-target="#modal-justif"><i class="ion ion-plus"></i> Tambah Justif</button>
        </div>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Designator</th>
            <th>Justif</th>
            <!-- <th>PO</th> -->
            <th>Reserved</th>
            <!-- <th>Out Gudang</th> -->
            <th>OnSite</th>
          </tr>
        </thead>
            @foreach($justif as $no => $j)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $j->designator }}</td>
                    <td class="text-right">{{ $j->justif }}</td>
                    <td class="text-right">{{ $j->res }}</td>
                    <td class="text-right">{{ $j->onsite }}</td>
                </tr>
            @endforeach
        <tbody>
        </tbody>
      </table>
      @if(!count($justif))
      <div class="table-footer">
        Tidak ada data ditemukan.
      </div>
      @endif
    </div>

    <!-- <div class="table-light">
      <div class="table-header">
        <div class="table-caption">
          List PO <button type="button" class="btn btn-primary btn-rounded btn-xs pull-right" data-toggle="modal" data-target="#modal-po"><i class="ion ion-plus"></i> Tambah PO</button>
        </div>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>No PO</th>
            <th>Tgl. PO</th>
            <th>Tgl. Diterima</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
      <div class="table-footer">
        Tidak ada data ditemukan.
      </div>
    </div> -->

  </div>
  <div class="col-md-6">
    

    <div class="table-light">
      <div class="table-header">
        <div class="table-caption">
          List Reservasi <button type="button" class="btn btn-primary btn-rounded btn-xs pull-right" data-toggle="modal" data-target="#modal-reservasi"><i class="ion ion-plus"></i> Tambah Reservasi</button>
        </div>
      </div>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>No</th>
            <th>Tgl</th>
            <th>Pid</th>
            <th>Gudang</th>
            <th>Mitra</th>
          </tr>
        </thead>
        <tbody>
            @foreach($reservasi as $no => $r)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $r->no_reservasi }}</td>
                    <td>{{ $r->tgl_submit }}</td>
                    <td>{{ str_replace('Project ID :','',$r->pid) }}</td>
                    <td>{{ str_replace('Gudang :','',$r->gudang) }}</td>
                    <td>{{ str_replace('Mitra :','',$r->mitra) }}</td>
                </tr>
            @endforeach
        </tbody>
      </table>
      @if(!count($reservasi))
      <div class="table-footer">
        Tidak ada data ditemukan.
      </div>
      @endif
    </div>
  </div>
</div>
<div class="modal fade" id="modal-justif" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">Tambah Justif</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" method="post" id="formJustif" autocomplete="off" action="/saveJustif/{{ Request::segment(2) }}" enctype="multipart/form-data">
            <div class="form-group form-message-dark">
                <label for="tgl_justif" class="col-md-3 control-label">Tgl Justif</label>
                <div class="col-md-9">
                    <input type="text" name="tgl_justif" id="tgl_justif" class="form-control" required/>
                </div>
            </div>
            <div class="form-group form-message-dark">
                <label for="paste_item" class="col-md-3 control-label">Paste Item</label>
                <div class="col-md-9">
                    <textarea name="paste_item" id="paste_item" class="form-control" placeholder="Paste dari Excel" required></textarea>
                </div>
            </div>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit" form="formJustif" value="Submit">Save changes</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-po" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        
        
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-reservasi" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h4 class="modal-title" id="myModalLabel">Tambah Reservasi</h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal" method="post" id="formReservasi" autocomplete="off" action="/saveReservasi/{{ Request::segment(2) }}" enctype="multipart/form-data">
            
            <div class="form-group form-message-dark">
                <label for="lop" class="col-md-3 control-label">Lop</label>
                <div class="col-md-9">
                    <input type="text" name="lop" id="lop" class="form-control" required/>
                </div>
            </div>
            <div class="form-group form-message-dark">
                <label for="no_reservasi" class="col-md-3 control-label">Nomor Reservasi</label>
                <div class="col-md-9">
                    <input type="text" name="no_reservasi" id="no_reservasi" class="form-control" required/>
                </div>
            </div>
            <div class="form-group form-message-dark">
                <label for="tgl" class="col-md-3 control-label">Tgl submit Reservasi</label>
                <div class="col-md-9">
                    <input type="text" name="tgl" id="tgl" class="form-control" required/>
                </div>
            </div>
        </form>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn" data-dismiss="modal">Close</button>
        <button class="btn btn-primary" type="submit" form="formReservasi" value="Submit">Save changes</button>
      </div>
    </div>
  </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    $(function() {
    $('#tgl,#tgl_justif').datepicker({
        autoclose:true,
        format: 'yyyy-mm-dd'
    });
    $('#lop').select2({
        data:<?= $lop; ?>,
        placeholder:'Pilih LOP',
        dropdownParent: $("#modal-reservasi")
    });
});
</script>
@endsection