@extends('layout')
@section('css')
<style type="text/css">

    .input-photos img {
        width: 100px;
        height: 150px;
        margin-bottom: 5px;
    }
    .no-search .select2-search {
        display:none
    }
</style>
@endsection
@section('heading')
<h1><a href="/" class="ion-arrow-left-a"></a> Info Material</h1>
@endsection
@section('content')
@if(isset($mtr))
<br/>
<div class="table-primary">
    <div class="table-responsive">
        <table class="table table-striped table-bordered" id="datatables">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Designator</th>
                    <th>BoQ Ondesk</th>
                    <th>BoQ Aanwijzing</th>
                    <th>Alista Keluar</th>
                    <th>Alista Kembali</th>
                    <th>Terpasang</th>
                    <th>Sisa</th>
                </tr>
                <tbody>
                    <?php
                    $sisa=0;
                    ?>
                    @foreach($mtr as $no => $m)
                    <?php
                    $sisa=$m->alista_keluar-($m->alista_kembali+$m->terpasang);
                    ?>
                    <tr>
                        <td> {{ ++$no }}</td>
                        <td> {{ $m->designator }}</td>
                        <td> {{ $m->boq_ondesk }}</td>
                        <td> {{ $m->boq_unwijing }}</td>
                        <td> {{ $m->alista_keluar }}</td>
                        <td> {{ $m->alista_kembali }}</td>
                        <td> {{ $m->terpasang }}</td>
                        <td> {{ $sisa ?:'' }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </thead> 
        </table>
    </div>
</div>
@endif
@endsection
@section('js')
@endsection