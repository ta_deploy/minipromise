@extends('layout')
@section('heading')
<h1><a href="/ring" class="ion-arrow-left-a"></a> Form Ring {{ $data->nama_ring or '' }}</h1>
@endsection
@section('content')
<form class="form-horizontal" method="post" enctype="multipart/form-data" id="form-ring">
    <div class="form-group form-message-dark">
        <label for="no_sp" class="col-md-2 control-label">No SP</label>
        <div class="col-md-9">
            <input type="text" class="form-control no_sp" name="no_sp" id="no_sp" value="" required>
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-offset-2 col-md-9">
            <button type="submit" class="btn">Simpan</button>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function() {
        $('#form-ring').pxValidate();
    });
</script>
@endsection