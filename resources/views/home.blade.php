@extends('layout')
@section('title', 'BTS || Home')
@section('heading')
<div class="row">
    <div class="col-md-3">
        <h1>
            <span class="text-muted font-weight-light"><i class="page-header-icon ion-ios-keypad"></i>Dashboard</span>
        </h1>
    </div>
    <div class="col-md-3">
        <input type="text" name="tl" class=" pull-right select2_filter" id="tl"/>
    </div>
    <div class="col-md-3">
        <input type="text" name="mitra" class=" pull-right select2_filter" id="mitra"/>
    </div>
    <div class="col-md-3">
        <input type="text" name="tematik" class=" pull-right select2_filter" id="filter"/>
    </div>
</div>
@endsection
@section('css')
<style type="text/css">
  tr > th {
    text-align: center;
  }
</style>
@endsection
@section('title', 'Dashboard')
@section('content')
<div class="row" id="kurva-s">
  <div class="col-md-12">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Kurva</span>
      </div>
      <div class="panel-body">
        <div id="kurva-chart" style="height: 150px"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Nilai Plan Per Tahapan</span>
      </div>
      <div class="panel-body">
        <div id="horisontalbar1" style="height: 150px"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Nilai Plan Vs Real</span>
      </div>
      <div class="panel-body">
        <div id="horisontalbar2" style="height: 150px"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Total LOP By Waspang</span>
      </div>
      <div class="panel-body">
        <div id="waspangbar" style="height: 250px"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Total LOP By Mitra</span>
      </div>
      <div class="panel-body">
        <div id="mitrabar" style="height: 250px"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Persentase Total LOP Per Tahapan</span>
      </div>
      <div class="panel-body">
        <div id="pie" style="height: 250px"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Persentase Plan vs Real</span>
      </div>
      <div class="panel-body">
        <div id="donat" style="height: 250px"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">ODP Realisasi By Waspang</span>
      </div>
      <div class="panel-body">
        <div id="waspangodp" style="height: 250px"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">ODP Realisasi By Mitra</span>
      </div>
      <div class="panel-body">
        <div id="mitraodp" style="height: 250px"></div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Nilai Realisasi By Waspang</span>
      </div>
      <div class="panel-body">
        <div id="waspangrealisasinilai" style="height: 250px"></div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="panel">
      <div class="panel-heading text-xs-center">
        <span class="panel-title">Nilai Realisasi By Mitra</span>
      </div>
      <div class="panel-body">
        <div id="mitrarealisasinilai" style="height: 250px"></div>
      </div>
    </div>
  </div>
</div>

@endsection
@section('js')
<script>
  $(function() {
    var filter = $('#filter').select2({
        data: [{'id':0,'text':'Semua Tematik'}]
    });
    filter.select2({
        data : $.merge([{'id':0,'text':'Semua Tematik'}],<?= json_encode($tematik); ?>),
        placeholder:'Select Tematik'
    });
    var tl = $('#tl').select2({
        data: [{'id':0,'text':'Semua TL'}]
    });
    tl.select2({
        data : $.merge([{'id':0,'text':'Semua TL'}],<?= json_encode($tl); ?>),
        placeholder:'Select TL'
    });
    var mitra = $('#mitra').select2({
        data: [{'id':0,'text':'Semua Mitra'}]
    });
    mitra.select2({
        data : $.merge([{'id':0,'text':'Semua Mitra'}],<?= json_encode($mitra); ?>),
        placeholder:'Select Mitra'
    });
    $('.select2_filter').change(function(){
      $.ajax({
        url: "/ajaxhome",
        data:{
          tematik: $('#filter').val(),
          tl: $('#tl').val(),
          mitra: $('#mitra').val()
        }
      }).done(function(data){
        $.each(JSON.parse(data), function( key, val ) {
          if(key=='horisontalbar1'){
              horisontalbar1.load({
                  unload: true,
                  columns: [
                      val
                  ]
              });
          }
          if(key=='horisontalbar2'){
              horisontalbar2.load({
                  unload: true,
                  columns: [
                      val
                  ]
              });
          }
          if(key=='donatchart'){
              donatchart.load({
                  unload: true,
                  columns: val
              });
          }
          if(key=='piechart'){
              piechart.load({
                  unload: true,
                  columns: val
              });
          }
          if(key=='mitrabar'){
              mitrabar.load({
                  unload: true,
                  columns: val
              });
          }
          if(key=='waspangbar'){
              waspangbar.load({
                  unload: true,
                  columns: val
              });
          }
          if(key=='mitraodp'){
              mitraodp.load({
                  unload: true,
                  columns: [val]
              });
          }
          if(key=='waspangodp'){
              waspangodp.load({
                  unload: true,
                  columns: [val]
              });
          }
          if(key=='mitrarealisasinilai'){
              mitrarealisasinilai.load({
                  unload: true,
                  columns: val
              });
          }
          if(key=='waspangrealisasinilai'){
              waspangrealisasinilai.load({
                  unload: true,
                  columns: val
              });
          }
          if(key=='kurva'){
              if(val){
                kurva.setData(val);
                $('#kurva-s').show();
              }else{
                $('#kurva-s').hide();
              }

          }
        });
      })
    });
    var horisontalbar1 = c3.generate({
        bindto: '#horisontalbar1',
        data: {
            x: 'x',
            labels: {
              format: function (v, id, i, j) { return "Rp. "+d3.format(",")(v) },
            },
            columns:
                [
              ['x','1.Antrian','2.Instalasi','3.Terminasi','4.Done'],
              <?= json_encode($data['horisontalbar1']) ?>
              ],
            type: 'bar',
        },
        bar: {
            width: 30
        },
        axis: {
            rotated: true,
            y: {
                show: false,
            },
            x: {
                type: 'category'
            }
        },
        legend: {
            show: false
        },
        grid: {
            lines: {
              front: false
          }
        },
    });

    var horisontalbar2 = c3.generate({
        bindto: '#horisontalbar2',
        data: {
            x: 'x',
            labels: {
              format: function (v, id, i, j) { return "Rp. "+d3.format(",")(v) },
            },
            columns:
                [
              ['x','Gap','Real','Plan'],
              <?= json_encode($data['horisontalbar2']) ?>
              ],
            type: 'bar',
        },
        bar: {
            width: 30
        },
        axis: {
            rotated: true,
            y: {
                        show: false,
                    },
            x: {
                type: 'category'
            }
        },
        legend: {
            show: false
        },
        grid: {
            lines: {
              front: false
          }
        },
    });

    var waspangrealisasinilai = c3.generate({
        bindto: '#waspangrealisasinilai',
        data: {
            x: 'x',
            labels: {
              format: function (v, id, i, j) { return "Rp. "+d3.format(",")(v) },
            },
            columns:<?= json_encode($data['waspangrealisasinilai']) ?>,
            type: 'bar',
        },
        bar: {
            width: 20
        },
        axis: {
            rotated: true,
            y: {
                        show: false,
                    },
            x: {
                type: 'category'
            }
        },
        legend: {
            show: false
        },
        grid: {
            lines: {
              front: false
          }
        },
    });

    var mitrarealisasinilai = c3.generate({
        bindto: '#mitrarealisasinilai',
        data: {
            x: 'x',
            labels: {
              format: function (v, id, i, j) { return "Rp. "+d3.format(",")(v) },
            },
            columns:<?= json_encode($data['mitrarealisasinilai']) ?>,
            type: 'bar',
        },
        bar: {
            width: 20
        },
        axis: {
            rotated: true,
            y: {
                        show: false,
                    },
            x: {
                type: 'category'
            }
        },
        legend: {
            show: false
        },
        grid: {
            lines: {
              front: false
          }
        },
    });

    var donatchart = c3.generate({
      bindto: '#donat',
      data: {
        columns: <?= json_encode($data['donatchart']) ?>,
        type : 'donut',
      }
    });

    var piechart = c3.generate({
      bindto: '#pie',
      data: {
        columns: <?= json_encode($data['piechart']) ?>,
        type : 'pie',
      },
    });

    var waspangbar = c3.generate({
      bindto: '#waspangbar',
      data: {
        columns: <?= json_encode($data['waspangbar']) ?>,
        type: 'bar',
        groups: [['1.Antrian','2.Instalasi','3.Terminasi','4.Done']]
      },
      axis: {
        x: {
          type: 'category',
          categories: <?= json_encode($data['waspangcat']) ?>
        }
      }
    });

    var mitrabar = c3.generate({
      bindto: '#mitrabar',
      data: {
        columns: <?= json_encode($data['mitrabar']) ?>,
        type: 'bar',
        groups: [['1.Antrian','2.Instalasi','3.Terminasi','4.Done']]
      },
      axis: {
        x: {
          type: 'category',
          categories: <?= json_encode($data['mitracat']) ?>
        }
      }
    });

    var mitraodp = c3.generate({
      bindto: '#mitraodp',
      data: {
        columns: [<?= json_encode($data['mitraodp']) ?>],
        type: 'bar'
      },
      axis: {
        x: {
          type: 'category',
          categories: <?= json_encode($data['mitracat']) ?>
        }
      }
    });

    var waspangodp = c3.generate({
      bindto: '#waspangodp',
      data: {
        columns: [<?= json_encode($data['waspangodp']) ?>],
        type: 'bar',
      },
      axis: {
        x: {
          type: 'category',
          categories: <?= json_encode($data['waspangcat']) ?>
        }
      }
    });
    var kurvas=<?= json_encode($data['kurva']); ?>;
    var kurva = new Morris.Line({
      element:        'kurva-chart',
      data:           kurvas,
      xkey:           'tgl',
      ykeys:          ['plan','real'],
      labels:          ['plan','real'],
      grid:false,
      xLabelFormat: function (d) {
        return ("0" + d.getDate()).slice(-2) + '-' + ("0" + (d.getMonth() + 1)).slice(-2) + '-' + d.getFullYear();
      },
    });
  });
</script>
@endsection