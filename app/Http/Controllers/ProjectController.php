<?php

namespace App\Http\Controllers;

use App\DA\LopModel;
use App\DA\MitraModel;
use App\DA\MaterialModel;
use App\DA\ProjectModel;
use App\DA\WaspangModel;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

class ProjectController extends Controller
{
  public function createform()
  {
    $sto = LopModel::getSto();
    $mitra = MitraModel::listMitra();
    $datalop = [];
    $waspang = WaspangModel::getWaspang();

    return view('project.registerForm', compact('sto', 'mitra', 'datalop', 'waspang') );
  }
  public function list()
  {
    $data = ProjectModel::getAll();
    return view('project.list', compact('data'));
  }

  public function saveform(Request $req, $id)
  {
    $id = ProjectModel::insertOrUpdate($id, $req);
    return redirect('/project')->with('alertblock', [
      ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
    ]);
  }
  public function savestts(Request $req)
  {
    ProjectModel::updatestts($req);
    return redirect('/project')->with('alertblock', [
      ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
    ]);
  }

  private function handleFileUpload($request, $id, $file)
  {
    foreach ($file as $name) {
      $input = $name;
      if ($request->hasFile($input)) {
        //dd($input);
        $path = public_path().'/storage/'.$id.'/registerfile/';
        if (! file_exists($path)) {
          if (! mkdir($path, 0770, true)) {
            return redirect()->back()->with('alertblock', [
              ['type' => 'danger', 'text' => 'Gagal menyiapkan folder'],
            ]);
          }
        }
        $file = $request->file($input);
        //TODO: path, move, resize
        try {
          $ext = $file->getClientOriginalExtension();
          $moved = $file->move("$path", "$name.$ext");
          ProjectModel::update($id, [$name => "$name.$ext"]);
        } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
          return redirect()->back()->with('alertblock', [
              ['type' => 'success', 'text' => 'Gagal upload foto'],
          ]);
        }
      }
    }
  }

  public function search_project(Request $req)
  {
    $term = trim($req->find);

    if (empty($term)) {
    return \Response::json([]);
    }

    $fetchData = ProjectModel::search_project($term);

    $formatted_tags = [];

    foreach ($fetchData as $row) {
    $formatted_tags[] = ["id" => $row->id, "text" => $row->nama . ' (' . ($row->pid ?: 'PID Kosong') . ')'];
    }

    return \Response::json($formatted_tags);
  }
  public function bobot(){
    $data = LopModel::get_bobot_per_tgl();
  }

  //mapping

  public function list_unmapping($id){
    $unmapping = ProjectModel::getUnmapping($id);
    $mapped = ProjectModel::getMapped($id);
    // dd($mapped);
    $count = ProjectModel::getMappedCount($id);
    $mitra = MitraModel::listMitra();
    $waspang = WaspangModel::getWaspang();
    return view('mapping_mitra.list', compact('unmapping', 'mapped', 'count','mitra','waspang'));
  }
  public function saveMapping($id, Request $req){
    $w = WaspangModel::getWaspangByID($req->waspang);
    $data = [
      "jenis_pekerjaan"     => $req->jenis_pekerjaan,
      "waspang"             => $req->waspang,
      "tl"                  => $w->nik_tl,
      "waspang_nm"          => $w->nama,
      "nama_project"        => ProjectModel::getById($req->tematik)->nama,
      "mitra"               => $req->mitra,
      "mpromise_project_id" => $id
    ];
    // dd($req->lop_id,$data);
    // $ch = curl_init();
    // curl_setopt($ch, CURLOPT_URL, 'https://promitos.tomman.app/API-GetBoq/'.$req->mitos_id);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    // curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // $result = curl_exec($ch);
    // curl_close($ch);
    // foreach(json_decode($result) as $r){
    //     $item[] = [
    //         "lop_id"=>$req->lop_id,
    //         "promitos_id"=>$r->mitos_lop_id,
    //         "designator"=>$r->designator,
    //         "qty"=>$r->qty,
    //         "hargamaterial"=>$r->hargamaterial,
    //         "hargajasa"=>$r->hargajasa
    //     ];
    // }
    // MaterialModel::insertHLD($req->lop_id,$item);
    LopModel::update($req->lop_id,$data);
    return redirect()->back();
  }
  public function remove_mapping($lopid){
    LopModel::update($lopid,["jenis_pekerjaan" => '',
      "waspang"             => 0,
      "tl"                  => 0,
      "waspang_nm"          => '',
      // "nama_project"        => '',
      "mitra"               => 0,
      // "mpromise_project_id" => 0
    ]);
    return redirect()->back();
  }
  public function sendAanwijzing($id){
    $auth = session('auth');
    $map = ProjectModel::getMappedByTematik($id);
    // dd($map);
    $logs=$item=[];
    foreach($map as $m){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, 'https://promitos.tomman.app/API-GetBoq/'.$m->promitos_id);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      $result = curl_exec($ch);
      curl_close($ch);
      foreach(json_decode($result) as $r){
        $item[] = [
          "lop_id"        => $m->id,
          "promitos_id"   => $r->mitos_lop_id,
          "designator"    => $r->designator,
          "qty"           => $r->qty,
          "hargamaterial" => $r->hargamaterial,
          "hargajasa"     => $r->hargajasa
        ];
      }
      $logs[] = [
        'lop_id'    => $m->id,
        'catatan'   => 'Aanwijzing',
        'step_id'   => '1',
        'update_by' => $auth->id_user,
        'bobot'     => '1.1',
        'tematik_id'=> $id
      ];
    }
    if(count($item)){
      MaterialModel::insertHLD($id,$item);
    }
    if(count($logs)){
      LopModel::insertlogs($logs);
    }
    ProjectModel::sendAanwijzingByTematik($id);
    return redirect()->back();
  }
}
