<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\SpanModel;

class SpanController extends Controller
{
  public function index(){
    $data = SpanModel::getAll();
    return view('span.list', compact('data'));
  }
  public function form($id){
    $data = SpanModel::getById($id);
    return view('span.form',compact('data'));
  }
  public function save($id, Request $req){
    $exists = SpanModel::getById($id);
    if($exists){
      SpanModel::update($id, $req->only('nama_span'));
    }else{
      SpanModel::insert($req->only('nama_span'));
    }
    return redirect('/span');
  }

  public function viewspan($id){
    $data = SpanModel::getById($id);
    return view('span.view',compact('data'));
  }

  public function saveview($id, Request $req){
    $exists = SpanModel::getById($id);
    if($exists){
      SpanModel::update($id, $req->only('nama_span'));
    }else{
      SpanModel::insert($req->only('nama_span'));
    }
    return redirect('/span');
  }
}