<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\PlanModel;
class PlanController extends Controller
{
    public function index()
    {
        // $tl=18960642;
        $tl=session('auth')->id_user;
        $data = PlanModel::getIndex($tl);
        // dd($data);
        return view('plan.index', compact('data'));
    }
    public function form($tematik,$mitra)
    {   
        $tl=session('auth')->id_user;
        // $tl=18960642;
        $bobot = PlanModel::getBobotPlanByTematikMitra($tematik,$mitra,$tl);
        $startdate = 0;
        if(count($bobot)){
            $startdate = substr($bobot[0]->ItemDate, 0,8).'01';
        }
        $data = PlanModel::getByTematikMitra($tematik,$mitra,$tl,$startdate);
        // dd($startdate);
        // dd($data);
        $tim = PlanModel::getTimByMitra($mitra);
        return view('plan.form', compact('data', 'tim', 'bobot', 'startdate'));
    }

    public function saveplan(Request $req){
        $id = PlanModel::getByID($req->id);
        $return = 0;
        if($id){
            PlanModel::update($id->id, ["value"=>$req->value]);
            $return = $id->id;
        }else{
            $cek = PlanModel::check($req->tgl,$req->lopid);
            if($cek){
                PlanModel::updateByTgl(["value"=>$req->value,"tgl"=>$req->tgl,"lop_id"=>$req->lopid,"tipe"=>$req->tipe]);
            }else{
                $return = PlanModel::insert(["value"=>$req->value,"tgl"=>$req->tgl,"lop_id"=>$req->lopid,"tipe"=>$req->tipe]);
            }
        }
        return response()->json(["status"=>"success","data"=>$return]);
    }

    public function savetim(Request $req){
        $id = PlanModel::getTimByID($req->id);
        if($id){
            PlanModel::updateTim($id->id, ["tim"=>$req->tim,"warna"=>$req->warna]);
        }else{
            PlanModel::insertTim(["tim"=>$req->tim,"warna"=>$req->warna,"mitra"=>$req->mitra]);
        }
        return redirect()->back();
    }
}
