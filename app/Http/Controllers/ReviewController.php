<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\ReviewModel;
use App\DA\LopModel;
class ReviewController extends Controller
{

    public function index()
    {
        $witel = session('auth')->witel;
        $data = ReviewModel::getMitraListByWitel($witel);
        return view('review.listmitra', compact('data'));
    }
    public function list_lop($mitra)
    {
        $witel = session('auth')->witel;
        $data = ReviewModel::getListLopByMitraWitel($mitra,$witel);
        return view('review.listlop', compact('data'));
    }
    public function formreview($mitra,$lopid)
    {   
        $lop = LopModel::getById($lopid);
        $bobot = ReviewModel::getByLopId($lopid);
        // dd($bobot);
        $data = ReviewModel::getReviewByLopID($lopid);
        return view('review.formreview',compact('bobot', 'data','lop'));
    }
    public function savereview($mitra,$lopid, Request $req)
    {
        ReviewModel::saveReview(["tgl"=>$req->tgl,"status"=>$req->status,"catatan"=>$req->catatan,"lop_id"=>$lopid]);
        return redirect("/review/$mitra/$lopid");
    }
    public function savereviewprogress($mitra,$lopid, Request $req)
    {   
        ReviewModel::saveReviewProgress($lopid,["review_progress"=>$req->review_progress]);
        return redirect("/review/$mitra");
    }
}
