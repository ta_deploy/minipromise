<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\APMModel;

class APMController extends Controller
{

  public function cashoutlist()
  {
    $data = APMModel::getCashout();
    return view('apm.cashout', compact('data'));
  }
  public function apmmon(){
    $data = APMModel::getBudgetList();
    return view('apm.apmmon', compact('data'));
  }
  public function update(Request $req){
    return view('apm.form');
  }
  public function save($id, Request $req){
    APMModel::save($req->only('pid', 'nama_project'));
    return redirect('/monitorapm');
  }
  public function delete(Request $req){
    APMModel::delete($req->id);
    return redirect('/monitorapm');
  }
  public static function getCashListByPID($pid)
  {
    $data = APMModel::getCashListByPID($pid);
    return view('apm.cashoutbypid', compact('data'));
  }
}