<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DA\HomeModel;
use App\DA\ReviewModel;
use App\DA\ProjectModel;
use Excel;

use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    public function home()
    {
        // dd(session('auth'));
        $witel = session('auth')->witel;
        $tematik = ProjectModel::getAll('0');
        $tl = ProjectModel::getAll_tl();
        $mitra = ProjectModel::getAll_sm();
        $data = HomeModel::getCharts(0,$witel,0,0);
        $data['kurva'] = null;
        return view('home',compact('data','tematik', 'tl', 'mitra') );
    }
    public function ajaxhome(Request $req)
    {
        $witel = session('auth')->witel;
        $tematik = $req->tematik;
        $tl = $req->tl;
        $mitra = $req->mitra;
        $data = HomeModel::getCharts($tematik,$witel,$tl,$mitra);
        $data['kurva'] = ReviewModel::getByTematik($tematik);
        return json_encode($data);
    }

}