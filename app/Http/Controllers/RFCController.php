<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\DA\RFCModel;
use Excel;

use Illuminate\Support\Facades\Session;

class RFCController extends Controller
{

    public function searchrfc(Request $req){

        $dataarray =[];
        if($req->q){
            // $qin = $req->q;
            // if(str_contains($req, ',')){
            $qin = explode(',', $req->q);
            // }
            // dd($qin);
            // $sql = 'RFC='.$req->q;
            // $data = DB::select('SELECT * FROM logistik_rfc_item lri WHERE '.$sql.' order by RFC desc');
            $data = DB::table('logistik_rfc_item')->whereIn('RFC', $qin)->orWhere(function ($query) use($qin){
                $query->where('PID', 'like', '%'.$qin[0].'%');
            })->get();
            $lastrfc = '';

            foreach ($data as $no => $m){
              if($lastrfc == $m->RFC){
                $dataarray[$m->RFC]->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];

              }else{
                $m->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];
                $dataarray[$m->RFC] = $m;
              }
              $lastrfc = $m->RFC;
            }
        }
        return view('rfc.carirfc', compact('dataarray'));
    }
    public function rekap_pengeluaran($tgl,$mitra,$pid){
        $start=explode(':', $tgl)[0];
        $end=explode(':', $tgl)[1];
        $sql=$sqlm=$sqlpid = '';
        if($mitra != 'all'){
            $sqlm = " and MITRA = '".$mitra."'";
            $sql .= $sqlm;
        }
        if($pid != 'all'){
            $sqlpid .= " and PID = '".$pid."'";
            $sql .= $sqlpid;
        }
        $rfc = DB::select("SELECT COUNT(*) AS `Baris`, `RFC` FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') ".$sql." GROUP BY `RFC` ORDER BY `RFC`");

        $mitra = DB::select("SELECT `MITRA` as id, `MITRA` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') GROUP BY `MITRA` ORDER BY `MITRA`");
        $pid = DB::select("SELECT `PID` as id, `PID` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') ".$sqlm." GROUP BY `PID` ORDER BY `PID`");
        $data = DB::select("SELECT ID_BARANG,
        sum(case when NAMA_GUDANG='WH Banjarmasin' then BERI else 0 end) as WH_BJM ,
        sum(case when NAMA_GUDANG='WH SO Banjarbaru' then BERI else 0 end) as WHSO_BJB ,
        sum(case when NAMA_GUDANG='WH SO Banjarmasin A.Yani' then BERI else 0 end) as WHSO_BJM2 ,
        sum(case when NAMA_GUDANG='WH SO Banjarmasin Centrum' then BERI else 0 end) as WHSO_BJM1 ,
        sum(case when NAMA_GUDANG='WH SO Batulicin' then BERI else 0 end) as WHSO_BLC ,
        sum(case when NAMA_GUDANG='WH SO Tabalong' then BERI else 0 end) as WHSO_TJL ,
        sum(case when NAMA_GUDANG='WH SO Barabai' then BERI else 0 end) as WHSO_BRI 

        FROM logistik_rfc_item where (TGL BETWEEN '".$start."' AND '".$end."') ".$sql." GROUP BY ID_BARANG ORDER BY ID_BARANG");

        $mitra = DB::select("SELECT `MITRA` as id, `MITRA` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') GROUP BY `MITRA` ORDER BY `MITRA`");
        $pid = DB::select("SELECT `PID` as id, `PID` as text FROM `logistik_rfc_item` where (TGL BETWEEN '".$start."' AND '".$end."') ".$sqlm." GROUP BY `PID` ORDER BY `PID`");
        return view('rfc.rekappengeluaran', compact('data', 'mitra', 'pid', 'rfc'));
    }
    public function download($tgl,$mitra,$pid){
        return Excel::download(new RFCExport($tgl,$mitra,$pid), 'MATERIAL_'.str_replace(":","-",$tgl).'_'.$mitra.'_'.$pid.'.xlsx');
    }
    public function downloadpdfrar($tgl,$mitra,$pid){
        //4902587382
        //4902587390
        $start=explode(':', $tgl)[0];
        $end=explode(':', $tgl)[1];

        $sql="(TGL BETWEEN '".$start."' AND '".$end."')";
        if($mitra != 'all'){
            $sql .= " and MITRA = '".$mitra."'";
        }
        if($pid != 'all'){
            $sql .= " and PID = '".$pid."'";
        }
        $data = DB::table('logistik_rfc_item')->whereRaw($sql)->orderBy('RFC','DESC')
          ->get();
        if(count($data)){
            $zip_file = 'MATERIAL_'.str_replace(":","-",$tgl).'_'.$mitra.'_'.$pid.'.zip'; // Name of our archive to download
            // $f=[];
            // Initializing PHP class
            $zip = new \ZipArchive();
            $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
            foreach($data as $d){
                $folder = $d->MITRA?:'NoMitra';
                $folder .= '/'.$d->PID.'/'.$d->RFC.'.pdf';
                // $f[]=$folder;
                $zip->addFile(public_path().'/storage/rfc_ttd/'.$d->RFC.'.pdf', $folder);
            }
            // dd($f);
            $zip->close();

            // We return the file immediately after download
            return response()->download($zip_file);
        }
    }

    public function ajaxrekaprfc($tgl,$so,$jenis){
        $sql = '';
        $sql2 = "type='Out'";
        if($tgl){
            $sql .= 'TGL="'.$tgl.'"';
            $sql2 .= ' and posting_datex ="'.$tgl.'"';
        }
        if($so){
            $sql .= ' and NAMA_GUDANG="'.$so.'"';
            $sql2 .= ' and plant="'.$so.'"';
        }
        if($jenis == 'TTD'){
            $sql .= ' and FILE_RFC_TTD!=""';
        }
        if($jenis == 'ALISTA'){
            $data = DB::select("SELECT `no_rfc`,(select count(RFC) from logistik_rfc_item where RFC=no_rfc) as existed FROM `inventory_material` where ".$sql2." GROUP BY `no_rfc` ORDER BY `no_rfc`");
            // dd($data);
            return view('ajaxlistonlyrfc', compact('data'));
        }else{
            $data = DB::select('SELECT * FROM logistik_rfc_item lri WHERE '.$sql.' order by FILE_RFC_TTD asc');
        
            $lastrfc = '';
            $dataarray =[];
            foreach ($data as $no => $m){
              if($lastrfc == $m->RFC){
                $dataarray[$m->RFC]->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];
                
              }else{
                $m->list[] = ['ID_BARANG'=>$m->ID_BARANG,'NAMA_BARANG'=>$m->NAMA_BARANG,'SATUAN'=>$m->SATUAN,'MINTA'=>$m->MINTA,'BERI'=>$m->BERI];
                $dataarray[$m->RFC] = $m;
              }
              $lastrfc = $m->RFC;
            }
            // dd($dataarray);
            return view('rfc.ajaxlist', compact('dataarray'));
        }
    }
}