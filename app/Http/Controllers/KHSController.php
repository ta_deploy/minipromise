<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\DA\UploadModel;
use App\DA\KHSModel;
use Excel;

use Illuminate\Support\Facades\Session;

class KHSController extends Controller
{
    public function upload() 
    {
        return view('khs.upload');
    }
    public function uploadsave(Request $req) 
    {
        if ($req->hasFile('KHS')) {
            $file = $req->file('KHS');
            $nama_file = $file->getClientOriginalName();
            $arr = Excel::toArray(new UploadModel, $file);
            foreach ($arr[0] as $no => $a) {
                if ($no > 1) {
                    $harga = [];
                    $paket=1;
                    for($i=4;$i<=27;$i++){
                        if($i%2){
                            $harga[$req->harga.'_'.$paket++.'_J'] =$a[$i];
                        }else{
                            $harga[$req->harga.'_'.$paket.'_M'] = $a[$i];
                        }
                        
                    }
                    // dd($harga);
                    $upload[] = array_merge(['designator' => $a[1], 'uraian' => $a[2], 'satuan' => $a[3], 'nama_khs' => $req->nama_khs],$harga);
                }
            }
            // dd($upload);
            $khs = KHSModel::cekKHSByNama($req->nama_khs);
            if($khs){
                KHSModel::saveUploadUpdate($upload,$req->nama_khs);
            }else{
                KHSModel::saveUpload($upload);
            }
            return redirect()->back();
        }
    }
}