<?php

namespace App\Http\Controllers;

use App\DA\LopModel;
use App\DA\UploadModel;
use App\DA\MitraModel;
use App\DA\LopExport;
use App\DA\WaspangModel;
use App\DA\GrabModel;
use App\DA\WITELModel;
use App\DA\ProjectModel;
use App\DA\MaterialModel;
use DB;
use Excel;
use Illuminate\Http\Request;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ZipArchive;
use Telegram;
use Illuminate\Support\Facades\Session;

class WaspangController extends Controller
{
    // protected static $evident = [2=>array("Material_Dimuat"),4=>array("Tanam_Tiang","Gelar_Kabel","Pasang_Terminal","Terminasi")];
    public function index()
    {
        $auth = session('auth');
        $data = LopModel::getAllByWaspang($auth->id_user);
        $tematik=[];
        foreach($data as $d){
            $tematik[] = $d->nama;
        }
        $tematik = array_unique($tematik);
        return view('lop.indexWaspang', compact('data', 'tematik'));
    }

    // public function uploadInstalasi($id)
    // {
    //     $core = LopModel::getCoreByLopId($id);
    //     $data = LopModel::getById($id);
    //     return view('lop.uploadInstalasi', compact('data','core'));
    // }



    // public function uploadEnd2end($id)
    // {
    //     $data = LopModel::getCoreByLopId($id);
    //     return view('lop.uploadEnd', compact('data'));
    // }

    public function progress($id)
    {
        $data = LopModel::getById($id);
        $tematik_data = ProjectModel::getById($data->mpromise_project_id);
        $sto = LopModel::getSto();
        $mitra = MitraModel::listMitra();
        $waspang = WaspangModel::getWaspang();

        $datalop = [];
        // dd($data);
        switch ($data->step_id) {
        case 1:
            $data = LopModel::getById($id);
            $datalop = LopModel::getByMproId($data->mpromise_project_id);
            $mitra = MitraModel::listMitra();
            $waspang = WaspangModel::getWaspang();
            $sto = LopModel::getSto();
            return view('lop.registerForm', compact('data', 'sto', 'mitra', 'datalop', 'waspang'), ['id' => $data->mpromise_project_id]);
            break;
        case 2:
            return view('lop.deliveryMaterialForm', compact('data'));
            break;
        case 3:
            return view('lop.pragelaranForm', compact('data'));
            break;
        case 6:
            // $core = LopModel::getCoreByLopId($id);
            // $boq = WaspangModel::getBoqByLop($id);
            // $mitra = MitraModel::listMitra();
            // return view('lop.unwizingForm', compact('data', 'mitra', 'core', 'boq','tematik_data'));
            break;
        case 4:
            $mtr = MaterialModel::getInstalasi($id);
            $mtrcount = WaspangModel::getMtrIns($id);
            $tim_jointer = WaspangModel::get_jointer();
            $get_all_tenor = WaspangModel::get_all_tenor($id);
            $load_data = WaspangModel::load_data($id);

            // dd($mtrcount);
            return view('lop.instalasiForm', compact('data', 'mtr', 'mtrcount', 'tim_jointer', 'get_all_tenor', 'load_data') );
            break;
        case 7:
            // pemberkasan
            $this->listFolderFiles(public_path().'/storage/'.$id.'/fotorar');
            // dd($this->diraray);
            $fotorar = $this->diraray;
            $core = LopModel::getCoreByLopId($id);
            return view('lop.uploadInstalasi', compact('data','core','fotorar') );
            break;
        case 8:
            // QC
            // dd(storage_path());
            $this->listFolderFiles(public_path().'/storage/'.$id.'/fotorar');
            // dd($this->diraray);
            $fotorar = $this->diraray;
            $core = LopModel::getCoreByLopId($id);
            return view('lop.qc', compact('data','core','fotorar'));
            break;
        case 5:
            // testCom
            $data = LopModel::getById($id);
            return view('lop.ctut', compact('data'));
            break;
        default:
            dd('wrong step, back and reload.');
            break;
        }
    }
    public function listUnwizing($waspang)
    {
        $mitra = LopModel::getDistinctMitraAanwijzing($waspang);
        $data = array();
        foreach($mitra as $key=> $m){
            $data[$key]['parent'] = $m;
            $data[$key]['anak'] = LopModel::getAanwijzingWaspangList($waspang,$m->mitra);
        }
        // dd($data);
        return view('lop.aanwijzingList', compact('data'));
    }
    public function formUnwizing($tematik,$waspang,$mitra, Request $req)
    {
        // $core = LopModel::getCoreByLopId($id);
        $data = LopModel::getAanwijzingWaspang($tematik,$waspang,$mitra);
        // $boq = WaspangModel::getBoqByLop($id);
        $mitra = MitraModel::listMitra();
        // dd($data);
        return view('lop.unwizingForm', compact('data', 'mitra'));
    }
    public function saveUnwizing($tematik,$waspang,$mitra, Request $req)
    {
        $auth = session('auth');
        $step = 2;
        $data = LopModel::getAanwijzingWaspang($tematik,$waspang,$mitra);
        // if ($req->hasFile('boq_unwizing')) {
        //     $file = $req->file('boq_unwizing');
        //     $nama_file = $file->getClientOriginalName();
        //     $arr = Excel::toArray(new UploadModel, $file);
        //     $upload = [];
        //     $key = array_search('DESIGNATOR', array_column($arr[0], 1));
        //     if ($key) {
        //         foreach ($arr[0] as $no => $a) {
        //             if ($no >= 8 && $a[1] != '' && $a[6] != '' && substr($a[6], 0, 1) != '=') {
        //                 $upload[] = ['designator' => $a[1],'hargamaterial' => $a[4],'hargajasa' => $a[5], 'qty' => $a[6], 'lop_id' => $id, 'status' => 2];
        //             }
        //         }
        //         WaspangModel::insertboq($id,$upload);
        //     }
        // }
        // if ($req->hasFile('mcore_unwizing')) {
        //     $file = $req->file('mcore_unwizing');
        //     $nama_file = $file->getClientOriginalName();
        //     $arr = Excel::toArray(new UploadModel, $file);
        //     $upload = [];
        //     $key = array_search('ODP', array_column($arr[0], 1));
        //     if (isset($key)) {
        //         foreach ($arr[0] as $no => $a) {
        //             if ($no > 2 && ($a[0] != '' || $a[0] != null) && ($a[1] != '' || $a[1] != null)) {
        //                 $upload[] = ['LOP_ID' => $id,
        //                 'Plan_Odp_Nama' => $a[1],
        //                 'Plan_IP_OLT'           => $a[2],
        //                 'Plan_Olt_Port' => $a[3],
        //                 'Plan_Odf_Otb' => $a[4],
        //                 'Plan_Otb_Panel' => $a[5],
        //                 'Plan_Otb_Port' => $a[6],
        //                 'Plan_Otb_Kap' => $a[7],
        //                 'Plan_Otb_Odf_o' => $a[8],
        //                 'Plan_Otb_Panel_o' => $a[9],
        //                 'Plan_Otb_Port_o' => $a[10],
        //                 'Plan_Otb_Kap_o' => $a[11],
        //                 'Plan_FE_Nama' => $a[12],
        //                 'Plan_FE_Kap' => $a[13],
        //                 'Plan_Odc_Panel' => $a[14],
        //                 'Plan_Odc_Port' => $a[15],
        //                 'Plan_Odc_Spl' => $a[16],
        //                 'Plan_Odc_Kap' => $a[17],
        //                 'Plan_Odp_Panel' => $a[18],
        //                 'Plan_Odp_Port' => $a[19],
        //                 'Plan_Odp_Spl' => $a[20],
        //                 'Plan_Odp_LAT'          => $a[21],
        //                 'Plan_Odp_LON'          => $a[22],
        //                 'Plan_Dist_Nama' => $a[23],
        //                 'Plan_Dist_Kap' => $a[24],
        //                 'Plan_Tenos' => $a[25],
        //                 'ID_VALIN'              => $a[26]];
        //             }
        //         }
        //     } else {
        //         return redirect('/')->with('alertblock', [
        //             ['type' => 'warning', 'text' => 'Format MCORE Tidak Sesuai!'],
        //         ]);
        //     }
        //     WaspangModel::insertcore($id,$upload);
        // }
        $lops = $logs = array();
        foreach($data as $d){
            $loker = 'Aanwijzing';
            $lops[] = [
                'status' => $req->status,
                'loker' => $loker,
                'Done_Aanwijzing' => date('Y-m-d H:i:s')
            ];
            $bobot=$this->getBobot($id,6);
            $logs[] = [
                'lop_id' => $d->id,
                'catatan' => 'Aanwijzing',
                'step_id'   => $d->step_id,
                'update_by' => $auth->id_user,
                'bobot'     => $bobot,
                'tematik_id'=> $d->mpromise_project_id
            ];
            $fname = "$d->waspang_$d->mpromise_project_id";
            $this->handleFileUploadWithOrName($req, $d->id, ['boq_unwizing', 'kml_unwizing', 'mcore_unwizing', 'apd_unwizing'],[$fname.'_boq_unwizing', $fname.'_kml_unwizing', $fname.'_mcore_unwizing', $fname.'_apd_unwizing'], public_path().'/storage/unwizingfile/');
        }

        WaspangModel::saveProgressAanwijzing($tematik,$waspang,$mitra,$lops);
        LopModel::insertlog($logs);
        return redirect('/')->with('alertblock', [
            ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
        ]);
    }

    public function saveSelesaiUnwizing($tematik,$waspang,$mitra, Request $req)
    {
        $step = 2;
        WaspangModel::saveSelesaiAanwijzing($tematik,$waspang,$mitra,['step_id'=>2]);
        return redirect('/listAanwijzing/'.$waspang)->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ],
        ]);
    }

    public function saveDelivery($id, Request $req)
    {
        $auth = session('auth');
        $step = 2;
        $data = LopModel::getById($id);
        $Done_Pengiriman_Material = '';
        $loker = 'Pengiriman_Material';
        $bobot=0;
        if ($req->status == 'Material_Ready') {
            $step = 4;
            $bobot=$this->getBobot($id,2);
            $loker = 'Pragelaran';
            $Done_Pengiriman_Material = date('Y-m-d H:i:s');
        }
        // $this->handlePotoUploads($id,$req, ["Material_Dimuat"]);
        WaspangModel::saveProgress($id, [
            'status' => $req->status,
            'tgl_material_tiba' => $req->tgl_material_tiba,
            'step_id' => $step,
            'loker' => $loker,
            'Done_Pengiriman_Material' => $Done_Pengiriman_Material
        ]);

        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'Delivery Material',
            'step_id'   => $data->step_id,
            'update_by' => $auth->id_user,
            'bobot'     => $bobot,
            'tematik_id'=> $data->mpromise_project_id
        ]);

        return redirect('/')->with('alertblock', [
            ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
        ]);
    }

    // public function savePragelaran($id, Request $req){
    //   $auth = session('auth');
    //   $this->handleFileUpload($req, $id, ["Cat_Tiang", "Slack_Kabel"]);
    //   $step = 3;
    //   $loker = "Pragelaran";
    //   if($req->status == "Pragelaran_Selesai"){
    //     $step = 4;
    //     $loker = "Tanam_Tiang";
    //   }
    //   WaspangModel::saveProgress($id, ["status"=>$req->status, "step_id" => $step, "loker"=>$loker]);
    //   LopModel::insertlog(["lop_id"=>$id, "catatan" => "Pragelaran : ".$req->status, "updated_at" => DB::raw('now()'), "update_by" => $auth->id_user]);
    //   return redirect('/')->with('alertblock', [
    //      ['type' => 'success', 'text' => 'Sukses Menyimpan data']
    //    ]);
    // }
    public function saveInstalasi($id, Request $req)
    {
        $auth = session('auth');
        $data = LopModel::getById($id);
        $stepdb = LopModel::getStepByID_Sys($data->step_id);

        $step=$stepdb->id;
        if ($req->status == 'Instalasi_Selesai') {
            $step=$stepdb->forward;
        }
        $status = '';
        $Done_Tanam_Tiang=$data->Done_Tanam_Tiang;
        $Done_Gelar_Kabel=$data->Done_Gelar_Kabel;
        $Done_Pasang_Terminal=$data->Done_Pasang_Terminal;
        $Done_Terminasi=$data->Done_Terminasi;
        if($req->status2){
            foreach ($req->status2 as $no => $s) {
                if ($s == 'Tanam_Tiang_Selesai' && !$Done_Tanam_Tiang) {
                    $Done_Tanam_Tiang=date('Y-m-d');
                } elseif ($s == 'Gelar_Kabel_Selesai' && !$Done_Gelar_Kabel) {
                    $Done_Gelar_Kabel=date('Y-m-d');
                } elseif ($s == 'Pasang_Terminal_Selesai' && !$Done_Pasang_Terminal) {
                    $Done_Pasang_Terminal=date('Y-m-d');
                } elseif ($s == 'Terminasi_Selesai' && !$Done_Terminasi) {
                    $Done_Terminasi=date('Y-m-d');
                }
                if ($no) {
                    $status .= ','.$s;
                } else {
                    $status .= $s;
                }
                $loker = $s;
            }
        }
        $return = 0;
        $mtr = MaterialModel::getInstalasi($id);
        foreach(json_decode($req->item) as $item){
            foreach($mtr as $itemplan){
                if($item->name==$itemplan->designator){
                    if($itemplan->onsite < $itemplan->installed+$item->value){
                        $return = 1;
                        break 2;
                    }
                }
            }
        }
        // dd($return,json_decode($req->item),$mtr);
        // 4902965446
        if($return){
            return redirect()->back()->with('alertblock', [
                    ['type' => 'Error', 'text' => 'Realisasi Melebihi Material On Site'],
                ]);
        }else{
            WaspangModel::saveMaterialIns(json_decode($req->item),$id);
            $this->handlePotoUploads($id,$req, ["Tanam_Tiang","Gelar_Kabel","Pasang_Terminal","Terminasi"]);
            // WaspangModel::saveMaterialInsLog(json_decode($req->item, true));
            $msg='';
            $designator = WaspangModel::getBoqByLop($id);
            $totalprogress = 0;
            foreach($designator as $no => $d){
              if($d->qty){
                $persentase = 0;
                if($d->inst && $d->deliv){
                  $persentase = $d->inst/$d->deliv*100;
                }
                $sisa = $d->deliv - $d->inst;
                $inst = $d->inst?:0;
                $msg .= "» ".$d->designator."  = ".$d->deliv."/".$inst."/".$sisa."/".round($persentase, 2)."%\n";
                $totalprogress += $persentase;
              }
            }
            $totalprogress = $totalprogress/($no+1);
            $msg .= "\nTotal Progress = ".round($totalprogress, 2)."%\n\nTeknisi/Waspang\n»".$data->waspang_nm."\n\nNote:\n»".$req->catatan."\n\nSTATUS :\n»".$data->status."";

            WaspangModel::saveProgress($id, ['NILAI_LOP' => $req->NILAI_LOP,'status' => $req->status, 'step_id' => $step, 'progres_instalasi' => $status, 'catatan_instalasi' => $req->catatan, 'Done_Tanam_Tiang'=>$Done_Tanam_Tiang,'Done_Gelar_Kabel'=>$Done_Gelar_Kabel,'Done_Pasang_Terminal'=>$Done_Pasang_Terminal,'Done_Terminasi'=>$Done_Terminasi,'persentase_mat_ins'=>round($totalprogress, 2)]);

            $persentaseawal = $data->persentase_mat_ins?:0;
            $bobot = (round($totalprogress, 2)-$persentaseawal)*0.666;
            if((round($totalprogress, 2)-$persentaseawal)<1){
                $bobot=0;
            }
            $getbobot = LopModel::gettotalbobotbylopidstep($id,4);
            if ($req->status == 'Instalasi_Selesai' || $bobot>66.6) {
                $bobot=66.6-$getbobot->total;
            }
            LopModel::insertlog(['lop_id' => $id, 'catatan' => 'Instalasi : '.$req->catatan, 'step_id'=>$data->step_id, 'update_by' => $auth->id_user, 'bobot' => $bobot, 'tematik_id'=> $data->mpromise_project_id]);
            $this->sendToTelegramInstalasi($id,$req,$msg,$totalprogress);
            return redirect('/progress/'.$id)->with('alertblock', [
                ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
            ]);
        }

    }
    public function uploadInstalasiSave(Request $req, $id)
    {
        $data = LopModel::getById($id);
        if ($req->hasFile('mcore_instalasi')) {
            $file = $req->file('mcore_instalasi');
            $nama_file = $file->getClientOriginalName();
            $arr = Excel::toArray(new UploadModel, $file);
            $upload = [];
            $key = array_search('ODP', array_column($arr[0], 1));
            if (isset($key)) {
                foreach ($arr[0] as $no => $a) {
                    if ($no > 2 && ($a[0] != '' || $a[0] != null)) {
                        $upload=[
                        'Real_Odp_Nama'         => $a[1],
                        'Real_IP_OLT'           => $a[2],
                        'Real_Olt_Port'         => $a[3],

                        'Real_Odf_Otb'          => $a[4],
                        'Real_Otb_Panel'        => $a[5],
                        'Real_Otb_Port'         => $a[6],
                        'Real_Otb_Kap'          => $a[7],

                        'Real_Otb_Odf_o'        => $a[8],
                        'Real_Otb_Panel_o'      => $a[9],
                        'Real_Otb_Port_o'       => $a[10],
                        'Real_Otb_Kap_o'        => $a[11],

                        'Real_FE_Nama'          => $a[12],
                        'Real_FE_Kap'           => $a[13],

                        'Real_Odc_Panel'        => $a[14],
                        'Real_Odc_Port'         => $a[15],
                        'Real_Odc_Spl'          => $a[16],
                        'Real_Odc_Kap'          => $a[17],

                        'Real_Odp_Panel'        => $a[18],
                        'Real_Odp_Port'         => $a[19],
                        'Real_Odp_Spl'          => $a[20],
                        'Real_Odp_LAT'          => $a[21],
                        'Real_Odp_LON'          => $a[22],

                        'Real_Dist_Nama'        => $a[23],
                        'Real_Dist_Kap'         => $a[24],
                        'Real_Tenos'            => $a[25],
                        'ID_VALIN'              => $a[26]];
                        // dd($upload);
                        WaspangModel::updatecore($upload, $a[1]);
                    }
                }
                GrabModel::UpdateGolive();
                GrabModel::UpdateValid3($id);
            } else {
                return redirect()->back()->with('alertblock', [
                    ['type' => 'Error', 'text' => 'Format MCORE Tidak Sesuai'],
                ]);
            }
        }
        $this->handleFileUpload($req, $id, ['abd', 'otdr', 'qrcode', 'kml_instalasi', 'mcore_instalasi', 'boq_instalasi', 'perijinan', 'RFS'], public_path().'/storage/'.$id.'/instalasifile/');
        $bobot = $this->getBobot($id,7);
        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'Pemberkasan',
            'step_id'   => $data->step_id,
            'update_by' => session('auth')->id_user,
            'tematik_id'=> $data->mpromise_project_id,
            'bobot'     =>$bobot
        ]);

        return redirect()->back()->with('alertblock', [
            ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
        ]);
    }
    public function saveQC(Request $req,$id)
    {
        $auth = session('auth');
        $data = LopModel::getById($id);
        $step = LopModel::getStepByID_Sys($data->step_id);
        $bobot = 0;
        if($req->status_qc=='QC_OK'){
            DB::table('mpromise_lop')->where('id', $id)->update([
                'step_id' => $step->forward,
                'status_qc' => $req->status_qc
            ]);
            $this->handleFileUpload($req,$id,["ba_ut", "ba_ct"], public_path().'/storage/'.$id.'/instalasifile/');
            $bobot = $this->getBobot($id,$data->step_id);
        }else{
            DB::table('mpromise_lop')->where('id', $id)->update([
                'step_id' => $step->backward,
                'status_qc' => $req->status_qc
            ]);
        }
        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'QC '.$req->catatan,
            'step_id'      => $data->step_id,
            'update_by' => $auth->id_user,
            'bobot'     => $bobot,
            'tematik_id'=> $data->mpromise_project_id
        ]);
        return redirect('/list/8')->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ],
        ]);
    }
    public function saveCTUT(Request $req,$id){
        $auth = session('auth');
        $data = LopModel::getById($id);
        $step = LopModel::getStepByID_Sys($data->step_id);
        DB::table('mpromise_lop')->where('id', $id)->update([
            'step_id' => $step->forward
        ]);
        $bobot = $this->getBobot($id,$data->step_id);
        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'CT/UT: '.$req->catatan,
            'step_id'   => $data->step_id,
            'update_by' => $auth->id_user,
            'bobot'     => $bobot,
            'tematik_id'=> $data->mpromise_project_id
        ]);
        $input = 'BUKTI_SETOR_BERKAS';
        if ($req->hasFile('photo-'.$input)) {
            $name = $input;
            //dd($input);
            $path = public_path().'/storage/'.$id.'/instalasifile/';
            if (! file_exists($path)) {
                if (! mkdir($path, 0770, true)) {
                    return [
                        'fail' => true,
                        'errors' => 'Gagal Menyiapkan Folder',
                    ];
                }
            }
            $file = $req->file('photo-'.$input);
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
                $moved = $file->move("$path", "$name.$ext");
                $img = new \Imagick($moved->getRealPath());
                $img->scaleImage(100, 150, true);
                $img->writeImage("$path/$name-th.$ext");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return [
                    'fail' => true,
                    'errors' => 'Gagal upload foto',
                ];
            }
        }
        return redirect('/')->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ],
        ]);
    }


    public function formKendala($id)
    {
        $data = LopModel::getById($id);
        return view('lop.formKendala', compact('data'));
    }
    public function saveKendala($id, Request $req)
    {
        $data = LopModel::getById($id);
        $auth = session('auth');
        $this->handlePotoUploads($id,$req, ["Evidence_Kendala"]);
        WaspangModel::saveProgress($id, [
            'timeplan_fisik' => $req->timeplan_fisik,
            'timeplan_berkas' => $req->timeplan_berkas,
        ]);

        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'Kendala',
            'step_id'   => $data->step_id,
            'update_by' => $auth->id_user,
            'tematik_id'=> $data->mpromise_project_id
        ]);

        return redirect('/project')->with('alertblock', [
            ['type' => 'success', 'text' => 'Sukses Menyimpan Data'],
        ]);
    }

    private function getBobot($id, $step)
    {

        $bobot = [1=>'1.1', 6=>'7.7', 2=>'7.7',7=>'16.5', 8=>'0', 5=>'0'];
        $exist = DB::table('mpromise_log')->where([
            ['step_id', $step],
            ['lop_id',$id]
        ])->first();
        if($exist){
            return 0;
        }else{
            return $bobot[$step];
        }
    }
    public function sendToTelegramInstalasi($id,$req,$m,$totalprogress){
        $diff = json_decode($req->item);
        setlocale(LC_ALL, 'IND');
        $dayList = array(
          'Sun' => 'Minggu',
          'Mon' => 'Senin',
          'Tue' => 'Selasa',
          'Wed' => 'Rabu',
          'Thu' => 'Kamis',
          'Fri' => 'Jumat',
          'Sat' => 'Sabtu'
        );
        $kegiatan="";
        foreach($diff as $d){
            if($d->value){
                $kegiatan .= "» ".$d->name." (".$d->value.")\n";
            }
        }
        // dd($kegiatan);
        $lop = LopModel::getById($id);
        $msg = "Waspang : ".$lop->waspang_nm."\nNama Proyek : ".$lop->nama_lop."\nID Proyek : ".$lop->pid."\nMitra : ".$lop->nama_mitra."\nTanggal : ".$dayList[date('D')].", ".date('d-m-Y')."\n\nKegiatan : \n".$kegiatan."\nTotal Realisasi\nItem/Target/Realisasi/Sisa/Persentase\n";


        $msg.=$m;

        $chat_id = '-767009927';
        Telegram::sendMessage([
            'chat_id' => $chat_id,
            'text' => $msg,
            'parse_mode' =>'html'
        ]);
        $foto = ["Tanam_Tiang","Gelar_Kabel","Pasang_Terminal","Terminasi"];
        foreach($foto as $f){
            $path = "/storage/".$id."/instalasifile/evident/".$f.".jpg";
            if (file_exists(public_path().$path)){
                Telegram::sendPhoto([
                  'chat_id' => $chat_id,
                  'caption' => "Foto ".str_replace("_"," ",$f),
                  'photo' => public_path().$path
                ]);
            }
        }
    }
    public function alista($id)
    {
        $data = LopModel::getById($id);
        $mtr = WaspangModel::getMtr($id, $data->pid);
        // dd($mtr);
        return view('alista.form', compact('data', 'mtr'));
    }

    // public function saveAlista($id, Request $req){
    //   WaspangModel::saveAlista(['alista_id' => $req->id_permintaan, 'lop_id'=>$id, 'create_at'=>DB::raw('now()'), 'create_by'=>session('auth')->id_user]);
    //   return redirect()->back()->with('alertblock', [
    //      ['type' => 'success', 'text' => 'Sukses Menyimpan data']
    //    ]);
    // }
    // public function deleteAlista($id){
    //   WaspangModel::deleteAlista($id);
    //   return redirect()->back()->with('alertblock', [
    //      ['type' => 'success', 'text' => 'Sukses Menghapus data']
    //    ]);
    // }
    public function uploadevident(Request $req)
    {
        $this->handlePotoUpload($req);
    }


    private function handleFileUpload($request, $id, $file, $path)
    {
        foreach ($file as $name) {
            $input = $name;
            if ($request->hasFile($input)) {
                if (! file_exists($path)) {
                    if (! mkdir($path, 0770, true)) {
                        return redirect()->back()->with('alertblock', [
                            ['type' => 'danger', 'text' => 'Gagal menyiapkan folder'],
                        ]);
                    }
                }
                $file = $request->file($input);
                //TODO: path, move, resize
                try {
                    $ext = $file->getClientOriginalExtension();
                    $moved = $file->move("$path", "$name.$ext");
                    $sv_data[$name] = "$name.$ext";
                    LopModel::update($id, $sv_data);
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return redirect()->back()->with('alertblock', [
                        ['type' => 'success', 'text' => 'Gagal Upload Foto'],
                    ]);
                }
            }
        }
    }
    private function handleFileUploadWithOrName($request, $id, $file, $nama, $path)
    {
        $sv_data = array();
        foreach ($file as $key => $name) {
            $input = $name;
            if ($request->hasFile($input)) {
                if (! file_exists($path)) {
                    if (! mkdir($path, 0770, true)) {
                        return redirect()->back()->with('alertblock', [
                            ['type' => 'danger', 'text' => 'Gagal menyiapkan folder'],
                        ]);
                    }
                }
                $file = $request->file($input);
                //TODO: path, move, resize
                try {
                    $ext = $file->getClientOriginalExtension();
                    $nama_file = $nama[$key].".$ext";
                    $moved = $file->move("$path", $nama_file);
                    $sv_data[$name] = $nama_file;
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return redirect()->back()->with('alertblock', [
                        ['type' => 'success', 'text' => 'Gagal Upload Foto'],
                    ]);
                }
            }
        }
        LopModel::update($id, $sv_data);
    }

    private function handlePotoUpload($request)
    {
        $input = 'file';
        if ($request->hasFile($input)) {
            $name = $request->nama;
            //dd($input);
            $path = public_path().'/storage/'.$request->id.'/instalasifile/evident/';
            if (! file_exists($path)) {
                if (! mkdir($path, 0770, true)) {
                    return [
                        'fail' => true,
                        'errors' => 'Gagal Menyiapkan Folder',
                    ];
                }
            }
            $file = $request->file($input);
            $ext = 'jpg';
            //TODO: path, move, resize
            try {
                $moved = $file->move("$path", "$name.$ext");
                $img = new \Imagick($moved->getRealPath());
                $img->scaleImage(100, 150, true);
                $img->writeImage("$path/$name-th.$ext");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return [
                    'fail' => true,
                    'errors' => 'Gagal upload foto',
                ];
            }
        }
    }
    private function handlePotoUploads($id,$request,$fotos)
    {
        foreach($fotos as $f){
            $input = "photo-".$f;
            if ($request->hasFile($input)) {
                $name = $f;
                //dd($input);
                $path = public_path().'/storage/'.$id.'/instalasifile/evident/';
                if (! file_exists($path)) {
                    if (! mkdir($path, 0770, true)) {
                        return [
                            'fail' => true,
                            'errors' => 'Gagal Menyiapkan Folder',
                        ];
                    }
                }
                $file = $request->file($input);
                $ext = 'jpg';
                //TODO: path, move, resize
                try {
                    $moved = $file->move("$path", "$name.$ext");
                    $img = new \Imagick($moved->getRealPath());
                    $img->scaleImage(100, 150, true);
                    $img->writeImage("$path/$name-th.$ext");
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return [
                        'fail' => true,
                        'errors' => 'Gagal upload foto',
                    ];
                }
            }
        }

    }

    public function waspang_search(Request $req)
    {
        $data = WaspangModel::search_waspang(trim($req->find));
        if ($data) {
            foreach ($data as $d) {
                $formatted_tags[] = ["id" => $d->nik, "text" => "$d->nama ( $d->nik )"];
            }
        } else {
            $formatted_tags = [];
        }

        return \Response::json($formatted_tags);
    }

    public function list_waspang()
    {
        $witel=session('auth')->witel;
        $data = WaspangModel::waspang_list_by_witel($witel);
        return view('waspang_crt.list_waspang', compact('data'));
    }

    public function form_waspang($id)
    {
        $data = WaspangModel::getWaspangByIDs($id);
        $witel = WITELModel::getAll();
        // dd($data);
        return view('waspang_crt.form', compact('data', 'witel'));
    }

    public function save_waspang($id,Request $req)
    {
        $data = WaspangModel::getWaspangByIDs($id);
        if($data){
            WaspangModel::updatewaspang($id, ["nik"=>$req->nik,"nama"=>$req->nama,"nik_tl"=>$req->nik_tl,"nama_tl"=>$req->nama_tl,"label"=>$req->label,"waspang_witel"=>$req->waspang_witel]);
        }else{
            WaspangModel::insertwaspang(["nik"=>$req->nik,"nama"=>$req->nama,"nik_tl"=>$req->nik_tl,"nama_tl"=>$req->nama_tl,"label"=>$req->label,"waspang_witel"=>$req->waspang_witel]);
        }
        return redirect('/waspang');
    }

    // public function preview_excel(Request $req)
    // {
    //     if(!$req->data)
    //     {
    //         $req->data = 'B,G';
    //     }
    //     $kolom = explode(',', trim($req->data, ' '));
    //     $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
    //     $reader->setReadDataOnly(true);
    //     // dd($req->all());
    //     if (!$req->hasFile('file')) {
    //         $data = WaspangModel::show_berkas($req->id);
    //         $file = public_path().'/storage/'.$req->id.'/unwizingfile/'.$data->boq_unwizing;
    //     } else {
    //         $file = $req->file('file');
    //     }
    //     // dd('tes');
    //     $spreadsheet = $reader->load($file);
    //     $worksheet = $spreadsheet->getSheet(0);
    //     // Get the highest row and column numbers referenced in the worksheet
    //     $highestRow = $worksheet->getHighestRow(); // e.g. 10
    //     // $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
    //     // $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 6
    //     // $highestColumn = $kolom[0];
    //     // $excel=[];
    //     // for ($row = 8; $row <= $highestRow; ++$row) {
    //     //     for ($col = 1; $col <= $highestColumnIndex; ++$col) {
    //     //         $value = $worksheet->getCellByColumnAndRow($col, $row)->getValue();
    //     //         $excel[] = $value;
    //     //     }
    //     // }
    //     // $highestColumn = $kolom[0];
    //     $excel=[];
    //     foreach ($kolom as $k){
    //         $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($k); // e.g. 6
    //         for ($row = 6; $row <= $highestRow; ++$row) {
    //             $value = $worksheet->getCellByColumnAndRow($highestColumnIndex, $row)->getFormattedValue();
    //             $excel[$row][] = $value;
    //         }
    //     }
    //     foreach($excel as $key => $val)
    //     {
    //         if($val[0] == '')
    //         {
    //             unset($excel[$key]);
    //         }
    //     }
    //     return $excel;
    // }

    public function berkas_index($id)
    {
        $data = WaspangModel::show_berkas($id);
        return view('lop.pemberkasan', compact('data'));
    }

    public function berkas_update_boq(Request $req, $id)
    {
        $arr = json_decode($req->result_berkas, true);
        $upload = [];
        foreach ($arr as $no => $a) {
            if ($no >= 8 && $a[0] != '' && $a[1] != '') {
                $upload[] = ['designator' => $a[0], 'qty' => $a[1], 'lop_id' => $id, 'status' => 2];
            }
        }
        WaspangModel::insertboq($id, $upload);
        $this->handleFileUpload($req, $id, ['boq_unwizing'], public_path().'/storage/'.$id.'/unwizingfile/');


        return redirect('/')->with('alertblock', [
            ['type' => 'success', 'text' => 'BOQ Telah Diperbaharui!'],
        ]);

    }

    public function update_wasp(Request $req, $id)
    {
        $msg = WaspangModel::updatetim_Mitra($req, $id);
        return redirect($msg['url'])->with($msg['isi']);
    }

	public function download_rar($id)
	{
		$time = round(microtime(true) * 1000);
		$data = DB::table('mpromise_lop')->where('id', $id)->first();
        // dd($data);

        \File::copyDirectory((public_path().'/storage/'.$id) , (public_path() . '/upload/' . $id . '_copyan_' . $time));

		$rootPath = public_path() . '/upload/' . $id . '_copyan_' . $time;
		$namel = str_replace('/', '-', "$data->nama_lop");
		$new_name_file = $namel . '.zip';

		$zip = new ZipArchive();
		$zip->open($new_name_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);

		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath) , RecursiveIteratorIterator::LEAVES_ONLY);
		foreach ($files as $name => $file)
		{
			if (!$file->isDir())
			{
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen($rootPath) + 1);
				$zip->addFile($filePath, $file->GetFileName());
			}
		}
		$zip->close();
		\File::deleteDirectory($rootPath);

		$headers =['Content-type: application/zip'];

		$response = response()->download(public_path() .'/'. $new_name_file, $new_name_file, $headers);
		register_shutdown_function('unlink', public_path() .'/'. $new_name_file);

        return $response;
	}
    public function plan(){
        $auth = session('auth');
        $data = LopModel::getAllByWaspang($auth->id_user);
        $plan = LopModel::getPlanWaspangItemByWaspang($auth->id_user);
        return view('lop.planwaspang', compact('data','plan'));
    }
    public function savePlanWaspang(Request $req){
        $auth = session('auth');
        WaspangModel::saveplanwaspang($auth,$req);
        return redirect('/')->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ],
        ]);
    }
    private $diraray = [];
    private function listFolderFiles($dir){
        $ffs=[];
        if(is_dir($dir)){
            $ffs = scandir($dir);
        }

        unset($ffs[array_search('.', $ffs, true)]);
        unset($ffs[array_search('..', $ffs, true)]);

        // prevent empty ordered elements
        if (count($ffs) < 1){
            return;
        }

        // echo '<ol>';
        $no=0;
        foreach($ffs as $ff){
            // echo '<li>'.$dir.$ff;
            if(is_dir($dir.'/'.$ff)){
                $this->listFolderFiles($dir.'/'.$ff);
            }else{
                if(!str_contains($ff, ['zip', 'rar'])){
                    // $this->diraray[] = ['url'=>$dir, 'name'=>array()];
                    if(!$no){
                        $this->diraray[] = ['url'=>$dir, 'name'=>array($ff)];
                    }else{
                        $this->diraray[count($this->diraray)-1]['name'][] = $ff;
                    }
                    $no++;
                }
            }
            // echo '</li>';
        }
        // echo '</ol>';
    }
    public function saveuploadfotorar($id,Request $req)
    {
        $input = 'fotorar';
        $path=public_path().'/storage/'.$id.'/fotorar/';
        if ($req->hasFile($input)) {
            if (! file_exists($path)) {
                if (! mkdir($path, 0770, true)) {
                    return redirect('/progress/'.$id)->with('alertblock', [
                        ['type' => 'danger', 'text' => 'Gagal menyiapkan folder'],
                    ]);
                }
            }
            $file = $req->file($input);
            try {
                $name = $file->getClientOriginalName();
                $moved = $file->move("$path", $name);
                $zip = new \ZipArchive();
                $res = $zip->open($path.$name);
                if ($res === TRUE) {
                  $zip->extractTo(public_path().'/storage/'.$id.'/fotorar/');
                  $zip->close();
                  echo 'woot!';
                } else {
                  echo 'doh!';
                }

            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return redirect('/progress/'.$id)->with('alertblock', [
                    ['type' => 'danger', 'text' => 'Gagal Upload File'],
                ]);
            }
        }
        return redirect('/progress/'.$id)->with('alertblock', [
            ['type' => 'success', 'text' => 'Sukses Upload File'],
        ]);
    }
    public function deletefotorar($id)
    {
        $this->delTree(public_path()."/storage/$id/fotorar/");
        return redirect('/progress/'.$id)->with('alertblock', [
            ['type' => 'success', 'text' => 'Berhasil menghapus foto zip'],
        ]);
    }
    private function delTree($dir) {
        $files = array_diff(scandir($dir), array('.','..'));
        foreach ($files as $file) {
            (is_dir("$dir/$file")) ? $this->delTree("$dir/$file") : unlink("$dir/$file");
        }
        return rmdir($dir);
    }

    public static function send_photo_doc()
    {
        $tematik = DB::table('mpromise_project')->where('stts', 0)->get();

        foreach($tematik as $v)
        {
            exec('cd /srv/htdocs/puppet_rend;nodejs bts_foto_matrix_waspang.js '. $v->id);
		    $file = '/srv/htdocs/puppet_rend/foto_bts'.$v->id.'.png';

            Telegram::sendPhoto([
                'chat_id' => '-767009927',
                'caption' => "Foto Matrix $v->nama Periode ".date('Y-m-d'),
                'photo' => $file
            ]);
        }

        $query = DB::table('mpromise_project')->select('*','nama as text', DB::raw('(select count(*) from mpromise_lop where mpromise_project_id=mpromise_project.id and isHapus=0) as totallop'))->where('stts', 0)->orderBy('id','desc')->get();

        foreach($query as $v)
        {
            Excel::store(new LopExport($v->id), '/Excel_DM/rekap.xlsx');
            Telegram::sendDocument([
                'chat_id' => '-767009927',
                'document' => 'storage/app/Excel_DM/rekap.xlsx',
                'caption' => "Dokumen Excel Project $v->nama",
              ]);
        }
    }

	public function send_laporan()
	{
		WaspangController::send_photo_doc();
	}

	public function pickup_jointer(Request $req, $id_ml)
	{
        DB::SELECT("DELETE prj, pm
        FROM mpromise_core mc
        LEFT JOIN mpromise_lop ml ON ml.id = mc.LOP_ID
        LEFT JOIN pt2_master pm ON pm.id_bts = mc.ID_Sys
        LEFT JOIN pt2_ready_jointer prj ON prj.id_wo = mc.ID_Sys AND prj.jenis = 'bts'
        WHERE ml.id = '$id_ml' AND pm.lt_status IS NULL AND (prj.id IS NOT NULL OR pm.id IS NOT NULL)");

		if($req->pilih_jointer == 'freelance')
        {
            foreach($req->odp as $v)
            {
                DB::table('pt2_ready_jointer')->insert([
                    'id_wo'      => $v,
                    'jenis'      => 'bts',
                    "created_by" => session('auth')->id_user,
                ]);
            }
        }
        else
        {
            $order['nik1'] = $req->tim_jointer;
            $nama_jointer = DB::table('1_2_employee')->where('nik', $req->tim_jointer)->first();

            $order['jointer_name'] = $nama_jointer->nama;

            foreach($req->odp as $v)
            {
                $data_outside =  DB::table('mpromise_lop As ml')
                ->Leftjoin('mpromise_core As mc', 'mc.LOP_ID', '=', 'ml.id')
                ->leftjoin('pt2_master As pm', 'mc.ID_Sys', '=', 'pm.id_bts')
                ->select('mc.ID_Sys As id', 'ml.sto', 'ml.created_at', 'mc.Plan_Odp_Nama As nama_odp', DB::RAW("CONCAT(mc.Plan_Odp_LAT,',',mc.Plan_Odp_LON) As koordinat"), 'ml.nama_lop As nama_order' )
                ->where('mc.ID_Sys', $v)
                ->first();

                $check_bts = DB::Table('pt2_master')->where('id_bts', $v)->first();

                if($check_bts)
                {
                    $id = $check_bts->id;
                    DB::table('pt2_master')->where('id', $id)->update($order);
                }
                else
                {
                    $order['jenis_wo']     = 'PT-2 SIMPLE';
                    $order['id_bts']       = $v;
                    $order['odp_nama']     = $data_outside->nama_odp;
                    $order['odp_koor']     = $data_outside->koordinat ?? '-';
                    $order['catatan_HD']   = $data_outside->nama_order;
                    $order['sto']          = $data_outside->sto;
                    $order['project_name'] = $data_outside->nama_order;

                    $get_odc = explode('/', $data_outside->nama_odp)[0];
                    $get_odc = explode('-', $get_odc);
                    $get_odc = 'ODC-'.$get_odc[1].'-'.$get_odc[2];

                    $order['odc_nama'] = $get_odc;

                    $split_odc = explode('-', $order['odc_nama']);

                    $get_info_odc = WaspangModel::get_info_odc($split_odc[2], $split_odc[1]);

                    if(count($get_info_odc) )
                    {
                        $order['odc_koor'] = str_replace(',', '.', $get_info_odc[0]->latitude) .','. str_replace(',', '.', $get_info_odc[0]->longitude);
                    }

                    $order['kategory_non_unsc'] = 1;
                    $order['delete_clm']        = 0;
                    $order['tgl_pengerjaan']    = date('Y-m-d H:i:s');

                    $order['status'] = 'Construction';
                    $order['tgl_buat'] = date('Y-m-d H:i:s');

                    $id = DB::table('pt2_master')->insertGetId($order);
                }

                DB::table('pt2_dispatch')->insert([
                    'id_order'           => $id,
                    'nik1'               => $order['nik1'],
                    'keterangan'         => 'Pickup Order Jointer',
                    'tgl_kerja_dispatch' => date('Y-m-d H:i:s'),
                    'updated_by'         => session('auth')->id_user
                ]);
            }
        }

        return redirect('/progress/'.$id_ml)->with('alertblock', [
            ['type' => 'success', 'text' => 'Berhasil Membuat Pekerjaan Jointer'],
        ]);
	}
}
