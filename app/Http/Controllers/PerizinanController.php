<?php
namespace App\Http\Controllers;

use App\DA\LopModel;
use App\DA\IzinModel;
use Illuminate\Http\Request;

class PerizinanController extends Controller
{
  //mitra
  public function form($id,$izinid)
  {
    $data = LopModel::getById($id);
    $form = IzinModel::getById($izinid);
    $list = IzinModel::getByLopId($id);
    return view('lop.perizinan', compact('data','form','list'));
  }
  public function save($id, Request $req)
  {
    $exist = IzinModel::getById($id);
    if($exist){
      IzinModel::update($id, ["lop_id"=> $req->lop_id, "status"=> $req->status, "nilai" =>$req->nilai, "tgl_status" =>$req->tgl_status, "tipe_izin" =>$req->tipe_izin]);
    }else{
      IzinModel::insert(["mitra" => $req->mitra, "label" => $req->label, "status" => $req->status, "witel_mitra" =>$req->witel_mitra]);
    }
    return redirect()->back();
  }

  //permit masuk site
  public function formPermit($id)
  {
    $data = LopModel::getById($id);
    return view('lop.permit', compact('data'));
  }
  public function savePermit($id, Request $req)
  {
    // $data = LopModel::getById($id);
    $this->handlePotoUploads($id,$req, ["Permit_A","Permit_B"]);
    LopModel::update($id,["tgl_submit_permit_a"=>$req->tgl_submit_permit_a,"tgl_submit_permit_b"=>$req->tgl_submit_permit_b,"tgl_permit_a"=>$req->tgl_permit_a, "tgl_permit_b"=>$req->tgl_permit_b]);
    return redirect()->back();
  }
  private function handlePotoUploads($id,$request,$fotos)
  {
      foreach($fotos as $f){
          $input = "photo-".$f;
          if ($request->hasFile($input)) {
              $name = $f;
              //dd($input);
              $path = public_path().'/storage/'.$id.'/instalasifile/evident/';
              if (! file_exists($path)) {
                  if (! mkdir($path, 0770, true)) {
                      return [
                          'fail' => true,
                          'errors' => 'Gagal Menyiapkan Folder',
                      ];
                  }
              }
              $file = $request->file($input);
              $ext = 'jpg';
              //TODO: path, move, resize
              try {
                  $moved = $file->move("$path", "$name.$ext");
                  $img = new \Imagick($moved->getRealPath());
                  $img->scaleImage(100, 150, true);
                  $img->writeImage("$path/$name-th.$ext");
              } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                  return [
                      'fail' => true,
                      'errors' => 'Gagal upload foto',
                  ];
              }
          }
      }

  }
}
