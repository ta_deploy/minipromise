<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Illuminate\Support\Facades\Session;

class APIController extends Controller
{
    public function insertLOP(Request $req)
    {
        if(!DB::table('mpromise_lop')->where('promitos_id', $req->promitos_id)->first()){
            $tematik = DB::table('mpromise_project')->where('promitos_tid',$req->tematik_id)->first();
            // dd($tematik);
            $param = [
                "step_id"             => 0,
                "nama_lop"            => $req->nama_lop,
                "ring"                => $req->ring,
                "witel_lop"           => 'KALSEL',
                "sto"                 => $req->sto,
                "jenis_pekerjaan"     => $req->jenis_pekerjaan,
                "promitos_id"         => $req->promitos_id,
                "mpromise_project_id" => $tematik->id,
                "nama_project"        => $tematik->nama
            ];
            return DB::table('mpromise_lop')->insertGetId($param);
        }
    }
    public function insertTEMATIK(Request $req)
    {
        if(!DB::table('mpromise_project')->where('promitos_tid', $req->tematik_id)->first()){
            $param = [
                "promitos_tid"      =>$req->tematik_id,
                "nama"              =>$req->nama_tematik,
                "budget"            =>$req->budget,
                "created_by"        =>'PROMITOS'
            ];
            return DB::table('mpromise_project')->insertGetId($param);
        }
    }
}