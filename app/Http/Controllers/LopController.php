<?php

namespace App\Http\Controllers;

use App\DA\LopModel;
use App\DA\UploadModel;
use App\DA\WaspangModel;
use App\DA\MitraModel;
use App\DA\ProjectModel;
use DB;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LopController extends Controller
{
    //-722356370 piloting tomman project
    public function list($id)
    {
    //     $auth = session('auth');
        $data = LopModel::getByStepId($id);
        // dd($data);
    //     $group = array();
    //     $no = 0;
    //     foreach ($data as $key => $value) {
    //         $group[trim($value->nama, " ")]['project_nama'] = $value->nama;
    //         $group[trim($value->nama, " ")]['count'] = $no += count($value);
    //         $group[trim($value->nama, " ")]['data'][$value->pid][] = $value;
    //     }
    //     // dd(count($group['lanjutan project rendy']['data']), $group);
        return view('lop.list', compact('data'));
    }
    public function droplop($id)
    {
        LopModel::droplop($id);
        return back();
    }
    public function hapuslop($id)
    {
        LopModel::hapuslop($id);
        return back();
    }
    public function listLopByProject($id)
    {
        $witel = session('auth')->witel;
        $data = LopModel::getByProject($witel,$id);
        return view('lop.listlop', compact('data'));
    }
    public function backstep($id)
    {
        $data = LopModel::getById($id);
        $step = LopModel::getStepByID_Sys($data->step_id);
        DB::table('mpromise_lop')->where('id', $id)->update([
            'step_id' => $step->backward
        ]);
        return redirect()->back();
    }
    public function nextstep(Request $req)
    {
        $data = LopModel::getById($req->id);
        $step = LopModel::getStepByID_Sys($data->step_id);
        DB::table('mpromise_lop')->where('id', $req->id)->update([
            'step_id' => $step->forward
        ]);
        return redirect('/')->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ],
        ]);
    }

    public function updateForm($id, $id_pro)
    {
        $datalop = LopModel::getByMproId($id);
        $mitra = MitraModel::listMitra();
        $data = LopModel::getById($id_pro);
        $waspang = WaspangModel::getWaspang();
        $sto = LopModel::getSto();
        return view('lop.registerForm', compact('data', 'sto', 'mitra', 'datalop', 'waspang'), ['id' => $id_pro]);
    }
    public function uploadLop($id)
    {
        $data = ProjectModel::getById($id);
        return view('lop.uploadLop', compact('data'));
    }

    // public function save($id, $jenis, $idp, Request $req)
    // {
    //     $auth = session('auth');
    //     if ($req->hasFile('upload_lop')) {
    //         $file = $req->file('upload_lop');
    //         $nama_file = $file->getClientOriginalName();
    //         $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader('Xlsx');
    //         $reader->setReadDataOnly(true);
    //         $spreadsheet = $reader->load($file);
    //         $worksheet = $spreadsheet->getSheet(0);

    //         $highestColumn = $worksheet->getHighestColumn(); // e.g 'F'
    //         $highestRow = $worksheet->getHighestRow();
    //         // dd($id);
    //         $highestColumnIndex = \PhpOffice\PhpSpreadsheet\Cell\Coordinate::columnIndexFromString($highestColumn); // e.g. 6
    //         $arr=[];
    //         for ($row = 1; $row <= $highestRow; ++$row) {
    //             for ($col = 1; $col <= $highestColumnIndex; ++$col) {
    //                 $value = $worksheet->getCellByColumnAndRow($col, $row)->getFormattedValue();
    //                 $arr[$row][] = $value;
    //             }
    //         }
    //         // dd($arr);
    //         $pid = array_search('ID Project', array_column($arr, 3));
    //         $sto = array_search('STO', array_column($arr, 2));
    //         $waspang = array_search('NIK Waspang', array_column($arr, 6));
    //         $waspang_nm = array_search('Nama Waspang', array_column($arr, 7));

    //         if ($sto !== FALSE && $waspang !== FALSE && $waspang_nm !== FALSE) {
    //             unset($arr[1]);
    //             foreach ($arr as $key => $val) {
    //                 $data[$key]=array(
    //                     "nama_lop"=>$val[12],
    //                     "nama_project"=>ProjectModel::getById($id)->nama,
    //                     "pid"=>$val[3],
    //                     "sto"=>$val[2],
    //                     "mitra"=>$val[5],
    //                     "waspang"=>$val[6],
    //                     "waspang_nm"=>$val[7],
    //                     "mpromise_project_id"=>$id,
    //                     "step_id"=>6,
    //                     "created_by"=>$auth->id_user,
    //                     "loker"=>"Aanwijzing"
    //                 );
    //             }
    //             // $temp_arr = array_unique(array_column($data, 'pid'));
    //             // $result_array = array_intersect_key(array_values($data), $temp_arr);
    //             // dd($data);
    //             LopModel::insert_upload_lop($data);
    //         } else {
    //             return redirect()->back()->with('alertblock', [
    //                 ['type' => 'warning', 'text' => 'Format LOP Tidak sesuai!'],
    //             ]);
    //         }
    //     } else {

    //         // DB::transaction(function () use ($req, $jenis, &$id, $auth){
    //             $id = LopModel::insert($id, $jenis, $req);
    //             if ($req->hasFile('boq_unwizing')) {
    //                 $file = $req->file('boq_unwizing');
    //                 $nama_file = $file->getClientOriginalName();
    //                 $arr = Excel::toArray(new UploadModel, $file);
    //                 $upload = [];
    //                 $key = array_search('DESIGNATOR', array_column($arr[0], 1));
    //                 if ($key) {
    //                     foreach ($arr[0] as $no => $a) {
    //                         if ($no >= 8 && $a[1] != '' && $a[6] != '' && substr($a[6], 0, 1) != '=') {
    //                             $upload[] = ['designator' => $a[1],'hargamaterial' => $a[4],'hargajasa' => $a[5], 'qty' => $a[6], 'lop_id' => $id, 'status' => 2];
    //                         }
    //                     }
    //                     // dd($upload);
    //                     WaspangModel::insertboq($id,$upload);
    //                 }
    //             }
    //             LopModel::insertlog([
    //                 'lop_id' => $id,
    //                 'catatan' => 'Registered LOP',
    //                 'updated_at' => DB::raw('now()'),
    //                 'step_id'   => 1,
    //                 'update_by' => $auth->id_user,
    //                 'tematik_id'=> $id
    //             ]);
    //         // });
    //     }
    //     return redirect()->back()->with('alertblock', [
    //         ['type' => 'success', 'text' => 'Sukses Menyimpan data'],
    //     ]);
    // }

    public function saveProject(Request $req)
    {
      DB::transaction(function () use ($req) {
          return LopModel::saveProject($req);
      });
    }

    public function search(Request $req)
    {
        $data = LopModel::getSearch($req->q);
        return view('lop.listlop', compact('data'));
    }

    public function download($path)
    {
        $path = str_replace('+', '/', $path);
        return response()->download($path);
    }

    public function find_boq_unw(Request $req)
    {
        $term = trim($req->q);

        if (empty($term)) {
            return \Response::json([]);
        }

        $fetchData = LopModel::search_boq_idlop($term);

        $formatted_tags = [];

        foreach ($fetchData as $row) {
            $formatted_tags[] = ["id" => $row->id_project, "text" => $row->boq_unwizing . ' ( Project ' .$row->nama. ' / ' . ($row->pid ?: 'PID Kosong') . ')'];
        }

        return \Response::json($formatted_tags);
    }
    public function detilajax($id)
    {
        $log = LopModel::getLogByLop($id);
        $core = LopModel::getCoreByLopId($id);
        $step = LopModel::getStepActive($id);
        $boq = WaspangModel::getBoqByLop($id);
        $data = LopModel::getById($id);
        return view('detillop', compact('log','core','boq', 'data','step'));
    }
    //PERUBAHAN DESIGN
    public function formperubahandesign($id)
    {
        $data = LopModel::getById($id);
        $boq = WaspangModel::getBoqByLop($id);
        // dd($boq);
        // $core = LopModel::getCoreByLopId($id);
        return view('lop.perubahandesign', compact('data', 'boq'));
    }
    public function saveperubahandesign($id, Request $req)
    {
        $auth = session('auth');
        if ($req->hasFile('boq_unwizing')) {
            $file = $req->file('boq_unwizing');
            $nama_file = $file->getClientOriginalName();
            $arr = Excel::toArray(new UploadModel, $file);
            $upload = [];
            $key = array_search('DESIGNATOR', array_column($arr[0], 1));
            if ($key) {
                foreach ($arr[0] as $no => $a) {
                    if ($no >= 8 && $a[1] != '' && $a[6] != '' && substr($a[6], 0, 1) != '=') {
                        $upload[] = ['designator' => $a[1],'hargamaterial' => $a[4],'hargajasa' => $a[5], 'qty' => $a[6], 'lop_id' => $id, 'status' => 2];
                    }
                }
                WaspangModel::insertboq($id,$upload);
            }
        }
        if ($req->hasFile('mcore_unwizing')) {
            $file = $req->file('mcore_unwizing');
            $nama_file = $file->getClientOriginalName();
            $arr = Excel::toArray(new UploadModel, $file);
            $upload = [];
            $key = array_search('ODP', array_column($arr[0], 1));
            if (isset($key)) {
                foreach ($arr[0] as $no => $a) {
                    if ($no > 2 && ($a[0] != '' || $a[0] != null)) {
                        $upload[] = [
                        'LOP_ID'           => $id,
                        'Plan_Odp_Nama'    => $a[1],
                        'Plan_IP_OLT'      => $a[2],
                        'Plan_Olt_Port'    => $a[3],

                        'Plan_Odf_Otb'     => $a[4],
                        'Plan_Otb_Panel'   => $a[5],
                        'Plan_Otb_Port'    => $a[6],
                        'Plan_Otb_Kap'     => $a[7],

                        'Plan_Otb_Odf_o'   => $a[8],
                        'Plan_Otb_Panel_o' => $a[9],
                        'Plan_Otb_Port_o'  => $a[10],
                        'Plan_Otb_Kap_o'   => $a[11],

                        'Plan_FE_Nama'     => $a[12],
                        'Plan_FE_Kap'      => $a[13],

                        'Plan_Odc_Panel'   => $a[14],
                        'Plan_Odc_Port'    => $a[15],
                        'Plan_Odc_Spl'     => $a[16],
                        'Plan_Odc_Kap'     => $a[17],

                        'Plan_Odp_Panel'   => $a[18],
                        'Plan_Odp_Port'    => $a[19],
                        'Plan_Odp_Spl'     => $a[20],
                        'Plan_Odp_LAT'     => $a[21],
                        'Plan_Odp_LON'     => $a[22],

                        'Plan_Dist_Nama'   => $a[23],
                        'Plan_Dist_Kap'    => $a[24],
                        'Plan_Tenos'       => $a[25],
                        'ID_VALIN'         => $a[26]];
                        // dd($upload);
                    }
                }

                foreach($upload as $v)
                {
                    foreach($v as $vv)
                    {
                        if(substr($vv, 0, 1) == '=')
                        {
                            return redirect('/perubahandesign/'.$id)->with('alertblock', [
                                ['type' => 'warning', 'text' => 'Terbaca Rumus Pada MCORE!'],
                            ]);
                        }
                    }
                }
            }else{
                return redirect('/perubahandesign/'.$id)->with('alertblock', [
                    ['type' => 'warning', 'text' => 'Format MCORE Tidak Sesuai!'],
                ]);
            }
            // dd($upload);
            WaspangModel::insertcore($id,$upload);
        }
        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'Perubahan Desgin',
            'updated_at' => DB::raw('now()'),
            'update_by' => $auth->id_user
        ]);

        $this->handleFileUpload($req, $id, ['boq_unwizing', 'mcore_unwizing'], public_path().'/storage/'.$id.'/unwizingfile/');
        return redirect('/perubahandesign/'.$id)->with('alertblock', [
            [
                'type' => 'success',
                'text' => 'Sukses Menyimpan Data'
            ],
        ]);
    }
    private function handleFileUpload($request, $id, $file, $path)
    {
        foreach ($file as $name) {
            $input = $name;
            if ($request->hasFile($input)) {
                if (! file_exists($path)) {
                    if (! mkdir($path, 0770, true)) {
                        return redirect('/perubahandesign/'.$id)->with('alertblock', [
                            ['type' => 'danger', 'text' => 'Gagal menyiapkan folder'],
                        ]);
                    }
                }
                $file = $request->file($input);
                try {
                    $ext = $file->getClientOriginalExtension();
                    $moved = $file->move("$path", "$name.$ext");
                    $sv_data[$name] = "$name.$ext";
                    LopModel::update($id, $sv_data);
                } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                    return redirect('/perubahandesign/'.$id)->with('alertblock', [
                        ['type' => 'success', 'text' => 'Gagal Upload File'],
                    ]);
                }
            }
        }
    }
    public function savemanualsetmcore(Request $req){
        return WaspangModel::manualsetcore($req);
    }

	public function editlop($id)
	{
		$datalop = [];
        $mitra = MitraModel::listMitra();
        $data = LopModel::getById($id);
        $waspang = WaspangModel::getWaspang();
        $sto = LopModel::getSto();
        return view('lop.registerForm', compact('data', 'sto', 'mitra', 'datalop', 'waspang'), ['id' => $id]);
	}

	public function updatelop(Request $req, $id)
	{
        $auth = session('auth');
        $data = LopModel::getById($id);
        LopModel::update_lop($id, $req);
        LopModel::insertlog([
            'lop_id' => $id,
            'catatan' => 'Edit LOP',
            'updated_at' => DB::raw('now()'),
            'step_id'   => 1,
            'update_by' => $auth->id_user
        ]);

        return redirect('/listLopByProject/'.$data->id_project)->with('alertblock', [
            ['type' => 'success', 'text' => 'Berhasil Mengubah LOP'],
        ]);
    }
}
