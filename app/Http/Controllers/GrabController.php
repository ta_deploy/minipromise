<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DB;
use App\DA\GrabModel;
use Telegram;

class GrabController extends Controller
{
	public static function odp_regional(){

    ini_set('max_execution_time', 60000);
    $chat_id = '52369916';
    Telegram::sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Start Sync ODP REG',
        'parse_mode' =>'html'
    ]);
    $filename = "ftp://witel:damantr6@10.128.16.116/ODP_INFO_WITEL_KALSEL.txt";
    $handle = fopen($filename, "r");
    $data_to_insert = array();

    while(!feof($handle)){
      $data = explode(";", fgets($handle));
      $data = str_replace('"', '', $data);

      if (count($data)>2){
          $data_to_insert[] = [
              "ODP_EID"         => $data[0],
              "REGIONAL"        => $data[1],
              "WITEL"           => $data[2],
              "DATEL"           => $data[3],
              "STO"             => $data[4],
              "STO_NAME"        => $data[5],
              "ODP_ID"          => $data[6],
              "ODP_NAME"        => $data[7],
              "ODP_LOCATION"    => $data[8],
              "LATITUDE"        => $data[9],
              "LONGITUDE"       => $data[10],
              "ODP_ADDRESS"     => $data[11],
              "ODP_STATUS"      => $data[12],
              "OWNEDBY"         => $data[13],
              "MANAGEDBY"       => $data[14],
              "ODP_ROLE"        => $data[15],
              "OCCUPANCY"       => $data[16],
              "CREATEDATE"      => $data[17],
              "PROCESS_DATE"    => $data[18],
              "ISI"             => $data[19],
              "ISI_DESCRIPTION" => $data[20],
              "KOSONG"          => $data[21],
              "DROPCORE"        => $data[22],
              "TOTAL"           => $data[23],
              "ODP_PANEL"       => $data[24],
              "ODP_PORT"        => $data[25],
              "ODP_OLT"         => $data[26],
              "ODP_OLT_PORT"    => $data[27],
              "ODP_STATUS_CONN" => $data[28],
              "ODC"             => $data[29],
              "ODF"             => $data[30],
              "DISTRIBUSI"      => $data[31],
              "DISTRIBUSI_CORE" => $data[32],
              "FEEDER"          => $data[33],
              "FEEDER_CORE"     => $data[34]
            ];
      }

    }
    // dd($data_to_insert[31350]);
    $records = array_chunk($data_to_insert, 1000);
    DB::table('master_odp')->truncate();
    foreach ($records as $batch) {
          try {
            DB::table('master_odp')
            ->insert($batch);
          }
          catch (\Exception $e) {

          }
    }

    GrabModel::UpdateGolive();

    $odp_pt2 = DB::select("SELECT *  FROM `pt2_master` WHERE `odp_nama` LIKE '%ODP-%' and GOLIVE=0 ORDER BY `odp_nama` DESC");
    $whereid_pt2 = [];
    $records_pt2 = array_chunk($odp_pt2, 50);
    foreach ($records_pt2 as $batch){
      $odps_pt2 = $ids_pt2 = [];
      foreach($batch as $b){
        if(str_contains($b->odp_nama, ['('])){
          $odps_pt2[] = explode('(', str_replace(' ', '', $b->odp_nama))[0];
          $ids_pt2[explode('(', str_replace(' ', '', $b->odp_nama))[0]]=$b->id;
        }else{
          $odps_pt2[] = $b->odp_nama;
          $ids_pt2[$b->odp_nama]=$b->id;
        }
      }
      $search = DB::table('master_odp')->whereIn('ODP_LOCATION', $odps_pt2)->get();
      foreach($search as $s){
        $whereid_pt2[] = $ids_pt2[$s->ODP_LOCATION];
      }
    }

    DB::table('pt2_master')->whereIn('id', $whereid_pt2)->update(['GOLIVE' => 1, 'TGL_GO_LIVE' => date('Y-m-d') ]);
    DB::table('pt2_master')->whereIn('id', $whereid_pt2)->Where('upload_abd_4', 0)->update(['upload_abd_4' => 2]);

    $chat_id = '52369916';
    Telegram::sendMessage([
        'chat_id' => $chat_id,
        'text' => 'End Sync ODP REG',
        'parse_mode' =>'html'
    ]);
    // dd($toupdatetglgolive);
  }
  public static function apam(){
    // 52369916
    $chat_id = '52369916';
    Telegram::sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Start Sync APM cost plan sap',
        'parse_mode' =>'html'
    ]);
    $akun = DB::table('akun')->where('user','18940297')->first();
    $apmmon = DB::table('proaktif_project')->whereIn('fase', ['Executing', 'Planning'])->where('status_project', 'Active')->get();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "uid=".$akun->user."&pwd=".$akun->pwd."&bsubmit=Login");
    $result = curl_exec($ch);
    $query=[];
    foreach($apmmon as $no => $a){
      curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/get_cost_plan_from_project_sap.php');
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, "vpid=".$a->pid);
      $result = curl_exec($ch);
      $dom = @\DOMDocument::loadHTML(trim($result));
      $options = $dom->getElementsByTagName('option');

      for($i=1; $i<$options->length;$i++){
        $query[] = [
          "pid"       => $a->pid,
          "wbs"       => $a->sapid,
          "keperluan" => explode(' = ', $options->item($i)->nodeValue)[0],
          "value"     => explode(' = ', $options->item($i)->nodeValue)[1]
        ];
      }
    }

    curl_close($ch);
    DB::table('budget_apm')->truncate();

    foreach (array_chunk($query, 1000) as $k => $v)
    {
      DB::table('budget_apm')->insert($v);
      print_r("Batch " . ++$k . " Done \n");
      sleep(1);
    }

    $chat_id = '52369916';
    Telegram::sendMessage([

      'chat_id' => $chat_id,
        'text' => 'End Sync APM cost plan sap',
        'parse_mode' =>'html'
    ]);
  }
  public static function apm_cashout(){
    $chat_id = '52369916';
    Telegram::sendMessage([
        'chat_id' => $chat_id,
        'text' => 'Start Sync APM cashout',
        'parse_mode' =>'html'
    ]);
    $akun = DB::table('akun')->where('user','18940297')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "uid=".$akun->user."&pwd=".$akun->pwd."&bsubmit=Login");
    $keyword = ["BANJARMASIN","KALSEL","BPN","BJM","BALIKPAPAN"];
    $thn = 2022;
    $result = curl_exec($ch);
    $rs=[];
    $resultarray = array();
    foreach($keyword as $key){

      $loop=1;
      do {
        echo "\n".$key." Get page:".$loop;
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_list_act.php?_bl_per='.date('m').'&_th_per='.$thn.'&_status=%&_unit=&page='.$loop.'&q='.$key);
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);

        $dom = @\DOMDocument::loadHTML(trim($result));
        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        if($rows->length>1){
          $loop++;
          $columns = array(
                  1 =>'doc',
                  'desc_fund',
                  'amount',
                  'template');
          for ($i = 1, $count = $rows->length; $i < $count; $i++)
          {
            $cells = $rows->item($i)->getElementsByTagName('td');
            if($cells->length > 10){
              $data = array();
              for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
              {
                  $td = $cells->item($j);
                  if($j==4){
                    $tdlagi = $td->getElementsByTagName('td')->item(0);
                    $isi = $tdlagi->getElementsByTagName('a')->item(0)->getAttribute('onclick');
                    if(str_contains($isi, 'cashout_multi_pc')){
                      $isi = 'cashout_multi_pc';
                    }else if(str_contains($isi, 'cashout_new')){
                      $isi = 'cashout_new';
                    }else if(str_contains($isi, 'cashout_bast')){
                      $isi = 'cashout_bast';
                    }else{
                      $isi = 'anomali';
                    }
                    $data[$columns[$j]] =  $isi;
                  }else{
                    $data[$columns[$j]] = trim($td->nodeValue);
                  }
              }

              $data['thn']=$thn;
              if($cells->item(9)->getElementsByTagName('a')->length){
                $data['status'] = trim($cells->item(9)->nodeValue);
              }else{
                $data['status'] = trim($cells->item(8)->nodeValue);
              }

              $resultarray[] = $data;
            }
          }
          // dd($resultarray);

        }else{
          $loop=0;
        }
      } while ($loop);
    }
    echo "\n".count($resultarray)." of Doc. Found.";
    $isi = [];
    foreach($resultarray as $r){
      $isi[$r['doc']] = $r;
    }
    // dd(count($resultarray), count($isi));
    // dd($resultarray);


    // //get detail
    // curl_close($ch);
    $no=0;
    foreach($isi as $n => $r){
      echo "\n".$r['doc']." Doc.no:$no+1 of ".count($resultarray);
      $no++;
      if($r['template']=='cashout_multi_pc'){
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_multi_pc.php?year='.$thn.'&docid='.$r['doc'].'&period=0&vendor=0&page=0&q=&_status=');
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);
        $dom = @\DOMDocument::loadHTML(trim($result));
        $table = $dom->getElementsByTagName('table')->item(2);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                  0 =>'date',
                  'text_detail',
                  'amount_detail',
                  'pid','budget','tax','amount_paid');
        for ($i = 1, $count = $rows->length; $i < $count; $i++)
        {
          if($rows->item($i)->getAttribute('class')=='row'){
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
            {
              $col = $j;

              do{
                $td = $cells->item($col);
                $col++;
                if($td->getAttribute('class')==''){
                  if($j==0 || $j==1 || $j==2 || $j==6){
                    $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
                  }else if($j==3 || $j==4){
                    $optionTags = $td->getElementsByTagName('option');
                    foreach ($optionTags as $tag){
                        if ($tag->hasAttribute("selected")){
                          $isi = $tag->nodeValue;
                        }
                    }
                  }
                  else{
                    $isi = $td->nodeValue;
                  }
                  $data[$columns[$j]] = trim(@$isi);
                  $col=0;
                }
              }while($col);
            }
            $rs[] = array_merge($r,$data);
          }
        }

      }else if($r['template']=='cashout_new'){
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_new.php?year='.$thn.'&docid='.$r['doc'].'&period=0&vendor=0&page=0&q=&_status=');
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);
        $dom = @\DOMDocument::loadHTML(trim($result));
        $select = $dom->getElementById('project');
        $pid = 0;
        $optionTags = $select->getElementsByTagName('option');
        foreach ($optionTags as $tag){
            if ($tag->hasAttribute("selected")){
              $pid = $tag->nodeValue;
            }
        }

        $table = $dom->getElementsByTagName('table')->item(2);
        $rows = $table->getElementsByTagName('tr');
        $columns = array(
                  0 =>'date',
                  'text_detail',
                  'amount_detail','budget','tax','amount_paid');
        // dd($rows->length);
        for ($i = 1, $count = $rows->length; $i < $count; $i++)
        {
          if($rows->item($i)->getAttribute('class')=='row'){
            $cells = $rows->item($i)->getElementsByTagName('td');
            $data = array();
            for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
            {
              $col = $j;

              do{
                $td = $cells->item($col);
                $col++;
                if($td->getAttribute('class')==''){
                  if($j==0 || $j==1 || $j==2 || $j==5){
                    $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
                  }else if($j==3){
                    $optionTags = $td->getElementsByTagName('option');
                    foreach ($optionTags as $tag){
                        if ($tag->hasAttribute("selected")){
                          $isi = $tag->nodeValue;
                        }
                    }
                  }
                  else{
                    $isi = $td->nodeValue;
                  }
                  $data[$columns[$j]] = trim(@$isi);
                  $col=0;
                }
              }while($col);
            }
            $data['pid']=$pid;
            $rs[] = array_merge($r,$data);
          }
        }

      }else if($r['template']=='cashout_bast'){
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_bast.php?year='.$thn.'&docid='.$r['doc'].'&period=0&vendor=1&page=1&q=&_status=');
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);
        $dom = @\DOMDocument::loadHTML(trim($result));
        $pono = 0;
        foreach($dom->getElementsByTagName('input') as $d){
          if($d->getAttribute('name') == 'pono'){
            $pono = $d->getAttribute('value');
          }

        }
        $col=1;$i=0;$bastarray=[];
        do{
          if($dom->getElementById('cek_'.$i)){
            $bastarray[] = $dom->getElementById('cek_'.$i)->getAttribute('value');
            $i++;
          }else{
            $col = 0;
          }
        }while($col);
        // dd($bastarray);
        foreach($bastarray as $bast){
          echo "\nBAST:".$bast;
          curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/get_detail_bast.php');
          curl_setopt($ch, CURLOPT_POST, true);
          curl_setopt($ch, CURLOPT_POSTFIELDS, "po=".$pono."&bast=".$bast."&ord=0&show=1&docid=".$thn."-".$r['doc']);
          $result = curl_exec($ch);
          $data = [];
          $ts = [];
          for($j=0;$j<json_decode($result)->rowsnum;$j++){
            $json = json_decode($result);
            $kuit_date="kuit_date_$j";
            $kuit_text="kuit_text_$j";
            $pembulatan="pembulatan_$j";
            $ttot="ttot_$j";
            $tpph="tpph_$j";
            $pid="pid_$j";
            $cc="cc_$j";
            $acc="acc_$j";
            $jml="jml_$j";
            // $tax_dpp_N3="tax_dpp_N3_$j";
            // $tax_mnt_N3="tax_mnt_N3_$j";
            // $tax_dpp_H14="tax_dpp_H14_$j";
            // $tax_mnt_H14="tax_mnt_H14_$j";
            // $v_tax_mnt_N3 = preg_replace("/([^0-9\\.])/i", "", $json->$tax_mnt_N3);
            // $v_tax_mnt_H14 = preg_replace("/([^0-9\\.])/i", "", $json->$tax_mnt_H14);
            $v_jml=preg_replace("/([^0-9\\.])/i", "", $json->$jml);
            $v_ttot = preg_replace("/([^0-9\\.])/i", "", $json->$ttot);
            $v_tpph = preg_replace("/([^0-9\\.])/i", "", $json->$tpph);
            $ppn = $v_ttot-$v_tpph;
            $data = ["date"=>$json->$kuit_date,
              "text_detail"=>$json->$kuit_text,
              "budget" =>0,
              "tax" => "ppn: ".$ppn.";pph:".$v_tpph.";",
              "pid"=>$json->$pid,
              "amount_detail"=>$v_jml,
              "amount_paid"=>$v_jml+$ppn-$v_tpph];
              // "pembulatan"=>$json->$pembulatan,
              // "tax_dpp_N3"=>$json->$tax_dpp_N3,
              // "tax_mnt_N3"=>$json->$tax_mnt_N3,
              // "tax_dpp_H14"=>$json->$tax_dpp_H14,
              // "tax_mnt_H14"=>$json->$tax_mnt_H14,
              // "ttot"=>$json->$ttot,
              // "tpph"=>$json->$tpph,
              // "cc"=>$json->$cc,
              // "acc"=>$json->$acc,
              // "jml"=>$json->$jml,
              $rs[] = array_merge($r,$data);
          }
        }

      }else{

      }

    }
    curl_close($ch);
    $records = array_chunk($rs, 500);
    DB::table('apm_cashout')->truncate();
    foreach ($records as $batch) {
      DB::table('apm_cashout')->insert($batch);
    }
    $chat_id = '52369916';
    Telegram::sendMessage([
        'chat_id' => $chat_id,
        'text' => 'END Sync APM cashout',
        'parse_mode' =>'html'
    ]);
    // dd($rs);
  }

  // public function detail($doc){
  //   $akun = DB::table('akun')->where('user','885870')->first();
  //   $ch = curl_init();
  //   curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/');
  //   curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
  //   curl_setopt($ch, CURLOPT_POST, true);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //   // curl_setopt($ch, CURLOPT_HEADER, true);
  //   curl_setopt($ch, CURLOPT_COOKIESESSION, true);
  //   curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
  //   curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
  //   curl_setopt($ch, CURLOPT_POSTFIELDS, "uid=".$akun->user."&pwd=".$akun->pwd."&bsubmit=Login");
  //   $result = curl_exec($ch);
  //   curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_new.php?year=2021&docid='.$doc.'&period=0&vendor=0&page=0&q=IF%20REG%206%20KALIMANTAN&_status=');
  //   $result = curl_exec($ch);
  //   $dom = @\DOMDocument::loadHTML(trim($result));
  //   curl_close($ch);
  //   $select = $dom->getElementById('project');
  //   $pid = 0;
  //   $optionTags = $select->getElementsByTagName('option');
  //   foreach ($optionTags as $tag){
  //       if ($tag->hasAttribute("selected")){
  //         $pid = $tag->nodeValue;
  //       }
  //   }

  //   $table = $dom->getElementsByTagName('table')->item(2);
  //   $rows = $table->getElementsByTagName('tr');
  //   $columns = array(
  //             0 =>'date',
  //             'text',
  //             'amount_detail','budget','tax','amount_paid');
  //   // dd($rows->length);
  //   for ($i = 1, $count = $rows->length; $i < $count; $i++)
  //   {
  //     if($rows->item($i)->getAttribute('class')=='row'){
  //       $cells = $rows->item($i)->getElementsByTagName('td');
  //       $data = array();
  //       for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
  //       {
  //         $col = $j;

  //         do{
  //           $td = $cells->item($col);
  //           $col++;
  //           if($td->getAttribute('class')==''){
  //             if($j==0 || $j==1 || $j==2 || $j==5){
  //               $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
  //             }else if($j==3){
  //               $optionTags = $td->getElementsByTagName('option');
  //               foreach ($optionTags as $tag){
  //                   if ($tag->hasAttribute("selected")){
  //                     $isi = $tag->nodeValue;
  //                   }
  //               }
  //             }
  //             else{
  //               $isi = $td->nodeValue;
  //             }
  //             $data[$columns[$j]] = trim(@$isi);
  //             $col=0;
  //           }
  //         }while($col);
  //       }
  //       $data['pid']=$pid;
  //       $rs[] = $data;
  //     }
  //   }
  //   dd($rs);
  // }
  // public function detail2($doc){
  //   $akun = DB::table('akun')->where('user','885870')->first();
  //   $ch = curl_init();
  //   curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/');
  //   curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
  //   curl_setopt($ch, CURLOPT_POST, true);
  //   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  //   // curl_setopt($ch, CURLOPT_HEADER, true);
  //   curl_setopt($ch, CURLOPT_COOKIESESSION, true);
  //   curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
  //   curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
  //   curl_setopt($ch, CURLOPT_POSTFIELDS, "uid=".$akun->user."&pwd=".$akun->pwd."&bsubmit=Login");
  //   $result = curl_exec($ch);
  //   curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_multi_pc.php?year=2022&docid='.$doc.'&period=0&vendor=0&page=0&q=IF%20REG%206%20KALIMANTAN&_status=');
  //   $result = curl_exec($ch);
  //   $dom = @\DOMDocument::loadHTML(trim($result));
  //   curl_close($ch);
  //   $table = $dom->getElementsByTagName('table')->item(2);
  //   $rows = $table->getElementsByTagName('tr');
  //   $columns = array(
  //             0 =>'date',
  //             'text',
  //             'amount_detail',
  //             'pid','budget','tax','amount_paid');
  //   // dd($rows->length);
  //   for ($i = 1, $count = $rows->length; $i < $count; $i++)
  //   {
  //     if($rows->item($i)->getAttribute('class')=='row'){
  //       $cells = $rows->item($i)->getElementsByTagName('td');
  //       $data = array();
  //       for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
  //       {
  //         $col = $j;

  //         do{
  //           $td = $cells->item($col);
  //           $col++;
  //           if($td->getAttribute('class')==''){
  //             if($j==0 || $j==1 || $j==2 || $j==6){
  //               $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
  //             }else if($j==3 || $j==4){
  //               $optionTags = $td->getElementsByTagName('option');
  //               foreach ($optionTags as $tag){
  //                   if ($tag->hasAttribute("selected")){
  //                     $isi = $tag->nodeValue;
  //                   }
  //               }
  //             }
  //             else{
  //               $isi = $td->nodeValue;
  //             }
  //             $data[$columns[$j]] = trim(@$isi);
  //             $col=0;
  //           }
  //         }while($col);
  //       }
  //       $rs[] = $data;
  //     }
  //   }
  //   dd($rs);
  // }

  public function sample_grab(){
    $akun = DB::table('akun')->where('user','18940297')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "uid=".$akun->user."&pwd=".$akun->pwd."&bsubmit=Login");
    $keyword = ["BANJARMASIN","KALSEL"];
    $thn = 2022;
    $result = curl_exec($ch);
    $rs=[];
    $resultarray = array();
    foreach($keyword as $key){

      $loop=1;
      do {
        echo "\n".$key." Get page:".$loop;
        curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/fista/cashout_list_act.php?_bl_per='.date('m').'&_th_per='.$thn.'&_status=%&_unit=&page='.$loop.'&q='.$key);
        curl_setopt($ch, CURLOPT_POST, false);
        $result = curl_exec($ch);

        $dom = @\DOMDocument::loadHTML(trim($result));
        $table = $dom->getElementsByTagName('table')->item(0);
        $rows = $table->getElementsByTagName('tr');
        if($rows->length>1){
          $loop++;
          $columns = array(
                  1 =>'doc',
                  'desc_fund',
                  'amount',
                  'template');
          for ($i = 1, $count = $rows->length; $i < $count; $i++)
          {
            $cells = $rows->item($i)->getElementsByTagName('td');
            if($cells->length > 10){
              $data = array();
              for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
              {
                  $td = $cells->item($j);
                  if($j==4){
                    $tdlagi = $td->getElementsByTagName('td')->item(0);
                    $isi = $tdlagi->getElementsByTagName('a')->item(0)->getAttribute('onclick');
                    if(str_contains($isi, 'cashout_multi_pc')){
                      $isi = 'cashout_multi_pc';
                    }else if(str_contains($isi, 'cashout_new')){
                      $isi = 'cashout_new';
                    }else if(str_contains($isi, 'cashout_bast')){
                      $isi = 'cashout_bast';
                    }else{
                      $isi = 'anomali';
                    }
                    $data[$columns[$j]] =  $isi;
                  }else{
                    $data[$columns[$j]] = trim($td->nodeValue);
                  }
              }

              $data['thn']=$thn;
              if($cells->item(9)->getElementsByTagName('a')->length){
                $data['status'] = trim($cells->item(9)->nodeValue);
              }else{
                $data['status'] = trim($cells->item(8)->nodeValue);
              }

              $resultarray[] = $data;
            }
          }
          // dd($resultarray);

        }else{
          $loop=0;
        }
      } while ($loop);
    }
    echo "\n".count($resultarray)." of Doc. Found.";
    dd($resultarray);


  }
}