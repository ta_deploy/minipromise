<?php
namespace App\Http\Controllers;

use DB;
use App\DA\ProaktifModel;
use App\DA\MitraModel;
use App\DA\WaspangModel;
use App\DA\LopModel;
use App\DA\ProjectModel;

use Illuminate\Http\Request;

class ProaktifController extends Controller
{

  public function index()
  {
    $witel = session('auth')->witel;
    $data = ProaktifModel::getByWitel($witel);
    return view('proaktif.index', compact('data'));
  }
  public function form($id)
  {
    $mitra = MitraModel::listMitra();
    $waspang = WaspangModel::getWaspang();
    $sto = LopModel::getSto();
    $data = ProaktifModel::getByID($id);
    $tematik = ProjectModel::getAll(0);
    $boq = self::get_proaktif_boq($data->proaktif_id);
    // dd($boq);
    return view('proaktif.form', compact('data','mitra','waspang','sto','tematik','boq'));
  }
  public function saveimport(Request $req, $id)
  {
    $auth = session('auth');
    $id = LopModel::import($req);
    $upload = [];
    foreach(json_decode($req->boq) as $b){
      $upload[] = [
        'designator' => $b->designator,
        'uraian' => $b->uraian,
        'satuan' => $b->satuan,
        'hargamaterial' => str_replace(',', '', $b->mitra_material),
        'hargajasa' => str_replace(',', '', $b->mitra_jasa),
        'telkom_material' => str_replace(',', '', $b->telkom_material),
        'telkom_jasa' => str_replace(',', '', $b->telkom_jasa),
        'telkom_total' => str_replace(',', '', $b->telkom_total),
        'mitra_total' => str_replace(',', '', $b->mitra_total),
        'qty' => str_replace(',', '', $b->volume),
        'lop_id' => $id,
        'status' => 2
      ];
    }
    WaspangModel::insertboq($id,$upload);
    LopModel::insertlog([
        'lop_id' => $id,
        'catatan' => 'Registered LOP',
        'updated_at' => DB::raw('now()'),
        'step_id'   => 1,
        'update_by' => $auth->id_user,
        'tematik_id'=> $req->tematik
    ]);
    return redirect('/proaktif')->with('alertblock', [
        ['type' => 'success', 'text' => 'Sukses Menyimpan data'],
    ]);
  }
  public static function get_proaktif_boq($id)
  {
    $akun = DB::table('akun')->where('user', '885870')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIESESSION, true);
    curl_setopt($ch, CURLOPT_COOKIEJAR, 'proaktif.jar');  //could be empty, but cause problems on some hosts
    curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
    curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$akun->user."&password=".$akun->pwd."&btn_login=Log+In");
    $rough_content = curl_exec($ch);

    curl_setopt($ch, CURLOPT_URL, 'https://apps.telkomakses.co.id/proactive/show_initiation.php?project_id='.$id);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $table = $dom->getElementsByTagName('table')->item(4);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
              0 =>'designator',
              'uraian',
              'satuan',
              'telkom_material','telkom_jasa','mitra_material','mitra_jasa','volume','telkom_total','mitra_total');
    for ($i = 2, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td');
        $data = array();
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
          $td = $cells->item($j);
            if($j==0 ){
              $optionTags = $td->getElementsByTagName('option');
              foreach ($optionTags as $tag){
                  if ($tag->hasAttribute("selected")){
                    $isi = $tag->nodeValue;
                  }
              }
            }else{
              $isi = $td->getElementsByTagName('input')->item(0)->getAttribute('value');
            }

            $data[$columns[$j]] = trim(@$isi);
        }
        // dd($data);
        $rs[] = $data;
    }
    return $rs;
  }
}
