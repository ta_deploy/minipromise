<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\DA\LopExport;
use App\DA\ExportTematik;
use App\DA\LopModel;
use App\DA\ProjectModel;
use App\DA\MatrikModel;
use App\DA\WaspangModel;
use App\DA\ReviewModel;
use App\DA\MitraModel;
use Excel;

use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function dashflow($mitra,$tl)
    {
        $mtr = ReviewModel::getMaterial($mitra,$tl);
        $bobot = ReviewModel::getBobotMitratel($mitra,$tl);
        $matrik = ReviewModel::getMatrikMtel($mitra,$tl,'all');
        $data = LopModel::getMitratelDash($mitra,$tl);
        $wo = ReviewModel::getMitratelTematik($mitra,$tl);
        $tl2 = MitraModel::getTLByMitra($mitra);
        $mitra = MitraModel::getMitraByTL($tl);
        return view('dashboardan',compact('data','bobot','matrik','wo','mtr','tl2','mitra'));
    }
    public function dashflowdetil($col,$row)
    {
        $data = LopModel::getMitratelDashDetil($col,$row);
        // dd($data);
        return view('dashboardAjax',compact('data'));
    }
    public function ajaxmatrixdetil($col,$row)
    {
        $data = ReviewModel::getMatrikDetilMtel($col,$row);
        // dd($data,$col,$row);
        return view('regional.ajaxmatrikdetil', compact('data'));
    }
    public function gantt()
    {
        $data = DB::table('mpromise_lop')->select('nama_lop', 'timeplan_fisik', 'timeplan_berkas', 'pid')->where('mpromise_project_id',37)->where('DROPLOP',0)->where('isHapus',0)->orderBy('timeplan_fisik','asc')->get();
        $dataarray = [];
        foreach($data as $d){
            $tgl = explode(" - ", $d->timeplan_fisik);
            $tgl2 = explode(" - ", $d->timeplan_berkas);
            // dd($tgl);
            $namalop = str_replace('STTF1_2022_', '', $d->nama_lop);
            $desc = explode("-",$namalop);
            if(count($desc)==4){
                $desc = $desc[3];
            }else{
                $desc = '';
            }
            $gantt = new \stdClass;
            $gantt->name=str_replace('-'.$desc,' ',$namalop);
            $gantt->desc=$desc;
            $gantt->values=[(object) array("from" => strtotime($tgl[0])* 1000,"to" => strtotime($tgl[1])* 1000,"label" => $d->pid,"customClass" => "ganttGreen"),(object) array("from" => strtotime($tgl2[0])* 1000,"to" => strtotime($tgl2[1])* 1000,"label" => $d->pid,"customClass" => "ganttRed")];
            $dataarray[] = $gantt;
            // break;
        }
        return view('gantt', compact('dataarray'));
    }
	// public function proaktif(){
	// 	$data = DB::select ('SELECT COUNT(*) AS Baris, Portofolio,
	// 		(select count(*) from proaktif where Fase="Initiation" and Portofolio=p.Portofolio) as fase_initiation,
	// 		(select count(*) from proaktif where Fase="Planning" and Portofolio=p.Portofolio) as fase_planning,
	// 		(select count(*) from proaktif where Fase="Executing" and Portofolio=p.Portofolio) as fase_executing,
	// 		(select count(*) from proaktif where Fase="Closing" and Portofolio=p.Portofolio) as fase_closing
	// 		FROM proaktif p GROUP BY Portofolio ORDER BY Portofolio
	// 	');
	// 	return view('dashboardproaktif', compact('data'));
	// }

	public function listcell($portofolio, $fase){
		$assetjump = DB::table('proaktif');
		if($fase){
			$assetjump->where('fase', $fase);
		}
		if($portofolio){
			$assetjump->where('Portofolio', $portofolio);
		}
		$assetjump = $assetjump->get();
		return view('detildashboardproaktif',compact('assetjump'));
	}

	public function refer_pt2()
	{
        $data = DB::table('pt2_master')->whereIn('kendala_detail', ["Distribusi Full", "Feeder Reti", "Core Ccd", "Feeder Full"])->get();
        dd($data);
	}
    public function matrikprogress($jns)
    {
        $tematik = ProjectModel::getAll('0');
        $tl = WaspangModel::getTLByWitel(session('auth')->witel);
        // dd($tl);
        // $data = LopModel::getMatrikByMitra();
        $data = new \stdClass;
        return view('report.reportwaspangpermitra',compact('data', 'tematik', 'tl'));
    }
    public function matrikmitraajax($jns, $id, $tl)
    {
        $id = json_decode($id);

        if($jns == 'mitra'){
            if(in_array(0, $id) )
            {
                $id_imp = null;
            }
            else
            {
                $id_imp = "'". implode("','", $id) . "'";
            }

            $data = MatrikModel::getMatrikByMitra($id_imp, $tl);
        }elseif($jns == 'sto'){
            if(in_array(0, $id) )
            {
                $id_imp = null;
            }
            else
            {
                $id_imp = "'". implode("','", $id) . "'";
            }

            $data = MatrikModel::getMatrikBySTO($id_imp, $tl);
        }elseif($jns == 'tl'){
            if(in_array(0, $id) )
            {
                $id_imp = null;
            }
            else
            {
                $id_imp = "'". implode("','", $id) . "'";
            }

            $data = MatrikModel::getMatrikByTL($id_imp, $tl);
        }else{
            $id_imp = "'". implode("','", $id) . "'";
            $data = MatrikModel::getMatrikByWaspang($id_imp,$tl);
        }

        if(!in_array(0, $id) && ProjectModel::getById_In($id)->pt3){
            return view('report.matrikajaxmitra',compact('data')) ;
        }else{
            return view('report.nonpt3matrikajax',compact('data') );
        }
    }
    public function ajaxdetailmatrix($jenis, $id, $col, $row, $aktor, $tl)
    {
        if($jenis == 1){
            //lop
            $data = MatrikModel::getDetailLop($id, $col, $row, $aktor,$tl);
            // dd($data);
            return view('report.ajaxdetailmatrik',compact('data'));
        }else{
            //odp
            $data = MatrikModel::getDetailLopODP($id, $col, $row, $aktor,$tl);
            $lastlop = '';
            $dataarray =[];
            foreach ($data as $no => $m){
              if($lastlop == $m->id_lop){
                $dataarray[$m->id_lop]->list[] = ['Plan_Odp_Nama'=>$m->Plan_Odp_Nama,'Real_Odp_Nama'=>$m->Real_Odp_Nama,'GOLIVE'=>$m->GOLIVE];

              }else{
                $m->list[] = ['Plan_Odp_Nama'=>$m->Plan_Odp_Nama,'Real_Odp_Nama'=>$m->Real_Odp_Nama,'GOLIVE'=>$m->GOLIVE];
                $dataarray[$m->id_lop] = $m;
              }
              $lastlop = $m->id_lop;
            }
            // dd($dataarray);
            return view('report.ajaxdetailmatrikodp',compact('dataarray'));
        }
    }
    public function export($id)
    {
        return Excel::download(new LopExport($id), 'rekap.xlsx');
    }
    public function export2($id)
    {
        return Excel::download(new ExportTematik($id), 'rekap.xlsx');
    }

}