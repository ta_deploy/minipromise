<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use App\DA\SPModel;
class SPController extends Controller
{
    public function index()
    {
        $data = SPModel::getAll();
        return view('sp.index', compact('data'));
    }
    public function form($id)
    {
        $data = SPModel::getById($id);
        return view('sp.form', compact('data'));
    }
    public function save($id, Request $req)
    {
        SPModel::insertOrUpdate($id, $req);
        $this->handleFileUpload($id, $req);
        return redirect('/sp');
    }
    private function handleFileUpload($id, $request)
    {
        $input = 'sp';
        if ($request->hasFile($input)) {
            //dd($input);
            $path = public_path().'/storage/' .$id. 'sp/';
            if (! file_exists($path)) {
                if (! mkdir($path, 0770, true)) {
                    return [
                        'fail' => true,
                        'errors' => 'Gagal Menyiapkan Folder',
                    ];
                }
            }
            $file = $request->file($input);
            $ext = 'pdf';
            //TODO: path, move, resize
            try {
                $moved = $file->move("$path", "$id.$ext");
            } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                return [
                    'fail' => true,
                    'errors' => 'Gagal upload file',
                ];
            }
        }
    }
}
