<?php
namespace App\Http\Controllers;

use App\DA\MitraModel;
use App\DA\WITELModel;
use Illuminate\Http\Request;

class MitraController extends Controller
{
  //mitra
  public function index()
  {
    $witel = session('auth')->witel;
    $data = MitraModel::getByWitel($witel);
    return view('mitra.index', compact('data'));
  }
  public function form($id)
  {
    $witel = WITELModel::getAll();
    $data = MitraModel::getById($id);
    return view('mitra.input', compact('data', 'witel'));
  }
  public function save($id, Request $req)
  {
    $exist = MitraModel::getById($id);
    if($exist){
      MitraModel::update($id, ["mitra"=> $req->mitra, "label"=> $req->label, "status" =>$req->status, "witel_mitra" =>$req->witel_mitra]);
    }else{
      MitraModel::insert(["mitra" => $req->mitra, "label" => $req->label, "status" => $req->status, "witel_mitra" =>$req->witel_mitra]);
    }
    return redirect('/mitra');
  }

}
