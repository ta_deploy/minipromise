<?php
namespace App\Http\Controllers;

use App\DA\MaterialModel;
use App\DA\ProjectModel;
use App\DA\AlistaModel;
use Illuminate\Http\Request;

class MaterialController extends Controller
{
  //justif
  // public function index()
  // {
  //   $witel = session('auth')->witel;
  //   $data = MitraModel::getByWitel($witel);
  //   return view('mitra.index', compact('data'));
  // }
  public function form($id)
  {
    $data = ProjectModel::getById($id);
    $lop = ProjectModel::getLopByTematik($id);
    $justif = MaterialModel::getJustif($id);
    // dd($justif);
    $reservasi = MaterialModel::getReservasiList($id);
    return view('alista.monitoring', compact('data', 'justif', 'reservasi','lop'));
  }

  public function savejustif($tematik, Request $req)
  {
    // dd($req->paste_item);
    $postrequest = trim($req->paste_item);
    $dirty = array(" \r\n", "\t", "\n");
    $clean = array('', ';', '<br>');
    $request = str_replace($dirty, $clean, $postrequest);
    foreach(explode('<br>', $request) as $rows){
          $d = explode(';',$rows);
          $array[] = ['designator'=>$d[0],'qty'=>$d[3],'tgl_justif'=>$req->tgl_justif,'tematik_id'=>$tematik];
          
    }
    MaterialModel::insertJustif($req->tgl_justif,$tematik,$array);
    return redirect()->back();
  }
  public function getreservasi($id)
  {
    AlistaModel::getreservasi($id);
  }
  public function savereservasi($id, Request $req)
  {
    $r = AlistaModel::getreservasi($req->no_reservasi);
    $reservasi = ['lop_id'=>$id,'tgl_submit'=>$req->tgl,'no_reservasi'=>$req->no_reservasi,'gudang'=>$r['head'][2],'mitra'=>$r['head'][4],'pid'=>$r['head'][3]];
    $item = array();
    // dd($r['result']);
    foreach($r['result'] as $l){
      $item[] = ['no_reserv'=>$req->no_reservasi,'designator'=>$l['designator'],'qty'=>$l['qty']];
    }
    // dd($item,$reservasi);
    MaterialModel::insertReservasi($reservasi);
    MaterialModel::insertReservasiItem($item);
    return redirect()->back();
  }

  //matdev
  public function listmatdev($lopid)
  {
    $data = MaterialModel::getMatdevByLopId($lopid);
    $info = MaterialModel::getMatdevInfoItem($lopid);
    return view('matdev.list', compact('data','info'));
  }

  public function formmatdev($lopid,$id)
  {
    $data = MaterialModel::getMatdevById($id);
    $item = MaterialModel::getReservasiItemByLopId($lopid);
    // dd($item);
    return view('matdev.form', compact('data','item'));
  }

  public function formmatonsite($lopid,$id)
  {
    $data = MaterialModel::getMatdevById($id);
    $item = MaterialModel::getMatdevItem($id);
    // dd($item);
    return view('matdev.formtiba', compact('data','item'));
  }

  public function savematdev($lopid,$id,Request $req)
  {
    $return = 0;
    $itemarray=[];
    $mtr = MaterialModel::getReservasiItemByLopId($lopid);
    // dd(json_decode($req->item),$mtr);
    // disable validasi kelebihan plan
    // foreach(json_decode($req->item) as $item){
    //   // dd($item);
    //     foreach($mtr as $itemplan){
    //         if($item->name==$itemplan->designator){
    //             if($itemplan->qtyplan < $itemplan->delv+$item->value){
    //                 $return = 1;
    //                 break 2;
    //             }
    //         }
    //     }
    // }
    // dd($return,json_decode($req->item),$mtr);
    // 4902965446
    if($return){
      return redirect()->back()->with('alertblock', [
              ['type' => 'Error', 'text' => 'Kuantitas Pengiriman Melebihi Plan'],
          ]);
    }else{
      $this->handlePotoUploads($lopid,$id,$req, ["Material_Dimuat"]);
      if($req->status=="Material_on_Delivery"){
        $delivery = ["lop_id"=>$lopid,"status"=>$req->status,"tgl_dimuat"=>$req->tgl];
      }else{
        $delivery = ["lop_id"=>$lopid,"status"=>$req->status,"tgl_tiba"=>$req->tgl];
      }
      $exists = MaterialModel::getMatdevById($id);
      if($exists){
        MaterialModel::updateMatdev($id,$delivery);
      }else{
        $id = MaterialModel::insertMatdev($delivery);
      }
      foreach(json_decode($req->item) as $i){
        $itemarray[] = ["designator"=>$i->name,"qty"=>$i->value,"deliv_id"=>$id];
      }
      MaterialModel::matdevItemTInsert($id, $itemarray);
      // dd($delivery,$lopid,$id,json_decode($req->item),$req->status);
      return redirect('/matdev/'.$lopid);
    }
  }

  public function savemataddition($lopid,$id,Request $req)
  {
    MaterialModel::addDesignator($lopid,$req->designator,$req->hargamaterial,$req->hargajasa);
    return redirect()->back();
  }
  public function savematonsite($lopid,$id,Request $req)
  {
    $delivery = ["lop_id"=>$lopid,"status"=>$req->status,"tgl_tiba"=>$req->tgl];
    $this->handlePotoUploads($lopid,$id,$req, ["Material_Dimuat","Material_Tiba"]);
    MaterialModel::updateMatdev($id,$delivery);
    return redirect('/matdev/'.$lopid);
  }
  private function handlePotoUploads($lopid,$id,$request,$fotos)
  {
      foreach($fotos as $f){
          $input = "photo-".$f;
          if ($request->hasFile($input)) {
              $name = $f;
              //dd($input);
              $path = public_path().'/storage/'.$lopid.'/instalasifile/evident/'.$id;
              if (! file_exists($path)) {
                  if (! mkdir($path, 0770, true)) {
                      return [
                          'fail' => true,
                          'errors' => 'Gagal Menyiapkan Folder',
                      ];
                  }
              }
              $file = $request->file($input);
              $ext = 'jpg';
              //TODO: path, move, resize
              try {
                  $moved = $file->move("$path", "$name.$ext");
                  $img = new \Imagick($moved->getRealPath());
                  $img->scaleImage(100, 150, true);
                  $img->writeImage("$path/$name-th.$ext");
              } catch (\Symfony\Component\HttpFoundation\File\Exception\FileException $e) {
                  return [
                      'fail' => true,
                      'errors' => 'Gagal upload foto',
                  ];
              }
          }
      }

  }
}
