<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\DA\RingModel;

class RingController extends Controller
{
  public function index(){
    $data = RingModel::getAll();
    return view('ring.list', compact('data'));
  }
  public function form($id){
    $data = RingModel::getById($id);
    return view('ring.form', compact('data'));
  }
  public function save($id, Request $req){
    $exists = RingModel::getById($id);
    if($exists){
      RingModel::update($req->only('nama_ring'));
    }else{
      RingModel::insert($id, $req->only('nama_ring'));
    }
    return redirect('/ring');
  }
}