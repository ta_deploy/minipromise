<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Excel;
use App\DA\RegModel;

use Illuminate\Support\Facades\Session;

class RegController extends Controller
{

    public function listplan() 
    {
        $data = RegModel::getPlanList();
        return view('regional.planlist',compact('data'));
    }    
    public function formplan($wo) 
    {
        $data = RegModel::getPlanByWO($wo);
        return view('regional.planform',compact('data', 'wo'));
    }
    public function formplansave(Request $req,$wo) 
    {
        RegModel::insertOrUpdatePlan($req,$wo);
        return redirect()->back()
                    ->withInput($req->all());
    }
    public function deleteplan($id) 
    {
        RegModel::deletePlan($id);
        return redirect()->back();
    }

    public function dashboard() 
    {
        $data = RegModel::getBobot('all','all');
        // dd($data);
        $witel = RegModel::getWitelSelect();
        $lastprogress=$data['lastprogress'];
        $data= $data['data'];
        return view('regional.dashboard', compact('data','lastprogress','witel'));
    }
    public function ajaxdashboard($witel){
        $data = RegModel::getBobot($witel);

        return json_encode($data);
    }
    public function matrix() 
    {
        $witel = RegModel::getWitelSelect();
        $wo = RegModel::getWoSelect('all');
        return view('regional.matrix',compact('wo','witel'));
    }
    public function ajaxmatrix($witel,$wo) 
    {
        $lastprogress=$kurva=null;
        if($wo!='all'){
            $data = RegModel::getBobot($witel,$wo);
            $lastprogress=$data['lastprogress'][$wo];
            $kurva= $data['data'][$wo];
        }
        $data = RegModel::getMatrik($witel,$wo);
        $mtr = RegModel::getMaterial($witel,$wo);
        // dd($data);
        return view('regional.ajaxmatrix', compact('data','witel','wo','kurva','lastprogress','mtr'));
    }
    public function ajaxmatrixdetil($col,$row) 
    {
        $data = RegModel::getMatrikDetil($col,$row);
        // dd($data,$col,$row);
        return view('regional.ajaxmatrikdetil', compact('data'));
    }

    public function ajaxgetwo($witel) 
    {
        $data = RegModel::getWoSelect($witel);
        return ($data);
    }
    public function upload() 
    {
        return view('regional.upload');
    }
    public function uploadsave(Request $req) 
    {
        // dd('asd');
        $postrequest = trim($req->paste);
        $dirty = array(" \r\n", "\t", "\n");
        $clean = array('', ';', '<br>');
        $coma = array(",", "%");
        $fl = array('.', '');
        $uang = array('.', ',', ' ');
        $r_uang = array('', '', '');
        $request = str_replace($dirty, $clean, $postrequest);
        $date=date('Y-m-d H:i:s');
        // dd($request);
        
        foreach(explode('<br>', $request) as $rows){
          $d = explode(';',$rows);
          $array[] = $d;
          // dd(str_replace('"', "", $d[6]));
          // $array[] = ['WO'=>$d[1],
          //           'WITEL'=>$d[2],
          //           'RING'=>$d[3],
          //           'AB'=>$d[4],
          //           'SPAN'=>$d[5],
          //           'ID_LOKASI'=>str_replace('"', "", $d[6]),
          //           'ID_SAP'=>$d[7],
          //           'MITRA'=>$d[8],
          //           'NO_PO'=>$d[9],
          //           'TGL_PO'=>$this->get_tgl($d[10]),
          //           'TGL_TOC'=>$this->get_tgl($d[11]),
          //           'NO_BAST'=>($d[12]),
          //           'TGL_BAST'=>$this->get_tgl($d[13]),
          //           'TARGET_DETIL'=>$d[14],
          //           'N_MATERIAL'=>str_replace($uang, $r_uang, $d[15]),
          //           'N_JASA'=>str_replace($uang, $r_uang, $d[16]),
          //           'N_TOTAL'=>str_replace($uang, $r_uang, $d[17]),
          //           'R_MATERIAL'=>str_replace($uang, $r_uang, $d[18]),
          //           'R_JASA'=>str_replace($uang, $r_uang, $d[19]),
          //           'R_TOTAL'=>str_replace($uang, $r_uang, $d[20]),
          //           'PROGRESS'=>$d[21],
          //           'DROP'=>$d[22],
          //           'AANWIJZING'=>$this->get_tgl($d[23]),
          //           'PERIZINAN'=>$this->get_tgl($d[24]),
          //           'DELIVERY_MATERIAL'=>$this->get_tgl($d[25]),
          //           'MATERIAL_ONSITE'=>$this->get_tgl($d[26]),
          //           'PRAGELARAN'=>$this->get_tgl($d[27]),
          //           'TANAM_TIANG'=>$this->get_tgl($d[28]),
          //           'GELAR_KABEL'=>$this->get_tgl($d[29]),
          //           'PASANG_TERMINAL'=>$this->get_tgl($d[30]),
          //           'TERMINASI'=>$this->get_tgl($d[31]),
          //           'TEST_COMM'=>$this->get_tgl($d[32]),
          //           'ACC_TEST'=>$this->get_tgl($d[33]),
          //           'RFS'=>$this->get_tgl($d[34]),
          //           'UT'=>$this->get_tgl($d[35]),
          //           'REKON'=>$this->get_tgl($d[36]),
          //           'BAST'=>$this->get_tgl($d[37]),
          //           'STATUS'=>$d[38],
          //           'PROGRESS_KET'=>@$d[39]
          //   ];
        }
            dd($array);
          
        RegModel::insertOrUpdate($array);
        return redirect('/mitratel');
    }
    function get_tgl($tgl){
        if($tgl){
            $bulan = array ('Januari'=>'01',
                'Februari'=>'02',
                'Maret'=>'03',
                'April'=>'04',
                'Mei'=>'05',
                'Juni'=>'06',
                'Juli'=>'07',
                'Agustus'=>'08',
                'September'=>'09',
                'Oktober'=>'10',
                'November'=>'11',
                'Desember'=>'12'
            );
            $hari = explode(' ', $tgl)[0];
            if(strlen($hari)==1){
                $hari = '0'.$hari;
            }
            $bln = $bulan[explode(' ', $tgl)[1]];
            $thn = explode(' ', $tgl)[2];
            return $thn.'-'.$bln.'-'.$hari;
        }else{
            return '';
        }
    }
    public function login() 
    {
        $akun = DB::table('akun')->where('user','mtelreg6')->first();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://mtt.diginav.id/login/proses');
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.jar');  //could be empty, but cause problems on some hosts
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "username=".$akun->user."&password=".$akun->pwd);
        $result = curl_exec($ch);
        // curl_setopt($ch, CURLOPT_URL, 'https://mtt.diginav.id/dashboard/progress/mitratel/ioh');

        // curl_setopt($ch, CURLOPT_POST, false);
        // $result = curl_exec($ch);
        // $dom = @\DOMDocument::loadHTML(trim($result));
        // $table = $dom->getElementsByTagName('table')->item(0);
        // $rows = $table->getElementsByTagName('tr');
        // $columns = array(1=>'WO',
        //           'RING',
        //           'reg',
        //           'project_id',
        //           'project_id2',
        //           'kendala',
        //           'SPAN',
        //           'tenant',
        //           'WITEL',
        //           'NE_ID',
        //           'NE_NAME1',
        //           'NE_NAME2',
        //           'NE_PROVIDER',
        //           'FE_ID',
        //           'FE_NAME',
        //           'FE_NAME2',
        //           'FE_PROVIDER',
        //           'pole_plan',
        //           'pole_real_existing',
        //           'pole_real_new',
        //           'pole_ach',
        //           'cable_plan',
        //           'cable_real',
        //           'cable_ach',
        //           'mat_dev',
        //           'instalasi',
        //           'odc_term',
        //           'otb_term',
        //           'comm_test',
        //           'acc_test_mt',
        //           'rfs_plan',
        //           'rfs_realisasi',
        //           'realisasi',
        //           'overall',
        //           'last_update',
        //           'SPAN_ID_SISTEM');
        // for ($i = 3, $count = $rows->length-1; $i < $count; $i++)
        // {
        //     $cells = $rows->item($i)->getElementsByTagName('td');
        //     $data = array();
        //     for ($j = 1, $jcount = count($columns); $j <= $jcount; $j++)
        //     {
        //         $td = $cells->item($j);
        //         if($j==$jcount){
        //             $data[$columns[$j]] = (str_replace('https://mtt.diginav.id/dashboard/update/', '', $td->getElementsByTagName('a')->item(0)->getAttribute('href')));
        //         }else{
        //             $data[$columns[$j]] = trim($td->nodeValue);
        //         }
        //     }
        //     if($data['reg']==6)
        //         $resultarray[] = $data;
        //     // $data['tgl']=date('Y-m-d H:i:s');
        // }
        // DB::table('mpromise_mitratel_master')->truncate();
        // DB::table('mpromise_mitratel_master')->insert($resultarray);
        // dd($resultarray);
        $master = DB::table('mpromise_mitratel_master')->where('SPAN_ID_SISTEM','!=',0)->get();
        foreach($master as $m){
            curl_setopt($ch, CURLOPT_URL, 'https://mtt.diginav.id/dashboard/update/'.$m->SPAN_ID_SISTEM);
            curl_setopt($ch, CURLOPT_POST, false);
            $result = curl_exec($ch);
            $dom = @\DOMDocument::loadHTML(trim($result));
            $unwizing = $perijinan = $terminasi_otb_fe = $testcomm = $rfs_real = $uji_terima = $bast = null;    
            foreach($dom->getElementsByTagName("input") as $searchNode)
            {
                $name = $searchNode->getAttribute('name');
                if($name=='anwizing')
                    $unwizing = $searchNode->getAttribute('value');
                if($name=='perijinan')
                    $perijinan = $searchNode->getAttribute('value');
                if($name=='terminasi_otb_fe')
                    $terminasi_otb_fe = $searchNode->getAttribute('value');
                if($name=='testcomm')
                    $testcomm = $searchNode->getAttribute('value');
                if($name=='rfs-real')
                    $rfs_real = $searchNode->getAttribute('value');
                if($name=='uji_terima')
                    $uji_terima = $searchNode->getAttribute('value');
                if($name=='bast')
                    $bast = $searchNode->getAttribute('value');
            }
            DB::table('mpromise_mitratel_master')->where('SPAN_ID_SISTEM',$m->SPAN_ID_SISTEM)->update(['AANWIJZING'=>$unwizing,'PERIZINAN'=>$perijinan,'TERMINASI'=>$terminasi_otb_fe,'TEST_COMM'=>$testcomm,'RFS'=>$rfs_real,'UT'=>$uji_terima,'BAST'=>$bast]);
            $table = $dom->getElementsByTagName('table')->item(1);
            $rows = $table->getElementsByTagName('tr');
            $columns = array(0=>'designator',
                      'deskripsi',
                      'satuan',
                      'plan_qty',
                      'mos',
                      'harga_mtr',
                      'harga_jasa',
                      'real_qty');
            for ($i = 1, $count = $rows->length; $i < $count; $i++)
            {
                $cells = $rows->item($i)->getElementsByTagName('td');
                $data = array();
                for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
                {
                    $td = $cells->item($j);
                    if($j==7){
                        $data[$columns[$j]] = (($td->getElementsByTagName('input')->item(0)->getAttribute('value')));
                    }else{
                        $data[$columns[$j]] = trim($td->nodeValue);
                    }
                }
                $data['tgl']=date('Y-m-d H:i:s');
                $data['span_id_sistem']=$m->SPAN_ID_SISTEM;
                $resultarray[] = $data;
            }
        }
        // dd($resultarray,$result);
        DB::table('mpromise_mtt_mtr')->where('tgl','like',date('Y-m-d').'%')->delete();
        DB::table('mpromise_mtt_mtr')->insert($resultarray);
    }
}