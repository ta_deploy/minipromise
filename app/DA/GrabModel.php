<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class GrabModel
{
  public static function UpdateGolive()
  {
    $odp = DB::select("SELECT *  FROM `mpromise_core` WHERE `Real_Odp_Nama` LIKE '%ODP-%' and GOLIVE=0 ORDER BY `Real_Odp_Nama` DESC");
    $records = array_chunk($odp, 50);
    foreach ($records as $batch){
      $odps = $ids = [];
      foreach($batch as $b){
        if(str_contains($b->Real_Odp_Nama, ['('])){
          $odps[] = explode('(', str_replace(' ', '', $b->Real_Odp_Nama))[0];
          $ids[explode('(', str_replace(' ', '', $b->Real_Odp_Nama))[0]]=$b->ID_Sys;
        }else{
          $odps[] = $b->Real_Odp_Nama;
          $ids[$b->Real_Odp_Nama]=$b->ID_Sys;
        }
      }
      $search = DB::table('master_odp')->whereIn('ODP_LOCATION', $odps)->get();
      foreach($search as $s){
        DB::table('mpromise_core')->where('ID_Sys', @$ids[$s->ODP_LOCATION])->update(['GOLIVE'=>1, 'TGLODPGOLIVE'=>date('Y-m-d',strtotime($s->CREATEDATE))]);
      }
    }

    $cgl = DB::select('SELECT COUNT(*) AS Baris, LOP_ID,sum(case when GOLIVE=1 then 1 else NULL end) as countgolive, max(TGLODPGOLIVE) as maxdate FROM mpromise_core left join mpromise_lop on mpromise_core.LOP_ID=mpromise_lop.id where mpromise_lop.TGL_GO_LIVE = ""  GROUP BY LOP_ID ORDER BY countgolive  DESC');
    foreach($cgl as $c){
      if($c->Baris == $c->countgolive){
        // $toupdatetglgolive[] = $c->LOP_ID;
        DB::table('mpromise_lop')->where('id', $c->LOP_ID)->update(['TGL_GO_LIVE'=>$c->maxdate]);
      }
    }
  }

  public static function UpdateValid3($lop_id)
  {
    $odp = DB::select("SELECT count(*) as row,SUM(CASE WHEN Real_Odp_Nama = Plan_Odp_Nama THEN 1 ELSE 0 END) as reals FROM `mpromise_core` where LOP_ID=".$lop_id)[0];
    if($odp->row){
      if($odp->row == $odp->reals){
        DB::table('mpromise_lop')->where('id', $lop_id)->update(['TGL_VALID3'=>DB::raw('now()')]);
      }
    }
  }
  
}
