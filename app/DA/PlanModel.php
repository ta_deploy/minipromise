<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class PlanModel
{

  public static function getByID($id)
  {
    return DB::table('mpromise_plan_lop')->where('id', $id)->first();
  }
  public static function check($tgl,$lopid)
  {
    return DB::table('mpromise_plan_lop')->where('lop_id', $lopid)->where('tgl', $tgl)->where('tipe', 1)->first();
  }
  public static function insert($param)
  {

    return DB::table('mpromise_plan_lop')->insertGetId($param);
  }
  public static function updateByTgl($param)
  {
    DB::table('mpromise_plan_lop')->where('lop_id', $param['lop_id'])->where('tgl', $param['tgl'])->where('tipe', $param['tipe'])
    ->update($param);
  }
  public static function update($id,$param)
  {
    DB::table('mpromise_plan_lop')->where('id',$id)
    ->update($param);
  }
  public static function getIndex($tl)
  {
    $isAdmin = session('auth')->level==2?1:0;
    // dd($isAdmin);
    $q = DB::table('mpromise_lop')
    ->select('mpromise_lop.id', 'mpromise_lop.mpromise_project_id', 'mpromise_lop.mitra', 'mpromise_lop.nama_project', 'mpromise_mitra.mitra as mitra_nama','bobot.bobot')
    ->leftJoin('mpromise_project', 'mpromise_lop.mpromise_project_id','=', 'mpromise_project.id')
    ->leftJoin('mpromise_mitra', 'mpromise_lop.mitra','=', 'mpromise_mitra.id')
    ->leftJoin(DB::raw('(SELECT `lop_id`, SUM(CASE 
    WHEN tipe = 1 
    THEN value 
    ELSE 0 
END) AS bobot FROM `mpromise_plan_lop` GROUP BY `lop_id` ORDER BY `lop_id`) as bobot'),'mpromise_lop.id','=','bobot.lop_id')
    ->where('mpromise_lop.isHapus',0)
    ->where('mpromise_project.stts',0);
    if(!$isAdmin){
      $q->where('mpromise_lop.tl',$tl);
    }
    $data=$q->orderBy('mpromise_lop.id','desc')->get();

    $indexarray = [];
    foreach($data as $d){
      if(!array_key_exists($d->mpromise_project_id,$indexarray)){
        $indexarray[$d->mpromise_project_id]['label'] = $d->nama_project;
        $indexarray[$d->mpromise_project_id]['data'] = array();
      }
      if(!array_key_exists($d->mitra,$indexarray[$d->mpromise_project_id]['data'])){
        $indexarray[$d->mpromise_project_id]['data'][$d->mitra]['nama'] =$d->mitra_nama;
        $indexarray[$d->mpromise_project_id]['data'][$d->mitra]['count'] =1;
        $indexarray[$d->mpromise_project_id]['data'][$d->mitra]['bobot'] =$d->bobot;
      }else{
        $indexarray[$d->mpromise_project_id]['data'][$d->mitra]['count'] += 1;
        $indexarray[$d->mpromise_project_id]['data'][$d->mitra]['bobot'] +=$d->bobot;
      }    
    }
    // dd($data,$indexarray);
    return $indexarray;
  }
  public static function getBobotPlanByTematikMitra($tematik,$mitra,$tl)
  {
    $isAdmin = session('auth')->level==2?1:0;
    // dd($isAdmin);
    $q = DB::table('mpromise_lop')
    ->where('mpromise_project_id',$tematik)
    ->where('mpromise_lop.isHapus',0)
    ->where('mitra', $mitra);
    if(!$isAdmin){
      $q->where('tl', $tl);
    }
    $count = $q->count();

    $q = DB::table('mpromise_plan_lop')
    ->select('mpromise_plan_lop.tgl', DB::raw('SUM(value) as value'))
    ->leftJoin('mpromise_lop','mpromise_plan_lop.lop_id','=','mpromise_lop.id')
    ->where('mpromise_plan_lop.tipe',1)
    ->where('mpromise_project_id',$tematik)
    ->where('mpromise_lop.isHapus',0)
    ->where('mitra', $mitra)
    ->orderBy('tgl', 'asc')
    ->groupBy('tgl');
    if(!$isAdmin){
      $q->where('tl', $tl);
    }
    $plan = $q->get();
    // dd($plan);
    $bobot = [];
    $sumbobot = 0;
    foreach($plan as $p){
      $sumbobot += $p->value/$count;
      $bobot[] = (object)["baris"=>$sumbobot, "ItemDate"=>$p->tgl];
    }
    // dd($bobot);
    return $bobot;
  }
  public static function getByTematikMitra($tematik,$mitra,$tl,$startdate)
  {
    $isAdmin = session('auth')->level==2?1:0;
    $start = $startdate?:date('Y')."-".date('m')."-01";
    $q = DB::table('mpromise_plan_lop')
    ->select('mpromise_plan_lop.*', 'mpromise_lop.mpromise_project_id')
    ->leftJoin('mpromise_lop','mpromise_plan_lop.lop_id','=','mpromise_lop.id')
    ->leftJoin('mpromise_project', 'mpromise_lop.mpromise_project_id','=', 'mpromise_project.id')
    ->where('mpromise_project_id',$tematik)
    ->where('mitra', $mitra)
    ->where('tgl','>=', $start)
    ->where('mpromise_lop.isHapus',0)
    ->where('mpromise_project.stts', 0);
    if(!$isAdmin){
      $q->where('tl', $tl);
    }
    $plan = $q->get();
// dd($plan);
    // $start = "2022-".date('m')."-01";
    $head =$data= [];
    $date = strtotime($start);
    $year=date('Y',$date);
    
    for($i=0;$i<3;$i++){
        $dn = strtotime($start." +$i Months");
        $mn=date('m',$dn);
        $lastdate = strtotime(date("Y-m-t", $dn ));
        $day = date("d", $lastdate);
        for($j=1;$j<=$day;$j++){
          $d = strlen($j)==1?'0'.$j:$j;
          $head["$year-$mn-$d"] = "-";
        }
    }

    $q = DB::table('mpromise_lop')
    ->select('mpromise_lop.*','bobot.bobot as sumbobot')
    ->leftJoin('mpromise_project', 'mpromise_lop.mpromise_project_id','=', 'mpromise_project.id')
    ->leftJoin(DB::raw('(SELECT `lop_id`, SUM(CASE 
    WHEN tipe = 1 
    THEN value 
    ELSE 0 
END) AS bobot FROM `mpromise_plan_lop` GROUP BY `lop_id` ORDER BY `lop_id`) as bobot'),'mpromise_lop.id','=','bobot.lop_id')
    ->where('mpromise_project_id',$tematik)
    ->where('mitra', $mitra)
    ->where('mpromise_lop.isHapus',0)
    ->where('mpromise_project.stts', 0);
    if(!$isAdmin){
      $q->where('tl', $tl);
    }
    $lop = $q->get();
    foreach($lop as $l){
      $data[$l->id]['kolom']=$head;
      $data[$l->id]['map']=$head;
      $data[$l->id]['data']=$l;
    }
    foreach($plan as $p){
      if($p->tipe==1){
        $data[$p->lop_id]['kolom'][$p->tgl]="$p->value:$p->id";
        echo $p->lop_id."\n";
      }else{
        $data[$p->lop_id]['map'][$p->tgl]="$p->value:$p->id";
        // echo $p->lop_id."\n";
      }

    }
    // dd($lop,$plan,$data);
    // dd($data);
    return $data;
  }


  public static function getTimByMitra($mitra)
  {
    return DB::table('mpromise_tim')->where('mitra', $mitra)->get();
  }
  public static function getTimByID($tim)
  {
    return DB::table('mpromise_tim')->where('id', $tim)->first();
  }
  
  public static function insertTim($param)
  {
    return DB::table('mpromise_tim')->insert($param);
  }
  public static function updateTim($id,$param)
  {
    return DB::table('mpromise_tim')->where('id',$id)
    ->update($param);
  }
}