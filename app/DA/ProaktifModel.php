<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class ProaktifModel
{
  private static function getQuery(){
    return DB::table('proaktif_project')
    ->select('proaktif_project.*', 'mpromise_lop.id as flag')
    ->leftJoin('mpromise_lop', 'proaktif_project.proaktif_id','=', DB::raw('mpromise_lop.pid AND mpromise_lop.isHapus = 0'))
    ->where('proaktif_project.portofolio', 'Konstruksi')
    ->where('proaktif_project.nama_project','not like', '%PT2%')
    ->whereIn('proaktif_project.status_project', ['Active', 'Init']);
  }
  public static function getAll()
  {
    return self::getQuery()
    ->get();
  }
  public static function getByWitel($witel)
  {
    return self::getQuery()
    ->where('proaktif_project.customer', 'like', '%'.$witel.'%')
    ->get();
  }
  public static function getByID($id)
  {
    return DB::table('proaktif_project')->where('id', $id)
    ->first();
  }

}
