<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class APMModel
{
  public static function getCashout()
  {
    return DB::table('apm_cashout')
    ->get();
  }
  public static function getCashListByPID($pid)
  {
    return DB::table('apm_cashout')->where('pid', 'like', '%'.$pid.'%')
    ->get();
  }
  public static function getBudgetList()
  {
    // return DB::table('proaktif_project as bam')->select('bam.*',DB::raw('(select value from budget_apm ba where ba.pid=bam.pid and ba.keperluan = "Material : Non Stock") as Material_Non_Stock'))->whereIn('fase',['Executing','Planning'])->where('status_project','Active')->orderBy('tgl_mulai','desc')
    // ->get();
    $data = DB::select('select bam.*,Material_Non_Stock,Material_Stock,Non_Material_Jasa,Non_Material_Sewa,Non_Material_Tenaga_Kerja,Non_Materiall_Operasional,
      (case when bam.sapid = "" then 0 else (select count(*) from apm_cashout where pid like CONCAT("%", bam.sapid, "%")) end) as apm_count
      from proaktif_project bam left join (select pid,sum(Material_Non_Stock) as Material_Non_Stock,
      sum(Material_Stock) as Material_Stock,
      sum(Non_Material_Jasa) as Non_Material_Jasa,
      sum(Non_Material_Sewa) as Non_Material_Sewa,
      sum(Non_Material_Tenaga_Kerja) as Non_Material_Tenaga_Kerja,
      sum(Non_Materiall_Operasional) as Non_Materiall_Operasional from (SELECT ba.pid, (case when ba.keperluan = "Material : Non Stock" then REPLACE(ba.value,",","") end) as Material_Non_Stock,
      (case when ba.keperluan = "Material : Stock" then REPLACE(ba.value,",","") end) as Material_Stock,
      (case when ba.keperluan = "Non Material : Jasa" then REPLACE(ba.value,",","") end) as Non_Material_Jasa,
      (case when ba.keperluan = "Non Material : Sewa" then REPLACE(ba.value,",","") end) as Non_Material_Sewa,
      (case when ba.keperluan = "Non Material : Tenaga Kerja" then REPLACE(ba.value,",","") end) as Non_Material_Tenaga_Kerja,
      (case when ba.keperluan = "Non Materiall : Operasional" then REPLACE(ba.value,",","") end) as Non_Materiall_Operasional FROM budget_apm ba) as t_apm group by pid) bim on bam.pid=bim.pid where fase IN ("Executing", "Planning") and status_project="Active" order by tgl_mulai desc');
    return $data;
  }
  // public static function save($query)
  // {
  //   DB::table('budget_apm_monitor')->insert($query);
  // }
  // public static function delete($id)
  // {
  //   DB::table('budget_apm_monitor')->where('id', $id)->delete();
  // }
}
