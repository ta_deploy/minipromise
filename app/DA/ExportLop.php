<?php
namespace App\DA;

use App\DA\LopModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ExportLop implements FromView, WithTitle
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function view(): View
    {
        // dd(LopModel::export($this->id));
        return view('export.listlop', [
            'data' => LopModel::export2($this->id)
        ]);
    }
    public function title(): string
    {
        return 'List LOP';
    }
}