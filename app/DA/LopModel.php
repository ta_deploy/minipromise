<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

use App\DA\WaspangModel;
use App\DA\ProjectModel;
use Telegram;

class LopModel
{
  const TABLE = 'mpromise_lop';

  
  public static function getMitratelDash($mitra,$tl)
  {
    $sql = "jenis_pekerjaan LIKE 'MITRATEL'";
    if($mitra!='all'){
      $sql .= " and mitra='".$mitra."'";
    }
    if($tl!='all'){
      $sql .= " and tl='".$tl."'";
    }
    return DB::select("SELECT COUNT(*) AS span, mpromise_project_id, nama_project,
      sum(case when DROPLOP=0 and RFS!='' then 1 else 0 end) as rfs,
      sum(case when DROPLOP=0 and RFS='' then 1 else 0 end) as blmrfs,
      sum(case when DROPLOP=0 and step_id in(4,5,7,8,9) and Done_Gelar_Kabel!='' and (tgl_permit_a is null or tgl_permit_b is null) and RFS='' then 1 else 0 end) as installed_blmpermit,
      sum(case when DROPLOP=0 and step_id in(4,5,7,8,9) and Done_Gelar_Kabel!='' and (tgl_permit_a is not null and tgl_permit_b is not null) and RFS='' then 1 else 0 end) as installed_sdhpermit,
      sum(case when DROPLOP=0 and step_id in(4) and Done_Gelar_Kabel='' and (tgl_permit_a is null or tgl_permit_b is null) then 1 else 0 end) as ogp_blmpermit,
      sum(case when DROPLOP=0 and step_id in(4) and Done_Gelar_Kabel='' and (tgl_permit_a is not null and tgl_permit_b is not null) then 1 else 0 end) as ogp_sdhpermit,
      sum(case when DROPLOP=0 and step_id in(0,1,6,2) and perizinan_comcase=0 then 1 else 0 end) as ny_comcase,
      sum(case when DROPLOP=0 and step_id in(0,1,6,2) and perizinan_pu=0 then 1 else 0 end) as ny_pu,
      sum(case when DROPLOP=0 and (step_id in(0,1,6,2) or tgl_material_tiba is null) then 1 else 0 end) as ny_mos
      FROM mpromise_lop where ".$sql."
      GROUP BY mpromise_project_id ORDER BY mpromise_project_id");
  }
  public static function getMitratelDashDetil($col,$row)
  {
    // COL=JLH_SPAN,RFS,BLM_RFS,TD_PD,TD_PNY,TD_TOT,OP_PD,OP_PNY,OP_TOT,NY_COMCASE,NY_PU,NY_MTR
    $q = DB::table('mpromise_lop as ml')->select('ml.*','mm.mitra as nama_mitra')->leftJoin('mpromise_mitra as mm','ml.mitra','=','mm.id')->where('DROPLOP',0)->where('jenis_pekerjaan','MITRATEL');
    if($col=='JLH_SPAN'){
    }else if($col=='RFS'){
      $q->where('RFS','!=', '');
    }else if($col=='BLM_RFS'){
      $q->where('RFS','=', '');
    }else if($col=='TD_PD'){
      $q->whereIn('step_id',[4,5,7,8,9])
      ->where('Done_Gelar_Kabel','!=','')
      ->whereNotNull('tgl_permit_a')
      ->whereNotNull('tgl_permit_b')->where('RFS','=', '');
    }else if($col=='TD_PNY'){
      $q->whereIn('step_id',[4,5,7,8,9])->where('Done_Gelar_Kabel','!=','')->where('RFS','=', '')
      ->where(function($query) {
          $query->whereNull('tgl_permit_a')
          ->orWhereNull('tgl_permit_b');
      });
    }else if($col=='TD_TOT'){
      $q->whereIn('step_id',[4,5,7,8,9])->where('Done_Gelar_Kabel','!=','');
    }else if($col=='OP_PD'){
      $q->whereIn('step_id',[4])->where('Done_Gelar_Kabel','')->whereNotNull('tgl_permit_a')->whereNotNull('tgl_permit_b');
    }else if($col=='OP_PNY'){
      $q->whereIn('step_id',[4])->where('Done_Gelar_Kabel','')
      ->where(function($query) {
          $query->whereNull('tgl_permit_a')
          ->orWhereNull('tgl_permit_b');

      });
    }else if($col=='OP_TOT'){
      $q->whereIn('step_id',[4])->where('Done_Gelar_Kabel','');
    }else if($col=='NY_COMCASE'){
      $q->where('perizinan_comcase','=', 0)->whereIn('step_id',[0,1,6,2]);
    }else if($col=='NY_PU'){
      $q->where('perizinan_pu','=', 0)->whereIn('step_id',[0,1,6,2]);
    }else if($col=='NY_MTR'){
      $q->where(function($query) {
          $query->whereNull('tgl_material_tiba')
          ->orWhereIn('step_id',[0,1,6,2]);
      });
    }
    if($row!='all'){
      $q->where('mpromise_project_id', $row);
    }
    return $q->get();
  }
  
  public static function getById($id)
  {
    return DB::table(self::TABLE)
    ->select(self::TABLE.'.id As id_lop', 'mpromise_project.id As id_project', 'mpromise_project.*',self::TABLE.'.*','mpromise_mitra.mitra as nama_mitra')
    ->leftjoin('mpromise_project', 'mpromise_lop.mpromise_project_id', '=', 'mpromise_project.id')
    ->leftjoin('mpromise_mitra', 'mpromise_lop.mitra', '=', 'mpromise_mitra.id')
    ->where('mpromise_lop.id', $id)
    ->first();
  }

  public static function getAanwijzingWaspangList($waspang,$mitra)
  {
    return DB::select('SELECT ml.mpromise_project_id,ml.* FROM mpromise_lop ml WHERE ml.waspang='.$waspang.' and mitra='.$mitra.' and step_id=6 and droplop=0 and isHapus=0 GROUP BY ml.mpromise_project_id ORDER BY ml.mpromise_project_id');
  }
  public static function getDistinctMitraAanwijzing($waspang)
  {
    return DB::select('SELECT COUNT(*) AS Baris, ml.mitra,mm.label FROM mpromise_lop ml left join mpromise_mitra mm on ml.mitra=mm.id WHERE ml.waspang='.$waspang.' and step_id=6 and droplop=0 and isHapus=0 GROUP BY ml.mitra ORDER BY ml.mitra');
  }
  public static function getAanwijzingWaspang($tematik,$waspang,$mitra)
  {
    return DB::table(self::TABLE.' as ml')
    ->select('ml.id As id_lop', 'mpromise_project.id As id_project', 'mpromise_project.*','ml.*','mpromise_mitra.mitra as nama_mitra')
    ->leftjoin('mpromise_project', 'ml.mpromise_project_id', '=', 'mpromise_project.id')
    ->leftjoin('mpromise_mitra', 'ml.mitra', '=', 'mpromise_mitra.id')
    ->where('ml.mpromise_project_id', $tematik)
    ->where('ml.waspang', $waspang)
    ->where('ml.mitra', $mitra)
    ->get();
  }
  public static function getByProject($witel, $id)
  {
    return DB::table(self::TABLE)
    ->select(self::TABLE.'.id As id_lop', 'mpromise_project.id As id_project', 'mpromise_project.*', 'mpromise_step.step',self::TABLE.'.*')
    ->leftjoin('mpromise_project', 'mpromise_lop.mpromise_project_id', '=', 'mpromise_project.id')
    ->leftjoin('mpromise_step', 'mpromise_lop.step_id', '=', 'mpromise_step.id')
    ->where('mpromise_project_id', $id)
    ->where('isHapus', 0)
    ->where('witel_lop', $witel)
    ->get();
  }
  public static function getByMproId($id)
  {
    return DB::table(self::TABLE)
    ->where('mpromise_project_id', $id)->OrderBy('id', 'desc')
    ->get();
  }

  public static function getCoreByLopId($id)
  {
    return DB::table('mpromise_core')->where('LOP_ID', $id)->get();
  }

  public static function getSearch($q)
  {
    return DB::table(self::TABLE)
    ->select(self::TABLE.'.id As id_lop', 'mpromise_project.id As id_project', 'mpromise_project.*','mpromise_step.step',self::TABLE.'.*')
    ->leftjoin('mpromise_project', 'mpromise_lop.mpromise_project_id', '=', 'mpromise_project.id')
    ->leftjoin('mpromise_step', 'mpromise_lop.step_id', '=', 'mpromise_step.id')
    ->where([
      ['mpromise_lop.nama_lop', 'like', '%' . $q . '%'],
      ['mpromise_lop.isHapus', 0]
    ])
    ->orWhere(function($query) use($q){
      $query->where('mpromise_lop.pid', 'like', '%' . $q . '%')
            ->where('mpromise_lop.isHapus',0);
    })->get();
  }

  public static function getByStepId($id)
  {
    return DB::table(self::TABLE)
    ->select(self::TABLE.'.id As id_lop', 'mpromise_project.id As id_project', 'mpromise_project.*',self::TABLE.'.*','mpromise_step.step')
    ->leftjoin('mpromise_project', 'mpromise_lop.mpromise_project_id', '=', 'mpromise_project.id')
    ->leftjoin('mpromise_step', 'mpromise_lop.step_id', '=', 'mpromise_step.id')
    ->where('step_id', $id)
    ->OrderBy('mpromise_project_id', 'DESC')
    ->OrderBy('updated_at', 'DESC')
    ->get();
  }

  public static function getAll()
  {
    return DB::table(self::TABLE)
    ->select(self::TABLE.'.id As id_lop', 'mpromise_project.id As id_project', 'mpromise_project.*',self::TABLE.'.*', 'mpromise_mitra.mitra')
    ->leftjoin('mpromise_project', 'mpromise_lop.mpromise_project_id', '=', 'mpromise_project.id')
    ->leftjoin('mpromise_mitra', 'mpromise_mitra.id', '=', self::TABLE.'.mitra')
    ->get();
  }

  public static function getSto()
  {
    return DB::table('maintenance_datel')->select('sto as id', 'sto as text')->get();
  }

  public static function getAllByWaspang($waspang)
  {
    return (DB::table(self::TABLE.' as ml')
          ->leftjoin('mpromise_project as mp', 'ml.mpromise_project_id', '=', 'mp.id')
          ->leftjoin('mpromise_mitra as mm', 'ml.mitra', '=', 'mm.id')
          ->leftJoin('mpromise_step as ms', 'ml.step_id', '=', 'ms.id')
          ->select('ml.id As id_lop', 'mp.id As id_project', 'mp.*','ml.*', 'ms.step','mm.mitra as nama_mitra')
          ->where('waspang', $waspang)
          ->where('ms.actor', 1)
          ->where('mp.stts', 0)
          ->where('ml.DROPLOP', 0)
          ->where('ml.isHapus', 0)
          ->where('ml.step_id', '!=', 6)
          ->get());
  }

  public static function delete($id)
  {
    DB::transaction(function () use ($id) {
      // return DB::table(self::TABLE)->where('id', $id)->delete();
    });
  }

  public static function insert_upload_lop($data)
  {
    DB::table(self::TABLE)
          ->insert($data);
  }

  public static function insert($id,$jenis, $req)
  {
    $auth = session('auth');
    $id = DB::table(self::TABLE)
      ->insertGetId([
        "nama_lop" => $req->nama_lop,

        "pid" => $req->pid,
        "sto" => $req->sto,
        "jenis_pekerjaan" => $req->jenis_pekerjaan,
        "waspang" => $req->waspang,
        "waspang_nm" => WaspangModel::getWaspangByID($req->waspang)->nama,

        "nama_project" => ProjectModel::getById($id)->nama,
        "mitra" => $req->mitra,

        "mpromise_project_id" => $id,
        "step_id" => 6,
        "created_by" => $auth->id_user,
        "created_at" => DB::raw('now()'),
        "loker" => "Aanwijzing",
        "timeplan_fisik" => $req->timeplan_fisik,
        "timeplan_berkas" => $req->timeplan_berkas,
        "PENGGANTI_LOP" => $req->PENGGANTI_LOP
      ]);
    return $id;
  }
  public static function import($req)
  {
    $auth = session('auth');
    $w = WaspangModel::getWaspangByID($req->waspang);
    return DB::table(self::TABLE)
      ->insertGetId([
        "nama_lop" => $req->nama_lop,
        "pid" => $req->pid,
        "sapid" => $req->sapid,
        "witel_lop" => session('auth')->witel,
        "sto" => $req->sto,
        "odc" => $req->odc,
        "jenis_pekerjaan" => $req->jenis_pekerjaan,
        "waspang" => $req->waspang,
        "tl" => $w->nik_tl,
        "waspang_nm" => $w->nama,
        "nama_project" => ProjectModel::getById($req->tematik)->nama,
        "mitra" => $req->mitra,
        "mpromise_project_id" => $req->tematik,
        "step_id" => 6,
        "created_by" => $auth->id_user,
        "created_at" => DB::raw('now()'),
        "loker" => "Aanwijzing",
        "timeplan_fisik" => $req->timeplan_fisik,
        "timeplan_berkas" => $req->timeplan_berkas
      ]);
  }

  public static function update_lop($id, $req)
  {
    $auth = session('auth');
    $w = WaspangModel::getWaspangByID($req->waspang);
    DB::table(self::TABLE)
    ->where('id', $id)
    ->update([
      "nama_lop"        => $req->nama_lop,
      "pid"             => $req->pid,
      "sto"             => $req->sto,
      "waspang"         => $req->waspang,
      "waspang_nm"      => $w->nama,
      "tl"              => $w->nik_tl,
      'DROPLOP'         => 0,
      "mitra"           => $req->mitra,
      "created_by"      => $auth->id_user,
      "timeplan_fisik"  => $req->timeplan_fisik,
      "timeplan_berkas" => $req->timeplan_berkas,
    ]);
  }

  public static function update($id, $update)
  {
    DB::table(self::TABLE)
      ->where("id", $id)
      ->update($update);
  }
  public static function getLastLogTime($id){
    $exist = DB::table('mpromise_log')->where('lop_id', $id)->orderBy('id','desc')->first();
    if($exist){
      return $exist->update_end;
    }else{
      return DB::raw('now()');
    }
  }
  public static function gettotalbobotbylopidstep($id,$step)
  {
    return DB::table('mpromise_log')
                ->select(DB::raw('SUM(bobot) as total'))
                ->where('lop_id', $id)
                ->where('step_id', $step)
                ->first();
  }

  public static function insertlog($insert)
  {
    // DB::statement('update mpromise_log set update_end=NOW() where lop_id='.$insert['lop_id'].' order by id desc limit 1');
    // $lop = self::getById($insert['lop_id']);
    // $insert['step_id'] = $lop->step_id;
    // $insert['tematik_id'] = $lop->mpromise_project_id;
    $logtime = self::getLastLogTime($insert['lop_id']);
    $insert['updated_at'] = $logtime;
    DB::table('mpromise_log')
      ->insert($insert);
      // 'chat_id' => '-722356370',
        // $chat_id = '-722356370';piloting
    // Telegram::sendMessage([
    //   'chat_id' => '-767009927',
    //   'text' => $lop->nama_lop."\n".session('auth')->nama."/".$insert['catatan'],
    //   'parse_mode' =>'html'
    // ]);
  }
  public static function insertlogs($insert)
  {
    $insertarray=[];
    foreach($insert as $i){
      $logtime = self::getLastLogTime($i['lop_id']);
      $i['updated_at'] = $logtime;
      $insertarray[] = $i;
    }
    DB::table('mpromise_log')->insert($insertarray);
  }
  public static function getLogByLop($id)
  {
    return DB::table('mpromise_log')
      ->where('lop_id', $id)->orderBy('id','desc')->get();
  }

  public static function getProject()
  {
    return DB::table('mpromise_project')->select('id', 'nama as text')->get();
  }

  public static function saveProject($req)
  {
    DB::transaction(function () use ($req) {
      return DB::table('mpromise_project')->insertGetId(['nama' => $req->nama]);
    });
  }

  public static function search_boq_idlop($q)
  {
    return DB::table(self::TABLE)
    ->select(self::TABLE.'.boq_unwizing', 'mpromise_lop.id As id_project', 'mpromise_project.nama', 'mpromise_project.pid')
    ->leftjoin('mpromise_project', self::TABLE.'.mpromise_project_id', '=', 'mpromise_project.id')
    ->whereNotNull('boq_unwizing')
    ->where(function($joins) use($q) {
      $joins->where('nama', 'like', '%' . $q . '%')
      ->orWhere('pid', 'like', '%' . $q . '%');
    })
    ->get();

  }
  public static function getStepByID_Sys($id){
    return DB::table('mpromise_step')->where('id', $id)->first();
  }
  public static function getStepActive($id){
    return DB::table('mpromise_step')->where('active', 1)->orderBy('urutan','asc')->get();
  }

  public static function countMenu($auth)
  {
    $id_user = $auth->id_user?:null;
    // dd($auth);
    $count = DB::select('select count( case when step_id=8 then 1 else NULL end) as QC,count( case when step_id in(6,2,4,7,5) and ml.waspang ="'.$id_user.'" then 1 else NULL end) as waspang from mpromise_lop ml left join mpromise_project mp on ml.mpromise_project_id=mp.id where mp.stts =0 and ml.DROPLOP=0 and ml.isHapus=0');
    return $count[0];
  }

  public static function getPlanWaspangItemByWaspang($waspang)
  {
    // $id_user = $auth->id_user;
    // dd($auth);
    return DB::table('mpromise_planwaspang_item')->where('waspang', $waspang)->get();
  }
  // ($isi->finish_terminasi+$isi->lopdrop)*100/$isi->total_lop
  public static function droplop($id)
  {
    DB::table('mpromise_lop')->where('id', $id)->update([
      'DROPLOP' => 1
    ]);
  }
  public static function hapuslop($id)
  {
    DB::table('mpromise_lop')->where('id', $id)->update([
      'isHapus' => 1
    ]);
  }
  public static function export($id){
    $sql = DB::table('mpromise_lop as ml')
    ->select(DB::raw('CONCAT(mpromise_step.urutan,".",mpromise_step.step) as step,(select sum((mb.hargajasa*mb.qty)+(mb.hargamaterial*mb.qty)) from mpromise_boq mb where lop_id = ml.id) as hargalop'),'ml.id','ml.step_id','ml.nama_lop','ml.pid','ml.sto','ml.waspang','ml.waspang_nm','ml.nama_project','ml.jenis_pekerjaan','mpromise_mitra.mitra', DB::RAW('SUM(CASE WHEN DROPODP = 0 THEN 1 ELSE 0 END) As plan_odp, SUM(CASE WHEN DROPODP = 0 AND Real_Odp_Nama IS NOT NULL THEN 1 ELSE 0 END) As real_odp, SUM(CASE WHEN DROPODP = 0 AND GOLIVE = 1 THEN 1 ELSE 0 END) As odp_golive'),'ml.timeplan_fisik','ml.timeplan_berkas','ml.created_at','ml.updated_at','ml.created_by','ml.status','ml.tgl_material_tiba','ml.progres_instalasi','ml.catatan_instalasi','ml.Done_Aanwijzing','ml.Done_Pengiriman_Material','ml.Done_Pragelaran','ml.Done_Tanam_Tiang','ml.Done_Gelar_Kabel','ml.Done_Pasang_Terminal','ml.Done_Terminasi','ml.Done_Perijinan','ml.status_qc','ml.DROPLOP','ml.PENGGANTI_LOP','ml.isHapus','ml.NILAI_LOP','ml.TGL_QC_OK','ml.TGL_GO_LIVE')
    ->leftJoin('mpromise_step','ml.step_id','=','mpromise_step.id')
    ->leftJoin('mpromise_mitra','ml.mitra','=','mpromise_mitra.id')
    ->leftJoin('mpromise_core as mc', 'ml.id', '=', 'mc.LOP_ID')
    ->where('mpromise_project_id',$id)
    ->where('isHapus',0)
    ->GroupBy('ml.id')
    ->get();

    return $sql;
  }
  public static function export2($id){
    $sql = DB::table('mpromise_lop as ml')
    ->select(DB::raw('CONCAT(mpromise_step.urutan,".",mpromise_step.step) as step,ring,(select sum((mb.hargajasa*mb.qty)+(mb.hargamaterial*mb.qty)) from mpromise_boq mb where lop_id = ml.id) as hargalop'),'ml.id','ml.step_id','ml.nama_lop','ml.pid','ml.sto','ml.waspang','ml.waspang_nm','ml.nama_project','ml.jenis_pekerjaan','mpromise_mitra.mitra', DB::RAW('SUM(CASE WHEN DROPODP = 0 THEN 1 ELSE 0 END) As plan_odp, SUM(CASE WHEN DROPODP = 0 AND Real_Odp_Nama IS NOT NULL THEN 1 ELSE 0 END) As real_odp, SUM(CASE WHEN DROPODP = 0 AND GOLIVE = 1 THEN 1 ELSE 0 END) As odp_golive'),'ml.timeplan_fisik','ml.timeplan_berkas','ml.created_at','ml.updated_at','ml.created_by','ml.status','ml.tgl_material_tiba','ml.progres_instalasi','ml.catatan_instalasi','ml.Done_Aanwijzing','ml.Done_Pengiriman_Material','ml.Done_Pragelaran','ml.Done_Tanam_Tiang','ml.Done_Gelar_Kabel','ml.Done_Pasang_Terminal','ml.Done_Terminasi','ml.Done_Perijinan','ml.status_qc','ml.DROPLOP','ml.PENGGANTI_LOP','ml.isHapus','ml.NILAI_LOP','ml.TGL_QC_OK','ml.TGL_GO_LIVE')
    ->leftJoin('mpromise_step','ml.step_id','=','mpromise_step.id')
    ->leftJoin('mpromise_mitra','ml.mitra','=','mpromise_mitra.id')
    ->leftJoin('mpromise_core as mc', 'ml.id', '=', 'mc.LOP_ID')
    ->where('mpromise_project_id',$id)
    ->where('isHapus',0)
    ->GroupBy('ml.id')
    ->get();

    return $sql;
  }
  public static function get_bobot_per_tgl(){
    $data = array();
    $timeplan_fisik_end = DB::select("SELECT count(*) as total,substr(timeplan_fisik,14,10) as plan FROM `mpromise_lop` WHERE `isHapus`=0 and `mpromise_project_id` = 40 group by plan ORDER BY `plan` ASC");
    $real = DB::select("SELECT avg(ml.bobot) as bobot,COUNT(*) AS `Baris`,DATE_FORMAT(ml.updated_at, '%Y-%m-%d') ItemDate FROM mpromise_log ml left join mpromise_lop mlop on ml.lop_id=mlop.id where `isHapus`=0 and mlop.mpromise_project_id=40 GROUP BY DATE_FORMAT(ml.updated_at, '%Y-%m-%d') ORDER BY `ItemDate` ASC");
    $b=0;
    foreach($real as $r){
      $b+=$r->bobot;
      echo $r->bobot." = ".$b."<br>";
    }
    foreach($timeplan_fisik_end as $tfe){
      $realbbt = null;
      foreach($real as $r){
        if($r->ItemDate <= $tfe->plan){
          $realbbt+=$r->bobot;
          // dd("asd");
        }else{
          break;
        }
      }
      $data['tfe'][] = ["plan"=>$tfe->total,"real"=>$realbbt,"tgl"=>$tfe->plan];
    }
      dd($data,$timeplan_fisik_end,$real);
  }
}
