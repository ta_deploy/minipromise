<?php
namespace App\DA;

use App\DA\LopModel;
use App\DA\MaterialModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ExportBoq implements FromView,WithTitle
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function view(): View
    {
        $boq = MaterialModel::getBoqTematik($this->id);
        // $designator = MaterialModel::getDesignatorTematik($this->id);
        // dd($designator);
        $arrayboq=[];
        foreach($boq as $b){
            if(array_key_exists($b->lop_id,$arrayboq) && array_key_exists($b->id_barang,$arrayboq[$b->lop_id])){
                $arrayboq[$b->lop_id][$b->id_barang]+=$b->qty;
            }else{
                $arrayboq[$b->lop_id][$b->id_barang]=$b->qty;
            }
            
        }
        return view('export.listboq', [
            'data' => LopModel::export2($this->id),
            'boq'  => $arrayboq,
            'designator'=> MaterialModel::getDesignatorTematik($this->id)
        ]);
    }
    public function title(): string
    {
        return 'BOQ';
    }
}