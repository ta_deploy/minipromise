<?php

namespace App\DA;

use App\DA\LopModel;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class LopExport implements FromCollection, WithHeadings
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function collection()
    {
        return LopModel::export($this->id);
    }
    public function headings(): array
    {
        return ['step','nilai boq',
            'id','step_id','nama_lop','pid','sto','waspang','waspang_nm',
            'nama_project','jenis_pekerjaan','mitra','plan_odp', 'real_odp', 'odp_golive','timeplan_fisik','timeplan_berkas','created_at','updated_at','created_by','status','tgl_material_tiba','progres_instalasi','catatan_instalasi','Done_Aanwijzing','Done_Pengiriman_Material','Done_Pragelaran','Done_Tanam_Tiang','Done_Gelar_Kabel','Done_Pasang_Terminal','Done_Terminasi','Done_Perijinan','status_qc','DROPLOP','PENGGANTI_LOP','isHapus','NILAI_LOP','TGL_QC_OK','TGL_GO_LIVE'];
    }
}