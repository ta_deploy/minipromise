<?php
namespace App\DA;

use App\DA\ReviewModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithTitle;

class ExportSummarymtr implements FromView, WithTitle
{
  public function __construct(int $id)
  {
    $this->id = $id;
  }
  public function view(): View
  {
    return view('export.summarymtr', [
      'mtr' => ReviewModel::getMaterialByTematik($this->id)
    ]);
  }
  public function title(): string
  {
    return 'Summary Material';
  }
}