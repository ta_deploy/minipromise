<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class ReviewModel
{

  public static function getReviewByLopID($id){
    return DB::table('mpromise_review as mr')
    ->where('lop_id', $id)->orderBy('id', 'desc')->get();
  }
  public static function saveReview($param){
    DB::table('mpromise_review')
    ->insert($param);
  }
  public static function saveReviewProgress($id, $param){
    DB::table('mpromise_lop')
    ->where('id', $id)->update($param);
  }
  public static function getMitraListByWitel($witel){
    $data = DB::table('mpromise_mitra as mm')
    ->leftJoin(DB::raw("(SELECT COUNT(*) AS Baris,avg(review_progress) as tp, mitra as mitra_id,count(ada_plan) as ap FROM mpromise_lop left join mpromise_project on mpromise_lop.mpromise_project_id=mpromise_project.id left join (SELECT COUNT(*) AS ada_plan, lop_id FROM mpromise_plan_lop GROUP BY lop_id) mp on mpromise_lop.id=mp.lop_id where isHapus=0 and DROPLOP=0 and mpromise_project.stts=0 and witel_lop like '%$witel%' GROUP BY mitra) as ml"),"mm.id","=","ml.mitra_id")
    ->where('status', 'Aktif')

    ->whereNotNull('ml.Baris')->get();
    return $data;
  }
  public static function getListLopByMitraWitel($mitra,$witel){
    $data = DB::table('mpromise_lop as ml')
    ->leftJoin(DB::raw("(SELECT COUNT(*) AS ada_plan, lop_id FROM mpromise_plan_lop GROUP BY lop_id) as mp"),"mp.lop_id","=","ml.id")
    ->leftJoin('mpromise_project as mp','ml.mpromise_project_id','=','mp.id')
    ->where('mp.stts', 0)
    ->where('isHapus', 0)
    ->where('DROPLOP', 0)
    ->where('mitra', $mitra)
    ->where('witel_lop', $witel)
    ->orderBy('sto','asc')
    ->get();
    return $data;
  }

  public static function getByLopId($id)
  {
    $plan = self::getPlan($id);
    $real = self::getReal($id);
    $start =$plan['awal'];
    if($plan['awal']>$real['awal']){
      $start = $real['awal'];
    }
    $akhir =$plan['akhir'];
    if($plan['akhir']<$real['akhir']){
      $akhir = $real['akhir'];
    }
    $begin = new \DateTime($start);
    $end   = new \DateTime($akhir);
    $data = null;
    $planbbt = $realbbt = 0;
    for($i = $begin; $i <= $end; $i->modify('+1 day')){
        $day = $i->format("Y-m-d");
        if(array_key_exists($day,$plan['data'])){
          $planbbt = $plan['data'][$day];
        }
        if(array_key_exists($day,$real['data'])){
          $realbbt = $real['data'][$day];
        }
        $data[] = (object)["plan"=>$planbbt,"real"=>$realbbt,"tgl"=>$day];
    }
    // dd($data,$plan,$real);
    return $data;

  }
  public static function getPlan($id)
  {
    $plan = DB::table('mpromise_plan_lop')
    ->select('mpromise_plan_lop.tgl', DB::raw('SUM(value) as value'))
    ->where('mpromise_plan_lop.tipe',1)
    ->where('lop_id',$id)
    ->orderBy('tgl', 'asc')
    ->groupBy('tgl')
    ->get();
    $bobot = ["awal"=>null,"akhir"=>null,"data"=>[]];
    $sumbobot = 0;
    $tgl = null;
    foreach($plan as $key => $p){
      if(!$key){
        $bobot['awal'] = $p->tgl;
      }
      
      $sumbobot += $p->value;
      $bobot['data'][$p->tgl] = $sumbobot;
      $tgl = $p->tgl;
    }
    $bobot['akhir'] = $tgl;
    // dd($bobot,$tgl);
    return $bobot;
  }
  public static function getReal($id)
  {
    $real = DB::select("SELECT sum(ml.bobot) as bobot,DATE_FORMAT(ml.updated_at, '%Y-%m-%d') ItemDate FROM mpromise_log ml where lop_id=$id GROUP BY DATE_FORMAT(ml.updated_at, '%Y-%m-%d') ORDER BY `ItemDate` ASC");
    $bobot = ["awal"=>null,"akhir"=>null,"data"=>[]];
    $sumbobot = 0;
    foreach($real as $key => $p){
      if(!$key){
        $bobot['awal'] = $p->ItemDate;
      }
      
      $sumbobot += $p->bobot;
      $bobot['data'][$p->ItemDate] = $sumbobot;
    }
    $bobot['akhir'] = $p->ItemDate;
    return $bobot;
  }
  private static function countLopByTematik($tematik){
    $data = DB::table('mpromise_lop')->where('isHapus',0)->where('DROPLOP',0);
    if($tematik){
      $data->where('mpromise_project_id',$tematik);
    }
    return $data->count('id');
  }
  public static function getByTematik($tematik)
  {
    $totallop = self::countLopByTematik($tematik);
    $plan = self::getPlanByTematik($tematik);
    // dd($plan);
    $real = self::getRealByTematik($tematik);
    $start =$plan['awal'];
    if($plan['awal']>$real['awal']){
      $start = $real['awal'];
    }
    $akhir =$plan['akhir'];
    if($plan['akhir']<$real['akhir']){
      $akhir = $real['akhir'];
    }
    $begin = new \DateTime($start);
    $end   = new \DateTime($akhir);
    $data = null;
    $planbbt = $realbbt = 0;
    for($i = $begin; $i <= $end; $i->modify('+1 day')){
        $day = $i->format("Y-m-d");
        if(array_key_exists($day,$plan['data'])){
          $planbbt = $plan['data'][$day]/$totallop;
        }
        if(array_key_exists($day,$real['data'])){
          $realbbt = $real['data'][$day]/$totallop;
        }
        $data[] = (object)["plan"=>$planbbt,"real"=>$realbbt,"tgl"=>$day];
    }
    // dd($data,$plan,$real);
    return $data;

  }
  public static function getPlanByTematik($tematik)
  {
    $data = DB::table('mpromise_plan_lop as mpl')
    ->select('mpl.tgl', DB::raw('SUM(value) as value'))
    ->leftJoin('mpromise_lop as ml', 'mpl.lop_id','=','ml.id')
    ->where('mpl.tipe',1)
    ->where('ml.isHapus',0)
    ->where('ml.DROPLOP',0)
    ->orderBy('tgl', 'asc')
    ->groupBy('tgl');
    if($tematik){
      $data->where('ml.mpromise_project_id',$tematik);
    }
    $plan = $data->get();
    // dd($plan);
    $bobot = ["awal"=>null,"akhir"=>null,"data"=>[]];
    $sumbobot = 0;
    $tgl = null;
    foreach($plan as $key => $p){
      if(!$key){
        $bobot['awal'] = $p->tgl;
      }
      
      $sumbobot += $p->value;
      $bobot['data'][$p->tgl] = $sumbobot;
      $tgl = $p->tgl;
    }
    $bobot['akhir'] = $tgl;
    // dd($bobot,$tgl);
    return $bobot;
  }
  public static function getRealByTematik($tematik)
  {
    $where="mlo.isHapus=0 and mlo.DROPLOP=0";
    if($tematik){
      $where.=" and mlo.mpromise_project_id=$tematik";
    }
    $real = DB::select("SELECT sum(ml.bobot) as bobot,DATE_FORMAT(ml.updated_at, '%Y-%m-%d') ItemDate FROM mpromise_log ml left join mpromise_lop mlo on ml.lop_id=mlo.id where $where GROUP BY DATE_FORMAT(ml.updated_at, '%Y-%m-%d') ORDER BY `ItemDate` ASC");
    $bobot = ["awal"=>null,"akhir"=>null,"data"=>[]];
    $sumbobot = 0;
    foreach($real as $key => $p){
      if(!$key){
        $bobot['awal'] = $p->ItemDate;
      }
      
      $sumbobot += $p->bobot;
      $bobot['data'][$p->ItemDate] = $sumbobot;
    }
    $bobot['akhir'] = $p->ItemDate;
    return $bobot;
  }
  public static function mitratelBobotWeekReal($tematik,$mitra,$tl)
  {
    $where="mlo.isHapus=0 and mlo.DROPLOP=0 and mlo.jenis_pekerjaan='MITRATEL'";
    if($tematik!='all'){
      $where.=" and mlo.mpromise_project_id=$tematik";
    }
    if($mitra!='all'){
      $where.=" and mlo.mitra=$mitra";
    }
    if($tl!='all'){
      $where.=" and mlo.tl=$tl";
    }
    $real = DB::select("SELECT sum(ml.bobot) as bobot,week(DATE_FORMAT(ml.updated_at, '%Y-%m-%d')) ItemDate FROM mpromise_log ml left join mpromise_lop mlo on ml.lop_id=mlo.id where ".$where." GROUP BY week(DATE_FORMAT(ml.updated_at, '%Y-%m-%d')) ORDER BY `ItemDate` ASC");
    // dd($real);
    $bobot = ["awal"=>null,"akhir"=>null,"data"=>[]];
    $sumbobot = 0;
    foreach($real as $key => $p){
      if(!$key){
        $bobot['awal'] = $p->ItemDate;
        $bobot['data'][$bobot['awal']-1]=0;
      }
      
      $sumbobot += $p->bobot;
      $bobot['data'][$p->ItemDate] = $sumbobot;
      $bobot['akhir'] = $p->ItemDate;
    }

    // dd($bobot);
    return $bobot;
  }
  public static function getBobotMitratel($mitra,$tl)
  {
    $rt = self::getMitratelTematik($mitra,$tl);
    $bobotarray = [];
    foreach($rt as $key => $r){
      $a_plan = ["Plan"];$a_real = ["Real"];$a_label = ["label"];
      // dd($r);
      $totallop = $r->Baris;
      $tematik = $r->mpromise_project_id;
      $plan = self::mitratelBobotWeekPlan($tematik);
      $real = self::mitratelBobotWeekReal($tematik,$mitra,$tl);
      $start =$plan['awal'];
      if($real['awal']){
        $start =min($plan['awal'],$real['awal']);
      }
      $akhir =max($plan['akhir'],$real['akhir']);
      $data = $data2=null;
      $planbbt = $realbbt = $lastbobot = 0;
      for($i = $start; $i <= $akhir; $i++){
          $realbbt = null;
          if(array_key_exists($i,$plan['data'])){
            $planbbt = $plan['data'][$i];
          }
          if(array_key_exists($i,$real['data'])){
            $realbbt = $real['data'][$i]/$totallop;
            $lastbobot = $real['data'][$i]/$totallop;
          }

          $data[] = (object)["plan"=>$planbbt,"real"=>$realbbt,"tgl"=>$i];
          $a_plan[] = $planbbt;
          $a_real[] = $realbbt;
          $a_label[] = $i;
      }
      // dd($a_real);
      $bobotarray[$tematik]['kurva'] = [$a_label,$a_plan,$a_real];
      $bobotarray[$tematik]['nama']=$r->nama_project;
      $bobotarray[$tematik]['lastbobot']=round($lastbobot,2);
      $bobotarray[$tematik]['data']=$data;
    }
    // dd($bobotarray);
    return $bobotarray;

  }
  public static function getMitratelTematik($mitra,$tl){
    $sql = "DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL'";
    if($tl!='all'){
      $sql .= " and tl='".$tl."'";
    }
    $data = DB::select("SELECT COUNT(*) AS `Baris`, `mpromise_project_id`,nama_project FROM mpromise_lop ml where ".$sql." GROUP BY `mpromise_project_id` ORDER BY `mpromise_project_id`");
    return $data;
  }

  public static function mitratelBobotWeekPlan($tematik)
  {
    $data = DB::table('mpromise_mitratel_plan as mpl')
    ->where('wo','!=',0)->orderBy('week','asc');
    if($tematik){
      $data->where('wo',$tematik);
    }
    $plan = $data->get();
    // dd($plan);
    $bobot = ["awal"=>null,"akhir"=>null,"data"=>[]];
    $sumbobot = 0;
    $tgl = null;
    foreach($plan as $key => $p){
      if(!$key){
        $bobot['awal'] = $p->week;
      }
      
      $sumbobot = $p->persen;
      $bobot['data'][$p->week] = $sumbobot;
      $tgl = $p->week;
    }
    $bobot['akhir'] = $tgl;
    // dd($bobot,$tgl);
    return $bobot;
  }
  public static function getMatrikMtel($mitra,$tl,$wo)
  {
    $q="DROPLOP=0 and isHapus=0 and jenis_pekerjaan ='MITRATEL'";
    if($mitra!= 'all'){
      $q .= " and mitra='".$mitra."'"; 
    }
    if($wo!= 'all'){
      $q .= " and mpromise_project_id='".$wo."'"; 
    }
    if($tl!= 'all'){
      $q .= " and tl='".$tl."'"; 
    }
    // if($witel!= 'all'){
    //   $q .= " and WITEL='".$witel."'"; 
    // }
    return DB::select("SELECT COUNT(*) AS `BARIS`, `ring`, nama_project as WO,
      SUM(case when Done_Aanwijzing='' then 1 else 0 end) as AANWIJZING, 
      SUM(case when Done_Aanwijzing!='' and Done_Pengiriman_Material='' then 1 else 0 end) as MATDEV,
      SUM(case when Done_Pengiriman_Material!='' and Done_Tanam_Tiang='' then 1 else 0 end) as TANAM_TIANG, 
      SUM(case when Done_Tanam_Tiang!='' and Done_Gelar_Kabel='' then 1 else 0 end) as GELAR_KABEL, 
      SUM(case when Done_Gelar_Kabel!='' and Done_Pasang_Terminal='' then 1 else 0 end) as PASANG_TERMINAL, 
      SUM(case when Done_Pasang_Terminal!='' and Done_Terminasi='' then 1 else 0 end) as TERMINASI, 
      SUM(case when Done_Terminasi!='' and ba_ct='' then 1 else 0 end) as TEST_COMM,  
      SUM(case when ba_ct!='' and TGL_GO_LIVE='' then 1 else 0 end) as RFS,  
      SUM(case when TGL_GO_LIVE!='' then 1 else 0 end) as FINISH
      FROM `mpromise_lop` where  ".$q." GROUP BY `ring`");
  }
  public static function getMatrikDetilMtel($col,$row)
  {
    $q="ring='$row'";
    if($col=='ALL'){
      $q.= "";
    }else if($col=='AANWIJZING'){
      $q.= " and Done_Aanwijzing=''";
    }else if($col=='MATDEV'){
      $q.= " and Done_Aanwijzing!='' and Done_Pengiriman_Material=''";
    }else if($col=='TANAM_TIANG'){
      $q.= " and Done_Pengiriman_Material!='' and Done_Tanam_Tiang=''";
    }else if($col=='GELAR_KABEL'){
      $q.= " and Done_Tanam_Tiang!='' and Done_Gelar_Kabel=''";
    }else if($col=='PASANG_TERMINAL'){
      $q.= " and Done_Gelar_Kabel!='' and Done_Pasang_Terminal=''";
    }else if($col=='TERMINASI'){
      $q.= " and Done_Pasang_Terminal!='' and Done_Terminasi=''";
    }else if($col=='TEST_COMM'){
      $q.= " and Done_Terminasi!='' and ba_ct=''";
    }else if($col=='RFS'){
      $q.= " and ba_ct!='' and TGL_GO_LIVE=''";
    }else if($col=='FINISH'){
      $q.= " and TGL_GO_LIVE!=''";
    }
    return DB::select("SELECT * FROM `mpromise_lop` where DROPLOP=0 and isHapus=0 and jenis_pekerjaan ='MITRATEL' and ".$q);
  }
  public static function getMaterial($mitra,$tl){
    // dd($mitra);
    $sql = "DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL'";
    if($mitra!='all'){
      $sql .= " and ml.mitra='".$mitra."'";
    }
    if($tl!='all'){
      $sql .= " and tl='".$tl."'";
    }
    return DB::select("select *,(ODC_REAL*100/ODC_MOS) as ODC_ACH,
      (ODP_REAL*100/ODP_MOS) as ODP_ACH,
      (KABEL_REAL*100/KABEL_MOS) as KABEL_ACH,
      (TIANG_REAL*100/TIANG_MOS) as TIANG_ACH,
      ((ODC_REAL*100/ODC_MOS)+(ODP_REAL*100/ODP_MOS)+(KABEL_REAL*100/KABEL_MOS)+(TIANG_REAL*100/TIANG_MOS))/4 as ACH_HI,
      ((ODC_REAL_HM1*100/ODC_PLAN)+(ODP_REAL_HM1*100/ODP_MOS)+(KABEL_REAL_HM1*100/KABEL_MOS)+(TIANG_REAL_HM1*100/TIANG_MOS))/4 as ACH_HM1,tgl_submit_permit_a,tgl_submit_permit_b,tgl_permit_a,tgl_permit_b
from (SELECT lop_id,ml.nama_project as WO,ml.nama_lop as SPAN,ml.ring as RING,ml.mpromise_project_id,mw.label as waspang_nm,mt.label as tl,mm.label as mitra,ms.step,tgl_submit_permit_a,tgl_submit_permit_b,tgl_permit_a,tgl_permit_b,
    SUM(case when designator like 'odc-%' then qty else 0 end) as ODC_PLAN,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then qty else 0 end) as ODP_PLAN,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then qty else 0 end) as KABEL_PLAN,
    SUM(case when designator like 'pu-s%' then qty else 0 end) as TIANG_PLAN
    FROM mpromise_hld_item hld 
    left join mpromise_lop ml on hld.lop_id=ml.id 
    left join mpromise_mitra mm on ml.mitra=mm.id
    left join mpromise_tl mt on ml.tl=mt.nik
    left join mpromise_waspang mw on ml.waspang=mw.nik
    left join mpromise_step ms on ml.step_id=ms.id
    WHERE ".$sql." GROUP BY lop_id ORDER BY lop_id) plan
left join
(SELECT lop_id,
    SUM(case when id_barang like 'odc-%' then qty else 0 end) as ODC_REAL,
    SUM(case when id_barang like 'odp-%' or id_barang like 'tc-sm%' then qty else 0 end) as ODP_REAL,
    SUM(case when id_barang like 'ac-of-sm%' or id_barang like 'dc-of-sm%' then qty else 0 end) as KABEL_REAL,
    SUM(case when id_barang like 'pu-s%' then qty else 0 end) as TIANG_REAL
    FROM mpromise_material_instalasi mmi left join mpromise_lop ml on mmi.lop_id=ml.id WHERE DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL' GROUP BY lop_id ORDER BY lop_id) mtr on plan.lop_id=mtr.lop_id
left join
(SELECT lop_id,
    SUM(case when designator like 'odc-%' then qty else 0 end) as ODC_MOS,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then qty else 0 end) as ODP_MOS,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then qty else 0 end) as KABEL_MOS,
    SUM(case when designator like 'pu-s%' then qty else 0 end) as TIANG_MOS
    FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id left join mpromise_lop ml on d.lop_id=ml.id WHERE DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL' GROUP BY lop_id ORDER BY lop_id) mos on plan.lop_id=mos.lop_id
left join
(SELECT lop_id,
    SUM(case when id_barang like 'odc-%' then qty else 0 end) as ODC_REAL_HM1,
    SUM(case when id_barang like 'odp-%' or id_barang like 'tc-sm%' then qty else 0 end) as ODP_REAL_HM1,
    SUM(case when id_barang like 'ac-of-sm%' or id_barang like 'dc-of-sm%' then qty else 0 end) as KABEL_REAL_HM1,
    SUM(case when id_barang like 'pu-s%' then qty else 0 end) as TIANG_REAL_HM1
    FROM mpromise_material_instalasi mi left join mpromise_lop ml on mi.lop_id=ml.id WHERE DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL' and create_at < '".date('Y-m-d')."%' GROUP BY lop_id ORDER BY lop_id) realm1 on plan.lop_id=realm1.lop_id");
  }
  public static function getMaterialByTematik($tematik){
    // dd($mitra);
    $sql = "DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL'";
    if($tematik!='all'){
      $sql .= " and ml.mpromise_project_id='".$tematik."'";
    }
    return DB::select("select *,(ODC_REAL*100/ODC_MOS) as ODC_ACH,
      (ODP_REAL*100/ODP_MOS) as ODP_ACH,
      (KABEL_REAL*100/KABEL_MOS) as KABEL_ACH,
      (TIANG_REAL*100/TIANG_MOS) as TIANG_ACH,
      ((ODC_REAL*100/ODC_MOS)+(ODP_REAL*100/ODP_MOS)+(KABEL_REAL*100/KABEL_MOS)+(TIANG_REAL*100/TIANG_MOS))/4 as ACH_HI,
      ((ODC_REAL_HM1*100/ODC_PLAN)+(ODP_REAL_HM1*100/ODP_MOS)+(KABEL_REAL_HM1*100/KABEL_MOS)+(TIANG_REAL_HM1*100/TIANG_MOS))/4 as ACH_HM1
from (SELECT lop_id,ml.nama_project as WO,ml.nama_lop as SPAN,ml.ring as RING,ml.mpromise_project_id,mw.label as waspang_nm,mt.label as tl,mm.label as mitra,ms.step,
    SUM(case when designator like 'odc-%' then qty else 0 end) as ODC_PLAN,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then qty else 0 end) as ODP_PLAN,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then qty else 0 end) as KABEL_PLAN,
    SUM(case when designator like 'pu-s%' then qty else 0 end) as TIANG_PLAN
    FROM mpromise_hld_item hld 
    left join mpromise_lop ml on hld.lop_id=ml.id 
    left join mpromise_mitra mm on ml.mitra=mm.id
    left join mpromise_tl mt on ml.tl=mt.nik
    left join mpromise_waspang mw on ml.waspang=mw.nik
    left join mpromise_step ms on ml.step_id=ms.id
    WHERE ".$sql." GROUP BY lop_id ORDER BY lop_id) plan
left join
(SELECT lop_id,
    SUM(case when id_barang like 'odc-%' then qty else 0 end) as ODC_REAL,
    SUM(case when id_barang like 'odp-%' or id_barang like 'tc-sm%' then qty else 0 end) as ODP_REAL,
    SUM(case when id_barang like 'ac-of-sm%' or id_barang like 'dc-of-sm%' then qty else 0 end) as KABEL_REAL,
    SUM(case when id_barang like 'pu-s%' then qty else 0 end) as TIANG_REAL
    FROM mpromise_material_instalasi mmi left join mpromise_lop ml on mmi.lop_id=ml.id WHERE DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL' GROUP BY lop_id ORDER BY lop_id) mtr on plan.lop_id=mtr.lop_id
left join
(SELECT lop_id,
    SUM(case when designator like 'odc-%' then qty else 0 end) as ODC_MOS,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then qty else 0 end) as ODP_MOS,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then qty else 0 end) as KABEL_MOS,
    SUM(case when designator like 'pu-s%' then qty else 0 end) as TIANG_MOS
    FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id left join mpromise_lop ml on d.lop_id=ml.id WHERE DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL' GROUP BY lop_id ORDER BY lop_id) mos on plan.lop_id=mos.lop_id
left join
(SELECT lop_id,
    SUM(case when id_barang like 'odc-%' then qty else 0 end) as ODC_REAL_HM1,
    SUM(case when id_barang like 'odp-%' or id_barang like 'tc-sm%' then qty else 0 end) as ODP_REAL_HM1,
    SUM(case when id_barang like 'ac-of-sm%' or id_barang like 'dc-of-sm%' then qty else 0 end) as KABEL_REAL_HM1,
    SUM(case when id_barang like 'pu-s%' then qty else 0 end) as TIANG_REAL_HM1
    FROM mpromise_material_instalasi mi left join mpromise_lop ml on mi.lop_id=ml.id WHERE DROPLOP=0 and isHapus=0 and jenis_pekerjaan='MITRATEL' and create_at < '".date('Y-m-d')."%' GROUP BY lop_id ORDER BY lop_id) realm1 on plan.lop_id=realm1.lop_id");
  }
  
}