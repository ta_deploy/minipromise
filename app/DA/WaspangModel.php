<?php

namespace App\DA;

use App\DA\LopModel;
use Session;
use Illuminate\Support\Facades\DB;

class WaspangModel
{
  const TABLE = 'mpromise_lop';
  //tes
  public static function insertwaspang($param)
  {
    DB::table('mpromise_waspang')->insert($param);
  }

  public static function updatewaspang($id, $param)
  {
    DB::table('mpromise_waspang')->where('id', $id)->update($param);
  }

  // public static function search_waspang($data)
  // {
  //   return DB::table('user As u')
  //   ->LeftJoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
  //   ->select('emp.nik', 'emp.nama')
  //   ->where('u.promise_level', 1)
  //   ->where('u.id_user', 'LIKE', "$data%")
  //   ->get();
  // }
  public static function getTLByWitel($witel)
  {
    return DB::select("SELECT nik_tl as id,nama_tl as text FROM `mpromise_waspang` where waspang_witel='KALSEL' and nik_tl !=0 GROUP BY `nik_tl` ORDER BY `nik_tl`");
  }
  public static function waspang_list_by_witel($witel)
  {
    return DB::table('mpromise_waspang')->where('waspang_witel', $witel)->where('status', 1)->get();
  }
  public static function getWaspang()
  {
    return DB::table('mpromise_waspang')->select('nik as id', 'nama as text')->get();
  }

  public static function getWaspangByIDs($id)
  {
    return DB::table('mpromise_waspang')->where('id', $id)->first();
  }
  public static function getWaspangByID($id)
  {
    return DB::table('mpromise_waspang')->where('nik', $id)->first();
  }
  public static function saveProgressAanwijzing($tematik,$waspang,$mitra,$lops)
  {
      DB::table(self::TABLE)
      ->where('mpromise_project_id', $tematik)
      ->where('waspang', $waspang)
      ->where('mitra', $mitra)
      ->update($lops);
  }
  public static function saveSelesaiAanwijzing($tematik,$waspang,$mitra,$lops)
  {
      DB::table(self::TABLE)
      ->where('step_id', 6)
      ->where('mpromise_project_id', $tematik)
      ->where('waspang', $waspang)
      ->where('mitra', $mitra)
      ->update($lops);
  }

  public static function saveProgress($id, $save)
  {
      DB::table(self::TABLE)->where('id', $id)->update($save);
  }
  public static function getAlista($id)
  {
    return DB::table('mpromise_mtr_alista')->where('lop_id', $id)->get();
  }
  public static function getMtr($id, $idp)
  {
    // $data = self::getAlista($id);
    // $in = [];
    // foreach($data as $d){
    //   $in[] = $d->alista_id;
    // }
    // return DB::table('alista_material_keluar')->where('project', 'like', '%'.$id.'%')->get();
    return DB::select('SELECT *,
    (select sum(qty) FROM mpromise_boq WHERE lop_id=' . $id . ' AND designator=mb.designator AND status=1) as boq_ondesk,
    (SELECT sum(qty) FROM mpromise_boq WHERE lop_id=' . $id . ' AND designator=mb.designator AND status=2) as boq_unwijing,
    (SELECT qty FROM mpromise_material_instalasi mmi WHERE mmi.lop_id = ' . $id . ' AND mmi.id_barang = mb.designator) as terpasang,
    (SELECT sum(jumlah) FROM alista_material_keluar amk WHERE project LIKE "%' . $idp . '%" AND id_barang=mb.designator) as alista_keluar,
    (SELECT sum(jumlah) FROM alista_material_kembali ak WHERE Project like "%' . $idp . '%" AND ID_Barang=mb.designator) as alista_kembali FROM mpromise_boq mb
    WHERE lop_id=' . $id);
  }
  public static function getMtrDistinct($id)
  {
    // $data = LopModel::getById($id);
    return DB::table('mpromise_boq as mb')
    ->select(
      DB::raw(
      "designator as id_barang,
      sum(qty) as avail,
      0 as qty,
      designator as nama_barang,
      $id as lop_id,
      COALESCE((SELECT qty FROM mpromise_material_instalasi WHERE id_barang = mb.designator and lop_id= $id),0) as terpasang")
    )
    ->where('lop_id', $id)
    ->groupBy('designator')
    ->orderBy('avail', 'DESC')
    ->orderBy('designator', 'ASC')
    ->get();
  }
  public static function saveMaterialIns($insert,$id)
  {
    $item = array();
    foreach($insert as $i){
      if($i->value){
        $item[] = ["id_barang"=>$i->name,"qty"=>$i->value,"lop_id"=>$id];
      }
    }
    if(count($item)){
      DB::table('mpromise_material_instalasi')->insert($item);
    }
    // DB::transaction(function () use ($insert) {
    //   foreach ($insert as $i) {
    //     $data = DB::table('mpromise_material_instalasi')->where('id_barang', $i->id_barang)->where('lop_id', $i->lop_id)->first();
    //     if ($data) {
    //       DB::table('mpromise_material_instalasi')->where('id', $data->id)->increment('qty', $i->qty);
    //       DB::table('mpromise_material_instalasi')->where('id', $data->id)->update(['avail' => $i->avail]);
    //     } else {
    //       DB::table('mpromise_material_instalasi')->insert(['id_barang' => $i->id_barang, 'nama_barang' => $i->nama_barang, 'qty' => $i->qty, 'lop_id' => $i->lop_id, 'avail' => $i->avail]);
    //     }
    //   }
    // });
  }
  public static function getMtrIns($id)
  {
    return DB::table('mpromise_material_instalasi')->where('lop_id', $id)->get();
  }
  public static function saveMaterialInsLog($insert)
  {
    DB::transaction(function () use ($insert) {
      DB::table('mpromise_material_instalasi_log')->insert($insert);
    });
  }

  public static function saveAlista($insert)
  {
    DB::transaction(function () use ($insert) {
      DB::table('mpromise_mtr_alista')->insert($insert);
    });
  }
  public static function deleteAlista($id)
  {
    DB::transaction(function () use ($id) {
      DB::table('mpromise_mtr_alista')->where('id', $id)->delete();
    });
  }
  public static function insertcore($id, $input)
  {
    DB::transaction(function () use ($id, $input) {
      DB::table('mpromise_core')->where('LOP_ID', $id)->delete();
      DB::table('mpromise_core')->insert($input);

      DB::SELECT("DELETE prj, pm
        FROM mpromise_core mc
        LEFT JOIN mpromise_lop ml ON ml.id = mc.LOP_ID
        LEFT JOIN pt2_master pm ON pm.id_bts = mc.ID_Sys
        LEFT JOIN pt2_ready_jointer prj ON prj.id_wo = mc.ID_Sys AND prj.jenis = 'bts'
        WHERE ml.id = '$id' AND pm.lt_status IS NULL AND (prj.id IS NOT NULL OR pm.id IS NOT NULL)");
    });
  }
  public static function manualsetcore($req)
  {
    // dd($req);
    if($req->ID_Sys){
      DB::table('mpromise_core')->where('ID_Sys', $req->ID_Sys)->update([$req->kolom => $req->value]);
    }
    return $req->newValue;
  }
  public static function insertboq($id, $input)
  {
    DB::table('mpromise_boq')->where('lop_id', $id)->delete();
    DB::table('mpromise_boq')->insert($input);
  }
  public static function getBoqByLop($id)
  {
      return DB::select('SELECT *,(select sum(mmi.qty) from mpromise_material_instalasi mmi where mmi.id_barang=mb.designator and mb.lop_id=mmi.lop_id) as inst,
        (select sum(mdi.qty) from mpromise_material_delivery_item mdi left join mpromise_material_delivery md on mdi.deliv_id=md.id where mdi.designator=mb.designator and mb.lop_id=md.lop_id) as deliv 
        FROM mpromise_hld_item mb WHERE mb.lop_id = '.$id);
  }
  public static function updatecore($input, $odp)
  {
    DB::table('mpromise_core')->where('Plan_Odp_Nama', $odp)->update($input);
    // $exists = DB::table('mpromise_core')->where('Plan_Odp_Nama', $odp)->first();
    // if($exists){
    // }else{
    //   DB::table('mpromise_core')->insert($input);
    // }
  }

  public static function show_berkas($id)
  {
     return DB::table(self::TABLE)
      ->leftjoin('mpromise_project', self::TABLE.'.mpromise_project_id', '=', 'mpromise_project.id')
      ->select('mpromise_project.nama', self::TABLE.'.*')
      ->where(self::TABLE.'.id', $id)
      ->first();
  }

  public static function saveplanwaspang($auth, $req)
  {
    DB::transaction(function () use ($auth, $req) {
      $iteminsert = [];
      $item = json_decode($req->item);
      if(count($item)){
          $id = DB::table('mpromise_planwaspang')->insertGetId([
              "waspang"       =>$auth->id_user,
              "waspang_nama"  =>$auth->nama,
              "lop_id"        =>$req->id
          ]);
          foreach($item as $i){
              $iteminsert[] = [
                  "designator"        => $i->designator,
                  "plan"              => $i->plan,
                  "planwaspang_id"    => $id,
                  "lop_id"            => $req->id,
                  "waspang"           => $auth->id_user
              ];
          }
          DB::table('mpromise_planwaspang_item')->insert($iteminsert);
      }
    });
  }

  public static function get_jointer()
  {
    return DB::table('user As u')
    ->Leftjoin('1_2_employee As emp', 'u.id_user', '=', 'emp.nik')
    ->select('u.id_user', 'emp.nama')
    ->where('is_jointer', 1)
    ->whereNotNull('emp.mitra_amija')
    ->get();
  }

  public static function get_all_tenor($id)
  {
    return DB::table('mpromise_lop As ml')
    ->Leftjoin('mpromise_core As mc', 'mc.LOP_ID', '=', 'ml.id')
    ->select('mc.ID_Sys As id', 'ml.sto', 'ml.created_at', 'mc.Plan_Odp_Nama As nama_odp', DB::RAW("CONCAT(mc.Plan_Odp_LAT,',',mc.Plan_Odp_LON) As koordinat"), 'ml.nama_lop As nama_order')
    ->where('ml.id', $id)
    ->get();
  }

  public static function load_data($id)
  {
    return DB::table('mpromise_lop As ml')
    ->Leftjoin('mpromise_core As mc', 'mc.LOP_ID', '=', 'ml.id')
    ->leftjoin('pt2_ready_jointer As prj', function($join){
			$join->On('prj.id_wo', '=', 'mc.ID_Sys')
			->Where('prj.jenis', '=', 'bts');
		})
    ->leftjoin('pt2_master As pm', 'mc.ID_Sys', '=', 'pm.id_bts')
    ->select('mc.ID_Sys As id', 'ml.sto', 'ml.created_at', 'mc.Plan_Odp_Nama As nama_odp', DB::RAW("CONCAT(mc.Plan_Odp_LAT,',',mc.Plan_Odp_LON) As koordinat"), 'ml.nama_lop As nama_order', 'pm.nik1')
    ->where('ml.id', $id)
    ->Where(function($join){
      $join->WhereNotNull('prj.id')
      ->OrWhereNull('pm.lt_status')
      ->WhereNotNull('pm.id');
    })
    ->GroupBy('mc.ID_Sys')
    ->get();
  }

  public static function get_info_odc($odc = 'all',  $sto = 'all', $datel= 'all')
	{
		$curl = curl_init();

		$post = [
			'datel' => $datel,
			'sto'   => $sto,
			'odc'   => $odc,
		];

		curl_setopt_array($curl, [
			CURLOPT_URL            => 'https://promitos.tomman.app/API_ODC',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_POSTFIELDS     => $post,
			CURLOPT_ENCODING       => '',
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => 'POST',
		]);

		$response = curl_exec($curl);
		curl_close($curl);

		$data = json_decode($response);

		return $data;
	}
}
