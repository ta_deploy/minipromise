<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class RegModel
{
  public static function getPlanList()
  {
    return DB::select('SELECT `WO` as `id`,`WO` as `text` FROM `mpromise_mitratel_master` GROUP BY `WO` ORDER BY `WO`');
  }
  public static function getPlanByWO($wo)
  {
    return DB::table('mpromise_mitratel_plan')->where('WO',$wo)->get();
  }
  public static function insertOrUpdatePlan($req,$wo)
  {
    $cek = DB::table('mpromise_mitratel_plan')->where('bln',$req->bulan)->where('week',$req->week)->where('wo',$wo)->first();
    if($cek){
      //to do update
      DB::Table('mpromise_mitratel_plan')->where('ID',$cek->id)->update(['week'=>$req->week,'persen'=>$req->persen]);
    }else{
      DB::table('mpromise_mitratel_plan')->insert(['week'=>$req->week,'persen'=>$req->persen,'wo'=>$wo]);
    }
    
  }
  public static function deletePlan($id)
  {
    DB::table('mpromise_mitratel_plan')->where('id', $id)->delete();
  }

  public static function getAll()
  {
    return DB::Table('mpromise_mitratel_master')->get();
  }
  public static function exists($array)
  {
    return DB::Table('mpromise_mitratel_master')->where('WO',$array['WO'])->where('RING',$array['RING'])->where('ID_LOKASI',$array['ID_LOKASI'])->first();
  }
  public static function insertOrUpdate($data)
  {
    $insert = array();
    foreach($data as $d){
      $cek = self::exists($d);
      if($cek){
        //to do update
        DB::Table('mpromise_mitratel_master')->where('ID',$cek->ID)->update($d);
      }else{
        //add to insert cart
        $insert[] = $d;
      }
    }
    DB::table('mpromise_mitratel_master')->insert($insert);
  }
  public static function getMatrik($witel,$wo)
  {
    $q="";
    if($wo!= 'all'){
      $q .= " and WO='".$wo."'"; 
    }
    if($witel!= 'all'){
      $q .= " and WITEL='".$witel."'"; 
    }
    return DB::select("SELECT COUNT(*) AS `BARIS`, `RING`, `WITEL`,`WO`,
      SUM(case when AANWIJZING='' then 1 else 0 end) as AANWIJZING, 
      SUM(case when AANWIJZING!='' and PERIZINAN='' then 1 else 0 end) as PERIZINAN, 
      SUM(case when PERIZINAN!='' and DELIVERY_MATERIAL='' then 1 else 0 end) as DELIVERY_MATERIAL, 
      SUM(case when DELIVERY_MATERIAL!='' and TANAM_TIANG='' then 1 else 0 end) as TANAM_TIANG, 
      SUM(case when TANAM_TIANG!='' and GELAR_KABEL='' then 1 else 0 end) as GELAR_KABEL, 
      SUM(case when GELAR_KABEL!='' and PASANG_TERMINAL='' then 1 else 0 end) as PASANG_TERMINAL, 
      SUM(case when PASANG_TERMINAL!='' and TERMINASI='' then 1 else 0 end) as TERMINASI, 
      SUM(case when TERMINASI!='' and TEST_COMM='' then 1 else 0 end) as TEST_COMM, 
      SUM(case when TEST_COMM!='' and ACC_TEST='' then 1 else 0 end) as ACC_TEST, 
      SUM(case when ACC_TEST!='' and RFS='' then 1 else 0 end) as RFS, 
      SUM(case when RFS!='' and UT='' then 1 else 0 end) as UT, 
      SUM(case when UT!='' and REKON='' then 1 else 0 end) as REKON, 
      SUM(case when REKON!='' and BAST='' then 1 else 0 end) as BAST, 
      SUM(case when BAST!='' then 1 else 0 end) as FINISH
      FROM `mpromise_mitratel_master` where ID !='' ".$q." GROUP BY `RING` ORDER BY `WITEL`");

  }
  public static function getMatrikDetil($col,$row)
  {
    $q="RING='$row'";
    if($col=='ALL'){
      $q.= "";
    }else if($col=='AANWIJZING'){
      $q.= " and AANWIJZING=''";
    }else if($col=='PERIZINAN'){
      $q.= " and AANWIJZING!='' and PERIZINAN=''";
    }else if($col=='DELIVERY_MATERIAL'){
      $q.= " and PERIZINAN!='' and DELIVERY_MATERIAL=''";
    }else if($col=='TANAM_TIANG'){
      $q.= " and DELIVERY_MATERIAL!='' and TANAM_TIANG=''";
    }else if($col=='GELAR_KABEL'){
      $q.= " and TANAM_TIANG!='' and GELAR_KABEL=''";
    }else if($col=='PASANG_TERMINAL'){
      $q.= " and GELAR_KABEL!='' and PASANG_TERMINAL=''";
    }else if($col=='TERMINASI'){
      $q.= " and PASANG_TERMINAL!='' and TERMINASI=''";
    }else if($col=='TEST_COMM'){
      $q.= " and TERMINASI!='' and TEST_COMM=''";
    }else if($col=='ACC_TEST'){
      $q.= " and TEST_COMM!='' and ACC_TEST=''";
    }else if($col=='RFS'){
      $q.= " and ACC_TEST!='' and RFS=''";
    }else if($col=='UT'){
      $q.= " and RFS!='' and UT=''";
    }else if($col=='REKON'){
      $q.= " and REKON!='' and BAST=''";
    }else if($col=='BAST'){
      $q.= " and REKON!='' and BAST=''";
    }else if($col=='FINISH'){
      $q.= " and BAST!=''";
    }
    return DB::select("SELECT * FROM `mpromise_mitratel_master` where ".$q);
  }
  public static function getWitelSelect()
  {
    return DB::select('SELECT `WITEL` as `id`,`WITEL` as `text` FROM `mpromise_mitratel_master` GROUP BY `WITEL` ORDER BY `WITEL`');
  }
  public static function getWoSelect($witel)
  {
    $q="";
    if($witel!='all'){
      $q.=" and WITEL='".$witel."'";
    }
    return DB::select("SELECT `WO` as `id`,`WO` as `text` FROM `mpromise_mitratel_master` where WO!='' ".$q." GROUP BY `WO` ORDER BY `WO`");
  }
  public static function getBobot($witel,$wo)
  {
    $sql = 'WITEL != ""';
    if($witel!='all'){
      $sql .= " and WITEL='".$witel."'";
    }
    if($wo!='all'){
      $sql .= " and WO='".$wo."'";
    }
    $wo = DB::select("SELECT COUNT(*) AS `Baris`, `WO` FROM `mpromise_mitratel_master` where ".$sql." GROUP BY `WO` ORDER BY `WO`");
    $data=$lastprogress=[];
    foreach($wo as $w){
      $label = array();
      $lastprogress[$w->WO]=0;
      $array=[];
      $query=DB::table('mpromise_mitratel_master')->where('WO',$w->WO)->get();
      $div = count($query);
      foreach($query as $q){
        $mw=self::getWeekNo($q->AANWIJZING);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->PERIZINAN);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->DELIVERY_MATERIAL);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->MATERIAL_ONSITE);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->TANAM_TIANG);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=20/$div:$array[$mw]=20/$div;
        }
        $mw=self::getWeekNo($q->GELAR_KABEL);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=20/$div:$array[$mw]=20/$div;
        }
        $mw=self::getWeekNo($q->TERMINASI);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=20/$div:$array[$mw]=20/$div;
        }
        $mw=self::getWeekNo($q->RFS);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->UT);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->REKON);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
        $mw=self::getWeekNo($q->BAST);
        if($mw){  
        in_array($mw, $label)?:$label[]=$mw;
        array_key_exists($mw, $array)?$array[$mw]+=5/$div:$array[$mw]=5/$div;
        }
      }
      $plan=array();
      $query=DB::table('mpromise_mitratel_plan')->where('wo',$w->WO)->orderBy('bln','asc')->orderBy('week','asc')->get();
      foreach($query as $q){
        $mw =$q->bln.'-'.$q->week; 
        in_array($mw, $label)?:$label[]=$mw;
        $plan[$mw]=$q->persen;
      }
      sort($label);
      $dataplan=$datareal=[];
      $persenreal=0;
      $persenplan=0;
      // dd($label,$plan,$array);
      foreach($label as $l){
        if(array_key_exists($l, $plan)){
          $dataplan[]=$plan[$l];
          $persenplan=$plan[$l];
        }else{
          $dataplan[]=$persenplan;
        }
        if(array_key_exists($l, $array)){
          $persenreal+=$array[$l];
          $datareal[]=$persenreal;
          $lastprogress[$w->WO]=$persenreal;
        }else{
          if($persenreal>99)
            $datareal[]=null;
          else{
            $datareal[]=$persenreal;
          }
        }
      }
      $data[$w->WO]=[array_merge(['label'],$label),array_merge(['Plan'],$dataplan),array_merge(['Real'],$datareal)];
      // dd($datareal,$dataplan,$plan,array_merge(['label'],$label),$array);
    }
    // dd($lastprogress);
      return (["data"=>$data,"lastprogress"=>$lastprogress]);
    
  }
  private static function getWeekNo($date){
    if(strlen($date)){

      $Date = explode("-",$date);
      $WeekNo = $Date[2] / 7; // devide it with 7
      if(is_float($WeekNo) == true)
      {
         $WeekNo = ceil($WeekNo); //So answer will be 1
      } 
      return $Date[1].'-M'.((int)$WeekNo);
    }else{
      return false;
    }
  }
  public static function getMaterial($witel,$wo){
    $sql = 'WITEL != ""';
    if($witel!='all'){
      $sql .= " and WITEL='".$witel."'";
    }
    if($wo!='all'){
      $sql .= " and WO='".$wo."'";
    }
    return DB::select("select *,
      (ODC_REAL*100/ODC_PLAN) as ODC_ACH,
      (ODP_REAL*100/ODP_PLAN) as ODP_ACH,
      (KABEL_REAL*100/KABEL_PLAN) as KABEL_ACH,
      (TIANG_REAL*100/TIANG_PLAN) as TIANG_ACH,
      ((ODC_REAL*100/ODC_PLAN)+(ODP_REAL*100/ODP_PLAN)+(KABEL_REAL*100/KABEL_PLAN)+(TIANG_REAL*100/TIANG_PLAN))/4 as ACH_HI,
      ((ODC_REAL_HM1*100/ODC_PLAN_HM1)+(ODP_REAL_HM1*100/ODP_PLAN_HM1)+(KABEL_REAL_HM1*100/KABEL_PLAN_HM1)+(TIANG_REAL_HM1*100/TIANG_PLAN_HM1))/4 as ACH_HM1 from 
      mpromise_mitratel_master mm left join 
      (SELECT span_id_sistem,
    SUM(case when designator like 'odc-%' then plan_qty else 0 end) as ODC_PLAN,
    SUM(case when designator like 'odc-%' then real_qty else 0 end) as ODC_REAL,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then plan_qty else 0 end) as ODP_PLAN,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then real_qty else 0 end) as ODP_REAL,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then plan_qty else 0 end) as KABEL_PLAN,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then real_qty else 0 end) as KABEL_REAL,
    SUM(case when designator like 'pu-s%' then plan_qty else 0 end) as TIANG_PLAN,
    SUM(case when designator like 'pu-s%' then real_qty else 0 end) as TIANG_REAL
    FROM mpromise_mtt_mtr WHERE tgl like '".date('Y-m-d')."%' GROUP BY span_id_sistem ORDER BY span_id_sistem) mtr on mm.SPAN_ID_SISTEM=mtr.span_id_sistem 

    left join 
      (SELECT span_id_sistem,
    SUM(case when designator like 'odc-%' then plan_qty else 0 end) as ODC_PLAN_HM1,
    SUM(case when designator like 'odc-%' then real_qty else 0 end) as ODC_REAL_HM1,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then plan_qty else 0 end) as ODP_PLAN_HM1,
    SUM(case when designator like 'odp-%' or designator like 'tc-sm%' then real_qty else 0 end) as ODP_REAL_HM1,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then plan_qty else 0 end) as KABEL_PLAN_HM1,
    SUM(case when designator like 'ac-of-sm%' or designator like 'dc-of-sm%' then real_qty else 0 end) as KABEL_REAL_HM1,
    SUM(case when designator like 'pu-s%' then plan_qty else 0 end) as TIANG_PLAN_HM1,
    SUM(case when designator like 'pu-s%' then real_qty else 0 end) as TIANG_REAL_HM1
    FROM mpromise_mtt_mtr WHERE tgl like '".date('Y-m-d',strtotime("-1 days"))."%' GROUP BY span_id_sistem ORDER BY span_id_sistem) mtrhm1 on mm.SPAN_ID_SISTEM=mtrhm1.span_id_sistem 
    where ".$sql);
  }
}
