<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class IzinModel
{
  //mitra
  public static function getAll()
  {
    return DB::Table('mpromise_perizinan')->get();
  }
  public static function getById($id)
  {
    return DB::Table('mpromise_perizinan')->where('id', $id)->first();
  }
  public static function getByLopId($id)
  {
    return DB::Table('mpromise_perizinan')->where('lop_id', $id)->where('status',0)->get();
  }
  public static function insert($param)
  {
    DB::table('mpromise_perizinan')->insert($param);
  }
  public static function update($id, $param)
  {
    DB::table('mpromise_perizinan')->where('id', $id)->update($param);
  }
}
