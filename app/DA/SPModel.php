<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class SPModel
{
  public static function getAll()
  {
    return DB::Table('mpromise_sp')->select('*', DB::raw('(select count(*) from mpromise_lop where sp_id=mpromise_sp.id) as total_lop'))->get();
  }
  public static function getById($id)
  {
    return DB::Table('mpromise_sp')->where('id', $id)->first();
  }
  public static function insertOrUpdate($id, $req)
  {
    if(self::getById($id)){
      DB::table('mpromise_sp')->where('id', $id)->update(["no_sp"=> $req->no_sp]);
    }else{
      $id = DB::table('mpromise_sp')->insertGetId(["no_sp", $req->no_sp]);
    }
    return $id;
  }
}
