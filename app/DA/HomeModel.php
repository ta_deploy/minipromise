<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class HomeModel
{
  public static function getCharts($tematik,$witel,$tl,$mitra)
  {
    //init
    $waspanga = $waspangcat = $mitraa = $mitracat = $mitranama = $waspangnama = array();
    $waspangbar = $mitrabar = $piechart = [['1.Antrian',0],['2.Instalasi',0],['3.Terminasi',0],['4.Done',0]];
    $horisontalbar1 = ['value', 0, 0, 0, 0];$horisontalbar2 = ['value', 0, 0, 0];$waspangodp = ['ODP'];$mitraodp = ['ODP'];
    $waspangrealisasinilai = [['x'],['value']];

    $mitrarealisasinilai = [['x'],['value']];
    $mitrarealisasinilaiarray=$waspangrealisasinilaiarray=[];
    $donatchart = [['Plan',0],['Real',0]];
    foreach(DB::table('mpromise_waspang')->where('status',1)->where('waspang_witel',$witel)->get() as $no => $w){
        $waspangbar[0][$no+1] = 0;
        $waspangbar[1][$no+1] = 0;
        $waspangbar[2][$no+1] = 0;
        $waspangbar[3][$no+1] = 0;
        $waspangodp[$no+1] =0;
        $waspanga[$w->nik]=++$no;
        $waspangcat[]=$w->label;
        $waspangnama[$w->nik]=$w->label;
    }
    foreach(DB::table('mpromise_mitra as mm')->leftJoin(DB::raw("(SELECT COUNT(*) AS `Baris`, `mitra` FROM mpromise_lop ml left join mpromise_project mp on ml.mpromise_project_id=mp.id where mp.stts=0 GROUP BY `mitra` ORDER BY `mitra`) as mpl"),"mm.id","=","mpl.mitra")->where('mm.witel_mitra',$witel)->whereNotNull('Baris')->get() as $no => $w){
        $mitrabar[0][$no+1] = 0;
        $mitrabar[1][$no+1] = 0;
        $mitrabar[2][$no+1] = 0;
        $mitrabar[3][$no+1] = 0;
        $mitranama[$w->id]=$w->label;
        $mitraodp[$no+1] =0;
        $mitraa[$w->id]=++$no;
        $mitracat[]=$w->label;
    }

    //transformdata
    $query = DB::table('mpromise_lop as ml')
        ->select('ml.*','ml.id as lops_id','mp.*',
            DB::raw('(select count(*) from mpromise_core mb where lop_id = ml.id and Real_Odp_Nama!="") as countodpgolive'),
            DB::raw('(select sum((mb.hargajasa*mb.qty)+(mb.hargamaterial*mb.qty)) from mpromise_hld_item mb where lop_id = ml.id) as hargalop'),
            DB::raw('(SELECT sum((mb.hargajasa*mmi.qty)+(mb.hargamaterial*mmi.qty)) FROM mpromise_hld_item mb left join mpromise_material_instalasi mmi on mb.designator=mmi.id_barang and mmi.lop_id=mb.lop_id where mb.lop_id=ml.id) as rhargalop'))
        ->leftJoin('mpromise_project as mp','ml.mpromise_project_id', '=', 'mp.id')
        ->where('mpromise_project_id','!=', 0)->where('mp.stts', 0)->where('ml.isHapus', 0)->where('witel_lop', $witel)->where('ml.mitra', '!=','0');
    if($tematik){
      $query->where('mpromise_project_id',$tematik);
    }
    if($tl){
      $query->where('ml.tl',$tl);
    }
    if($mitra){
      $query->where('ml.mitra',$mitra);
    }
    $data = $query->get();
    // dd($data);
    $tidakmsk = [];
    foreach($data as $d){
        if(in_array($d->step_id,[1,6,2])){
            $waspangbar[0][$waspanga[$d->waspang]]+=1;
            if(array_key_exists($d->mitra, $mitraa) ) $mitrabar[0][$mitraa[$d->mitra]]+=1;
            $piechart[0][1] +=$d->hargalop;
            $horisontalbar1[1] += $d->hargalop;
        }
        else if($d->step_id == 4 && $d->Done_Gelar_Kabel == ''){
            $waspangbar[1][$waspanga[$d->waspang]]+=1;
            if(array_key_exists($d->mitra, $mitraa) ) $mitrabar[1][$mitraa[$d->mitra]]+=1;
            $piechart[1][1] +=$d->hargalop;
            $horisontalbar1[2] += $d->hargalop;
        }
        else if($d->step_id == 4 && $d->Done_Gelar_Kabel != ''){
            $waspangbar[2][$waspanga[$d->waspang]]+=1;
            if(array_key_exists($d->mitra, $mitraa) ) $mitrabar[2][$mitraa[$d->mitra]]+=1;
            $piechart[2][1] +=$d->hargalop;
            $horisontalbar1[3] += $d->hargalop;
        }
        else if(in_array($d->step_id,[5,7,8,9])){
            $waspangbar[3][$waspanga[$d->waspang]]+=1;
            if(array_key_exists($d->mitra, $mitraa) ) $mitrabar[3][$mitraa[$d->mitra]]+=1;
            $piechart[3][1] +=$d->hargalop;
            $horisontalbar1[4] += $d->hargalop;
            $horisontalbar2[2] +=$d->hargalop;
        }
        else{
            $tidakmsk[] = $d->lops_id;
        }
        $donatchart[0][1]+=1;
        $horisontalbar2[3] +=$d->hargalop;
        if($d->Done_Terminasi!=''){
            $donatchart[1][1]+=1;
        }
        // $waspangrealisasinilai[1][$waspanga[$d->waspang]]+=$d->rhargalop;
        if(array_key_exists($d->waspang, $waspangrealisasinilaiarray)){
            $waspangrealisasinilaiarray[$d->waspang] += $d->rhargalop;
        }else{
            $waspangrealisasinilaiarray[$d->waspang] = $d->rhargalop;
        }
        $waspangodp[$waspanga[$d->waspang]]+=$d->countodpgolive;
        if(array_key_exists($d->mitra, $mitraa) ) $mitraodp[$mitraa[$d->mitra]]+=$d->countodpgolive;

        if(array_key_exists($d->mitra, $mitrarealisasinilaiarray)){
            $mitrarealisasinilaiarray[$d->mitra] += $d->rhargalop;
        }else{
            $mitrarealisasinilaiarray[$d->mitra] = $d->rhargalop;
        }
    }

    //sort
    arsort($waspangrealisasinilaiarray);
    foreach($waspangrealisasinilaiarray as $k => $m){
        $waspangrealisasinilai[0][] =$waspangnama[$k];
        $waspangrealisasinilai[1][] =$m;
    }
    arsort($mitrarealisasinilaiarray);
    // dd($mitranama,$mitrarealisasinilaiarray);
    foreach($mitrarealisasinilaiarray as $k => $m){
        $mitrarealisasinilai[0][] =$mitranama[$k];
        $mitrarealisasinilai[1][] =$m;
    }

    $horisontalbar2[1]=$horisontalbar2[2]-$horisontalbar2[3];
    return ['waspangcat'=>$waspangcat,'mitracat'=>$mitracat,'waspangbar'=>$waspangbar,'mitrabar'=>$mitrabar,'piechart'=>$piechart,'donatchart'=>$donatchart,'horisontalbar1'=>$horisontalbar1,'horisontalbar2'=>$horisontalbar2,'waspangrealisasinilai'=>$waspangrealisasinilai,'mitrarealisasinilai'=>$mitrarealisasinilai,'waspangodp'=>$waspangodp,'mitraodp'=>$mitraodp];
  }
}
