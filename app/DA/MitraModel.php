<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MitraModel
{
  //mitra
  public static function getAll()
  {
    return DB::Table('mpromise_mitra')->select('*', 'mitra as text')->get();
  }
  public static function getById($id)
  {
    return DB::Table('mpromise_mitra')->where('id', $id)->first();
  }
  public static function getByWitel($witel)
  {
    return DB::Table('mpromise_mitra')->where('witel_mitra', $witel)->get();
  }

  public static function insert($param)
  {
    DB::table('mpromise_mitra')->insert($param);
  }
  public static function update($id, $param)
  {
    DB::table('mpromise_mitra')->where('id', $id)->update($param);
  }
  public static function listMitra()
  {
    return DB::Table('mpromise_mitra')->select('*', 'mitra as text')->get();
  }
  public static function listMitraByWitel($witel)
  {
    return DB::Table('mpromise_mitra')->select('*', 'mitra as text')->where('witel_mitra', $witel)->get();
  }
  public static function getMitraByTL($tl)
  {
    $sql = '';
    if($tl!='all'){
      $sql.=" and tl='".$tl."'";
    }
    return DB::select("SELECT ml.mitra as id,mm.label as text FROM mpromise_lop ml left join mpromise_mitra mm on ml.mitra=mm.id where jenis_pekerjaan='MITRATEL' ".$sql." GROUP BY ml.mitra ORDER BY ml.mitra");
  }
  public static function getTLByMitra($mitra)
  {
    $sql = '';
    if($mitra!='all'){
      $sql.=" and ml.mitra='".$mitra."'";
    }
    return DB::select("SELECT ml.tl as id,mm.label as text FROM mpromise_lop ml left join mpromise_tl mm on ml.tl=mm.nik where jenis_pekerjaan='MITRATEL' ".$sql." GROUP BY ml.tl ORDER BY ml.tl");
  }
}
