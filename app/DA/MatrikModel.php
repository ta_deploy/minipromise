<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

use App\DA\WaspangModel;
use App\DA\ProjectModel;

class MatrikModel
{
  const TABLE = 'mpromise_lop';
  public static function getMatrikBySTO($id = null,$tl)
  {
    $sql = ' mp.stts=0';
    if($id){
      $sql = ' ml.mpromise_project_id IN ('. $id . ')';
    }
    if(session('auth')->witel){
      $sql.= ' and ml.witel_lop="'.session('auth')->witel.'"';
    }
    if($tl!='all'){
      $sql.= ' and ml.tl="'.$tl.'"';
    }
    return DB::select('select x.id, x.sto,x.label, x.mpromise_project_id, x.nama_project,
    sum(x.lop) AS total_lop,
    sum(x.unwizing) AS unwizing,
    sum(x.delivery) AS delivery,
    sum(x.ogp_instalasi) AS ogp_instalasi,
    sum(x.finish_instalasi) AS finish_instalasi,
    sum(x.ogp_terminasi) AS ogp_terminasi,
    sum(x.finish_terminasi) AS finish_terminasi,
    sum(x.ctut) AS ctut,
    sum(x.finish) AS finish,
    sum(x.doc) AS doc,
    sum(x.lopdrop) AS lopdrop,
    sum(x.loppengganti) AS loppengganti,
    sum(x.odpplan) as odpplan,
    sum(x.odpreal) AS odpreal,
    sum(x.golive) AS golive,
    sum(x.odpdrop) AS odpdrop,
    sum(x.score_fisik) AS score_fisik,
    sum(x.score_berkas) AS score_berkas,
    ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) as prod,(sum(x.score_fisik)+sum(x.score_berkas))
      from (
        SELECT 1 as lop,ml.sto as label,ml.sto,(case when DROPLOP=0 and step_id=6 then 1 else 0 end) as unwizing,
        (case when DROPLOP=0 and step_id=2 then 1 else 0 end) as delivery,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel ="") then 1 else 0 end) as ogp_instalasi,
        (case when DROPLOP=0 and step_id in (4,5,7,8,9) and (Done_Gelar_Kabel != "") then 1 else 0 end) as finish_instalasi,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel != "") then 1 else 0 end) as ogp_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8,9) then 1 else 0 end) as finish_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8) then 1 else 0 end) as ctut,
        (case when DROPLOP=0 and step_id=9 then 1 else 0 end) as finish,
        (case when DROPLOP=0 and (TGL_GO_LIVE!="" or TGL_VALID3!="") then 1 else 0 end) as doc,
        (case when DROPLOP=1 then 1 else 0 end) as lopdrop,
        (case when DROPLOP=0 and PENGGANTI_LOP=1 then 1 else 0 end) as loppengganti,
        (case when DROPLOP=0 and Done_Terminasi!="" then (DATEDIFF(SUBSTR(timeplan_fisik, 14, 10), Done_Terminasi)+1) else 0 end) as score_fisik,
        (case when DROPLOP=0 and TGL_GO_LIVE!="" then (DATEDIFF(SUBSTR(timeplan_berkas, 14, 10), TGL_GO_LIVE)+1) else 0 end) as score_berkas,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and DROPODP=0) else 0 end) as odpplan,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and Real_Odp_Nama !="" and DROPODP=0) else 0 end) as odpreal,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and GOLIVE=1 and DROPODP=0) else 0 end) as golive,
        (select count(*) from mpromise_core mc where LOP_ID=ml.id and DROPODP=1) as odpdrop, ml.sto as id, mm.mitra, ml.mpromise_project_id, ml.nama_project
        FROM mpromise_lop ml
        left join mpromise_project mp on ml.mpromise_project_id = mp.id
        left join mpromise_mitra mm on mm.id = ml.mitra where  ml.isHapus = 0 and '.$sql.'
      ) x GROUP BY x.sto, x.nama_project ORDER BY ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) desc,(sum(x.score_fisik)+sum(x.score_berkas)) desc');
  }
  public static function getMatrikByTL($id = null,$tl)
  {
    $sql = ' mp.stts=0';
    if($id){
      $sql .= ' and mpromise_project_id IN ('.$id .')';
    }
    if(session('auth')->witel){
      $sql.= ' and ml.witel_lop="'.session('auth')->witel.'"';
    }
    if($tl!='all'){
      $sql.= ' and ml.tl="'.$tl.'"';
    }
    return DB::select('select x.id, x.label, x.tl, x.mpromise_project_id, x.nama_project,
    sum(x.lop) AS total_lop,
    sum(x.unwizing) AS unwizing,
    sum(x.delivery) AS delivery,
    sum(x.ogp_instalasi) AS ogp_instalasi,
    sum(x.finish_instalasi) AS finish_instalasi,
    sum(x.ogp_terminasi) AS ogp_terminasi,
    sum(x.finish_terminasi) AS finish_terminasi,
    sum(x.ctut) AS ctut,
    sum(x.finish) AS finish,
    sum(x.doc) AS doc,
    sum(x.lopdrop) AS lopdrop,
    sum(x.loppengganti) AS loppengganti,
    sum(x.odpplan) as odpplan,
    sum(x.odpreal) AS odpreal,
    sum(x.golive) AS golive,
    sum(x.odpdrop) AS odpdrop,
    sum(x.score_fisik) AS score_fisik,
    sum(x.score_berkas) AS score_berkas,
    ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) as prod,(sum(x.score_fisik)+sum(x.score_berkas))
      from (
        SELECT 1 as lop,mw.label as label,(case when DROPLOP=0 and step_id=6 then 1 else 0 end) as unwizing,
        (case when DROPLOP=0 and step_id=2 then 1 else 0 end) as delivery,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel ="") then 1 else 0 end) as ogp_instalasi,
        (case when DROPLOP=0 and step_id in (4,5,7,8,9) and (Done_Gelar_Kabel != "") then 1 else 0 end) as finish_instalasi,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel != "") then 1 else 0 end) as ogp_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8,9) then 1 else 0 end) as finish_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8) then 1 else 0 end) as ctut,
        (case when DROPLOP=0 and step_id=9 then 1 else 0 end) as finish,
        (case when DROPLOP=0 and (TGL_GO_LIVE!="" or TGL_VALID3!="") then 1 else 0 end) as doc,
        (case when DROPLOP=1 then 1 else 0 end) as lopdrop,
        (case when DROPLOP=0 and PENGGANTI_LOP=1 then 1 else 0 end) as loppengganti,
        (case when DROPLOP=0 and Done_Terminasi!="" then (DATEDIFF(SUBSTR(timeplan_fisik, 14, 10), Done_Terminasi)+1) else 0 end) as score_fisik,
        (case when DROPLOP=0 and TGL_GO_LIVE!="" then (DATEDIFF(SUBSTR(timeplan_berkas, 14, 10), TGL_GO_LIVE)+1) else 0 end) as score_berkas,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and DROPODP=0) else 0 end) as odpplan,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and Real_Odp_Nama !="" and DROPODP=0) else 0 end) as odpreal,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and GOLIVE=1 and DROPODP=0) else 0 end) as golive,
        (select count(*) from mpromise_core mc where LOP_ID=ml.id and DROPODP=1) as odpdrop,
        ml.tl as id, mw.label as nama_tl,ml.tl,ml.mpromise_project_id, ml.nama_project

        FROM mpromise_lop ml left join mpromise_project mp on ml.mpromise_project_id = mp.id
        left join mpromise_mitra mm on mm.id = ml.mitra
        left join mpromise_tl mw on ml.tl = mw.nik
        where ml.isHapus = 0 and '.$sql.'
      ) x GROUP BY x.id, x.nama_project ORDER BY ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) desc,(sum(x.score_fisik)+sum(x.score_berkas)) desc');
  }
  public static function getMatrikByMitra($id = null,$tl)
  {
    $sql = ' mp.stts=0';

    if($id)
    {
      $sql = ' ml.mpromise_project_id IN (' . $id . ')';
    }

    if(session('auth')->witel)
    {
      $sql.= ' and ml.witel_lop="'.session('auth')->witel.'"';
    }

    if($tl!='all')
    {
      $sql.= ' and ml.tl="'.$tl.'"';
    }

    return DB::select('select x.id, x.mitra,x.label, x.mpromise_project_id, x.nama_project,
    sum(x.lop) AS total_lop,
    sum(x.unwizing) AS unwizing,
    sum(x.delivery) AS delivery,
    sum(x.ogp_instalasi) AS ogp_instalasi,
    sum(x.finish_instalasi) AS finish_instalasi,
    sum(x.ogp_terminasi) AS ogp_terminasi,
    sum(x.finish_terminasi) AS finish_terminasi,
    sum(x.ctut) AS ctut,
    sum(x.finish) AS finish,
    sum(x.doc) AS doc,
    sum(x.lopdrop) AS lopdrop,
    sum(x.loppengganti) AS loppengganti,
    sum(x.odpplan) as odpplan,
    sum(x.odpreal) AS odpreal,
    sum(x.golive) AS golive,
    sum(x.odpdrop) AS odpdrop,
    sum(x.score_fisik) AS score_fisik,
    sum(x.score_berkas) AS score_berkas,
    ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) as prod,(sum(x.score_fisik)+sum(x.score_berkas))
      from (
        SELECT 1 as lop,mm.label as label,(case when DROPLOP=0 and step_id=6 then 1 else 0 end) as unwizing,
        (case when DROPLOP=0 and step_id=2 then 1 else 0 end) as delivery,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel ="") then 1 else 0 end) as ogp_instalasi,
        (case when DROPLOP=0 and step_id in (4,5,7,8,9) and (Done_Gelar_Kabel != "") then 1 else 0 end) as finish_instalasi,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel != "") then 1 else 0 end) as ogp_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8,9) then 1 else 0 end) as finish_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8) then 1 else 0 end) as ctut,
        (case when DROPLOP=0 and step_id=9 then 1 else 0 end) as finish,
        (case when DROPLOP=0 and (TGL_GO_LIVE!="" or TGL_VALID3!="") then 1 else 0 end) as doc,
        (case when DROPLOP=1 then 1 else 0 end) as lopdrop,
        (case when DROPLOP=0 and PENGGANTI_LOP=1 then 1 else 0 end) as loppengganti,
        (case when DROPLOP=0 and Done_Terminasi!="" then (DATEDIFF(SUBSTR(timeplan_fisik, 14, 10), Done_Terminasi)+1) else 0 end) as score_fisik,
        (case when DROPLOP=0 and TGL_GO_LIVE!="" then (DATEDIFF(SUBSTR(timeplan_berkas, 14, 10), TGL_GO_LIVE)+1) else 0 end) as score_berkas,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and DROPODP=0) else 0 end) as odpplan,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and Real_Odp_Nama !="" and DROPODP=0) else 0 end) as odpreal,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and GOLIVE=1 and DROPODP=0) else 0 end) as golive,
        (select count(*) from mpromise_core mc where LOP_ID=ml.id and DROPODP=1) as odpdrop, mm.id as id, mm.mitra, ml.mpromise_project_id, ml.nama_project
        FROM mpromise_lop ml
        left join mpromise_project mp on ml.mpromise_project_id = mp.id
        left join mpromise_mitra mm on mm.id = ml.mitra where  ml.isHapus = 0 and '.$sql.'
      ) x GROUP BY x.mitra, x.nama_project ORDER BY ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) desc,(sum(x.score_fisik)+sum(x.score_berkas)) desc');
  }
  public static function getMatrikByWaspang($id = null,$tl)
  {
    $sql = ' mp.stts=0';
    if($id){
      $sql .= ' and mpromise_project_id IN (' . $id . ')';
    }
    if(session('auth')->witel){
      $sql.= ' and ml.witel_lop="'.session('auth')->witel.'"';
    }
    if($tl!='all'){
      $sql.= ' and ml.tl="'.$tl.'"';
    }
    return DB::select('select x.id, x.waspang_nm, x.label, x.mpromise_project_id, x.nama_project,
    sum(x.lop) AS total_lop,
    sum(x.unwizing) AS unwizing,
    sum(x.delivery) AS delivery,
    sum(x.ogp_instalasi) AS ogp_instalasi,
    sum(x.finish_instalasi) AS finish_instalasi,
    sum(x.ogp_terminasi) AS ogp_terminasi,
    sum(x.finish_terminasi) AS finish_terminasi,
    sum(x.ctut) AS ctut,
    sum(x.finish) AS finish,
    sum(x.doc) AS doc,
    sum(x.lopdrop) AS lopdrop,
    sum(x.loppengganti) AS loppengganti,
    sum(x.odpplan) as odpplan,
    sum(x.odpreal) AS odpreal,
    sum(x.golive) AS golive,
    sum(x.odpdrop) AS odpdrop,
    sum(x.score_fisik) AS score_fisik,
    sum(x.score_berkas) AS score_berkas,
    ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) as prod,(sum(x.score_fisik)+sum(x.score_berkas))
      from (
        SELECT 1 as lop,mw.label as label,(case when DROPLOP=0 and step_id=6 then 1 else 0 end) as unwizing,
        (case when DROPLOP=0 and step_id=2 then 1 else 0 end) as delivery,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel ="") then 1 else 0 end) as ogp_instalasi,
        (case when DROPLOP=0 and step_id in (4,5,7,8,9) and (Done_Gelar_Kabel != "") then 1 else 0 end) as finish_instalasi,
        (case when DROPLOP=0 and step_id=4 and (Done_Gelar_Kabel != "") then 1 else 0 end) as ogp_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8,9) then 1 else 0 end) as finish_terminasi,
        (case when DROPLOP=0 and step_id in (5,7,8) then 1 else 0 end) as ctut,
        (case when DROPLOP=0 and step_id=9 then 1 else 0 end) as finish,
        (case when DROPLOP=0 and (TGL_GO_LIVE!="" or TGL_VALID3!="") then 1 else 0 end) as doc,
        (case when DROPLOP=1 then 1 else 0 end) as lopdrop,
        (case when DROPLOP=0 and PENGGANTI_LOP=1 then 1 else 0 end) as loppengganti,
        (case when DROPLOP=0 and Done_Terminasi!="" then (DATEDIFF(SUBSTR(timeplan_fisik, 14, 10), Done_Terminasi)+1) else 0 end) as score_fisik,
        (case when DROPLOP=0 and TGL_GO_LIVE!="" then (DATEDIFF(SUBSTR(timeplan_berkas, 14, 10), TGL_GO_LIVE)+1) else 0 end) as score_berkas,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and DROPODP=0) else 0 end) as odpplan,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and Real_Odp_Nama !="" and DROPODP=0) else 0 end) as odpreal,
        (case when DROPLOP=0 and jenis_pekerjaan="PT3" then (select count(*) from mpromise_core mc where LOP_ID = ml.id and GOLIVE=1 and DROPODP=0) else 0 end) as golive,
        (select count(*) from mpromise_core mc where LOP_ID=ml.id and DROPODP=1) as odpdrop,
        ml.waspang as id, waspang_nm, ml.mpromise_project_id, ml.nama_project

        FROM mpromise_lop ml left join mpromise_project mp on ml.mpromise_project_id = mp.id
        left join mpromise_mitra mm on mm.id = ml.mitra
        left join mpromise_waspang mw on ml.waspang = mw.nik
        where ml.isHapus = 0 and '.$sql.'
      ) x GROUP BY x.waspang_nm, x.nama_project ORDER BY ((case when sum(x.lop) then (sum(x.finish_instalasi)+sum(x.lopdrop))*100/sum(x.lop) else 0 end)*0.3)+
    ((case when sum(x.odpplan) then (sum(x.odpreal)+sum(x.odpdrop))*100/sum(x.odpplan) else 0 end)*0.5)+
    ((case when sum(x.doc) then sum(x.doc)*100/(sum(x.lop)-sum(x.lopdrop)) else 0 end)*0.2) desc,(sum(x.score_fisik)+sum(x.score_berkas)) desc');
  }
  public static function getDetailLop($id, $col, $row, $jenis,$tl)
  {
    $query = DB::table('mpromise_lop as ml')
      ->leftJoin('mpromise_project as mp', 'ml.mpromise_project_id', '=', 'mp.id')
      ->leftJoin('mpromise_mitra as mm', 'ml.mitra', '=', 'mm.id')
      ->leftJoin('mpromise_step as ms', 'ml.step_id', '=', 'ms.id')
      ->select('ml.*','mm.mitra as nama_mitra','mp.nama as nama_tematik', 'ms.step', 'ml.id as id_lop',DB::raw('(DATEDIFF(SUBSTR(timeplan_fisik, 14, 10), Done_Terminasi)+1) as score_fisik,(DATEDIFF(SUBSTR(timeplan_berkas, 14, 10), TGL_GO_LIVE)+1) as score_berkas'))
      ->where('mp.stts', 0)->where('ml.isHapus', 0);
    if($jenis==1){
      $query = $query->where('ml.mitra', $row);
    }elseif($jenis==2){
      $query = $query->where('ml.sto', $row);
    }elseif($jenis==3){
      $query = $query->where('ml.tl', $row);
    }else{
      $query = $query->where('ml.waspang', $row);
    }
    if($id){
      $query = $query->where('mpromise_project_id', $id);
    }

    if($tl!='all'){
      $query = $query->where('ml.tl', $tl);
    }
    if(session('auth')->witel){
      $query = $query->where('witel_lop', session('auth')->witel);
    }
    if($col == "PLAN_LOP"){
      return $query->get();
    }else if($col == "REDLINE_PREP"){
      return $query->where('ml.DROPLOP', 0)->where('ml.step_id', 6)->get();
    }else if($col == "REDLINE_MATDEV"){
      return $query->where('ml.DROPLOP', 0)->where('ml.step_id', 2)->get();
    }else if($col == "REDLINE_INSTALASI"){
      return $query->where('ml.DROPLOP', 0)->whereRaw('step_id=4 and (Done_Gelar_Kabel ="")')->get();
    }else if($col == "REDLINE_TERMINASI"){
      return $query->where('ml.DROPLOP', 0)->whereRaw('step_id=4 and (Done_Gelar_Kabel !="")')->get();
    }else if($col == "REDLINE_TESTCOMM"){
      return $query->where('ml.DROPLOP', 0)->whereRaw('step_id in (5,7,8)')->get();
    }else if($col == "REDLINE_FINISH"){
      return $query->where('ml.DROPLOP', 0)->where('step_id', 9)->get();
    }else if($col == "REAL_DROP"){
      return $query->where('DROPLOP', 1)->get();
    }else if($col == "REAL_PENGGANTI"){
      return $query->where('ml.DROPLOP', 0)->where('PENGGANTI_LOP', 1)->get();
    }else if($col == "REAL_INSTALASI"){
      return $query->where('ml.DROPLOP', 0)->whereRaw('step_id in (4,5,7,8,9) and (Done_Gelar_Kabel !="")')->get();
    }else if($col == "REAL_FINISH"){
      return $query->where('ml.DROPLOP', 0)->whereRaw('step_id in (5,7,8,9)')->get();
    }else if($col == "REAL_DOC"){
      return $query->where('ml.DROPLOP', 0)->where('TGL_GO_LIVE','!=', '')->get();
    }

  }
  public static function getDetailLopODP($id, $col, $row, $jenis,$tl)
  {
    $query = DB::table('mpromise_lop as ml')
      ->leftJoin('mpromise_project as mp', 'ml.mpromise_project_id', '=', 'mp.id')
      ->leftJoin('mpromise_mitra as mm', 'ml.mitra', '=', 'mm.id')
      ->leftJoin('mpromise_step as ms', 'ml.step_id', '=', 'ms.id')
      ->leftJoin('mpromise_core as mc', 'ml.id', '=', 'mc.LOP_ID')
      ->select('ml.*','mm.mitra as nama_mitra','mp.nama as nama_tematik', 'ms.step', 'ml.id as id_lop', 'mc.Plan_Odp_Nama', 'mc.Real_Odp_Nama', 'mc.GOLIVE')
      ->where('mp.stts', 0)->where('ml.DROPLOP', 0)->where('ml.isHapus', 0)->where('ml.jenis_pekerjaan', 'PT3');

    if($row != 'all')
    {
      if($jenis==1){
        $query = $query->where('ml.mitra', $row);
      }else if($jenis==2){
        $query = $query->where('ml.sto', $row);
      }else if($jenis==3){
        $query = $query->where('ml.tl', $row);
      }else{
        $query = $query->where('ml.waspang', $row);
    }
    }
    if($id){
      $query = $query->where('mpromise_project_id', $id);
      }
    if($id){
      $query = $query->where('mpromise_project_id', $id);
    }

    if(Count($id) || in_array(0, $id) ){
      $query = $query->whereIn('mpromise_project_id', $id);
    }

    if($tl!='all'){
      $query = $query->where('ml.tl', $tl);
    }
    if(session('auth')->witel){
      $query = $query->where('witel_lop', session('auth')->witel);
    }
    if($col == "PLAN_ODP"){
      return $query->where('DROPODP', 0)->get();
    }else if($col == "REAL_ODP"){
      return $query->where('DROPODP', 0)->whereNotNull('Real_Odp_Nama')->get();
    }else if($col == "REAL_ODP_DROP"){
      return $query->where('DROPODP', 1)->get();
    }else if($col == "REAL_ODP_GOLIVE"){
      return $query->where('DROPODP', 0)->where('GOLIVE', 1)->get();
    }

  }
}
