<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class WITELModel
{
  public static function getAll()
  {
    return DB::Table('witel')->select('nama_witel as id', 'nama_witel as text')->get();
  }
}
