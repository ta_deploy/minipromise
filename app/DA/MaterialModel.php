<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class MaterialModel
{

  public static function insertHLD($id,$item)
  {
    DB::table('mpromise_hld_item')->where('tematik_id', $id)->delete();
    DB::table('mpromise_hld_item')->insert($item);
  }
  public static function getInstalasi($id)
  {
    return DB::select('select os.lop_id,ins.qty as installed,os.qty as onsite,os.designator from 
      (SELECT lop_id,sum(qty) AS qty, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' and d.status="Material_on_Site" and qty>0 GROUP BY designator ORDER BY designator) as os
      left join (SELECT ri.lop_id,sum(qty) AS qty, id_barang FROM mpromise_material_instalasi ri
      where ri.lop_id='.$id.' GROUP BY id_barang ORDER BY id_barang) as ins
      on os.designator=ins.id_barang');
  }
  public static function getJustif($id)
  {
    return DB::select('select reserv.lop_id,reserv.res,os.qty as onsite,ji.designator,ji.qty as justif from mpromise_material_justif_item ji 
      left join (SELECT r.lop_id,sum(qty) AS res, designator FROM mpromise_material_reservasi_item ri 
      left join mpromise_material_reservasi r on ri.no_reserv=r.no_reservasi 
      left join mpromise_lop ml on r.lop_id=ml.id
      where ml.mpromise_project_id='.$id.' GROUP BY designator ORDER BY designator) as reserv 
      on ji.designator=reserv.designator
      left join
      (SELECT lop_id,sum(qty) AS qty, designator FROM mpromise_material_delivery_item di 
      left join mpromise_material_delivery d on di.deliv_id=d.id 
      left join mpromise_lop ml on d.lop_id=ml.id
      where ml.mpromise_project_id='.$id.' and d.status="Material_on_Site" GROUP BY designator ORDER BY designator) as os
      on reserv.designator=os.designator
      where ji.tematik_id='.$id);
  }
  public static function insertJustif($tgl,$tematik,$insert)
  {
    DB::table('mpromise_material_justif_item')->where('tgl_justif',$tgl)->where('tematik_id',$tematik)->delete();
    DB::table('mpromise_material_justif_item')->insert($insert);
  }
  
  public static function getReservasiList($id)
  {
    return DB::table('mpromise_material_reservasi as mr')->leftJoin('mpromise_lop as ml','mr.lop_id','=','ml.id')->select('mr.*')->where('ml.mpromise_project_id', $id)->get();
  }
  public static function getReservasiItemByLopId($id)
  {
    // return DB::select('select reserv.lop_id,reserv.res,deliv.delv,reserv.designator,plan.qtyplan from (SELECT r.lop_id,sum(qty) AS res, designator FROM mpromise_material_reservasi_item ri 
    //   left join mpromise_material_reservasi r on ri.no_reserv=r.no_reservasi 
    //   where r.lop_id='.$id.' GROUP BY designator ORDER BY designator) as reserv 
    //   left join
    //   (SELECT lop_id,sum(qty) AS qtyplan, designator FROM mpromise_hld_item hld where hld.lop_id='.$id.' GROUP BY designator ORDER BY designator) as plan
    //   on reserv.designator=plan.designator
    //   left join
    //   (SELECT lop_id,sum(qty) AS delv, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' GROUP BY designator ORDER BY designator) as deliv
    //   on plan.designator=deliv.designator');
    return DB::select('select plan.lop_id,deliv.delv,plan.designator,plan.qtyplan from
      (SELECT lop_id,sum(qty) AS qtyplan, designator FROM mpromise_hld_item hld where hld.lop_id='.$id.' and qty>0 GROUP BY designator ORDER BY designator) as plan
      left join
      (SELECT lop_id,sum(qty) AS delv, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' GROUP BY designator ORDER BY designator) as deliv
      on plan.designator=deliv.designator');
  }
  public static function insertReservasi($insert)
  {
    DB::table('mpromise_material_reservasi')->insert($insert);
  }
  public static function insertReservasiItem($insert)
  {
    DB::table('mpromise_material_reservasi_item')->insert($insert);
  }

  public static function getMatdevByLopId($id)
  {
    return DB::table('mpromise_material_delivery')->where('lop_id', $id)->get();
  }
  public static function getMatdevById($id)
  {
    DB::table('mpromise_material_delivery')->where('id', $id)->first();
  }
  public static function insertMatdev($insert)
  {
    return DB::table('mpromise_material_delivery')->insertGetId($insert);
  }
  public static function updateMatdev($id,$update)
  {
    DB::table('mpromise_material_delivery')->where('id',$id)->update($update);
  }
  public static function matdevItemTInsert($id,$insert)
  {
    DB::table('mpromise_material_delivery_item')->where('id',$id)->delete();
    DB::table('mpromise_material_delivery_item')->insert($insert);
  }
  public static function getMatdevItem($id)
  {
    return DB::table('mpromise_material_delivery_item')->where('deliv_id',$id)->get();
  }
  public static function getMatdevInfoItem($id)
  {
    // return DB::select('select reserv.lop_id,reserv.res,deliv.delv,os.qty as onsite,reserv.designator,plan.qtyplan from (SELECT r.lop_id,sum(qty) AS res, designator FROM mpromise_material_reservasi_item ri 
    //   left join mpromise_material_reservasi r on ri.no_reserv=r.no_reservasi 
    //   where r.lop_id='.$id.' GROUP BY designator ORDER BY designator) as reserv 
    //   left join
    //   (SELECT lop_id,sum(qty) AS qtyplan, designator FROM mpromise_hld_item hld where hld.lop_id='.$id.' GROUP BY designator ORDER BY designator) as plan
    //   on reserv.designator=plan.designator
    //   left join
    //   (SELECT lop_id,sum(qty) AS delv, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' and d.status="Material_on_Delivery" GROUP BY designator ORDER BY designator) as deliv
    //   on reserv.designator=deliv.designator
    //   left join
    //   (SELECT lop_id,sum(qty) AS qty, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' and d.status="Material_on_Site" GROUP BY designator ORDER BY designator) as os
    //   on reserv.designator=os.designator');
    return DB::select('select plan.lop_id,deliv.delv,os.qty as onsite,plan.designator,plan.qtyplan from 
      (SELECT lop_id,sum(qty) AS qtyplan, designator FROM mpromise_hld_item hld where hld.lop_id='.$id.' and qty>0 GROUP BY designator ORDER BY designator) as plan
      left join
      (SELECT lop_id,sum(qty) AS delv, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' and d.status="Material_on_Delivery" GROUP BY designator ORDER BY designator) as deliv
      on plan.designator=deliv.designator
      left join
      (SELECT lop_id,sum(qty) AS qty, designator FROM mpromise_material_delivery_item di left join mpromise_material_delivery d on di.deliv_id=d.id where d.lop_id='.$id.' and d.status="Material_on_Site" GROUP BY designator ORDER BY designator) as os
      on plan.designator=os.designator');
  }
  public static function getBoqTematik($id)
  {
    return DB::table('mpromise_material_instalasi as mtr')->leftJoin('mpromise_lop as ml','mtr.lop_id','=','ml.id')->where('ml.mpromise_project_id',$id)->get();
  }
  public static function getDesignatorTematik($id)
  {
    return DB::select("SELECT id_barang FROM mpromise_material_instalasi mtr left join mpromise_lop ml on mtr.lop_id=ml.id where ml.mpromise_project_id=".$id." GROUP BY id_barang ORDER BY id_barang");
  }
  public static function addDesignator($id, $des, $hm, $hs)
  {
    $exists = DB::table('mpromise_hld_item')->where('designator', $des)->where('lop_id',$id)->first();
    if(!$exists){
      DB::table('mpromise_hld_item')->insert(['lop_id'=>$id,'designator'=>$des,'hargamaterial'=>$hm,'hargajasa'=>$hs,'status'=>1,'qty'=>1]);
    }
  }

}
