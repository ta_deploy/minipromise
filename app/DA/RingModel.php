<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class RingModel
{
  public static function getAll()
  {
    return DB::Table('mpromise_ring_master')->get();
  }
  public static function getById($id)
  {
    return DB::Table('mpromise_ring_master')->where('id', $id)->first();
  }
  public static function insert($param)
  {
    DB::table('mpromise_ring_master')->insert($param);
  }
  public static function update($id, $param)
  {
    DB::table('mpromise_ring_master')->where('id', $id)->update($param);
  }
}
