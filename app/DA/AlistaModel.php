<?php
namespace App\DA;
use Illuminate\Support\Facades\DB;
class AListaModel
{
  public static function getreservasi($id){
    $u = DB::table('akun')->where('user', '885870')->first();
    $auth = session('auth');
    $cookie = $u->cookie_alista;
    $login = self::cekloginalista($cookie);
    if(!str_contains($login, "HENDY KURNIAWAN")){
      $cookie = self::login();
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=reservation/details');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".$id);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    $dom = @\DOMDocument::loadHTML(trim($result));
    $table = $dom->getElementsByTagName('table')->item(0);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
        0 =>'request',
        'tgl_target',
        'gudang',
        'pid',
        'mitra',
        'nik_pemakai'
    );
    $result = array();
    $head = array();
    for ($i = 0, $count = $rows->length; $i < $count; $i++)
    {
        $cells = $rows->item($i)->getElementsByTagName('td')->item(0)->nodeValue;
        $head[$i] = $cells;
        // dd($cells);
    }
    // dd($data);
    $table = $dom->getElementsByTagName('table')->item(1);
    $rows = $table->getElementsByTagName('tr');
    $columns = array(
            0 =>'no',
            'designator',
            'uraian',
            'qty',
            'qty_req_pr',
            'qty_acc',
            'proses'
        );
    $result = array();
    for ($i = 1, $count = $rows->length; $i < $count; $i++)
    {
        $data = array();
        $cells = $rows->item($i)->getElementsByTagName('td');
        for ($j = 0, $jcount = count($columns); $j < $jcount; $j++)
        {
            $td = $cells->item($j);
            $data[$columns[$j]] =  $td->nodeValue;
        }
        $result[] = $data;
        // dd($data);
    }
    return ['result'=>$result,'head'=>$head];
    // dd($result,$head);
  }
  public static function cekloginalista($cookie){
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_COOKIE, $cookie);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }
  public static function login(){
    $u = DB::table('akun')->where('user', '885870')->first();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://alista.telkomakses.co.id/index.php?r=site/login');
    curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "LoginForm%5Busername%5D=".$u->user."&LoginForm%5Bpassword%5D=".$u->pwd."&yt0=");
    $rough_content = curl_exec($ch);
    $err = curl_errno($ch);
    $errmsg = curl_error($ch);
    $header = curl_getinfo($ch);

    $header_content = substr($rough_content, 0, $header['header_size']);
    $body_content = trim(str_replace($header_content, '', $rough_content));
    $pattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m";
    preg_match_all ($pattern, $header_content, $matches);
    $cookiesOut = $matches['cookie'][1];
    if($cookiesOut){
      DB::table('akun')->where('user', '885870')->update(["cookie_alista"=>$cookiesOut]);
    }
    return $cookiesOut;
  }
}
