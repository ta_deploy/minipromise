<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class ProjectModel
{
  public static function getById($id)
  {
    return DB::table('mpromise_project')->where('id', $id)->first();
  }
  public static function getById_In($id)
  {
    return DB::table('mpromise_project')->whereIn('id', $id)->first();
  }
  public static function getLopByTematik($id)
  {
    return DB::table('mpromise_lop')->select('id','nama_lop as text')->where('mpromise_project_id', $id)
        ->get();
  }
  public static function getAll($status = 'ALL')
  {
    $query = DB::table('mpromise_project')->select('*','nama as text', DB::raw('(select count(*) from mpromise_lop where mpromise_project_id=mpromise_project.id and isHapus=0 and witel_lop="'.session('auth')->witel.'") as totallop'))->orderBy('id','desc');

    if($status == 'ALL'){
      return $query->get();
    }
    return $query->where('stts', $status)
        ->get();
  }
  public static function getAll_sm()
  {
    $query = DB::table('mpromise_project As a')
      ->leftjoin('mpromise_lop As b','a.id','=','b.mpromise_project_id')
      ->leftjoin('1_2_employee As d', 'd.nik', '=', 'b.tl')
      ->leftjoin('mpromise_mitra As c','b.mitra','=','c.id')
      ->select('b.mitra As id', 'c.mitra As text')->GroupBy('b.mitra')->orderBy('c.mitra', 'ASC');

    return $query->get();
  }
  public static function getAll_tl()
  {
    $query = DB::table('mpromise_tl As a')
      ->leftjoin('1_2_employee As d', 'd.nik', '=', 'a.nik')
      ->select('d.nik As id', DB::RAW("CONCAT(d.nama, ' (', d.nik,')') As text"))->GroupBy('d.nik')->orderBy('d.nama', 'ASC');

    return $query->get();
  }
  public static function update($id, $update)
  {
    DB::transaction(function () use ($id, $update) {
      DB::table('mpromise_project')
        ->where("id", $id)
        ->update($update);
    });
  }
  public static function updatestts($req)
  {
      DB::table('mpromise_project')
        ->where("id", $req->id)
        ->update([
          "stts" => $req->stts
        ]);
  }

  public static function search_project($id)
  {
    return DB::table('mpromise_project')
    ->where('nama', 'like', "%$id%")
    ->orwhere('pid', 'like', "%$id%")
    ->get();
  }

  public static function insertOrUpdate($id, $req)
  {

    $auth = session('auth');
    $exist = DB::table('mpromise_project')->where('id', $id)->first();
    if ($exist) {
        DB::table('mpromise_project')
      ->where("id", $id)
      ->update([
        "nama" => $req->nama,
        "pt3" => $req->pt3,
        "created_by" => $auth->id_user,
      ]);
    } else {
        $id = DB::table('mpromise_project')
      ->insertGetId([
        "nama" => $req->nama,
        "pt3" => $req->pt3,
        "created_by" => $auth->id_user,
      ]);
    }
    return $id;
  }
  public static function getUnmapping($id)
  {
    return DB::table('mpromise_lop')
    ->where('mitra',0)
    ->where('mpromise_project_id',$id)
    ->get();
  }
  public static function getMapped($id)
  {
    return DB::table('mpromise_lop')
    ->where('mitra','!=','0')
    ->where('mpromise_project_id',$id)
    ->get();
  }
  public static function getMappedCount($id)
  {
    return DB::select('SELECT COUNT(*) AS Baris, ml.mitra,mm.label,mm.mitra as nama_mitra FROM mpromise_lop ml left join mpromise_mitra mm on ml.mitra=mm.id where mpromise_project_id='.$id.' GROUP BY ml.mitra ORDER BY ml.mitra');
  }
  public static function getMappedByTematik($id)
  {
    return DB::table('mpromise_lop')->where('mpromise_project_id', $id)->where('step_id',0)->where('mitra','!=','0')->get();
  }
  public static function sendAanwijzingByTematik($id)
  {
    DB::table('mpromise_lop')->where('mpromise_project_id', $id)->where('step_id',0)->where('mitra','!=','0')->update(["step_id"=>6]);
  }
}
