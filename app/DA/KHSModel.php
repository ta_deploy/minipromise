<?php

namespace App\DA;

use Illuminate\Support\Facades\DB;

class KHSModel
{
  const TABLE = 'designator_nasional';

  public static function getByNama($nama)
  {
    return DB::table(self::TABLE)
    ->where('nama_khs', $nama)
    ->get();
  }
  public static function cekKHSByNama($nama)
  {
    return DB::table(self::TABLE)
    ->where('nama_khs', $nama)
    ->first();
  }

  public static function saveUpload($array)
  {
    return DB::table(self::TABLE)
    ->insert($array);
  }
  public static function saveUploadUpdate($array,$nama)
  {
    foreach($array as $a){
      unset($a['satuan'],$a['uraian']);
      // dd($a);
      DB::table(self::TABLE)->where('nama_khs', $nama)->where('designator', $a['designator'])->update($a);
    }
  }
}
