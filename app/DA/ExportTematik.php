<?php
namespace App\DA;

use Maatwebsite\Excel\Concerns\WithMultipleSheets;
class ExportTematik implements WithMultipleSheets 
{
    public function __construct(int $id)
    {
        $this->id = $id;
    }
    public function sheets(): array 
    {   
        return [
            new ExportLop($this->id),
            new ExportSummarymtr($this->id),
            new ExportBoq($this->id),
        ];
    }
}