<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
use App\DA\LopModel;
use App\DA\WaspangModel;
// Route::get('/sc', 'SyncronController@starclick');
Route::get('/grabodp', 'GrabController@odp_regional');
Route::post('/API-InsertLOP', 'APIController@insertLOP');
Route::post('/API-InsertTEMATIK', 'APIController@insertTEMATIK');
Route::get('/apam', 'GrabController@apam');
Route::get('/get_proaktif', 'ProaktifController@get_proaktif');

Route::get('/baca', 'GrabController@bacapdf');
Route::get('/send_laporan', 'WaspangController@send_laporan');
Route::get('/sample_apm', 'GrabController@sample_grab');
Route::get('/login', function () {
  return view('login_v2');
});
Route::post('/login', 'UserController@login');

Route::get('/dashflow/{mitra}/{tl}', 'DashboardController@dashflow');
Route::get('/dashflowdetil/{col}/{row}', 'DashboardController@dashflowdetil');
Route::group(['middleware' => 'pt3_MW'], function () {
  Route::get('/kurva', function () {
  return view('report.kurva');
});


  Route::get('/bobot', 'ProjectController@bobot');
  Route::get('/gantt', 'DashboardController@gantt');
  Route::get('/timeplanscore', 'DashboardController@timeplanscore');

  Route::get('/detillop/{id}', 'LopController@detilajax');

  Route::get('/getMatrikWaspangAjax/{id}', 'DashboardController@matrikwaspangajax');
  Route::get('/getMatrikMitraAjax/{jns}/{id}/{tl}', 'DashboardController@matrikmitraajax');
  Route::get('/ajaxDetailMatrik/{jenis}/{id}/{col}/{row}/{aktor}/{tl}', 'DashboardController@ajaxdetailmatrix');

  Route::get('/ajaxGetMtr/{id}', function ($id) {
    return json_encode(WaspangModel::getMtrDistinct($id));
  });
  Route::get('/theme', 'UserController@themes');
  Route::post('/theme', 'UserController@themeSave');

  Route::get('/logout', 'UserController@logout');

  //home
  Route::get('/home', 'HomeController@home');
  Route::get('/ajaxhome', 'HomeController@ajaxhome');


  Route::get('/', 'WaspangController@index');

  //dashboard
  Route::get('/dashboard', 'DashboardController@proaktif');
  Route::get('/dashboard/{portofolio}/{fase}', 'DashboardController@listcell');

//lop
  Route::get('/droplop/{id}', 'LopController@droplop');
  Route::get('/hapuslop/{id}', 'LopController@hapuslop');
  Route::get('/list/{id}', 'LopController@list');
  Route::get('/listLopByProject/{id}', 'LopController@listLopByProject');
  Route::get('/backstep/{id}', 'LopController@backstep');
  Route::post('/nextstep', 'LopController@nextstep');
  Route::post('/qc/{id}', 'WaspangController@saveQC');
  Route::get('/lop/{id}/register/{idp}', 'LopController@updateForm');
  Route::post('/lop/{id}/{jenis}/{idp}', 'LopController@save');
  Route::get('/editLop/{id}', 'LopController@editlop');
  Route::post('/editLop/{id}', 'LopController@updatelop');
  Route::get('/lop/{id}/upload', 'LopController@uploadLop');
  Route::get('/search', 'LopController@search');
  Route::get('/download/{path}', 'LopController@download');
  Route::get('/find_boq_unw', 'LopController@find_boq_unw');
  Route::post('/saveproject', 'LopController@saveProject');
  Route::get('/getDetail/{id}', function ($id) {
    return json_encode(App\DA\LopModel::getById($id));
  });
  Route::get('/getProject', function () {
      return json_encode(App\DA\LopModel::getProject());
  });
//waspang
  Route::get('/materialalista/{id}', 'WaspangController@alista');
  // Route::get('/planwaspang', 'WaspangController@plan');
  Route::get('/deletealista/{id}', 'WaspangController@deleteAlista');
  Route::post('/materialalista/{id}', 'WaspangController@saveAlista');
  Route::post('/planwaspang', 'WaspangController@savePlanWaspang');
  Route::get('/progress/{id}', 'WaspangController@progress');
  Route::post('/pickup_jointer/{id}', 'WaspangController@pickup_jointer');
  // Route::get('/uploadInstalasi/{id}', 'WaspangController@uploadInstalasi');
  Route::post('/uploadInstalasi/{id}', 'WaspangController@uploadInstalasiSave');
  Route::get('/uploadEnd2end/{id}', 'WaspangController@uploadEnd2end');
  Route::post('/uploadEnd2end/{id}', 'WaspangController@uploadEnd2endSave');
  Route::post('/saveDelivery/{id}', 'WaspangController@saveDelivery');
  Route::post('/savePragelaran/{id}', 'WaspangController@savePragelaran');
  Route::post('/saveInstalasi/{id}', 'WaspangController@saveInstalasi');
  Route::post('/saveUnwizing/{id}', 'WaspangController@saveUnwizing');
  Route::post('/uploadodp/{id}', 'WaspangController@uploadodp');
  Route::post('/ctut/{id}', 'WaspangController@saveCTUT');
  Route::post('/uploadevident', 'WaspangController@uploadevident');
  Route::post('/uploadfotorar/{id}', 'WaspangController@saveuploadfotorar');


  // Route::get('/waspang_search', 'WaspangController@waspang_search');
  Route::get('/waspang', 'WaspangController@list_waspang');
  Route::get('/waspang/{id}', 'WaspangController@form_waspang');
  Route::post('/waspang/{id}', 'WaspangController@save_waspang');

//mitra
  // Route::get('/timmitra/list', 'MitraController@list_tim');
  // Route::get('/timmitra/create', 'MitraController@createMitra');
  // Route::post('/timmitra/create', 'MitraController@savetim_Mitra');

  Route::get('/mitra', 'MitraController@index');
  Route::get('/mitra/{id}', 'MitraController@form');
  Route::post('/mitra/{id}', 'MitraController@save');

//project
  Route::get('/project', 'ProjectController@list');
  Route::get('/project/{id}', 'ProjectController@createform');
  Route::post('/project/{id}', 'ProjectController@saveform');
  Route::post('/projectstts', 'ProjectController@savestts');
  Route::get('/projectsearching', 'ProjectController@search_project');

//pemberkasan
  // Route::get('/pemberkasan/{id}', 'WaspangController@berkas_index');
  // Route::post('/pemberkasan/{id}', 'WaspangController@berkas_update_boq');
  // Route::post('/excel/preview_excel', 'WaspangController@preview_excel');
  //referensi
  Route::get('/refer_pt2', 'DashboardController@refer_pt2');
  Route::get('/matrikprogress/{jns}', 'DashboardController@matrikprogress');
  // Route::get('/matrikwaspang', 'DashboardController@matrikwaspang');

  //sp
  Route::get('/sp', 'SPController@index');
  Route::get('/sp/{id}', 'SPController@form');
  Route::post('/sp/{id}', 'SPController@save');

  //download RAR
  Route::get('/download/rar/{id}', 'WaspangController@download_rar');

  //time log
  Route::get('/time_count/log/{user_id}', 'DashboardController@countdown_log');


  //perubahan design
  Route::get('/perubahandesign/{id}', 'LopController@formperubahandesign');
  Route::post('/perubahandesign/{id}', 'LopController@saveperubahandesign');

  Route::post('/manualsetmcore', 'LopController@savemanualsetmcore');
  Route::post('/deletefotorar/{idlop}', 'WaspangController@deletefotorar');
  Route::get('/downloadlop/{id}', 'DashboardController@export');
  Route::get('/khsupload', 'KHSController@upload');
  Route::post('/khsupload', 'KHSController@uploadsave');
  Route::get('/khs', 'KHSontroller@index');

  //rfc
  Route::get('/carirfc', 'RFCController@searchrfc');
  Route::get('/rekap_pengeluaran/{tgl}/{mitra}/{pid}', 'RFCController@rekap_pengeluaran');
  Route::get('/downloads/{tgl}/{mitra}/{pid}', 'RFCController@download');
  Route::get('/downloadspdfrar/{tgl}/{mitra}/{pid}', 'RFCController@downloadpdfrar');


  //apm
  Route::get('/apm_cashout', 'GrabController@apm_cashout');
  Route::get('/test/{doc}', 'GrabController@detail');
  Route::get('/monitorapm', 'APMController@apmmon');
  Route::get('/apm/cashout', 'APMController@cashoutlist');
  Route::get('/monitorapm/{id}', 'APMController@update');
  Route::post('/monitorapm/{id}', 'APMController@save');
  Route::post('/deletemonitorapm', 'APMController@delete');
  Route::get('/getCashListByPID/{pid}', 'APMController@getCashListByPID');


  Route::get('/kendala/{id}', 'WaspangController@formKendala');
  Route::post('/kendala/{id}', 'WaspangController@saveKendala');

  //proaktif
  Route::get('/proaktif', 'ProaktifController@index');
  Route::get('/proaktif/{id}', 'ProaktifController@form');
  Route::post('/proaktif/{id}', 'ProaktifController@saveimport');

  //project plan
  Route::get('/plan_tl_list', 'PlanController@index');
  Route::get('/plan_per_tl/{tematik}/{mitra}', 'PlanController@form');
  Route::post('/plan_persen', 'PlanController@saveplan');
  Route::post('/updatetim', 'PlanController@savetim');

  //review
  Route::get('/review', 'ReviewController@index');
  Route::get('/review/{id}', 'ReviewController@list_lop');
  Route::get('/review/{id}/{lopid}', 'ReviewController@formreview');
  Route::post('/review/{id}/{lopid}', 'ReviewController@savereview');
  Route::post('/savereviewprogress/{id}/{lopid}', 'ReviewController@savereviewprogress');

  //regional mitratel
  Route::get('/mitratel', 'RegController@matrix');
  Route::get('/ajaxmatrixmitratel/{witel}/{wo}', 'RegController@ajaxmatrix');
  Route::get('/ajaxmatrixmitrateldetil/{col}/{row}', 'RegController@ajaxmatrixdetil');
  Route::get('/getmatrixmitrateldetil/{col}/{row}', 'DashboardController@ajaxmatrixdetil');

  Route::get('/ajaxgetwo/{witel}', 'RegController@ajaxgetwo');

  Route::get('/dashboard_mitratel', 'RegController@dashboard');
  Route::get('/mitratel/upload', 'RegController@upload');
  Route::post('/mitratel/upload', 'RegController@uploadsave');

  Route::get('/mitratelplan', 'RegController@listplan');
  Route::get('/mitratelplan/{wo}', 'RegController@formplan');
  Route::get('/mitratelplandelete/{id}', 'RegController@deleteplan');
  Route::post('/mitratelplan/{wo}', 'RegController@formplansave');

  Route::get('/ajaxdashboardmitratel/{witel}', 'RegController@ajaxdashboard');
  Route::get('/singkronmtt', 'RegController@login');


  Route::get('/perizinan/{id}/{izinid}', 'PerizinanController@form');

  Route::get('/mon_material/{id}', 'MaterialController@form');
  Route::post('/saveJustif/{tematik}', 'MaterialController@savejustif');
  Route::get('/getreservasi/{id}', 'MaterialController@getreservasi');
  Route::post('/saveReservasi/{id}', 'MaterialController@savereservasi');

  Route::get('/matdev/{id}', 'MaterialController@listmatdev');
  Route::get('/matdev/{lopid}/{id}', 'MaterialController@formmatdev');
  Route::get('/matonsite/{lopid}/{id}', 'MaterialController@formmatonsite');

  Route::post('/savemataddition/{lopid}/{id}', 'MaterialController@savemataddition');
  Route::post('/saveDeliveryNew/{lopid}/{id}', 'MaterialController@savematdev');
  Route::post('/saveOnsiteNew/{lopid}/{id}', 'MaterialController@savematonsite');

  Route::get('/permit/{id}', 'PerizinanController@formPermit');
  Route::post('/permit/{id}', 'PerizinanController@savePermit');

  Route::get('/mapping_mitra/{id}', 'ProjectController@list_unmapping');
  Route::get('/remove_mapping_mitra/{id}', 'ProjectController@remove_mapping');
  Route::post('/mapping_mitra/{id}', 'ProjectController@saveMapping');
  Route::get('/done_mapping_mitra/{id}', 'ProjectController@sendAanwijzing');
  Route::get('/aanwijzing/{tematik}/{wasp}/{mitra}', 'WaspangController@formUnwizing');
  Route::get('/listAanwijzing/{wasp}', 'WaspangController@listUnwizing');
  Route::post('/selesaiaanwijzing/{tematik}/{wasp}/{mitra}', 'WaspangController@saveSelesaiUnwizing');
  Route::get('/exporttiang', 'SyncronController@exporttiang');
  Route::post('/exporttiang', 'SyncronController@exporttiangsave');

  Route::get('/downloadtematik/{id}', 'DashboardController@export2');

  Route::get('/ring', 'RingController@index');
  Route::get('/ring/{id}', 'RingController@form');
  Route::post('/ring/{id}', 'RingController@save');

  Route::get('/span', 'SpanController@index');
  Route::get('/span/{id}', 'SpanController@form');
  Route::post('/span/{id}', 'SpanController@save');
  Route::get('/viewspan/{id}', 'SpanController@viewspan');
  Route::post('/viewspan/{id}', 'SpanController@saveview');

});
Route::get('/layoutodc',function () {
  return view('layoutodc');
});